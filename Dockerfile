FROM cypress/included:8.3.0
RUN mkdir /app
WORKDIR /app
COPY . /app
RUN npm install
RUN mkdir /.npm
RUN chmod +x entrypoint.sh
RUN $(npm bin)/cypress verify
ENTRYPOINT ["/app/entrypoint.sh"]

