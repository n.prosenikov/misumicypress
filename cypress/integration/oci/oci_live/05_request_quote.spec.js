import MyPage from '../pom/myPage';
import LoginPage from '../pom/loginPage';

const loginPage = new LoginPage()
const myPage = new MyPage()

const authUser = require('../../../fixtures/auth-user.json');
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Request Quote', () => {
  beforeEach(() => {
    // cy.visit('https://stg0-wos.misumi-ec.com/de/purchase/PA202PurchaseAutoAssignmentCmd.do?id=59149')
    // login.acceptCookies().click()
    // login.ociSignIn() These are STG0 Settings
    loginPage.login(username, password)
    cy.visit('/')

  })

  /*
      Description: eCatalog top page - Click the [Request a Quote] button on the right side of the page.
      Test Steps:
          1. Quotation: the page is displayed. (New Quotation)
  */
  it('Request Quote UK', { baseUrl: url_uk }, () => {
    myPage.quickOrderPadBtn().click()
    cy.contains('Create New Quotation')
  })

  /*
      Description: eCatalog top page - Click the [Request a Quote] button on the right side of the page.
      Test Steps:
          1. Quotation: the page is displayed. (New Quotation)
  */
  it('Request Quote DE', { baseUrl: url_de }, () => {
    myPage.quickOrderPadBtn().click()
    cy.contains('Neue Anfrage')
  })

  /*
      Description: eCatalog top page - Click the [Request a Quote] button on the right side of the page.
      Test Steps:
          1. Quotation: the page is displayed. (New Quotation)
  */
  it('Request Quote IT', { baseUrl: url_it }, () => {
    myPage.quickOrderPadBtn().click()
    cy.contains('Richiesta quotazione')
  })

  /*
      Description: eCatalog top page - Click the [Request a Quote] button on the right side of the page.
      Test Steps:
          1. Quotation: the page is displayed. (New Quotation)
  */
  it('Request Quote FR', { baseUrl: url_fr }, () => {
    myPage.quickOrderPadBtn().click()
    cy.contains('Demande de devis')
  })

});
