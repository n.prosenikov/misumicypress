import LoginPage from '../pom/loginPage';

const loginPage = new LoginPage()

const authUser = require('../../../fixtures/auth-user.json');

const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Search Window Keyword', () => {
  beforeEach(() => {
    // cy.visit('https://stg0-wos.misumi-ec.com/de/purchase/PA202PurchaseAutoAssignmentCmd.do?id=59149')
    // login.acceptCookies().click()
    // login.ociSignIn() These are STG0 Settings
    loginPage.login(username, password)
    cy.visit('/')
    cy.wait(1500)
  })
  /*
    Description: eCatalog Top Page - Enter LINEAR in the search field at the top of the screen and click the displayed [linear shaft] link which is suggested.
    Test Steps:
        1. Search result: search results of Linear Shaft are displayed in Category of the page (MISUMI Startseite> Suchergebnisse).
  */
  it('Search Window Keyword UK', { baseUrl: url_uk }, () => {
    cy.get('#keyword_input').type('Linear')
    cy.contains('linear shaft').invoke('attr', 'href').then(($linearUrl1) => {
      const linearUrl1 = $linearUrl1
      cy.visit(url_uk + linearUrl1)
    })
    cy.contains('Linear Shafts')
  })

  /*
    Description: eCatalog Top Page - Enter LINEAR in the search field at the top of the screen and click the displayed [linear shaft] link which is suggested.
    Test Steps:
        1. Search result: search results of Linear Shaft are displayed in Category of the page (MISUMI Startseite> Suchergebnisse).
  */
  it('Search Window Keyword DE', { baseUrl: url_de },() => {
    cy.get('#keyword_input').type('linear')
    cy.contains('linear').invoke('attr', 'href').then(($linearUrl1) => {
      const linearUrl1 = $linearUrl1
      cy.visit(url_de + linearUrl1)
    })
    cy.contains('Linearbewegung')
  })

  /*
    Description: eCatalog Top Page - Enter LINEAR in the search field at the top of the screen and click the displayed [linear shaft] link which is suggested.
    Test Steps:
        1. Search result: search results of Linear Shaft are displayed in Category of the page (MISUMI Startseite> Suchergebnisse).
  */
  it('Search Window Keyword IT', { baseUrl: url_it },() => {
    cy.get('#keyword_input').type('lineare')
    cy.contains('lineare').invoke('attr', 'href').then(($linearUrl1) => {
      const linearUrl1 = $linearUrl1
      cy.visit(url_it + linearUrl1)
    })
    cy.contains('lineare')
  })

  /*
    Description: eCatalog Top Page - Enter LINEAR in the search field at the top of the screen and click the displayed [linear shaft] link which is suggested.
    Test Steps:
        1. Search result: search results of Linear Shaft are displayed in Category of the page (MISUMI Startseite> Suchergebnisse).
  */
  it('Search Window Keyword FR', { baseUrl: url_fr },() => {
    cy.get('#keyword_input').type('linéaire')
    cy.contains('linéaire').invoke('attr', 'href').then(($linearUrl1) => {
      const linearUrl1 = $linearUrl1
      cy.visit(url_fr + linearUrl1)
    })
    cy.contains('linéaire')
  })

});