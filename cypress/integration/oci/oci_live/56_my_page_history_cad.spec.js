import LoginPage from '../pom/loginPage';

const loginPage = new LoginPage()

const authUser = require('../../../fixtures/auth-user.json');

const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('My Page CAD Download', () => {
  beforeEach(() => {
    // cy.visit('https://stg0-wos.misumi-ec.com/de/purchase/PA202PurchaseAutoAssignmentCmd.do?id=59149')
    // login.acceptCookies().click()
    // login.ociSignIn() These are STG0 Settings
    loginPage.login(username, password)
    cy.visit('/')
    cy.wait(2000)
  })
  /*
    Description: My page top page - Click the [History of CAD data download] link on the left side of the page.

    Test Steps:
        1. CAD download history: the page is displayed. (MISUMI Top Page>Top of My Page>History of CAD data download)
  */
  it('My Page CAD Download UK', { baseUrl: url_uk }, () => {
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')

  })

  it('My Page CAD Download DE', { baseUrl: url_de }, () => {
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')

  })

  it('My Page CAD Download IT', { baseUrl: url_it }, () => {
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')

  })

  it('My Page CAD Download FR', { baseUrl: url_fr }, () => {
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')

  })

});