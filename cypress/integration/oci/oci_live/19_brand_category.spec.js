import LoginPage from '../pom/loginPage';

const loginPage = new LoginPage()

const authUser = require('../../../fixtures/auth-user.json');

const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Brand Category', () => {
  beforeEach(() => {
    // cy.visit('https://stg0-wos.misumi-ec.com/de/purchase/PA202PurchaseAutoAssignmentCmd.do?id=59149')
    // login.acceptCookies().click()
    // login.ociSignIn() These are STG0 Settings
    loginPage.login(username, password)
    cy.visit('/')
  })
  /*
    Description: Click [Self-Drill Screws ] link in the detail page of AX BRAIN.
    Test Steps:
        1. Category page of AX BRAIN Self-Drill Screws is displayed. (uk.misumi-ec.com/vona2/maker/axbrain/mech_screw/M3301000000/M3301180000/)
  */
  it('Brand Category UK', { baseUrl: url_uk }, () => {
    cy.contains("Brands").trigger('mouseover')
    cy.contains('Brand Overview').click({ force: true })
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('ACCURATE').click()
    cy.url().should('include', 'uk.misumi-ec.com/vona2/maker/accurate')
    cy.get('[src*=".misumi-ec.com/linked/material/mech/category/M1202000000.jpg?$category_first$"]').click()
    cy.url().should('include', 'mech/M1200000000/M1202000000/')
  })


  /*
    Description: Click [AX BRAIN] in Manufacturer List
    Test Steps:
        1. Detail page of AX BRAIN is displayed. (de.misumi-ec.com/vona2/maker/axbrain/)
  */
  it('Brand Category DE', { baseUrl: url_de }, () => {
    cy.contains("Marken").trigger('mouseover')
    cy.contains('Markenübersicht').click({ force: true })

    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('ACCURATE').click()
    cy.url().should('include', 'de.misumi-ec.com/vona2/maker/accurate')
    cy.get('[src*=".misumi-ec.com/linked/material/mech/category/M1202000000.jpg?$category_first$"]').click()
    cy.url().should('include', 'mech/M1200000000/M1202000000/')
  })

  /*
    Description: Click [AX BRAIN] in Manufacturer List
    Test Steps:
        1. Detail page of AX BRAIN is displayed. (it.misumi-ec.com/vona2/maker/axbrain/)
  */
  it('Brand Category IT', { baseUrl: url_it }, () => {
    cy.contains("Marchi").trigger('mouseover')
    cy.contains('Tutti i marchi').click({ force: true })

    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('ACCURATE').click()
    cy.url().should('include', 'it.misumi-ec.com/vona2/maker/accurate')
    cy.get('[src*=".misumi-ec.com/linked/material/mech/category/M1202000000.jpg?$category_first$"]').click()
    cy.url().should('include', 'mech/M1200000000/M1202000000/')
  })


  /*
    Description: Click [AX BRAIN] in Manufacturer List
    Test Steps:
        1. Detail page of AX BRAIN is displayed. (fr.misumi-ec.com/vona2/maker/axbrain/)
  */
  it('Brand Category FR', { baseUrl: url_fr }, () => {
    cy.contains("Marques").trigger('mouseover')
    cy.contains('Toutes les marques').click({ force: true })

    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('ACCURATE').click()
    cy.url().should('include', 'fr.misumi-ec.com/vona2/maker/accurate')
    cy.get('[src*=".misumi-ec.com/linked/material/mech/category/M1202000000.jpg?$category_first$"]').click()
    cy.url().should('include', 'mech/M1200000000/M1202000000/')
  })
});