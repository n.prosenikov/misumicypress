import LoginPage from '../pom/loginPage';

const loginPage = new LoginPage()

const authUser = require('../../../fixtures/auth-user.json');

const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Search No Hits', () => {
  beforeEach(() => {
    // cy.visit('https://stg0-wos.misumi-ec.com/de/purchase/PA202PurchaseAutoAssignmentCmd.do?id=59149')
    // login.acceptCookies().click()
    // login.ociSignIn() These are STG0 Settings
    loginPage.login(username, password)
    cy.visit('/')
    cy.wait(1500)
  })
  /*
    Description: eCatalog Top Page - Enter @@@ in the search field at the top of the screen and click the [Search] button.
    Test Steps:
        1. Search result: Message for no results is displayed in the page (MISUMI Top Page> Search results). (The searched keyword returned no results.)
  */
  it('Search Result No Hits UK', { baseUrl: url_uk },() => {
    cy.get('#keyword_input').type('@@@')
    cy.get('#keyword_go').click()
    cy.contains('The searched keyword returned no results.')
  })

  /*
    Description: eCatalog Top Page - Enter @@@ in the search field at the top of the screen and click the [Search] button.
    Test Steps:
        1. Search result: Message for no results is displayed in the page (MISUMI Top Page> Search results). (The searched keyword returned no results.)
  */
  it('Search Result No Hits DE', { baseUrl: url_de },() => {
    cy.get('#keyword_input').type('@@@')
    cy.get('#keyword_go').click()
    cy.contains('Die Suche nach dem Stichwort ergab keine Ergebnisse.')
  })

  /*
    Description: eCatalog Top Page - Enter @@@ in the search field at the top of the screen and click the [Search] button.
    Test Steps:
        1. Search result: Message for no results is displayed in the page (MISUMI Top Page> Search results). (The searched keyword returned no results.)
  */
  it('Search Result No Hits IT', { baseUrl: url_it },() => {
    cy.get('#keyword_input').type('@@@')
    cy.get('#keyword_go').click()
    cy.contains('La parola chiave ricercata non ha restituito alcun risultato.')
  })
  
  /*
    Description: eCatalog Top Page - Enter @@@ in the search field at the top of the screen and click the [Search] button.
    Test Steps:
        1. Search result: Message for no results is displayed in the page (MISUMI Top Page> Search results). (The searched keyword returned no results.)
  */
  it('Search Result No Hits FR', { baseUrl: url_fr },() => {
    cy.get('#keyword_input').type('@@@')
    cy.get('#keyword_go').click()
    cy.wait(1000)
    cy.contains('Le mot-clé recherché n\'a donné aucun résultat.')
  })
});