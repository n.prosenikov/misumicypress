import LoginPage from '../pom/loginPage';

const loginPage = new LoginPage()

const authUser = require('../../../fixtures/auth-user.json');

const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('3D Product View', () => {
  beforeEach(() => {
    // cy.visit('https://stg0-wos.misumi-ec.com/de/purchase/PA202PurchaseAutoAssignmentCmd.do?id=59149')
    // login.acceptCookies().click()
    // login.ociSignIn() These are STG0 Settings
    loginPage.login(username, password)
    cy.visit('/')
    cy.wait(1500)
  })
  /*
    Description: Part number of product SFJ3-10: product details page - Click the [3D Preview] button at the top right of the page.
    Test Steps:
        1. 3D preview is displayed.
  */
  it('3D Preview UK', { baseUrl: url_uk },() => {
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.wait(3000)
    cy.get('#Tab_preview').click()
    cy.wait(5000)
    cy.get('#Tab_preview_contents')
  })

  it('3D Preview DE', { baseUrl: url_de },() => {
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.wait(3000)
    cy.get('#Tab_preview').click()
    cy.wait(5000)
    cy.get('#Tab_preview_contents')
  })

  it('3D Preview IT', { baseUrl: url_it },() => {
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.wait(3000)
    cy.get('#Tab_preview').click()
    cy.wait(5000)
    cy.get('#Tab_preview_contents')
  })

  it('3D Preview FR', { baseUrl: url_fr },() => {
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.wait(3000)
    cy.get('#Tab_preview').click()
    cy.wait(5000)
    cy.get('#Tab_preview_contents')
  })

});