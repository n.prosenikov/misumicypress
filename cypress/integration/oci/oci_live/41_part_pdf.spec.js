import LoginPage from '../pom/loginPage';

const loginPage = new LoginPage()

const authUser = require('../../../fixtures/auth-user.json');

const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('PDF', () => {
  beforeEach(() => {
    // cy.visit('https://stg0-wos.misumi-ec.com/de/purchase/PA202PurchaseAutoAssignmentCmd.do?id=59149')
    // login.acceptCookies().click()
    // login.ociSignIn() These are STG0 Settings
    loginPage.login(username, password)
    cy.visit('/')
  })

  const pdfUK = 'https://uk.misumi-ec.com/pdf/fa/2014/P1_0117-0118_F01_EN.pdf'
  const pdfDE = 'https://de.misumi-ec.com/pdf/fa/2014/P1_0117-0118_F01_DE_001.pdf'
  const pdfIT = 'https://it.misumi-ec.com/pdf/fa/2014/P1_0117-0118_F01_IT_001.pdf'
  const pdfFR = 'https://fr.misumi-ec.com/pdf/fa/2014/P1_0117-0118_F01_FR_001.pdf'
  /*
    Description: Part number of product SFJ3-10: product details page - Click the [PDF] button at the top right of the page.
    Test Steps:
        1. PDF is displayed.
  */
  it('PDF UK', { baseUrl: url_uk },() => {
    cy.request({
      url: pdfUK,
      encoding: 'binary',
    }).then((response) => {
      cy.log(response.headers['content-type'])
      cy.expect(response.headers['content-type']).eq('application/pdf')
    })
  })

  /*
    Description: Part number of product SFJ3-10: product details page - Click the [PDF] button at the top right of the page.
    Test Steps:
        1. PDF is displayed.
  */
  it('PDF DE', { baseUrl: url_de },() => {
    // cy.searchPart('SFJ3-100')
    // cy.contains('SFJ3-100')

    cy.request({
      url: pdfDE,
      encoding: 'binary',
    }).then((response) => {
      cy.log(response.headers['content-type'])
      cy.expect(response.headers['content-type']).eq('application/pdf')
    })
  })

  /*
    Description: Part number of product SFJ3-10: product details page - Click the [PDF] button at the top right of the page.
    Test Steps:
        1. PDF is displayed.
  */
  it('PDF IT', { baseUrl: url_it },() => {
    // cy.searchPart('SFJ3-100')
    // cy.contains('SFJ3-100')

    cy.request({
      url: pdfIT,
      encoding: 'binary',
    }).then((response) => {
      cy.log(response.headers['content-type'])
      cy.expect(response.headers['content-type']).eq('application/pdf')
    })
  })

  /*
    Description: Part number of product SFJ3-10: product details page - Click the [PDF] button at the top right of the page.
    Test Steps:
        1. PDF is displayed.
  */
  it('PDF FR', { baseUrl: url_fr },() => {
    // cy.searchPart('SFJ3-100')
    // cy.contains('SFJ3-100')
   
    cy.request({
      url: pdfFR,
      encoding: 'binary',
    }).then((response) => {
      cy.log(response.headers['content-type'])
      cy.expect(response.headers['content-type']).eq('application/pdf')
    })
  })
});