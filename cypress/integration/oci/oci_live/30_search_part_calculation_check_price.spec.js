import LoginPage from '../pom/loginPage';

const loginPage = new LoginPage()

const authUser = require('../../../fixtures/auth-user.json');

const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('Search part Calculation Check', () => {
    beforeEach(() => {
        // cy.visit('https://stg0-wos.misumi-ec.com/de/purchase/PA202PurchaseAutoAssignmentCmd.do?id=59149')
        // login.acceptCookies().click()
        // login.ociSignIn() These are STG0 Settings
        loginPage.login(username, password)
        cy.visit('/')
        cy.wait(1500)
    })

    const partName = 'SFJ10-100'
    /*
        Description: Click the [Check Price / discount rate] button with Order Qty. set to 1 and Unit Price to --- on the left side of product details page.
        Test Steps:
            1. Values must be set for three items: Unit Price, Total, Shipping Days
    */
    it('Price Check UK', { baseUrl: url_uk }, () => {
        cy.searchPart(partName)
        cy.url().should('include', partName)
        cy.contains(partName)
        cy.contains("Total") //If it exists then a price is avaiable
        //cy.get('.m-cartBox__desc--inline').find('[type="text"]').dblclick()
        cy.contains('3.08')
    })

    /*
        Description: Click the [Check Price / discount rate] button with Order Qty. set to 1 and Unit Price to --- on the left side of product details page.
        Test Steps:
            1. Values must be set for three items: Unit Price, Total, Shipping Days
    */
    it('Price Check DE', { baseUrl: url_de }, () => {
        cy.searchPart(partName)
        cy.url().should('include', partName)
        cy.contains(partName)
        cy.contains("Gesamtsumme")
        //cy.get('.m-inputText--right').type('3')
        cy.contains('3.08')
    })
    /*
        Description: Click the [Check Price / discount rate] button with Order Qty. set to 1 and Unit Price to --- on the left side of product details page.
        Test Steps:
            1. Values must be set for three items: Unit Price, Total, Shipping Days
    */
    it('Price Check IT', { baseUrl: url_it }, () => {
        cy.searchPart(partName)
        cy.url().should('include', partName)
        cy.contains(partName)
        cy.contains("Totale")
        //cy.get('.m-inputText--right').type('3')
        cy.contains('3.08')
    })

    /*
        Description: Click the [Check Price / discount rate] button with Order Qty. set to 1 and Unit Price to --- on the left side of product details page.
        Test Steps:
            1. Values must be set for three items: Unit Price, Total, Shipping Days
    */
    it('Price Check FR', { baseUrl: url_fr }, () => {
        cy.searchPart(partName)
        cy.url().should('include', partName)
        cy.contains(partName)
        cy.contains("Total")
        //cy.get('.m-inputText--right').type('3')
        cy.contains('3.08')
    })


});