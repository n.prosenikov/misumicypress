import Footer from '../../mobile/pom/footer';
import LoginPage from '../pom/loginPage';

const loginPage = new LoginPage()
const footer = new Footer
const authUser = require('../../../fixtures/auth-user.json');

const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Footer', () => {
  beforeEach(() => {
    // cy.visit('https://stg0-wos.misumi-ec.com/de/purchase/PA202PurchaseAutoAssignmentCmd.do?id=59149')
    // login.acceptCookies().click()
    // login.ociSignIn() These are STG0 Settings
    loginPage.login(username, password)
    cy.visit('/')
  })
  /*
      Description: eCatalog top page - expand the menu of [login user name] on the top right of the page and click [Logout] link.
      Test Steps:
          1. Logout process is executed, and the notation of [login user name] button changes to Login.
  
    */
  it('Footer UK', { baseUrl: url_uk },() => {
    footer.checkFooterOptionsExistUK()
  })

  it('Footer DE', { baseUrl: url_de },() => {
    footer.checkFooterOptionsExistDE()
  })

  it('Footer IT', { baseUrl: url_it },() => {
    footer.checkFooterOptionsExistIT()
  })

  it('Footer FR', { baseUrl: url_fr },() => {
    footer.checkFooterOptionsExistFR()
  })

});