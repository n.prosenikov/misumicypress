import Login from '../pom/login';

const login = new Login()

const authUser = require('../../../fixtures/auth-user.json');

const {
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Improve our site', () => {
  beforeEach(() => {
		cy.visit('https://stg0-wos.misumi-ec.com/de/purchase/PA202PurchaseAutoAssignmentCmd.do?id=59149')
		login.acceptCookies().click()
		login.ociSignIn() 
    // loginPage.login(username, password)
    cy.visit('/')
  })
  /*
    Description: Display any category page in the eCatalog and click the [Please assist us in our effort to improve this site.] link on the upper right of the page.
    Test Steps:
        1. Comment and feedback: the page is expanded. (Please give us a feedback on the MISUMI e-Catalog.)
  */
  it('Improve Our Site UK', { baseUrl: url_uk }, () => {
    cy.contains("Brands").trigger('mouseover')
    cy.contains('Brand Overview').click({ force: true })
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.get('#commentLink').click()
    cy.contains('Please give us a feedback on the MISUMI e-Catalog.')
  })

  /*
    Description: Display any category page in the eCatalog and click the [Please assist us in our effort to improve this site.] link on the upper right of the page.
    Test Steps:
        1. Comment and feedback: the page is expanded. (Please give us a feedback on the MISUMI e-Catalog.)
  */
  it('Improve Our Site DE', { baseUrl: url_de }, () => {
    cy.contains("Marken").trigger('mouseover')
    cy.contains('Markenübersicht').click({ force: true })
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.get('#commentLink').click()
    cy.contains('Bitte geben Sie uns Ihr Feedback zu unserem MISUMI e-Katalog.')
  })

  /*
    Description: Display any category page in the eCatalog and click the [Please assist us in our effort to improve this site.] link on the upper right of the page.
    Test Steps:
        1. Comment and feedback: the page is expanded. (Please give us a feedback on the MISUMI e-Catalog.)
  */
  it('Improve Our Site IT', { baseUrl: url_it }, () => {
    cy.contains("Marchi").trigger('mouseover')
    cy.contains('Tutti i marchi').click({ force: true })
    cy.contains('Tutti i marchi').click({ force: true })
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.get('#commentLink').click()
    cy.contains('É gradito un commento sull\'e-Catalog MISUMI')
  })

  /*
    Description: Display any category page in the eCatalog and click the [Please assist us in our effort to improve this site.] link on the upper right of the page.
    Test Steps:
        1. Comment and feedback: the page is expanded. (Please give us a feedback on the MISUMI e-Catalog.)
  */
  it('Improve Our Site FR', { baseUrl: url_fr }, () => {
    cy.contains("Marques").trigger('mouseover')
    cy.contains('Toutes les marques').click({ force: true })
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.get('#commentLink').click()
    cy.contains('Donnez-nous votre avis sur le catalogue en ligne MISUMI.')
  })

});