import Login from '../pom/login';

const login = new Login()

const authUser = require('../../../fixtures/auth-user.json');

const {
  url_uk,
  url_de,
} = authUser;

describe('Incad Library', () => {
  beforeEach(() => {
		cy.visit('https://stg0-wos.misumi-ec.com/de/purchase/PA202PurchaseAutoAssignmentCmd.do?id=59149')
		login.acceptCookies().click()
		login.ociSignIn() 
    // loginPage.login(username, password)
    cy.visit('/')
    cy.wait(1000)
  })


  /*
    Description: On eCatalog top page, click the [inCAD Library] link at the bottom of eCatalog.
    Test Steps:
        1. inCAD Library is displayed. (https://pre-uk.misumi-ec.com/eu/incadlibrary/)
  */
  it('Incad Library UK', { baseUrl: url_uk }, () => {
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.get('[alt="inCAD Library"]').last().invoke('removeAttr', 'target').click()
    cy.get('.closebutton').click()
    cy.get('[href="/eu/incadlibrary/detail/000153.html"]')
    cy.url().should('include', 'uk.misumi-ec.com/eu/incadlibrary')
  })

  /*
    Description: On eCatalog top page, click the [inCAD Library] link at the bottom of eCatalog.
    Test Steps:
        1. inCAD Library is displayed. (https://pre-de.misumi-ec.com/eu/incadlibrary/)
  */
  it('Incad Library DE', { baseUrl: url_de }, () => {
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.get('[alt="inCAD Library"]').last().invoke('removeAttr', 'target').click()
    cy.get('.closebutton').click()
    cy.get('[href="/eu/incadlibrary/detail/000153.html"]')
    cy.url().should('include', 'de.misumi-ec.com/eu/incadlibrary')
  })

});