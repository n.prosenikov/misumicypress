import Login from '../pom/login';

const login = new Login()

const authUser = require('../../../fixtures/auth-user.json');

const {
  url_uk,
  url_de,
} = authUser;

describe('Incad Library Details', () => {
  beforeEach(() => {
		cy.visit('https://stg0-wos.misumi-ec.com/de/purchase/PA202PurchaseAutoAssignmentCmd.do?id=59149')
		login.acceptCookies().click()
		login.ociSignIn() 
    // loginPage.login(username, password)
    cy.visit('/')
    cy.wait(1000)
  })

/*
  Description: Click the case [No.000153 Lifting Mechanism Using Rack and Pinions] at inCAD Library.
  Test Steps:
      1. Case detail of No.000153 Lifting Mechanism Using Rack and Pinions is displayed.
*/ 
  it('Incad details Library UK', { baseUrl: url_uk },() => {
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.get('[alt="inCAD Library"]').last().invoke('removeAttr', 'target').click()
    cy.get('.closebutton').click()
    cy.contains('No.000153').click()
    cy.contains('No.000153')
  })

/*
  Description: Click the case [No.000153 Lifting Mechanism Using Rack and Pinions] at inCAD Library.
  Test Steps:
      1. Case detail of No.000153 Lifting Mechanism Using Rack and Pinions is displayed.
*/ 
  it('Incad details Library DE', { baseUrl: url_de },() => {
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    cy.get('[alt="inCAD Library"]').last().invoke('removeAttr', 'target').click()  
    cy.get('.closebutton').click()
    cy.contains('Nr.000153').click()
    cy.contains('Nr.000153')
  })

});