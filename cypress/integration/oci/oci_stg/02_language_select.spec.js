import Login from '../pom/login';
import LoginPage from '../pom/loginPage';


const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const loginPage = new LoginPage()

const {
	url_uk,
	url_de,
	url_fr,
	url_it
} = authUser;

describe('Switch Language', () => {
	beforeEach(() => {
		cy.visit('https://stg0-wos.misumi-ec.com/de/purchase/PA202PurchaseAutoAssignmentCmd.do?id=59149')
		login.acceptCookies().click()
		login.ociSignIn() 
		//loginPage.login(username, password)
		cy.visit('/')
		cy.wait(2000)
	})
	/*
	  Description: Switching between UK and DE local office and asserting that the correct language is displayed.
	  Test Steps:
		1. Login onto the UK server with correct credentials and assert correct username
		2. Switch to German using the language select menu and assert German is displayed
	  */
	it('Language - UK', { baseUrl: url_uk }, () => {
		cy.contains('Quick Order Pad')
		loginPage.checkLanguage()
	})

	/*
	Description: Switching between DE and IT local office and asserting that the correct language is displayed.
	Test Steps:
	  1. Login onto the UK server with correct credentials and assert correct username
	  2. Switch to German using the language select menu and assert German is displayed
	*/
	it('Language - DE', { baseUrl: url_de }, () => {
		cy.contains('Schnellbestellung')
		loginPage.checkLanguage()
	})

	/*
	Description: Switching between IT and FR local office and asserting that the correct language is displayed.
	Test Steps:
	  1. Login onto the UK server with correct credentials and assert correct username
	  2. Switch to German using the language select menu and assert German is displayed
	*/
	it('Language - IT', { baseUrl: url_it }, () => {
		cy.contains('Ordine veloce')
		loginPage.checkLanguage()
	})

	/*
	Description: Switching between FR and UK local office and asserting that the correct language is displayed.
	Test Steps:
	  1. Login onto the UK server with correct credentials and assert correct username
	  2. Switch to German using the language select menu and assert German is displayed
	*/
	it('Language - FR', { baseUrl: url_fr }, () => {
		cy.contains('Commande rapide')
		loginPage.checkLanguage()
	})
})