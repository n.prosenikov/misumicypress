import Login from '../pom/login';
import UserMenu from '../pom/userMenu';
import MyPage from '../pom/myPage';
import SideMenu from '../../boc/pom/sideMenu';

const login = new Login()
const userMenu = new UserMenu()
const myPage = new MyPage()
const sideMenu = new SideMenu()

const authUser = require('../../../fixtures/auth-user.json');

const {
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('CAD Download', () => {
    beforeEach(() => {
		cy.visit('https://stg0-wos.misumi-ec.com/de/purchase/PA202PurchaseAutoAssignmentCmd.do?id=59149')
		login.acceptCookies().click()
		login.ociSignIn() 
        // loginPage.login(username, password)
        cy.visit('/')
    })

    /*
        Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [My components] link.
        Test Steps:
            1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
    */
    it('CAD Download - Login Menu UK', { baseUrl: url_uk }, () => {
        userMenu.userPanel().click()
        userMenu.myCadDownloads().click()

        myPage.pageTitle().contains('History of CAD data download')
    })

    /*
        Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [My components] link.
        Test Steps:
            1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
    */
    it('CAD Download - Login Menu DE', { baseUrl: url_de }, () => {
        userMenu.userPanel().click()
        userMenu.myCadDownloads().click()

        myPage.pageTitle().contains('CAD-Download Historie')
    })

    /*
        Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [My components] link.
        Test Steps:
            1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
    */
    it('CAD Download - Login Menu IT', { baseUrl: url_it }, () => {
        userMenu.userPanel().click()
        userMenu.myCadDownloads().click()

        myPage.pageTitle().contains('Storico download dati CAD')
    })

    /*
        Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [My components] link.
        Test Steps:
            1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
    */
    it('CAD Download - Login Menu FR', { baseUrl: url_fr }, () => {
        userMenu.userPanel().click()
        userMenu.myCadDownloads().click()

        myPage.pageTitle().contains('Historique des téléchargements CAO')
    })


    /*
        Description: eCatalog top page - Click the [My CAD data downloads] link on the right side of the page.
        Test Steps:
            1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
    */
    it('CAD Download - Side Menu UK', { baseUrl: url_uk }, () => {
        sideMenu.myCadDownloads().click()
        myPage.pageTitle().contains('History of CAD data download')
    })

    /*
        Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [My components] link.
        Test Steps:
            1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
    */
    it('CAD Download - Side Menu DE', { baseUrl: url_de }, () => {
        sideMenu.myCadDownloads().click()
        myPage.pageTitle().contains('CAD-Download Historie')
    })

    /*
        Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [My components] link.
        Test Steps:
            1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
    */
    it('CAD Download - Side Menu IT', { baseUrl: url_it }, () => {
        sideMenu.myCadDownloads().click()
        myPage.pageTitle().contains('Storico download dati CAD')
    })

    /*
        Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [My components] link.
        Test Steps:
            1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
    */
    it('CAD Download - Side Menu FR', { baseUrl: url_fr }, () => {
        sideMenu.myCadDownloads().click()
        myPage.pageTitle().contains('Historique des téléchargements CAO')
    })
});
