import Login from '../pom/login';
import MyPage from '../pom/myPage';

const login = new Login()
const myPage = new MyPage()

const authUser = require('../../../fixtures/auth-user.json');
const {
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Request Quote', () => {
  beforeEach(() => {
    cy.visit('https://stg0-wos.misumi-ec.com/de/purchase/PA202PurchaseAutoAssignmentCmd.do?id=59149')
    login.acceptCookies().click()
    login.ociSignIn()
    //loginPage.login(username, password)
    cy.visit('/')

  })

  /*
      Description: eCatalog top page - Click the [Request a Quote] button on the right side of the page.
      Test Steps:
          1. Quotation: the page is displayed. (New Quotation)
  */
  it('Request Quote UK', { baseUrl: url_uk }, () => {
    myPage.quickOrderPadBtn().click()
    cy.contains('Create Item List')
  })

  /*
      Description: eCatalog top page - Click the [Request a Quote] button on the right side of the page.
      Test Steps:
          1. Quotation: the page is displayed. (New Quotation)
  */
  it('Request Quote DE', { baseUrl: url_de }, () => {
    myPage.quickOrderPadBtn().click()
    cy.contains('Artikelliste für den Warenkorb erstellen')
  })

  /*
      Description: eCatalog top page - Click the [Request a Quote] button on the right side of the page.
      Test Steps:
          1. Quotation: the page is displayed. (New Quotation)
  */
  it('Request Quote IT', { baseUrl: url_it }, () => {
    myPage.quickOrderPadBtn().click()
    cy.contains('Crea una lista articoli')
  })

  /*
      Description: eCatalog top page - Click the [Request a Quote] button on the right side of the page.
      Test Steps:
          1. Quotation: the page is displayed. (New Quotation)
  */
  it('Request Quote FR', { baseUrl: url_fr }, () => {
    myPage.quickOrderPadBtn().click()
    cy.contains("Établir une liste d'articles pour le panier")
  })

});
