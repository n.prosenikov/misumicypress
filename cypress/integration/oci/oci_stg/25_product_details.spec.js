import Login from '../pom/login';

const login = new Login()

const authUser = require('../../../fixtures/auth-user.json');

const {
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Product details', () => {
  beforeEach(() => {
		cy.visit('https://stg0-wos.misumi-ec.com/de/purchase/PA202PurchaseAutoAssignmentCmd.do?id=59149')
		login.acceptCookies().click()
		login.ociSignIn() 
    // loginPage.login(username, password)
    cy.visit('/')
  })
  /*
    Description: Click [Straight] on the specification search page [Linear Shafts].
    Test Steps:
        1. Product details page: the page is displayed. (MISUMI Top Page> Automation Components> Linear Motion> Linear Shafts> Straight)
  */
  it('Product Details UK', { baseUrl: url_uk }, () => {
    cy.contains('Automation Components').click({ force: true })
    cy.contains('Search by category of Automation Components')
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('Linear Shafts').click()
    cy.contains('Straight').invoke('removeAttr', 'target').click()
    cy.contains('Straight')
  })

  /*
    Description: Click [Straight] on the specification search page [Linear Shafts].
    Test Steps:
        1. Product details page: the page is displayed. (MISUMI Top Page> Automation Components> Linear Motion> Linear Shafts> Straight)
  */
  it('Product Details DE', { baseUrl: url_de }, () => {
    cy.contains('Mechanische Komponenten').click({ force: true })
    cy.contains('Suche nach Kategorie der Mechanische Komponenten')
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('Linearwellen').click()
    cy.contains('Gerade').invoke('removeAttr', 'target').click()
    cy.contains('Gerade')
    //cy.contains('Linearwellen / gerade / Bearbeitung wählbar').invoke('removeAttr', 'target').click()
    //cy.contains('Linearwellen / gerade / Bearbeitung wählbar')
  })

  /*
    Description: Click [Straight] on the specification search page [Linear Shafts].
    Test Steps:
        1. Product details page: the page is displayed. (MISUMI Top Page> Automation Components> Linear Motion> Linear Shafts> Straight)
  */
  it('Product Details IT', { baseUrl: url_it }, () => {
    cy.get('[href="/vona2/mech/"]').click({ force: true })
    cy.contains('Eseguire la ricerca specificando la categoria di Componenti meccanici')
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('Alberi lineari').click()
    //cy.contains('Dritto').click()
    cy.contains('Dritti').invoke('removeAttr', 'target').click()
    cy.contains('Dritti')
  })

  /*
    Description: Click [Straight] on the specification search page [Linear Shafts].
    Test Steps:
        1. Product details page: the page is displayed. (MISUMI Top Page> Automation Components> Linear Motion> Linear Shafts> Straight)
  */
  it('Product Details FR', { baseUrl: url_fr }, () => {
    cy.contains('Composants mécaniques').click({ force: true })
    cy.contains('Rechercher en précisant la catégorie de Composants mécaniques')
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('Arbres linéaires').click()
    //cy.contains('Droit')
    cy.contains('Droit').invoke('removeAttr', 'target').click()
    cy.contains('Droit')
  })

});