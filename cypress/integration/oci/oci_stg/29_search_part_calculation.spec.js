import Login from '../pom/login';

const login = new Login()

const authUser = require('../../../fixtures/auth-user.json');

const {
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('Search part Calculation', () => {
    beforeEach(() => {
		cy.visit('https://stg0-wos.misumi-ec.com/de/purchase/PA202PurchaseAutoAssignmentCmd.do?id=59149')
		login.acceptCookies().click()
		login.ociSignIn() 
        // loginPage.login(username, password)
        cy.visit('/')
        cy.wait(1500)
    })
    const partName = "SFJ10-100"
    /*
        Description: Enter LHQRW10 in the Part Number field on the product details page and press the [Enter] key
        Test Steps:
            1. Price calculation: the area is displayed on the left side of the page.
    */
    it('Suggestion Calculation UK', { baseUrl: url_uk }, () => {
        cy.searchPart(partName)
        cy.url().should('include', partName)
        cy.contains(partName)
        cy.contains("Total") //If it exists then a price is avaiable
    })

    /*
        Description: Enter LHQRW10 in the Part Number field on the product details page and press the [Enter] key
        Test Steps:
            1. Price calculation: the area is displayed on the left side of the page.
    */
    it('Suggestion Calculation DE', { baseUrl: url_de }, () => {
        cy.searchPart(partName)
        cy.url().should('include', partName)
        cy.contains(partName)
        cy.contains("Gesamtsumme")
    })

    /*
        Description: Enter LHQRW10 in the Part Number field on the product details page and press the [Enter] key
        Test Steps:
            1. Price calculation: the area is displayed on the left side of the page.
    */
    it('Suggestion Calculation IT', { baseUrl: url_it }, () => {
        cy.searchPart(partName)
        cy.url().should('include', partName)
        cy.contains(partName)
        cy.contains("Totale")
    })

    /*
        Description: Enter LHQRW10 in the Part Number field on the product details page and press the [Enter] key
        Test Steps:
            1. Price calculation: the area is displayed on the left side of the page.
    */
    it('Suggestion Calculation FR', { baseUrl: url_fr }, () => {
        cy.searchPart(partName)
        cy.url().should('include', partName)
        cy.contains(partName)
        cy.contains("Total")
    })

});