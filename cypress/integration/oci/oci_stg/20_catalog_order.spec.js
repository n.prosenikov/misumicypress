import Login from '../pom/login';

const login = new Login()

const authUser = require('../../../fixtures/auth-user.json');

const {
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Catalog order', () => {
  beforeEach(() => {
		cy.visit('https://stg0-wos.misumi-ec.com/de/purchase/PA202PurchaseAutoAssignmentCmd.do?id=59149')
		login.acceptCookies().click()
		login.ociSignIn() 
    // loginPage.login(username, password)
    cy.visit('/')
  })
/*
  Description: catalog top page - click the [Request Catarog] link at the bottom center of the page
  Test Steps:
      1. Catalog image is displayed.(Category search)
        *This page will not be displayed in Pre environment since this does not exit in Pre environment.
*/ 
  it('Catalogue Request UK', { baseUrl: url_uk },() => {
    cy.contains('Catalogue Request').click({ force: true })
    cy.contains('Request catalogue')
  })

/*
  Description: catalog top page - click the [Request Catarog] link at the bottom center of the page
  Test Steps:
      1. Catalog image is displayed.(Category search)
        *This page will not be displayed in Pre environment since this does not exit in Pre environment.
*/ 
  it('Catalogue Request DE', { baseUrl: url_de },() => {
    cy.contains('Katalog anfordern').click({ force: true })
    cy.contains('Kataloge anfordern')
  })


/*
  Description: catalog top page - click the [Request Catarog] link at the bottom center of the page
  Test Steps:
      1. Catalog image is displayed.(Category search)
        *This page will not be displayed in Pre environment since this does not exit in Pre environment.
*/ 
  it('Catalogue Request IT', { baseUrl: url_it },() => {
    cy.contains('Richiedi cataloghi').click({ force: true })
    cy.contains('Richiesta di cataloghi')
  })

/*
  Description: catalog top page - click the [Request Catarog] link at the bottom center of the page
  Test Steps:
      1. Catalog image is displayed.(Category search)
        *This page will not be displayed in Pre environment since this does not exit in Pre environment.
*/ 
  it('Catalogue Request FR', { baseUrl: url_fr },() => {
  cy.contains('Demande de catalogues').click({ force: true })
  cy.contains('Demande de catalogues')
})

});