import Login from '../pom/login';

const login = new Login()

const authUser = require('../../../fixtures/auth-user.json');

const {
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('No suggestion', () => {
  beforeEach(() => {
		cy.visit('https://stg0-wos.misumi-ec.com/de/purchase/PA202PurchaseAutoAssignmentCmd.do?id=59149')
		login.acceptCookies().click()
		login.ociSignIn() 
    // loginPage.login(username, password)
    cy.visit('/')
  })
  /*
    Description: eCatalog top page - Enter @@@ in the search field at the top of the page.
    Test Steps:
        1. Suggestion is not displayed.
  */
  it('Suggestion No Hits UK', { baseUrl: url_uk }, () => {
    cy.get('#keyword_input').type('@@@')
    cy.contains('@@@').should('not.exist');
  })

  /*
    Description: eCatalog top page - Enter @@@ in the search field at the top of the page.
    Test Steps:
        1. Suggestion is not displayed.
  */
  it('Suggestion No Hits DE', { baseUrl: url_de },() => {
    cy.get('#keyword_input').type('@@@')
    cy.contains('@@@').should('not.exist');
  })

  /*
    Description: eCatalog top page - Enter @@@ in the search field at the top of the page.
    Test Steps:
        1. Suggestion is not displayed.
  */
  it('Suggestion No Hits IT', { baseUrl: url_it },() => {
    cy.get('#keyword_input').type('@@@')
    cy.contains('@@@').should('not.exist');
  })

  /*
    Description: eCatalog top page - Enter @@@ in the search field at the top of the page.
    Test Steps:
        1. Suggestion is not displayed.
  */
  it('Suggestion No Hits FR', { baseUrl: url_fr },() => {
    cy.get('#keyword_input').type('@@@')
    cy.contains('@@@').should('not.exist');
  })

});