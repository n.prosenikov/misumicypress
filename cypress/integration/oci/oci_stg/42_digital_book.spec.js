import Login from '../pom/login';

const login = new Login()

const authUser = require('../../../fixtures/auth-user.json');

const {
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Digital Book', () => {
  beforeEach(() => {
		cy.visit('https://stg0-wos.misumi-ec.com/de/purchase/PA202PurchaseAutoAssignmentCmd.do?id=59149')
		login.acceptCookies().click()
		login.ociSignIn() 
    // loginPage.login(username, password)
    cy.visit('/')
    cy.wait(2000)
  })
  /*
    Description: Part number of product SFJ3-10: product details page - Click the [Catalog] button at the top right of the page.
    Test Steps:
        1. Digital Book is displayed.
  */
  it('Digital Book UK', { baseUrl: url_uk }, () => {
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.get('#Tab_catalog').click()
    cy.get('.catalogViewer')
  })

  it('Digital Book DE', { baseUrl: url_de }, () => {
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.get('#Tab_catalog').click()
    cy.get('.catalogViewer')
  })

  it('Digital Book IT', { baseUrl: url_it }, () => {
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.get('#Tab_catalog').click()
    cy.get('.catalogViewer')
  })

  it('Digital Book FR', { baseUrl: url_fr }, () => {
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.get('#Tab_catalog').click()
    cy.get('.catalogViewer')
  })

});