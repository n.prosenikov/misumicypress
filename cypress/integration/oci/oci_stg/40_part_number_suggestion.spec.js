import Login from '../pom/login';

const login = new Login()

const authUser = require('../../../fixtures/auth-user.json');

const {
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Part Number suggestion', () => {
  beforeEach(() => {
		cy.visit('https://stg0-wos.misumi-ec.com/de/purchase/PA202PurchaseAutoAssignmentCmd.do?id=59149')
		login.acceptCookies().click()
		login.ociSignIn() 
    // loginPage.login(username, password)
    cy.visit('/')
    cy.wait(2000)
  })
  /*
    Description: eCatalog top page - Enter SFJ3-10 in the search field at the top of the page and click the displayed [SFJ3-10 [MISUMI]] link which is suggested.
    Test Steps:
        1. Search result: Product details page is displayed on which part number of product SFJ3-10 is selected.
  */
  it('Suggestion - Part Number UK', { baseUrl: url_uk },() => {
    cy.searchPart('SFJ3-100')
    cy.wait(1000)
    cy.contains('SFJ3-100')
  })

  /*
    Description: eCatalog top page - Enter SFJ3-10 in the search field at the top of the page and click the displayed [SFJ3-10 [MISUMI]] link which is suggested.
    Test Steps:
        1. Search result: Product details page is displayed on which part number of product SFJ3-10 is selected.
  */
  it('Suggestion - Part Number DE', { baseUrl: url_de }, () => {
    cy.searchPart('SFJ3-100')
    cy.wait(1000)
    cy.contains('SFJ3-100')
  })

  /*
    Description: eCatalog top page - Enter SFJ3-10 in the search field at the top of the page and click the displayed [SFJ3-10 [MISUMI]] link which is suggested.
    Test Steps:
        1. Search result: Product details page is displayed on which part number of product SFJ3-10 is selected.
  */
  it('Suggestion - Part Number IT', { baseUrl: url_it },() => {
    cy.searchPart('SFJ3-100')
    cy.wait(1000)
    cy.contains('SFJ3-100')
  })
  
  /*
    Description: eCatalog top page - Enter SFJ3-10 in the search field at the top of the page and click the displayed [SFJ3-10 [MISUMI]] link which is suggested.
    Test Steps:
        1. Search result: Product details page is displayed on which part number of product SFJ3-10 is selected.
  */
  it('Suggestion - Part Number FR', { baseUrl: url_fr },() => {
    cy.searchPart('SFJ3-100')
    cy.wait(1000)
    cy.contains('SFJ3-100')
  })

});