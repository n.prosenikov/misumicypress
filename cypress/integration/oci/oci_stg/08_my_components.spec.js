import Login from '../pom/login';
import MyPage from '../pom/myPage';

const login = new Login()
const myPage = new MyPage()

const authUser = require('../../../fixtures/auth-user.json');

const {
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('My Components', () => {
  beforeEach(() => {
		cy.visit('https://stg0-wos.misumi-ec.com/de/purchase/PA202PurchaseAutoAssignmentCmd.do?id=59149')
		login.acceptCookies().click()
		login.ociSignIn() 
    // loginPage.login(username, password)
    cy.visit('/')
  })
  
/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [My components] link.
    Test Steps:
        1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
*/
  it('My Components UK', { baseUrl: url_uk }, () => {
    myPage.myComponents().click()
    cy.location('pathname').should('eq', '/my/parts.html')
    cy.contains('Main folder')
  })

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [My components] link.
    Test Steps:
        1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
*/
  it('My Components DE', { baseUrl: url_de }, () => {
    myPage.myComponents().click()
    cy.location('pathname').should('eq', '/my/parts.html')
    cy.contains('Hauptordner (Liste meiner Komponenten)')
  })

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [My components] link.
    Test Steps:
        1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
*/
  it('My Components IT', { baseUrl: url_it }, () => {
    myPage.myComponents().click()
    cy.location('pathname').should('eq', '/my/parts.html')
    cy.contains('Cartella principale (La lista dei miei componenti)')
  })

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [My components] link.
    Test Steps:
        1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
*/
  it('My Components FR', { baseUrl: url_fr }, () => {
    myPage.myComponents().click()
    cy.location('pathname').should('eq', '/my/parts.html')
    cy.contains('Dossier principal (Liste de mes composants)')
  })

});