import Login from '../pom/login';

const login = new Login()

const authUser = require('../../../fixtures/auth-user.json');
const partName = "SFJ10-100"

const {
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('Search parts', () => {
    beforeEach(() => {
		cy.visit('https://stg0-wos.misumi-ec.com/de/purchase/PA202PurchaseAutoAssignmentCmd.do?id=59149')
		login.acceptCookies().click()
		login.ociSignIn() 
        // loginPage.login(username, password)
        cy.visit('/')
        cy.wait(1500)
    })
    /*
        Description: eCatalog top page - Enter SFJ10-100 in the search field at the top of the page.
        Test Steps:
            1. The suggestion for the product not listed in MISUMI catalog, whose part number is inputted, is displayed. (SFJ10-100[MISUMI Discontinued product,limited availability])
    */
    it('Search Parts UK', { baseUrl: url_uk }, () => {
        cy.get('#keyword_input').type(partName)
        cy.contains('SFJ10-100 [MISUMI]');
    })

    /*
        Description: eCatalog top page - Enter SFJ10-100 in the search field at the top of the page.
        Test Steps:
            1. The suggestion for the product not listed in MISUMI catalog, whose part number is inputted, is displayed. (SFJ10-100[MISUMI Discontinued product,limited availability])
    */
    it('Search parts DE', { baseUrl: url_de }, () => {
        cy.get('#keyword_input').type(partName)
        cy.contains('SFJ10-100 [MISUMI]');
    })

    /*
        Description: eCatalog top page - Enter SFJ10-100 in the search field at the top of the page.
        Test Steps:
            1. The suggestion for the product not listed in MISUMI catalog, whose part number is inputted, is displayed. (SFJ10-100[MISUMI Discontinued product,limited availability])
    */
    it('Search parts IT', { baseUrl: url_it }, () => {
        cy.get('#keyword_input').type(partName)
        cy.contains('SFJ10-100 [MISUMI]');
    })

    /*
        Description: eCatalog top page - Enter SFJ10-100 in the search field at the top of the page.
        Test Steps:
            1. The suggestion for the product not listed in MISUMI catalog, whose part number is inputted, is displayed. (SFJ10-100[MISUMI Discontinued product,limited availability])
    */
    it('Search parts FR', { baseUrl: url_fr }, () => {
        cy.get('#keyword_input').type(partName)
        cy.contains('SFJ10-100 [MISUMI]');
    })

    /*
        Description: eCatalog top page - Enter SFJ10-100 in the search field at the top of the page.
        Test Steps:
            1. The suggestion for the product not listed in MISUMI catalog, whose part number is inputted, is displayed. (SFJ10-100[MISUMI Discontinued product,limited availability])
    */
    it('Search Parts Open Part Number UK', { baseUrl: url_uk }, () => {
        cy.searchPart(partName)
        cy.url().should('include', partName)
        cy.contains(partName)
    })

    /*
        Description: eCatalog top page - Enter SFJ10-100 in the search field at the top of the page.
        Test Steps:
            1. The suggestion for the product not listed in MISUMI catalog, whose part number is inputted, is displayed. (SFJ10-100[MISUMI Discontinued product,limited availability])
    */
    it('Search Parts Open Part Number DE', { baseUrl: url_de }, () => {
        cy.searchPart(partName)
        cy.url().should('include', partName)
        cy.contains(partName)
    })

    /*
        Description: eCatalog top page - Enter SFJ10-100 in the search field at the top of the page.
        Test Steps:
            1. The suggestion for the product not listed in MISUMI catalog, whose part number is inputted, is displayed. (SFJ10-100[MISUMI Discontinued product,limited availability])
    */
    it('Search Parts Open Part Number IT', { baseUrl: url_it }, () => {
        cy.searchPart(partName)
        cy.url().should('include', partName)
        cy.contains(partName)
    })

    /*
        Description: eCatalog top page - Enter SFJ10-100 in the search field at the top of the page.
        Test Steps:
            1. The suggestion for the product not listed in MISUMI catalog, whose part number is inputted, is displayed. (SFJ10-100[MISUMI Discontinued product,limited availability])
    */
    it('Search Parts Open Part Number FR', { baseUrl: url_fr }, () => {
        cy.searchPart(partName)
        cy.url().should('include', partName)
        cy.contains(partName)
    })
}); 