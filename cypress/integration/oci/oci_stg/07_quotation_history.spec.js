import Login from '../pom/login';
import MyPage from '../pom/myPage';

const login = new Login()
const myPage = new MyPage()

const authUser = require('../../../fixtures/auth-user.json');

const {
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Quote History', () => {
  beforeEach(() => {
		cy.visit('https://stg0-wos.misumi-ec.com/de/purchase/PA202PurchaseAutoAssignmentCmd.do?id=59149')
		login.acceptCookies().click()
		login.ociSignIn() 
    // loginPage.login(username, password)
    cy.visit('/')
    cy.wait(1500)

  })

  /*
    Description: eCatalog top page - Click the [Manage Quote History] link on the right side of the page.
    Test Steps:
        1. Quotation history: the page is displayed.(MISUMI Home>My MISUMI>Quotation History)
  */
  it('Quotation History UK', { baseUrl: url_uk }, () => {
    myPage.orderHistory(0).click() //0-UK, 1- DE, 2-It, 3-FR
    cy.contains('Quotation History')
  })

  /*
      Description: eCatalog top page - Click the [Manage Quote History] link on the right side of the page.
      Test Steps:
          1. Quotation history: the page is displayed.(MISUMI Home>My MISUMI>Quotation History)
  */
  it('Quotation History DE', { baseUrl: url_de }, () => {
    myPage.orderHistory(1).click() //0-UK, 1- DE, 2-It, 3-FR
    cy.contains('Angebotshistorie')
  })

  /*
      Description: eCatalog top page - Click the [Manage Quote History] link on the right side of the page.
      Test Steps:
          1. Quotation history: the page is displayed.(MISUMI Home>My MISUMI>Quotation History)
  */
  it('Quotation History IT', { baseUrl: url_it }, () => {
    myPage.orderHistory(2).click() //0-UK, 1- DE, 2-It, 3-FR
    cy.contains('Storico quotazioni')
  })

  /*
      Description: eCatalog top page - Click the [Manage Quote History] link on the right side of the page.
      Test Steps:
          1. Quotation history: the page is displayed.(MISUMI Home>My MISUMI>Quotation History)
  */
  it('Quotation History FR', { baseUrl: url_fr }, () => {
    myPage.orderHistory(3).click() //0-UK, 1- DE, 2-It, 3-FR
    cy.contains('Historique des devis')
  })
});