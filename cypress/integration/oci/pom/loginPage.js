const authUser = require('../../../../auth-user.json');

const loginRegisterStrings = [
    "Login/Register",
    "Login/Registrierung",
    "Se connecter/Crèe...",
    "Login/Registrati"
]

const languageName = [
    "uk","de", "fr", "it"
]

export default class LoginPage {
    languageBtn() {
        return cy.get('[data-language="button"]')
    }

    languageList(lanugageOption) {
        //return cy.get('.l-switchLanguage__balloon').find('ul').find('li').find(`.lc-lang--${lanugageOption}`)
        return cy.get(`.lc-lang--${lanugageOption}`)
    }

    checkLanguage() {
        this.languageBtn().click()
        languageName.forEach($language => {
            this.languageList($language).should('exist')
        })

    }

    loginFunc = (username, password, name) => {
        const log = Cypress.log({
            name: "login",
            displayName: `LOGIN ${cy.url()}`,
            message: [`🔐 Authenticating | ${username}`],
            autoEnd: false
        })
        cy.session([username, password], () => {
            cy.visit('/')
            // cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
            cy.intercept('GET', '**userInfo**').as('loginUser')
            this.acceptCookies().click()
            this.loginMenuButton().click()
            this.username().type(username)
            this.password().type(password)
            this.signInButton().click()
            cy.wait("@loginUser")
            log.end();
        })
    }

    login = () => {
        cy.fixture('auth-user').then(env => {
            const {
                username,
                password,
                name
            } = env
            this.loginFunc(username, password, name)
        })
    }

    signIn() {
        const { username, password } = authUser;
        this.acceptCookies().click()
        loginMenuButton().click()
        this.username().type(username)
        this.password().type(password)
        this.signInButton().click()
        return this
    }
    loginMenuButton() {
        return cy.get('[data-login="loginButton"]');
    }

    acceptCookies() {
        return cy.get('.l-cookieNotification__accept');
    }

    essentialCookies() {
        return cy.get('.l-cookieNotification__reject');
    }

    username() {
        return cy.get('[data-login="userid"]')
    }

    password() {
        return cy.get('[data-login="password"]')
    }

    signInButton() {
        return cy.get('[data-login="submit"]')
    }

    logOut(i) {
        cy.get('.lc-user').trigger('mouseover')
        cy.get('[data-login="logoutLink"]').click({ force: true })
        this.checkLoginBtn(i)
    }

    checkLoginBtn(i) {
        cy.contains(loginRegisterStrings[i])
    }
}

