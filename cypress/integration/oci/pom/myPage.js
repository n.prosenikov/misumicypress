const orderHistoryOption = ["Checkout History", "Bestellhistorie", "Accedi a storico ordini", "Historique commandes"]

export default class MyPage {

    pageTitle() {
        return cy.get('title')
    }

    pageTopic() {
        return cy.get('.topicPath')
    }


    couponCode() {
        return cy.get('.code');
    }

    couponPrice() {
        return cy.get('.rate');
    }

    couponDate() {
        return cy.get('.date');
    }

    couponType() {
        return cy.get('.type');
    }

    couponTitle() {
        return cy.get('.title');
    }

    couponStatus() {
        return cy.get('.status');
    }

    quickOrderPadBtn() {
        return cy.get('.m-btn--headerOrder')
    }

    orderHistory(language) {
        return cy.get('.l-header__linkList').find('li').contains(orderHistoryOption[language])
    }

    myComponents(){
        return cy.get('[data-common-userlink="partslist"]')
    }

    pageTitle() {
        return cy.get('.titleBlock__h1')
    }
    
    pageTag() {
        return cy.get('.caddatadlArea')
    }
}