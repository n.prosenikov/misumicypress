import Login from "../../boc/pom/login"
import PartSolutions from "../pom/psol";

const loginPage = new Login()
const partSolution = new PartSolutions()

const authUser = require('../../../fixtures/auth-user.json');
const {
    username,
    password,
    url_uk,
} = authUser;

describe('Copy to Part Solutions', () => {
    beforeEach(() => {
        cy.clearCookies()
        loginPage.login(username, password)
        cy.visit('/')
        cy.visit('/psol')
        cy.wait(1000)
    });

    it('Copy Completed Part Number LMUW5 [UK]', { baseUrl: url_uk }, () => {
        partSolution.searchPartNumberFn(0, "LMUW5") //choose option from result list
        partSolution.checkIfPartNumberIsCompleted("LMUW5", 0) //if the part is completed, it will be added to the cart
    })

    it('Not Completed Part Number [UK]', { baseUrl: url_uk }, () => {
        partSolution.searchPartNumberFn(0, "LMU") //choose option from result list
        partSolution.checkIfPartNumberIsCompleted("LMU", 0) //if the part is completed, it will be added to the cart
    })

    it('Create a Completed Part [UK]', { baseUrl: url_uk }, () => {
        partSolution.searchPartNumberFn(0, "JBYM") //choose option from result list
        partSolution.checkIfPartNumberIsCompleted("JBYM", 0) //if the part is completed, it will be added to the cart
        partSolution.diameterOption(25).click()
        cy.wait(2500)
        cy.get('.m-chL').find(`[data-sitelog-value="15"]`).last().click()
        partSolution.orderQuantityField().should('exist')
    })

    it('Download PDF Completed Part Number LMUW5 [UK]', { baseUrl: url_uk }, () => {
        partSolution.searchPartNumberFn(0, "LMUW5") //choose option from result list
        partSolution.partNumberTitle().should('contain.text', "LMUW5") //if the part is completed, it will be added to the cart
        partSolution.pdfBtnSelector().find('a').then(link => {
            cy.request(link.prop('href')).its('status').should('eq', 200);
        })
    })

})