const suggestionMsgsByNotCompletedPartNumber = [
    "Suggestion of an applicable part number",
    "Vorgeschlagene Teilenummer",
    "Numéro de référence applicable suggérée",
    "Codice componente applicabile suggerito"
]

const addedItemToCartMsgs = [
    "The item has been added to the shopping cart.",
    "Der Artikel wurde zum Warenkorb hinzugefügt.",
    "Un produit a été ajouté au panier.",
    "Un prodotto aggiunto al carrello."
]

const automationComponents = [
    "Linear Bushings",
    "Ball Guides",
    "Washers, Collars",
    "Non-Metal Washers, Collars",
    "Locating Pins, Bushings",
    "Bushings for Fixtures",
    "Linear Guides",
    "Standard Linear Guides",
    "Shaft Supports"
]

const subComponentsOptionsLIVE = [
    "Linear ball bearings / flange selectable / stainless steel, steel / treatment selectable / seal selectable",
    "Shafts for Miniature Ball Bearing Guides / Both Ends Machined / Both Ends Tapped",
    "Thermally insulating ceramic washers / sleeves",
    "Flanged drill bushes / round flange / bore +0.01 / steel, stainless steel / 50HRC-60HRC",
    "Miniature profiled rail guides / wide rail / wide carriage / material selectable",
    "Shaft holders / guided round flange, round flange flattened on both sides / slotted"
]

const subComponentsOptionsSTG = [
    "Linear Bushings/Standard/Double",
    "Shafts for Miniature Ball Bearing Guides/Both Ends Machined/Both Ends Tapped",
    "Thermal Insulation Ceramic Washers/Collars",
    "Bushings for Locating Pins/Round Flanged",
    "Miniature Linear Guides/Wide Rails/Wide Block",
    "Shaft Supports/Flanged Mount Type with Slit"
]

const automationOptionsInTheLeftSection = [
    'Linear Motion',
    'Rotary Motion',
    'Connecting Parts',
    'Rotary Power Transmission',
    'Conveyors & Material Handling',
    'Locating, Positioning, Jigs & Fixtures',
    'Inspection',
    'Sensors, Switches',
    'Vacuum Components',
    'Hydraulic Equipment',
    'Discharging / Painting Devices',
    'Pipe, Tubes, Hoses & Fittings',
    'Heaters, Temperature Control',
    'Framing & Support',
    'Casters, Leveling Mounts, Posts',
    'Doors, Cabinet Hardware',
    'Springs, Shock Absorbers',
    'Adjusting, Fastening,  Magnets',
    'Antivibration, Soundproofing Materials, Safety Products'
]

const subCategoriesPart1Stg = [
    'Linear Shafts',
    'Shaft Supports',
    'Linear Bushings',
    'Ball Guides',
    'Spline Shaft',
    'Oil Free Bushings, Washers',
    'Oil Free Plates, Guide Rails',
    'Linear Guides',
    'Cable Carriers',
    'Ball Screw',
    'Support Units',
    'Lead Screws, Slide Screws',
    'Slide Packs, V Guides, Linear Rails',
    'Actuators',
    'Locating Pins, Bushings',
    'Stop Pins, Stopper Blocks',
    'Adjusting Screws, Threaded Stopper Blocks',
    'Locating and Guide Components',
    'Locating Units',
    'Automotive Inspection Components',
    'Plungers, Index Plungers',
    'Clamp',
    'Workpiece Clamps',
    'Bearings',
    'Bearing Units',
    'Cam Followers, Roller Followers',
    'Rotary Shafts, Drive Shafts',
    'Cantilever Shafts',
    'Fulcrum Pins',
    'Hinge Pins',
    'Hinge Bases, Hinge Plates, Hinge Bolt',
    'Links',
    'Rod End Bearings, Spherical Bearings',
    'Connecting Bars, Link Cables/Wires',
    'Shaft Couplings',
    'Keyless Bushing (Fastening Fittings)',
    'Timing Belts, Pulleys',
    'Round Belts, Pulleys',
    'Chains, Sprockets',
    'Tensioners',
    'Gears',
    'Belt Conveyors, Plastic Chain Conveyors',
    'Roller Conveyors',
    'Wheel Conveyors, Roller Carriers',
    'Flat Belts, Pulleys',
    'Conveyer Timing Belts',
    'Conveyer Chains, Sprockets',
    'Rollers',
    'Bearings with Resin',
    'Ball Rollers',
    'Manual Stages',
    'Simplified Adjustment Units',
    'Contact Probes',
    'Sensors',
    'Sensor Rails, DIN Rails',
    'Sensor Peripheral Components',
    'Cylinders, Rotary Actuators',
    'Cylinder Connecting Components',
    'Pneumatic Pipe Fittings',
    'Speed Controllers',
    'Tubes',
    'Fluororesin Products for Chemicals',
]

const subCategoriesPart1 = [
    'Linear Shafts',
    'Shaft Supports',
    'Set Collars / Clamping Collars',
    'Linear Bushings',
    'Ball Guides',
    'Spline Shaft',
    'Oil Free Bushings, Washers',
    'Oil Free Plates, Guide Rails',
    'Linear Guides',
    'Cable Carriers',
    'Ball Screw',
    'Support Units',
    'Lead Screws, Slide Screws',
    'Slide Packs, V Guides, Linear Rails',
    'Actuators',
    'Locating Pins, Bushings',
    'Stop Pins, Stopper Blocks',
    'Adjusting Screws, Threaded Stopper Blocks',
    'Locating and Guide Components',
    'Locating Units',
    'Automotive Inspection Components',
    'Plungers, Index Plungers',
    'Clamp',
    'Workpiece Clamps',
    'Bearings',
    'Bearing Units',
    'Cam Followers, Roller Followers',
    'Rotary Shafts, Drive Shafts',
    'Cantilever Shafts',
    'Fulcrum Pins',
    'Hinge Pins',
    'Hinge Bases, Hinge Plates, Hinge Bolt',
    'Links',
    'Rod End Bearings, Spherical Bearings',
    'Connecting Bars, Link Cables/Wires',
    'Shaft Couplings',
    'Keyless Bushing (Fastening Fittings)',
    'Timing Belts, Pulleys',
    'Round Belts, Pulleys',
    'Chains, Sprockets',
    'Tensioners',
    'Gears',
    'Belt Conveyors, Plastic Chain Conveyors',
    'Roller Conveyors',
    'Wheel Conveyors, Roller Carriers',
    'Flat Belts, Pulleys',
    'Conveyer Timing Belts',
    'Conveyer Chains, Sprockets',
    'Rollers',
    'Bearings with Resin',
    'Ball Rollers',
    'Manual Stages',
    'Simplified Adjustment Units',
    'Contact Probes',
    'Sensors',
    'Sensor Rails, DIN Rails',
    'Sensor Peripheral Components',
    'Cylinders, Rotary Actuators',
    'Cylinder Connecting Components',
    'Pneumatic Pipe Fittings',
    'Speed Controllers',
    'Tubes',
    'Fluororesin Products for Chemicals',
]
const subCategoriesPart2 = [
    'Vacuum Fitting Parts',
    'Suction Cups, Components for Suctioning',
    'Hydraulic Hoses, Adaptors',
    'Nozzles',
    'Tanks',
    'Steel Pipes, Copper Pipes, Stainless Steel Pipes',
    'Screw Fittings',
    'Couplers, One-Touch Joints',
    'Valves',
    'Pipe Supports',
    'Manifolds',
    'Resins, Ducts, Flexible Hoses',
    'Duct Hoses, Duct Plumbing Components',
    'Flexible Hoses',
    'Sanitary Pipes',
    'Seal Materials',
    'Temperature Sensors',
    'Cooling Related Products',
    'Aluminum Extrusion and Brackets',
    'Pipe Frames',
    'Mounting Plates & Brackets',
    'Angle Plates',
    'Gussets',
    'Posts, Strut Clamps, Stands',
    'Casters',
    'Wheels',
    'Adjuster, Leveling Mounts, Rubber Feet',
    'Door Stays',
    'Telescopic Slide Rails',
    'Fasteners, Draw Latches',
    'Wires, Chains, Metal Fittings, Small Work Pieces',
    'Parts, Small Work Pieces',
    'Handle',
    'Lever',
    'Rotation Handles, Crank Handles',
    'Handles',
    'Hinges',
    'Springs',
    'Shock Absorbers, Gas Springs',
    'Urethane, Rubber, Sponge, Felt',
    'Magnets',
    'Dowel Pins, Spring Pins, Stepped Pins',
    'Screws, Bolts',
    'Washers',
    'Nuts',
    'Insert',
    'Washers, Collars',
    'Shims',
    'Cotter Pins',
    'Machine Keys',
    'Retaining Rings',
    'Sound Proofing, Antivibration',
    'Safety Protection Materials',
    'Antistatic Materials',
    'Stickers, Plates'
]

export default class PartSolutions {
    //Array
    automationComponentsCopyArray = [
        ...automationComponents
    ]

    subComponentsOptionsCopyArrayLIVE = [
        ...subComponentsOptionsLIVE
    ]

    subComponentsOptionsCopyArraySTG = [
        ...subComponentsOptionsSTG
    ]

    automationOptionsInTheLeftSectionCopyArray = [
        ...automationOptionsInTheLeftSection
    ]

    subCategoriesCopyArrayPart1 = [
        ...subCategoriesPart1
    ]

    subCategoriesCopyArrayPart2 = [
        ...subCategoriesPart2
    ]
    subCategoriesCopyArrayPart1Stg = [
        ...subCategoriesPart1Stg
    ]

    //Selectors
    searchBarSelector() {
        return cy.get('#keyword_input')
    }

    resultOptionSelector(optionNumber) {
        return cy.get(`#keyword_productCode_${optionNumber}`)
    }

    keywordSuggestionOptions() {
        return cy.get('[data-keyword-suggest="keywordLink"]').eq(0)
    }

    suggestionMsg() {
        return cy.get('.lc-msg--code').find('.lc-text')
    }

    partNumberTitle() {
        return cy.get('#ProductCode')
    }

    copyToPartSolutionsBtn() {
        return cy.get('#partsolution_button')
    }

    orderQuantityField() {
        return cy.get('.m-inputText--right')
    }

    addToCartBtnSelector() {
        return cy.get('[data-cartbox-btn="cart"]')
    }

    addedItemMsg() {
        return cy.get('.m-h3')
    }

    diameterOption(diameter) {
        return cy.get(`[data-sitelog-value="${diameter}"]`).eq(0)
    }

    pdfBtnSelector() {
        return cy.get('.u-right')
    }

    automationComponentOption(name) {
        return cy.get('.mc-name').contains(automationComponents[name])
    }

    subCategoryOptionSTG(name) {
        return cy.get('a').contains(subComponentsOptionsSTG[name])
    }

    subCategoryOptionLIVE(name) {
        return cy.get('a').contains(subComponentsOptionsLIVE[name])
    }

    optionInTheLeftPart() {
        return cy.get('[class*="lc-level3"]').find('a')
    }

    subCategoryOptionLeftPart() {
        return cy.get('.lc-level4').find('li')
    }

    //Functions

    searchPartNumberFn(chooseOption, partNumber) {
        this.searchBarSelector().type(partNumber)
        cy.wait(1000)
        this.resultOptionSelector(chooseOption).click()
    }

    searchKeywordSuggestionsFn(partNumber) {
        this.searchBarSelector().type(partNumber)
        cy.wait(1000)
        this.keywordSuggestionOptions().click()
    }

    addItemToCart(language) {
        //this.orderQuantityField().clear().type(quantity)
        this.addToCartBtnSelector().click()
        this.addedItemMsg().should('contain.text', addedItemToCartMsgs[language])
    }

    checkIfPartNumberIsCompleted(partName, language) {
        this.partNumberTitle().then($el => {
            const innerText = $el.text()
            cy.log(innerText) //console log the innerText
            if (innerText.localeCompare(partName) > 0) {
                this.copyToPartSolutionsBtn().should("have.css", "background-color", "rgb(1, 105, 198)")
            } else {
                this.copyToPartSolutionsBtn().should("have.css", "background-color", "rgb(191, 191, 191)")
                this.suggestionMsg().should('contain.text', suggestionMsgsByNotCompletedPartNumber[language])
            }
        })
    }

    defineAComplexPartNumber(railWidth, height, length, lubrication, type) {
        this.diameterOption(railWidth).click()
        this.diameterOption(height).click()
        this.diameterOption(length).click()
        this.diameterOption(lubrication).click()
        this.diameterOption(type).click()
    }

}