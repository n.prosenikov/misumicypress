import Login from "../../boc/pom/login"
import PartSolutions from "../pom/psol";
import Product from "../../mobile/pom/products";

const loginPage = new Login()
const partSolution = new PartSolutions()
const product = new Product()

const authUser = require('../../../fixtures/auth-user.json');
const {
    username,
    password,
    url_uk,
} = authUser;

describe('Keywords search', () => {
    beforeEach(() => {
        cy.clearCookies()
        loginPage.login(username, password)
        cy.visit('/')
        cy.visit('/psol')
    });

    it('Keywords: linear shaft [UK]', { baseUrl: url_uk }, () => {
        partSolution.searchKeywordSuggestionsFn("linear shaft")
        product.itemHeaderSelector().should('contain.text', "linear shaft")
    })

    it('Keywords: ball bearing [UK]', { baseUrl: url_uk }, () => {
        partSolution.searchKeywordSuggestionsFn("ball bearing")
        product.itemHeaderSelector().should('contain.text', "ball bearing")
    })

    it('Keywords: ball screw [UK]', { baseUrl: url_uk }, () => {
        partSolution.searchKeywordSuggestionsFn("ball screw")
        product.itemHeaderSelector().should('contain.text', "ball screw")
    })

    it('Keywords: locating pin [UK]', { baseUrl: url_uk }, () => {
        partSolution.searchKeywordSuggestionsFn("locating pin")
        product.itemHeaderSelector().should('contain.text', "locating pin")
    })

    it('Keywords: ball bearing [UK]', { baseUrl: url_uk }, () => {
        partSolution.searchKeywordSuggestionsFn("ball bearing")
        product.itemHeaderSelector().should('contain.text', "ball bearing")
    })

    it('General Category Overview [UK]', { baseUrl: url_uk }, () => {
        partSolution.automationOptionsInTheLeftSectionCopyArray.forEach(option => {
            partSolution.optionInTheLeftPart().should('contain.text', option)
        })   
    })

    it('Subcategory Overview Part 1 [UK]', { baseUrl: url_uk }, () => {
        partSolution.subCategoriesCopyArrayPart1.forEach(suboption => {
            partSolution.subCategoryOptionLeftPart().should('contain.text', suboption)
        })   
    })

    it('Subcategory Overview Part 2 [UK]', { baseUrl: url_uk }, () => {
        partSolution.subCategoriesCopyArrayPart2.forEach(suboption => {
            partSolution.subCategoryOptionLeftPart().should('contain.text', suboption)
        })   
    })

})