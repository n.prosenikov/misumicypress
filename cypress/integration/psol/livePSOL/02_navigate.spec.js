import Login from "../../boc/pom/login"
import PartSolutions from "../pom/psol";
import Product from "../../mobile/pom/products";

const loginPage = new Login()
const partSolution = new PartSolutions()
const product = new Product()

const authUser = require('../../../fixtures/auth-user.json');
const {
    username,
    password,
    url_uk,
} = authUser;

describe('Navigate to different parts', () => {
    beforeEach(() => {
        cy.clearCookies()
        loginPage.login(username, password)
        cy.visit('/')
        cy.visit('/psol')
    });

    it('Navigate to - Counterbored Holes and Tapped Holes (LHFC40L) [UK]', { baseUrl: url_uk }, () => {
        partSolution.automationComponentOption(0).click() //the number is the serial component we wanna reach
        partSolution.subCategoryOptionLIVE(0).invoke('removeAttr', 'target').click()
        product.itemHeaderSelector().should('contain.text', partSolution.subComponentsOptionsCopyArrayLIVE[0])
        partSolution.diameterOption(80).click()
        partSolution.diameterOption("Type L (Heat Resistance)").click()
        partSolution.diameterOption("LHFC").click()
        partSolution.orderQuantityField().should('exist')
        partSolution.partNumberTitle().should('contain.text', "LHFC40L")
    })

    it('Navigate to - Shafts for Miniature Ball Bearing Guides/Both Ends Machined/Both Ends Tapped (BGFP10-100)[UK]', { baseUrl: url_uk }, () => {
        partSolution.automationComponentOption(1).click() //the number is the serial component we wanna reach
        partSolution.subCategoryOptionLIVE(1).invoke('removeAttr', 'target').click()
        product.itemHeaderSelector().should('contain.text', partSolution.subComponentsOptionsCopyArrayLIVE[1])
        partSolution.diameterOption(10).click()
        partSolution.diameterOption(100).click()
        partSolution.orderQuantityField().should('exist')
        partSolution.partNumberTitle().should('contain.text', "BGFP10-100")
    })

    it('Navigate to - Thermal Insulation Ceramic Washers/Collars (DJB4-8-15)[UK]', { baseUrl: url_uk }, () => {
        partSolution.automationComponentOption(2).click() //the number is the serial component we wanna reach
        partSolution.automationComponentOption(3).click()
        partSolution.subCategoryOptionLIVE(2).invoke('removeAttr', 'target').click()
        product.itemHeaderSelector().should('contain.text', partSolution.subComponentsOptionsCopyArrayLIVE[2])
        partSolution.diameterOption(8).click()
        partSolution.diameterOption(15).click()
        partSolution.orderQuantityField().should('exist')
        partSolution.partNumberTitle().should('contain.text', "DJB4-8-15")
    })

    it('Navigate to - Bushings for Locating Pins/Round Flanged (JBT6-8)[UK]', { baseUrl: url_uk }, () => {
        partSolution.automationComponentOption(4).click() //the number is the serial component we wanna reach
        partSolution.automationComponentOption(5).click()
        partSolution.subCategoryOptionLIVE(3).invoke('removeAttr', 'target').click()
        product.itemHeaderSelector().should('contain.text', partSolution.subComponentsOptionsCopyArrayLIVE[3])
        partSolution.diameterOption(10).click()
        cy.wait(1500)
        cy.get('.m-chL').find(`[data-sitelog-value="8"]`).last().click()
        partSolution.diameterOption("JBT").click()
        partSolution.orderQuantityField().should('exist')
        partSolution.partNumberTitle().should('contain.text', "JBT6-8")
    })

    it('Navigate to -  Miniature Linear Guides/Wide Rails/Wide Block (SSEBWM16L-70)[UK]', { baseUrl: url_uk }, () => {
        partSolution.automationComponentOption(6).click() //the number is the serial component we wanna reach
        partSolution.automationComponentOption(7).click()
        partSolution.subCategoryOptionLIVE(4).invoke('removeAttr', 'target').click()
        product.itemHeaderSelector().should('contain.text', partSolution.subComponentsOptionsCopyArrayLIVE[4])
        partSolution.defineAComplexPartNumber(42,16,70,"Heat Resistant Grease (L Type)", "SSEBWM")
        partSolution.orderQuantityField().should('exist')
        partSolution.partNumberTitle().should('contain.text', "SSEBWM16L-70")
    })

    it('Navigate to -  Shaft support, category page (STHWIR8)[UK]', { baseUrl: url_uk }, () => {
        partSolution.automationComponentOption(8).click() //the number is the serial component we wanna reach
        partSolution.subCategoryOptionLIVE(5).invoke('removeAttr', 'target').click()
        product.itemHeaderSelector().should('contain.text', partSolution.subComponentsOptionsCopyArrayLIVE[5])
        partSolution.diameterOption(8).click()
        partSolution.diameterOption("STHWIR").click()
        partSolution.orderQuantityField().should('exist')
        partSolution.partNumberTitle().should('contain.text', "STHWIR8")
    })

})