import Login from '../pom/login';
import Product from '../pom/products';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const products = new Product()
const {
    url_uk,
} = authUser;

describe('Automation Components [UK] - Mobile', () => {
    beforeEach(() => {
        cy.viewport('iphone-x')
        cy.visit('/')
        login.mobileSignIn()
    })

    it('Linear Shafts [UK]', { baseUrl: url_uk }, () => {
        products.chooseComponentOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Linear Shafts").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Linear shafts / straight / machining selectable")
    })

    it('Shaft Supports [UK]', { baseUrl: url_uk }, () => {
        products.chooseComponentOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Shaft Supports").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Shaft holders / T-shape / slotted / cast iron")
    })

    it('Linear Shaft Support Flanged Mount/Thick Sleeve [UK]', { baseUrl: url_uk }, () => {
        products.chooseComponentOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Shaft Supports").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Shaft holders / guided round flange, round flange flattened on both sides / slotted")
    })

    it('Linear Bushings/Standard/Single [UK]', { baseUrl: url_uk }, () => {
        products.chooseComponentOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Linear Bushings").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Linear ball bearings / square flange / steel / LMK")
    })

    it('Ball Guides - Miniature Ball Bearing Guide Sets [UK]', { baseUrl: url_uk }, () => {
        products.chooseComponentOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Ball Guides").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Miniature Ball Bearing Guide Sets")
    })

    it('Ball Guides - Miniature Ball Bearing Guide Components/Ball Slider [UK]', { baseUrl: url_uk }, () => {
        products.chooseComponentOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Ball Guides").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Miniature Ball Bearing Guide Components / Ball Slider")
    })

    it('Ball Guides - Shafts for Miniature Ball Bearing Guides/Both Ends Machined/Both Ends Tapped [UK]', { baseUrl: url_uk }, () => {
        products.chooseComponentOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Ball Guides").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Miniature Ball Bearing Guide Sets / Both Ends Tapped")
    })

    it('Ball Guides - Shafts for Miniature Ball Bearing Guide/with Retaining Ring Grooves [UK]', { baseUrl: url_uk }, () => {
        products.chooseComponentOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Ball Guides").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Shafts for Miniature Ball Bearing Guide / with Retaining Ring Grooves")
    })

    it('Ball Screw - Rolled Ball Screws/Shaft Ends Configurable/Standard Nuts [UK]', { baseUrl: url_uk }, () => {
        products.chooseComponentOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Ball Screws").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Ball screws / standard flange / diameter 15 / pitch 5, 10, 20 / C5")
    })

    it('Ball Screw - Shaft Dia. 6, 8/Lead 2/Leade 1 or 2 [UK]', { baseUrl: url_uk }, () => {
        products.chooseComponentOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Ball Screws").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Ball screws / compact flange / diameter 8 - 32 / pitch 2 - 32 / C10 / configurable / steel / LTBC / 58-62HRC")
    })

    it('Ball Screw - Thread Diameter 10/Lead 2/4 or 10 [UK]', { baseUrl: url_uk }, () => {
        products.chooseComponentOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Ball Screws").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Ball screws / square flange, compact flange / diameter 12 / pitch 2, 4, 5, 10 / C3, C5, C7 / preloaded, 5µ, 30µ / steel / 58-62HRC")
    })

    it('Ball Screw - Compact Nuts/Shaft Dia. 8/Lead 2 [UK]', { baseUrl: url_uk }, () => {
        products.chooseComponentOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Ball Screws").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Ball screw nut mountings / pitch / steel, aluminium / coating selectable")
    })

    it('Ball Screw - Compact Nut/Shaft Dia. 15/Lead 5/10 [UK]', { baseUrl: url_uk }, () => {
        products.chooseLinearMotionOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Ball Screws").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Ball screws / standard flange / diameter 15 / pitch 5, 10, 20 / C5")
    })

    it('Set Collars / Clamping Collars [UK]', { baseUrl: url_uk }, () => {
        products.chooseLinearMotionOptionFn("Automation Components", "Linear Motion")
        products.chooseShaftCollardPart("Set Collars / Clamping Collars")
    })

    it('Oil Free Bushings, Washers [UK]', { baseUrl: url_uk }, () => {
        products.chooseLinearMotionOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Oil Free Bushings, Washers").click()
        products.bearingBoxOptionsSelector("All Oil Free Bushings, Washers").click()
        products.itemHeaderSelector().should('contain.text', "Oil Free Bushings, Washers")
    })

    it('Cable Carriers [UK]', { baseUrl: url_uk }, () => {
        products.chooseLinearMotionOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Cable Carriers").click()
        products.bearingBoxOptionsSelector("All Cable Carriers").click()
        products.itemHeaderSelector().should('contain.text', "Cable Carriers")
    })

    it('Lead Screws, Slide Screws [UK]', { baseUrl: url_uk }, () => {
        products.chooseLinearMotionOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Lead Screws, Slide Screws").click()
        products.bearingBoxOptionsSelector("All Lead Screws, Slide Screws").click()
        products.itemHeaderSelector().should('contain.text', "Lead Screws, Slide Screws")
    })

    it('Actuators [UK]', { baseUrl: url_uk }, () => {
        products.chooseLinearMotionOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Actuators").click()
        products.bearingBoxOptionsSelector("All Actuators").click()
        products.itemHeaderSelector().should('contain.text', "Actuators")
    })

    it('Single Axis Robots [UK]', { baseUrl: url_uk }, () => {
        products.chooseLinearMotionOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Actuators").click()
        products.bearingBoxOptionsSelector("Single Axis Robots").click()
        products.itemHeaderSelector().should('contain.text', "Single Axis Robots")
    })

    it('Single Axis Actuators [UK]', { baseUrl: url_uk }, () => {
        products.chooseLinearMotionOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Actuators").click()
        products.bearingBoxOptionsSelector("Single Axis Actuators").click()
        products.itemHeaderSelector().should('contain.text', "Single Axis Actuators")
    })

    it('Bellows [UK]', { baseUrl: url_uk }, () => {
        products.chooseLinearMotionOptionFn("Automation Components", "Linear Motion")
        products.chooseShaftCollardPart("Bellows")
    })

    it('All Bearings [UK]', { baseUrl: url_uk }, () => {
        products.chooseRotaryMotionFn("Automation Components")
        products.motionOptionsSelectors("Bearings").invoke('removeAttr', 'target').click()
        products.bearingBoxOptionsSelector("All Bearings").click()
        products.itemHeaderSelector().should('contain.text', "Bearings")
    })

    it('Ball Bearings [UK]', { baseUrl: url_uk }, () => {
        products.chooseRotaryMotionFn("Automation Components")
        products.motionOptionsSelectors("Bearings").invoke('removeAttr', 'target').click()
        products.chooseBearingOption("Ball Bearings")
    })

    it('Roller Bearings [UK]', { baseUrl: url_uk }, () => {
        products.chooseRotaryMotionFn("Automation Components")
        products.motionOptionsSelectors("Bearings").invoke('removeAttr', 'target').click()
        products.chooseBearingOption("Roller Bearings")
    })

    it('All Cross Roller [UK]', { baseUrl: url_uk }, () => {
        products.chooseLinearMotionOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Cross Roller").invoke('removeAttr', 'target').click()
        products.bearingBoxOptionsSelector("All Cross Roller").click()
        products.itemHeaderSelector().should('contain.text', "Cross Roller")
    })

    it('All Bearing Units [UK]', { baseUrl: url_uk }, () => {
        products.chooseRotaryMotionFn("Automation Components")
        products.motionOptionsSelectors("Bearing Units").invoke('removeAttr', 'target').click()
        products.bearingBoxOptionsSelector("All Bearing Units").click()
        products.itemHeaderSelector().should('contain.text', "Bearing Units")
    })

    it('Pillow Type [UK]', { baseUrl: url_uk }, () => {
        products.chooseRotaryMotionFn("Automation Components")
        products.motionOptionsSelectors("Bearing Units").invoke('removeAttr', 'target').click()
        products.chooseBearingOption("Pillow Type")
    })

    it('Flange Type [UK]', { baseUrl: url_uk }, () => {
        products.chooseRotaryMotionFn("Automation Components")
        products.motionOptionsSelectors("Bearing Units").invoke('removeAttr', 'target').click()
        products.chooseBearingOption("Flange Type")
    })

    it('Other Shapes [UK]', { baseUrl: url_uk }, () => {
        products.chooseRotaryMotionFn("Automation Components")
        products.motionOptionsSelectors("Bearing Units").invoke('removeAttr', 'target').click()
        products.chooseBearingOption("Other Shapes")
    })

    it('Bearings with Housing [UK]', { baseUrl: url_uk }, () => {
        products.chooseRotaryMotionFn("Automation Components")
        products.chooseMotionOptionWithoutSubmotions("Bearings with Housing")
    })

    it('Cam Followers, Roller Followers [UK]', { baseUrl: url_uk }, () => {
        products.chooseRotaryMotionFn("Automation Components")
        products.chooseMotionOptionWithoutSubmotions("Cam Followers, Roller Followers")
    })

    it('Rotary Shafts, Drive Shafts [UK]', { baseUrl: url_uk }, () => {
        products.chooseRotaryMotionFn("Automation Components")
        products.motionOptionsSelectors("Rotary Shafts, Drive Shafts").invoke('removeAttr', 'target').click()
        products.bearingBoxOptionsSelector("All Rotary Shafts, Drive Shafts").click()
        products.itemHeaderSelector().should('contain.text', "Rotary Shafts, Drive Shafts")
    })

    it('Cantilever Shafts [UK]', { baseUrl: url_uk }, () => {
        products.chooseRotaryMotionFn("Automation Components")
        products.chooseMotionOptionWithoutSubmotions("Cantilever Shafts")
    })

    it('Fulcrum Pins [UK]', { baseUrl: url_uk }, () => {
        products.chooseComponentOptionConnectingPartsFn("Automation Components", "Connecting Parts")
        products.chooseConnectingPart(2, "Fulcrum Pins")
    })

    it('Hinge Pins [UK]', { baseUrl: url_uk }, () => {
        products.chooseComponentOptionConnectingPartsFn("Automation Components", "Connecting Parts")
        products.chooseConnectingPart(3, "Hinge Pins")
    })

    it('Hinge Bases, Hinge Plates, Hinge Bolt [UK]', { baseUrl: url_uk }, () => {
        products.chooseComponentOptionConnectingPartsFn("Automation Components", "Connecting Parts")
        products.chooseConnectingPart(4, "Hinge Bases, Hinge Plates, Hinge Bolt")
    })

    it('Links [UK]', { baseUrl: url_uk }, () => {
        products.chooseComponentOptionConnectingPartsFn("Automation Components", "Connecting Parts")
        products.chooseConnectingPart(5, "Links")
    })

    it('Rod End Bearings, Spherical Bearings [UK]', { baseUrl: url_uk }, () => {
        products.chooseComponentOptionConnectingPartsFn("Automation Components", "Connecting Parts")
        products.chooseConnectingPart(6, "Rod End Bearings, Spherical Bearings")
    })

    it('Connecting Bars, Link Cables/Wires [UK]', { baseUrl: url_uk }, () => {
        products.chooseComponentOptionConnectingPartsFn("Automation Components", "Connecting Parts")
        products.chooseConnectingPart(7, "Connecting Bars, Link Cables/Wires")
    })

})