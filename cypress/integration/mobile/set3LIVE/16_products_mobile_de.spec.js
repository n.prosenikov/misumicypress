import Login from '../pom/login';
import Product from '../pom/products';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const products = new Product()
const {
    url_de,
} = authUser;

describe('Automation Components [DE] - Mobile', () => {
    beforeEach(() => {
        cy.viewport('iphone-x')
        cy.visit('/')
        login.mobileSignIn()
    })

    it('Linear Shafts [DE]', { baseUrl: url_de }, () => {
        products.chooseComponentOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Linearwellen").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Linearwellen / hohl")
    })

    it('Shaft Supports [DE]', { baseUrl: url_de }, () => {
        products.chooseComponentOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Wellenhalter").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Wellenhalter / Blockform / zweiteilig")
    })

    it('Shaft Collars [DE]', { baseUrl: url_de }, () => {
        products.chooseComponentOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Stellringe / Klemmringe").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Stellringe")
    })

    it('Linear Bushings/Standard/Single [DE]', { baseUrl: url_de }, () => {
        products.chooseComponentOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Linearkugellager").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Linearkugellager / breite Blockform / Aluminium / eloxiert")
    })

    it('Ball Guides - Miniature Ball Bearing Guide Sets [DE]', { baseUrl: url_de }, () => {
        products.chooseComponentOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Kugelführungen").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Miniatur-Kugelkäfigführungen")
    })

    it('Ball Guides - Rolled Ball Screws/Shaft Ends Configurable/Standard Nuts [DE]', { baseUrl: url_de }, () => {
        products.chooseComponentOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Kugelführungen").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Komponenten für Miniatur-Kugelkäfigführungen / Kugelkäfig")
    })

    it('Ball Guides - Shafts for Miniature Ball Bearing Guides/Both Ends Machined/Both Ends Tapped [DE]', { baseUrl: url_de }, () => {
        products.chooseComponentOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Kugelführungen").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Führungswelle mit Innengewinde beidseitig")
    })

    it('Ball Guides - Shafts for Miniature Ball Bearing Guide/with Retaining Ring Grooves [DE]', { baseUrl: url_de }, () => {
        products.chooseComponentOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Kugelführungen").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Wellen für Miniatur-Kugelkäfigführungen / Mit Sicherungsringnuten")
    })

    it('Ball Screw - Rolled Ball Screws/Dia 25 [DE]', { baseUrl: url_de }, () => {
        products.chooseComponentOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Kugelgewindetriebe").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Kugelgewindetriebe / Normflansch / Durchmesser 25 / Steigung 5, 10, 25 / C5")
    })

    it('Ball Screw - C3, C5, C7 [DE]', { baseUrl: url_de }, () => {
        products.chooseComponentOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Kugelgewindetriebe").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Kugelgewindetriebe / Kompaktflansch / Durchmesser 4 - 14 / Steigung 1 - 5 / C3, C5, C7 / MDK")
    })

    it('Ball Screw - Aluminium Part [DE]', { baseUrl: url_de }, () => {
        products.chooseComponentOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Kugelgewindetriebe").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Kugelgewindetrieb-Mutterhalterungen / Steigung / Stahl, Aluminium / Beschichtung wählbar")
    })

    it.only('Ball Screw - Compact Nut/Shaft Dia. 15/Lead 5/10 [DE]', { baseUrl: url_de }, () => {
        products.chooseLinearMotionOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Kugelgewindetriebe").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Kugelgewindetriebe / Kompaktflansch / Durchmesser 8 - 32 / Steigung 2 - 32 / C10 / konfigurierbar / Stahl / LTBC / 58-62HRC")
    })

    it('Shaft Collars [DE]', { baseUrl: url_de }, () => {
        products.chooseLinearMotionOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.chooseShaftCollardPart("Stellringe")
    })

    it('Oil Free Bushings, Washers [DE]', { baseUrl: url_de }, () => {
        products.chooseLinearMotionOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Gleitlager").click()
        products.bearingBoxOptionsSelector("Alle Gleitlager").click()
        products.itemHeaderSelector().should('contain.text', "Gleitlager")
    })

    it('Cable Carriers [DE]', { baseUrl: url_de }, () => {
        products.chooseLinearMotionOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Energieketten").click()
        products.bearingBoxOptionsSelector("Alle Energieketten").click()
        products.itemHeaderSelector().should('contain.text', "Energieketten")
    })

    it('Lead Screws, Slide Screws [DE]', { baseUrl: url_de }, () => {
        products.chooseLinearMotionOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Trapezgewindetriebe / Gleitgewindetriebe").click()
        products.bearingBoxOptionsSelector("Alle Trapezgewindetriebe / Gleitgewindetriebe").click()
        products.itemHeaderSelector().should('contain.text', "Trapezgewindetriebe / Gleitgewindetriebe")
    })

    it('Actuators [DE]', { baseUrl: url_de }, () => {
        products.chooseLinearMotionOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Aktuatoren").click()
        products.bearingBoxOptionsSelector("Alle Aktuatoren").click()
        products.itemHeaderSelector().should('contain.text', "Aktuatoren")
    })

    it('Single Axis Robots [DE]', { baseUrl: url_de }, () => {
        products.chooseLinearMotionOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Aktuatoren").click()
        products.bearingBoxOptionsSelector("Einspindelroboter").click()
        products.itemHeaderSelector().should('contain.text', "Einspindelroboter")
    })

    it('Single Axis Actuators [DE]', { baseUrl: url_de }, () => {
        products.chooseLinearMotionOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Aktuatoren").click()
        products.bearingBoxOptionsSelector("Linearmodule, kompakt").click()
        products.itemHeaderSelector().should('contain.text', "Linearmodule, kompakt")
    })

    it('All Bearings [DE]', { baseUrl: url_de }, () => {
        products.chooseRotaryMotionFn("Mechanische Komponenten")
        products.italianExpandBearingSelector().invoke('removeAttr', 'target').click()
        products.bearingBoxOptionsSelector("Alle Wälzlager").click()
        products.itemHeaderSelector().should('contain.text', "Wälzlager")
    })

    it('Ball Bearings [DE]', { baseUrl: url_de }, () => {
        products.chooseRotaryMotionFn("Mechanische Komponenten")
        products.italianExpandBearingSelector().invoke('removeAttr', 'target').click()
        products.chooseBearingOption("Kugellager")
    })

    it('Roller Bearings [DE]', { baseUrl: url_de }, () => {
        products.chooseRotaryMotionFn("Mechanische Komponenten")
        products.italianExpandBearingSelector().invoke('removeAttr', 'target').click()
        products.chooseBearingOption("Rollenlager")
    })

    it('All Achs for a machine [DE]', { baseUrl: url_de }, () => {
        products.chooseRotaryMotionFn("Mechanische Komponenten")
        products.motionOptionsSelectors("Achsen / Wellen für den Maschinenbau").invoke('removeAttr', 'target').click()
        products.bearingBoxOptionsSelector("Alle Achsen / Wellen für den Maschinenbau").click()
        products.itemHeaderSelector().should('contain.text', "Achsen / Wellen für den Maschinenbau")
    })

    it('All Bearing Units [DE]', { baseUrl: url_de }, () => {
        products.chooseRotaryMotionFn("Mechanische Komponenten")
        products.motionOptionsSelectors("Lagerzubehör").invoke('removeAttr', 'target').click()
        products.bearingBoxOptionsSelector("Alle Lagerzubehör").click()
        products.itemHeaderSelector().should('contain.text', "Lagerzubehör")
    })

    it('Adapter Type [DE]', { baseUrl: url_de }, () => {
        products.chooseRotaryMotionFn("Mechanische Komponenten")
        products.motionOptionsSelectors("Lagerzubehör").invoke('removeAttr', 'target').click()
        products.chooseBearingOption("Lageradapter")
    })

    it('Mother Type [DE]', { baseUrl: url_de }, () => {
        products.chooseRotaryMotionFn("Mechanische Komponenten")
        products.motionOptionsSelectors("Lagerzubehör").invoke('removeAttr', 'target').click()
        products.chooseBearingOption("Wellenmuttern / Nutmuttern")
    })

    it('Lager - Parts [DE]', { baseUrl: url_de }, () => {
        products.chooseRotaryMotionFn("Mechanische Komponenten")
        products.motionOptionsSelectors("Lagerzubehör").invoke('removeAttr', 'target').click()
        products.chooseBearingOption("Lager-Bauteile")
    })

    it('Bearings with Housing [DE]', { baseUrl: url_de }, () => {
        products.chooseRotaryMotionFn("Mechanische Komponenten")
        products.chooseMotionOptionWithoutSubmotions("Lager mit Gehäuse")
    })

    it('Cam Followers, Roller Followers [DE]', { baseUrl: url_de }, () => {
        products.chooseRotaryMotionFn("Mechanische Komponenten")
        products.chooseMotionOptionWithoutSubmotions("Kurvenrollen / Kurvenrollen mit Achsbolzen / Stützrollen")
    })

    it('Rotary Shafts, Drive Shafts [DE]', { baseUrl: url_de }, () => {
        products.chooseRotaryMotionFn("Mechanische Komponenten")
        products.motionOptionsSelectors("Achsen / Wellen für den Maschinenbau").invoke('removeAttr', 'target').click()
        products.bearingBoxOptionsSelector("Alle Achsen / Wellen für den Maschinenbau").click()
        products.itemHeaderSelector().should('contain.text', "Achsen / Wellen für den Maschinenbau")
    })

    it('Cantilever Shafts [DE]', { baseUrl: url_de }, () => {
        products.chooseRotaryMotionFn("Mechanische Komponenten")
        products.chooseMotionOptionWithoutSubmotions("Achsbolzen")
    })

    it('Fulcrum Pins [DE]', { baseUrl: url_de }, () => {
        products.chooseComponentOptionConnectingPartsFn("Mechanische Komponenten", "Verbindungsteile")
        products.chooseConnectingPart(2, "Schrauben mit Ansatz")
    })

    it('Hinge Pins [DE]', { baseUrl: url_de }, () => {
        products.chooseComponentOptionConnectingPartsFn("Mechanische Komponenten", "Verbindungsteile")
        products.chooseConnectingPart(3, "Scharnierbolzen")
    })

    it('Hinge Bases, Hinge Plates, Hinge Bolt [DE]', { baseUrl: url_de }, () => {
        products.chooseComponentOptionConnectingPartsFn("Mechanische Komponenten", "Verbindungsteile")
        products.chooseConnectingPart(4, "Scharnierlagerböcke / Scharnierplatten / Scharnierbolzen")
    })

    it('Links [DE]', { baseUrl: url_de }, () => {
        products.chooseComponentOptionConnectingPartsFn("Mechanische Komponenten", "Verbindungsteile")
        products.chooseConnectingPart(5, "Gelenkarme")
    })

    it('Rod End Bearings, Spherical Bearings [DE]', { baseUrl: url_de }, () => {
        products.chooseComponentOptionConnectingPartsFn("Mechanische Komponenten", "Verbindungsteile")
        products.chooseConnectingPart(6, "Gelenkköpfe / Gelenklager")
    })

    it('Connecting Bars, Link Cables/Wires [DE]', { baseUrl: url_de }, () => {
        products.chooseComponentOptionConnectingPartsFn("Mechanische Komponenten", "Verbindungsteile")
        products.chooseConnectingPart(7, "Verbindungsstäbe, Anschlusskabel / Drähte")
    })

})