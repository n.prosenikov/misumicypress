import Login from '../pom/login';
import Product from '../pom/products';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const products = new Product()
const {
    url_fr,
} = authUser;

describe('Automation Components [FR] - Mobile', () => {
    beforeEach(() => {
        cy.viewport('iphone-x')
        cy.visit('/')
        login.mobileSignIn()
    })

    it('Linear Shafts [FR]', { baseUrl: url_fr }, () => {
        products.chooseComponentOptionFn("Composants mécaniques", "Mouvement linéaire")
        products.motionOptionsSelectors("Arbres linéaires").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Droit")
    })

    it('Shaft Supports [FR]', { baseUrl: url_fr }, () => {
        products.chooseComponentOptionFn("Composants mécaniques", "Mouvement linéaire")
        products.motionOptionsSelectors("Supports d'arbre").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Supports d'arbre - Type à montage à embase avec rainure de clavette")
    })

    it('Linear Shaft Support Flanged Mount/Thick Sleeve [FR]', { baseUrl: url_fr }, () => {
        products.chooseComponentOptionFn("Composants mécaniques", "Mouvement linéaire")
        products.motionOptionsSelectors("Supports d'arbre").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Supports d'arbre - Type à montage à embase avec fente")
    })

    it('Linear Bushings/Standard/Single [FR]', { baseUrl: url_fr }, () => {
        products.chooseComponentOptionFn("Composants mécaniques", "Mouvement linéaire")
        products.motionOptionsSelectors("Manchons linéaires").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Manchons linéaires / Standard / simple")
    })

    it('Ball Guides - Miniature Ball Bearing Guide Sets [FR]', { baseUrl: url_fr }, () => {
        products.chooseComponentOptionFn("Composants mécaniques", "Mouvement linéaire")
        products.motionOptionsSelectors("Guides à billes").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Ensembles miniatures de guidage à roulement à billes")
    })

    it('Ball Guides - Miniature Ball Bearing Guide Components/Ball Slider [FR]', { baseUrl: url_fr }, () => {
        products.chooseComponentOptionFn("Composants mécaniques", "Mouvement linéaire")
        products.motionOptionsSelectors("Guides à billes").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Composants pour guidage miniature à roulement à billes - Glissière à billes")
    })

    it('Ball Guides - Shafts for Miniature Ball Bearing Guides/Both Ends Machined/Both Ends Tapped [FR]', { baseUrl: url_fr }, () => {
        products.chooseComponentOptionFn("Composants mécaniques", "Mouvement linéaire")
        products.motionOptionsSelectors("Guides à billes").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Arbre pour guidage miniature à roulement à billes, 1 extrémité usinée / 1 filetée, creux")
    })

    it('Ball Guides - Shafts for Miniature Ball Bearing Guide/with Retaining Ring Grooves [FR]', { baseUrl: url_fr }, () => {
        products.chooseComponentOptionFn("Composants mécaniques", "Mouvement linéaire")
        products.motionOptionsSelectors("Guides à billes").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Arbres pour guidage miniature à roulement à billes- Avec rainures pour bagues de retenue")
    })

    it('Ball Screw - Rolled Ball Screws/Compact Nut/Shaft Dia. 20/Lead 5/10 [FR]', { baseUrl: url_fr }, () => {
        products.chooseComponentOptionFn("Composants mécaniques", "Mouvement linéaire")
        products.motionOptionsSelectors("Vis à billes").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Vis à billes roulées / Écrous compacts / dia. 20 au pas de 5 ou 10")
    })

    it('Ball Screw - Thread Diameter 10/Lead 2/4 or 10 [FR]', { baseUrl: url_fr }, () => {
        products.chooseComponentOptionFn("Composants mécaniques", "Mouvement linéaire")
        products.motionOptionsSelectors("Vis à billes").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Vis à bille de précision - Diam. de filetage 10 - Pas 2, 4 ou 10")
    })

    it('Ball Screw - Compact Nuts/Shaft Dia. 8/Lead 2 [FR]', { baseUrl: url_fr }, () => {
        products.chooseComponentOptionFn("Composants mécaniques", "Mouvement linéaire")
        products.motionOptionsSelectors("Vis à billes").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Vis à billes")
    })

    it('Ball Screw - Compact Nut/Shaft Dia. 10/Lead 2/4/10 [FR]', { baseUrl: url_fr }, () => {
        products.chooseLinearMotionOptionFn("Composants mécaniques", "Mouvement linéaire")
        products.motionOptionsSelectors("Vis à billes").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Vis à bille de précision - Diam. de filetage 10 - Pas 2, 4 ou 10")
    })

    it('Shaft Collars [FR]', { baseUrl: url_fr }, () => {
        products.chooseLinearMotionOptionFn("Composants mécaniques", "Mouvement linéaire")
        products.chooseShaftCollardPart("Supports d'arbre")
    })

    it('Oil Free Bushings, Washers [FR]', { baseUrl: url_fr }, () => {
        products.chooseLinearMotionOptionFn("Composants mécaniques", "Mouvement linéaire")
        products.motionOptionsSelectors("Rondelles et manchons sans lubrification").click()
        products.bearingBoxOptionsSelector("Tout Rondelles et manchons sans lubrification").click()
        products.itemHeaderSelector().should('contain.text', "Rondelles et manchons sans lubrification")
    })

    it('Cable Carriers [FR]', { baseUrl: url_fr }, () => {
        products.chooseLinearMotionOptionFn("Composants mécaniques", "Mouvement linéaire")
        products.motionOptionsSelectors("Porte-câbles").click()
        products.bearingBoxOptionsSelector("Tout Porte-câbles").click()
        products.itemHeaderSelector().should('contain.text', "Porte-câbles")
    })

    it('Lead Screws, Slide Screws [FR]', { baseUrl: url_fr }, () => {
        products.chooseLinearMotionOptionFn("Composants mécaniques", "Mouvement linéaire")
        products.motionOptionsSelectors("Vis-mères / vis de glissière").click()
        products.bearingBoxOptionsSelector("Tout Vis-mères / vis de glissière").click()
        products.itemHeaderSelector().should('contain.text', "Vis-mères / vis de glissière")
    })

    it('Actuators [FR]', { baseUrl: url_fr }, () => {
        products.chooseLinearMotionOptionFn("Composants mécaniques", "Mouvement linéaire")
        products.motionOptionsSelectors("Actionneurs").click()
        products.bearingBoxOptionsSelector("Tout Actionneurs").click()
        products.itemHeaderSelector().should('contain.text', "Actionneurs")
    })

    it('Single Axis Robots [FR]', { baseUrl: url_fr }, () => {
        products.chooseLinearMotionOptionFn("Composants mécaniques", "Mouvement linéaire")
        products.motionOptionsSelectors("Actionneurs").click()
        products.bearingBoxOptionsSelector("Robots à axe simple").click()
        products.itemHeaderSelector().should('contain.text', "Robots à axe simple")
    })

    it('Single Axis Actuators [FR]', { baseUrl: url_fr }, () => {
        products.chooseLinearMotionOptionFn("Composants mécaniques", "Mouvement linéaire")
        products.motionOptionsSelectors("Actionneurs").click()
        products.bearingBoxOptionsSelector("Actionneurs à axe simple").click()
        products.itemHeaderSelector().should('contain.text', "Actionneurs à axe simple")
    })

    it('All Bearings [FR]', { baseUrl: url_fr }, () => {
        products.chooseRotaryMotionFn("Composants mécaniques")
        products.motionOptionsSelectors("Roulements").invoke('removeAttr', 'target').click()
        products.bearingBoxOptionsSelector("Tout Roulements").click()
        products.itemHeaderSelector().should('contain.text', "Roulements")
    })

    it('Ball Bearings [FR]', { baseUrl: url_fr }, () => {
        products.chooseRotaryMotionFn("Composants mécaniques")
        products.motionOptionsSelectors("Roulements").invoke('removeAttr', 'target').click()
        products.chooseBearingOption("Roulements à billes")
    })

    it('Roller Bearings [FR]', { baseUrl: url_fr }, () => {
        products.chooseRotaryMotionFn("Composants mécaniques")
        products.motionOptionsSelectors("Roulements").invoke('removeAttr', 'target').click()
        products.chooseBearingOption("Roulements à rouleaux")
    })

    it('All Hinge Pins [FR]', { baseUrl: url_fr }, () => {
        products.chooseRotaryMotionFn("Composants mécaniques")
        products.motionOptionsSelectors("Accessoires de roulements").invoke('removeAttr', 'target').click()
        products.bearingBoxOptionsSelector("Tout Accessoires de roulements").click()
        products.itemHeaderSelector().should('contain.text', "Accessoires de roulements")
    })

    it('All Bearing Units [FR]', { baseUrl: url_fr }, () => {
        products.chooseRotaryMotionFn("Composants mécaniques")
        products.motionOptionsSelectors("Ensemble roulements").invoke('removeAttr', 'target').click()
        products.bearingBoxOptionsSelector("Tout Ensemble roulements").click()
        products.itemHeaderSelector().should('contain.text', "Ensemble roulements")
    })

    it('Pillow Type [FR]', { baseUrl: url_fr }, () => {
        products.chooseRotaryMotionFn("Composants mécaniques")
        products.motionOptionsSelectors("Ensemble roulements").invoke('removeAttr', 'target').click()
        products.chooseBearingOption("Paliers lisses")
    })

    it('Flange Type [FR]', { baseUrl: url_fr }, () => {
        products.chooseRotaryMotionFn("Composants mécaniques")
        products.motionOptionsSelectors("Ensemble roulements").invoke('removeAttr', 'target').click()
        products.chooseBearingOption("Paliers à bride")
    })

    it('Other Shapes [FR]', { baseUrl: url_fr }, () => {
        products.chooseRotaryMotionFn("Composants mécaniques")
        products.motionOptionsSelectors("Ensemble roulements").invoke('removeAttr', 'target').click()
        products.chooseBearingOption("Autres formes")
    })

    it('Bearings with Housing [FR]', { baseUrl: url_fr }, () => {
        products.chooseRotaryMotionFn("Composants mécaniques")
        products.chooseMotionOptionWithoutSubmotions("Paliers")
    })

    it('Cam Followers, Roller Followers [FR]', { baseUrl: url_fr }, () => {
        products.chooseRotaryMotionFn("Composants mécaniques")
        products.chooseMotionOptionWithoutSubmotions("Poussoirs de cames, galets suiveurs")
    })

    it('Rotary Shafts, Drive Shafts [FR]', { baseUrl: url_fr }, () => {
        products.chooseRotaryMotionFn("Composants mécaniques")
        products.motionOptionsSelectors("Arbres rotatifs, arbres de commande").invoke('removeAttr', 'target').click()
        products.bearingBoxOptionsSelector("Tout Arbres rotatifs, arbres de commande").click()
        products.itemHeaderSelector().should('contain.text', "Arbres rotatifs, arbres de commande")
    })

    it('Cantilever Shafts [FR]', { baseUrl: url_fr }, () => {
        products.chooseRotaryMotionFn("Composants mécaniques")
        products.chooseMotionOptionWithoutSubmotions("Arbres en porte-à-faux")
    })

    it('Fulcrum Pins [FR]', { baseUrl: url_fr }, () => {
        products.chooseComponentOptionConnectingPartsFn("Composants mécaniques", "Pièces de connexion")
        products.chooseConnectingPart(2, "Vis épaulées")
    })

    it('Hinge Pins [FR]', { baseUrl: url_fr }, () => {
        products.chooseComponentOptionConnectingPartsFn("Composants mécaniques", "Pièces de connexion")
        products.chooseConnectingPart(3, "Axes d'articulation")
    })

    it('Hinge Bases, Hinge Plates, Hinge Bolt [FR]', { baseUrl: url_fr }, () => {
        products.chooseComponentOptionConnectingPartsFn("Composants mécaniques", "Pièces de connexion")
        products.chooseConnectingPart(4, "Bases de charnière / boulons de charnière")
    })

    it('Links [FR]', { baseUrl: url_fr }, () => {
        products.chooseComponentOptionConnectingPartsFn("Composants mécaniques", "Pièces de connexion")
        products.chooseConnectingPart(5, "Liaisons")
    })

    it('Rod End Bearings, Spherical Bearings [FR]', { baseUrl: url_fr }, () => {
        products.chooseComponentOptionConnectingPartsFn("Composants mécaniques", "Pièces de connexion")
        products.chooseConnectingPart(6, "Roulements d'extrémité de tige / roulements sphériques")
    })

    it('Connecting Bars, Link Cables/Wires [FR]', { baseUrl: url_fr }, () => {
        products.chooseComponentOptionConnectingPartsFn("Composants mécaniques", "Pièces de connexion")
        products.chooseConnectingPart(7, "Tiges d'accouplement / câbles/fils de liaison")
    })

})