export default class MyPage {

    pageTitle() {
        return cy.get('title')
    }

    pageTopic() {
        return cy.get('.topicPath')
    }


    couponCode() {
        return cy.get('.code');
    }    
    
    couponPrice() {
        return cy.get('.rate');
    }    

    couponDate() {
        return cy.get('.date');
    }

    couponType() {
        return cy.get('.type');
    }

    couponTitle() {
        return cy.get('.title');
    }

    couponStatus() {
        return cy.get('.status');
    }

    allBrandsButton(){
        return cy.get('[class="lc-h3 is-purchase-flag"]')
    }

    viewBrandListOption(){
        return cy.get('[href="/vona2/maker/"]').eq(0)
    }

    brandListHeader(){
        return cy.get('.m-h1')
    }

}