export default class SearchPartNumbers {
    //Selectors
    keywordInputField() {
        return cy.get("#keyword_input")
    }

    numberOfFoundedElements(number){
        return cy.get(`#keyword_productCode_${number}`)
    }

    productCodeSelector(){
        return cy.get("#ProductCode")
    }

    productTitleSelector(){
        return cy.get('.m-h1')
    }
    //Functions

    mobileSearch(partNumber, resultNumber) {
        this.keywordInputField().type(partNumber)
        this.numberOfFoundedElements(resultNumber).click()
        this.productCodeSelector().should('contain.html', `${partNumber}`)
    }
}