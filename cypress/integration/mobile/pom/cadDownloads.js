export default class CadDownload {

    pageTitle() {
        return cy.get('.titleBlock__h1')
    }
    
    cadDownloadHeader() {
        return cy.get('.caddatadlArea')
    }
    
}

