const authUser = require('../../../fixtures/auth-user.json');

export default class Login {
    signIn() {
        const { username, password } = authUser;
        this.acceptCookies().click()
        const loginMenuButton = cy.get('[data-login="loginButton"]');
        loginMenuButton.click()
        this.username().type(username)
        this.password().type(password)
        this.signInButton().click()
        return this
    }

    mobileSignIn() {
        const { username, password } = authUser;
        this.acceptCookies().click()
        cy.get('[data-chenge-mobile]').click()
        this.profileIconSelector().click()
        this.username().type(username)
        this.password().type(password)
        this.signInButton().click()
        cy.waitFor(this.loginBoxSelector().should('not.exist'))
        return this
    }

    acceptCookiesAndNavigate(){
        this.acceptCookies().click()
        cy.get('[data-chenge-mobile]').click()
    }

    acceptCookies() {
        return cy.get('.l-cookieNotification__accept');
    }

    username() {
        return cy.get('[data-login="userid"]')
    }    
    
    password() {
        return cy.get('[data-login="password"]')
    }

    signInButton(){
        return cy.get('[data-login="submit"]')
    }

    loginBoxSelector(){
        return cy.get('[data-login="baloonBox"]')
    }

    loggedUsernameSelector(){
        return cy.get('[data-header-bal="user"]').find('h2')
    }

    profileIconSelector(){
        return cy.get('[data-header-func="login"]')
    }

    logOutButtonSelector(){
        return cy.get('[data-login="logoutLink"]')
    }

}