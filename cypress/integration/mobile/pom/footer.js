const prodsUK = [
    'href*="/en/products/discounted-products"',
    'href*="https://online-live.flipaio.de"',
    'href*="/en/products"',
    'href*="https://misumi.freshdesk.com/en/support/home"',
];

const prodsDE = [
    'href*="/de/produkte/reduzierte-produkte"',
    'href*="https://online-live.flipaio.de"',
    'href*="/de/produkte"',
    'href*="https://misumi.freshdesk.com/en/support/home"',
];

const prodsFR = [
    'href*="/fr/produits/produits-a-prix-reduit"',
    'href*="https://online-live.flipaio.de"',
    'href*="/fr/produits"',
    'href*="https://misumi.freshdesk.com/en/support/home"',
];

const prodsIT = [
    'href*="/it/prodotti/prodotti-scontati"',
    'href*="https://online-live.flipaio.de"',
    'href*="/it/prodotti"',
    'href*="https://misumi.freshdesk.com/en/support/home"',
];

const brandsUK = [
    'href*="/vona2/maker/misumi"',
    'href*="/vona2/maker"',
    'href*="/en/brands/european-brands"',
    'href*="/en/brands/japanese-brands"',
];

const brandsDE = [
    'href*="/vona2/maker/misumi/"',
    'href*="/vona2/maker/"',
    'href*="/de/marken/europaeische-marken"',
    'href*="/de/marken/japanische-marken"',
];

const brandsFR = [
    'href*="/vona2/maker/misumi"',
    'href*="/vona2/maker"',
    'href*="/fr/marques/marques-europeennes"',
    'href*="/fr/marques/marques-japonaises"',
];

const brandsIT = [
    'href*="/vona2/maker/misumi/"',
    'href*="/vona2/maker/"',
    'href*="/it/marchi/marchi-europei"',
    'href*="/it/marchi/marche-giapponesi"',
];

const appsUK = [
    'href*="/en/applications/automotive-industry"',
    'href*="/en/applications/products-by-application/application-for-packaging-industry"',
    'href*="/en/applications/products-by-application/medical-and-pharma-industry"',
    'href*="/en/applications/products-by-application/3d-printer-parts"',
    'href*="/en/services/why-misumi"',
    'href*="https://meviy.misumi-ec.com/en_gb-gb/"',
    'href*="/eu/incadlibrary/"',
    'href*="/service/rd/icc/sw/pr/incadcomponents/"',
];

const appsDE = [
    'href*="/de/applikationen/automobilindustrie"',
    'href*="/de/applikationen/produkte-nach-anwendungsbereichen/verpackungsherstellung"',
    'href*="/de/applikationen/produkte-nach-anwendungsbereichen/medizin-und-pharmaindustrie"',
    'href*="/de/applikationen/produkte-nach-anwendungsbereichen/3d-drucker-teile"',
    'href*="/de/service/warum-misumi"',
    'href*="https://meviy.misumi-ec.com/de-de/"',
    'href*="/eu/incadlibrary/"',
    'href*="/service/rd/icc/sw/pr/incadcomponents/"',
];

const appsFR = [
    'href*="/fr/applications/industrie-automobile"',
    'href*="/fr/applications/produits-par-domaine-dapplication/fabrication-demballages"',
    'href*="/fr/applications/industrie-medicale-et-pharmaceutique"',
    'href*="/fr/applications/produits-par-domaine-dapplication/composant-imprimante-3D"',
    'href*="/fr/services/pourquoi-misumi"',
    'href*="https://meviy.misumi-ec.com/en_gb-gb/"',
    'href*="https://uk.misumi-ec.com/eu/incadlibrary"',
    'href*="https://uk.misumi-ec.com/service/rd/icc/sw/pr/incadcomponents/"',
];

const appsIT = [
    'href*="/it/applicazioni/industria-automibilistica"',
    'href*="/it/applicazioni/prodotti-per-settori-di-impiego/produzione-di-imballaggi"',
    'href*="/it/applicazioni/industria-medica-e-farmaceutica"',
    'href*="/it/applicazioni/prodotti-per-settori-di-impiego/componenti-stampanti-3d"',
    'href*="/it/servizi/perché-misumi"',
    'href*="/it/servizi/perché-misumi"',
    'href*="https://uk.misumi-ec.com/eu/incadlibrary"',
    'href*="https://uk.misumi-ec.com/service/rd/icc/sw/pr/incadcomponents/"',
];

const serviceAndContactUK = [
    'href*="/en/customer/register"',
    'href*="/en/services/contact"',
    'href*="/en/services/why-misumi"',
    'href*="/files/docs/supplier-self-declaration/en.pdf"',
    'href*="/en/services/faq-and-tutorials"',
    'href*="/en/info-hub"',
    'href*="/en/services/configurable-components-make-to-order"',
    'href*="/en/services/catalog-request"',
    'href*="/en/services/eprocurement"',
    'href*="/en/services/newsletter"',
];

const serviceAndContactDE = [
    'href*="/de/registrieren"',
    'href*="/de/service/kontakt"',
    'href*="/de/service/warum-misumi"',
    'href*="/files/docs/supplier-self-declaration/de.pdf"',
    'href*="/de/service/faq-und-tutorials"',
    'href*="/de/info-hub"',
    'href*="/de/service/konfigurierbare-komponenten-make-to-order"',
    'href*="/de/service/katalog-anfordern"',
    'href*="/de/service/eprocurement"',
    'href*="/de/service/newsletter"',
];

const serviceAndContactFR = [
    'href*="/fr/inscrivez"',
    'href*="/fr/service/nous-contacter"',
    'href*="/fr/services/pourquoi-misumi"',
    'href*="/files/docs/supplier-self-declaration/fr.pdf"',
    'href*="/fr/services/faq-et-tutoriels"',
    'href*="/fr/info-hub"',
    'href*="/fr/services/composants-configurables-make-to-order"',
    'href*="/fr/service/demande-de-catalogues"',
    'href*="/fr/service/eprocurement"',
    'href*="/fr/service/newsletter"',
];
const serviceAndContactIT = [
    'href*="/it/registro"',
    'href*="/it/servizi/contatti"',
    'href*="/it/servizi/perché-misumi"',
    'href*="/files/docs/supplier-self-declaration/it.pdf"',
    'href*="/it/servizi/faq-e-tutorial"',
    'href*="/it/info-hub"',
    'href*="/it/servizi/componenti-configurabili-make-to-order"',
    'href*="/it/servizi/richiedi-cataloghi"',
    'href*="/it/servizi/eprocurement"',
    'href*="/it/servizi/newsletter"',
];

const aboutUsUK = [
    'href*="https://www.misumi.co.jp/english"',
    'href*="/en/company/misumi-europe"',
    'href*="/en/company/career"',
    'href*="/en/company/responsibility"',
    'href*="/en/company/customer-references"',
];

const aboutUsDE = [
    'href*="https://www.misumi.co.jp/english/"',
    'href*="/de/unternehmen/misumi-europa"',
    'href*="/de/unternehmen/karriere"',
    'href*="/de/unternehmen/verantwortung"',
    'href*="/de/unternehmen/kundenreferenzen"',
];

const aboutUsFR = [
    'href*="https://www.misumi.co.jp/english"',
    'href*="/fr/societe/misumi-europe"',
    'href*="/fr/societe/carriere"',
    'href*="/fr/societe/responsabilite"',
    'href*="/fr/societe/references-clients"',
];
const aboutUsIT = [
    'href*="https://www.misumi.co.jp/english/"',
    'href*="/it/societa/misumi-europe"',
    'href*="/it/societa/lavora-con-noi"',
    'href*="/it/societa/responsabilita"',
    'href*="/it/societa/referenze-dei-clienti"',
];

const legalNoticeUK = [
    'href*="/en/legal-notice/imprint"',
    'href*="/en/legal-notice/terms-and-conditions"',
    'href*="/en/legal-notice/privacy-policy"',
    'href*="/en/legal-notice/cancellation-policy"',
];

const legalNoticeDE = [
    'href*="/de/rechtliche-hinweise/impressum"',
    'href*="/de/rechtliche-hinweise/agb"',
    'href*="/de/rechtliche-hinweise/datenschutz"',
    'href*="/de/rechtliche-hinweise/stornierungsbedingungen"',
];

const legalNoticeFR = [
    'href*="/fr/informations-legales/mentions-legales"',
    'href*="/fr/informations-legales/conditions-generales-de-vente-cgv"',
    'href*="/fr/informations-legales/protection-des-donnees-privees"',
    'href*="/fr/informations-legales/conditions-dannulation"',
];

const legalNoticeIT = [
    'href*="/it/informazioni-legali/imprint"',
    'href*="/it/informazioni-legali/condizioni-generali-di-vendita-cgv"',
    'href*="/it/informazioni-legali/informativa-sulla-privacy"',
    'href*="/it/informazioni-legali/condizioni-di-annullamento"',
];

const paymentMethodUK = [
    'alt*="Payment on account"',
    'alt*="Pre Payment"',
    'alt*="PayPal"'
];

const paymentMethodDE = [
    'alt*="Rechnung"',
    'alt*="Vorauskasse"',
    'alt*="PayPal"'
];

const paymentMethodFR = [
    'alt*="Facture"',
    'alt*="Prépaiement"',
    'alt*="PayPal"'
];

const paymentMethodIT = [
    'alt*="Fattura"',
    'alt*="Pagamento anticipato"',
    'alt*="PayPal"'
];

const socialMedia = [
    'class*="lc-facebook-square"',
    'class*="lc-twitter-square"',
    'class*="lc-youtube-square"',
    'class*="lc-xing-square"',
    'class*="lc-linkedin-square"'
]

const mobileVersionFooterUK = [
    'href*="/en/company/responsibility/quality-policy/"',
    'href*="/en/company/responsibility/environmental-policy/"',
    'href*="/worldwide"',
    'class*="lc-webMode"'
]

const mobileVersionFooterDE = [
    'href*="/de/unternehmen/verantwortung/qualitaetspolitik"',
    'href*="/de/unternehmen/verantwortung/umweltpolitik"',
    'href*="/worldwide"',
    'class*="lc-webMode"'
]

const mobileVersionFooterFR = [
    'href*="/fr/societe/responsabilite/politique-qualite/"',
    'href*="/fr/societe/responsabilite/politique-environnementale/"',
    'href*="/worldwide"',
    'class*="lc-webMode"'
]

const mobileVersionFooterIT = [
    'href*="/it/societa/responsabilita/politica-sulla-qualit%C3%A0"',
    'href*="/it/societa/responsabilita/politica-ambientale"',
    'href*="/worldwide/"',
    'class*="lc-webMode"'
]

const linkArrUK = [
    ...prodsUK,
    ...brandsUK,
    ...appsUK,
    ...serviceAndContactUK,
    ...aboutUsUK,
    ...legalNoticeUK,
    ...paymentMethodUK,
    ...socialMedia
];

const linkArrDE = [
    ...prodsDE,
    ...brandsDE,
    ...appsDE,
    ...serviceAndContactDE,
    ...aboutUsDE,
    ...legalNoticeDE,
    ...paymentMethodDE,
    ...socialMedia
];

const linkArrFR = [
    ...prodsFR,
    ...brandsFR,
    ...appsFR,
    ...serviceAndContactFR,
    ...aboutUsFR,
    ...legalNoticeFR,
    ...paymentMethodFR,
    ...socialMedia
];

const linkArrIT = [
    ...prodsIT,
    ...brandsIT,
    ...appsIT,
    ...serviceAndContactIT,
    ...aboutUsIT,
    ...legalNoticeIT,
    ...paymentMethodIT,
    ...socialMedia
];

export default class Footer {

    footerOptionSelector(link) {
        cy.get(`[${link}]`).should('exist')
    }

    checkFooterOptionsExistUK() {
        linkArrUK.forEach(el => {
            this.footerOptionSelector(el)
        })
    }

    checkFooterOptionsExistDE() {
        linkArrDE.forEach(el => {
            this.footerOptionSelector(el)
        })
    }

    checkFooterOptionsExistFR() {
        linkArrFR.forEach(el => {
            this.footerOptionSelector(el)
        })
    }

    checkFooterOptionsExistIT() {
        linkArrIT.forEach(el => {
            this.footerOptionSelector(el)
        })
    }

    checkFooterOptionMobileVersionUK() {
        mobileVersionFooterUK.forEach(el => {
            this.footerOptionSelector(el)
        })
    }

    checkFooterOptionMobileVersionDE() {
        mobileVersionFooterDE.forEach(el => {
            this.footerOptionSelector(el)
        })
    }

    checkFooterOptionMobileVersionFR() {
        mobileVersionFooterFR.forEach(el => {
            this.footerOptionSelector(el)
        })
    }

    checkFooterOptionMobileVersionIT() {
        mobileVersionFooterIT.forEach(el => {
            this.footerOptionSelector(el)
        })
    }


}