export default class HeaderOptions {
    //Selectors

    leftOptionButton() {
        return cy.get('[data-header-func="wos"]')
    }

    manageOrderHistoryOption(){
        return cy.get('[href*="commandCode=TEMP_AUTH_SO_HISTORY"]')
    }

    manageQuoteHistoryOption(){
        return cy.get('[href*="commandCode=TEMP_AUTH_QT_HISTORY"]')
    }

    manageQuoteHistoryItalianVersion(){
        return cy.get('[data-user="headermenu"]').find('li').eq(2)
    }

    orderHistoryHeadLine(){
        return cy.get('#historySO')
    }

    quoteHistoryHeadLine(){
        return cy.get('[id="historyQT"]')
    }


    //Functions
}