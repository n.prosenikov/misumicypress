export default class Product {

    //Selectors
    categoryOptionsSelectors(option) {
        return cy.get('h4').contains(option)
    }

    automationComponentsSelectors(component) {
        return cy.get('a').contains(component)
    }

    linearMotionSelector(linearMotion){
        return cy.get('.lc-level3').find('li').contains(linearMotion)
    }

    motionOptionsSelectors(motion) {
        return cy.get('.lc-level4').find('.lc-title').contains(motion)
    }

    italianExpandBearingSelector(){
        return cy.get(':nth-child(3) > .lc-level4 > :nth-child(2) > .is-accordion')
    }

    expandBearingSelector(){
        return cy.get(':nth-child(2) > .lc-level4 > :nth-child(2) > .is-accordion')
    }

    connectingPartsOptionSelector(optionOrder){
        return cy.get(`:nth-child(4) > .lc-level4 > :nth-child(${optionOrder}) > a`)
    }

    connectingPartsOptionSelectorSTG(optionOrder){
        return cy.get(`:nth-child(3) > .lc-level4 > :nth-child(${optionOrder}) > a`)
    }

    italianConnectingPartsOptionSelector(optionOrder){
        return cy.get(`:nth-child(4) > .lc-level4 > :nth-child(${optionOrder}) > a`)
    }

    collarShaftSelector(option){
        return cy.get(':nth-child(1) > .lc-level4').find('span').contains(option)
    }

    // shaftCollarsSelector(){
    //     //return cy.get(':nth-child(1) > .lc-level4 > :nth-child(4) > a')
    //     return cy.get(':nth-child(1) > .lc-level4').find('span').contains("Stellringe")
    // }

    subMotionOptionsSelector(subOption) {
        return cy.get('[data-category="btn_accordion"]').contains(subOption)
    }

    itemOptionsSelectors(item) {
        return cy.get('.mc-name').contains(item)
    }

    itemHeaderSelector() {
        return cy.get('.m-h1')
    }

    bearingBoxOptionsSelector(option) {
        return cy.get('.lc-level5').find('li').contains(option)
    }

    connectingPartSelector(option) {
        return cy.get('.lc-level4').find('li').contains(option)
    }


    //functions
    chooseLinearMotionOptionFn(optionFromCategoryList, componentFromList) {
        this.categoryOptionsSelectors(optionFromCategoryList).click()
        this.linearMotionSelector(componentFromList).click()
    }

    chooseComponentOptionFn(optionFromCategoryList, componentFromList) {
        this.categoryOptionsSelectors(optionFromCategoryList).click()
        cy.get('.lc-level3 > :nth-child(1) > .is-accordion').click()
        //this.automationComponentsSelectors(componentFromList).click()
    }

    chooseRotaryMotionFn(optionFromCategoryList) {
        this.categoryOptionsSelectors(optionFromCategoryList).click()
        cy.get('.lc-level3 > :nth-child(3) > .is-accordion').click()
        //this.automationComponentsSelectors(componentFromList).click()
    }

    chooseRotaryMotionSTGFn(optionFromCategoryList) {
        this.categoryOptionsSelectors(optionFromCategoryList).click()
        cy.get('.lc-level3 > :nth-child(2) > .is-accordion').click()
        //this.automationComponentsSelectors(componentFromList).click()
    }

    chooseComponentOptionConnectingPartsFn(optionFromCategoryList, componentFromList) {
        this.categoryOptionsSelectors(optionFromCategoryList).click()
        cy.get('.lc-level3 > :nth-child(4) > .is-accordion').click() //open Connection Parts
    }

    chooseItalianConnectingPart(option){
        this.categoryOptionsSelectors(option).click()
        cy.get('.lc-level3 > :nth-child(4) > .is-accordion').click()
    }

    clickOnAnItemFn(itemName) {
        this.itemOptionsSelectors(itemName).invoke('removeAttr', 'target').click({force:true})
        this.itemHeaderSelector().should('contain.text', itemName)
    }

    chooseBearingOption(option) {
        this.bearingBoxOptionsSelector(option).click()
        this.itemHeaderSelector().should('contain.text', option)
    }

    chooseMotionOptionWithoutSubmotions(motion) {
        this.motionOptionsSelectors(motion).invoke('removeAttr', 'target').click({ force: true })
        this.itemHeaderSelector().should('contain.text', motion)
    }

    chooseConnectingPart(order, option) {
        this.connectingPartsOptionSelector(order).click()
        this.itemHeaderSelector().should('contain.text', option)
    }

    chooseConnectingPartSTG(order, option) {
        this.connectingPartsOptionSelectorSTG(order).click()
        this.itemHeaderSelector().should('contain.text', option)
    }

    italianChooseConnectingPart(order, option) {
        this.italianConnectingPartsOptionSelector(order).click()
        this.itemHeaderSelector().should('contain.text', option)
    }

    chooseShaftCollardPart(option) {
        this.collarShaftSelector(option).click()
        this.itemHeaderSelector().should('contain.text', option)
    }


}