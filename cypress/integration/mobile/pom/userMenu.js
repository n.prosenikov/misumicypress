export default class UserMenu {
    userPanel() {
        return cy.get('[data-header-func="user"]');
    }

    myPageUserMenu() {
        return cy.get('[data-common="headerUserInfo"]')
    }

    myPageSideMenu() {
        return cy.get('[data-common-userlink="mypagetop"]')
    }

    myCadDownloads() {
        return cy.get('[href="/mypage/cad_history.html"]')
    }

    myComponents() {
        return cy.get('[data-common-userlink="partslist"]')
    }

    myComponentsHeadline(){
        return cy.get('.wording')
    }

    changeUserData() {
        return cy.get('[data-common-userlink="editprofile"]')
    }

    changePassword() {
        return cy.get('[data-common-userlink="passchange"]')
    }


}

