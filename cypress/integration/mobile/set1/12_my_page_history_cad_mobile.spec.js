import Login from '../pom/login';
import UserMenu from '../pom/userMenu';
import Dashboard from '../../cssp/pom/dashboard';
import CadDownload from '../pom/cadDownloads';


const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const dashboard = new Dashboard()
const cadDownload = new CadDownload()
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('CAD Download History Mobile', () => {

    beforeEach(() => {
        cy.viewport('iphone-x')
        cy.visit('/')
        login.mobileSignIn(username, password)
        cy.wait(2000)
    })

    it('CAD Download History UK', { baseUrl: url_uk }, () => {
        login.profileIconSelector().click()
        userMenu.myPageUserMenu().click()
        dashboard.chooseCADLinkFromArchive()
        cadDownload.pageTitle().should('contain.text', "History of CAD data download")
    })

    it('CAD Download History DE', { baseUrl: url_de }, () => {
        login.profileIconSelector().click()
        userMenu.myPageUserMenu().click()
        dashboard.chooseCADLinkFromArchive()
        cadDownload.pageTitle().should('contain.text', "CAD-Download Historie")
    })

    it('CAD Download History FR', { baseUrl: url_fr }, () => {
        login.profileIconSelector().click()
        userMenu.myPageUserMenu().click()
        dashboard.chooseCADLinkFromArchive()
        cadDownload.pageTitle().should('contain.text', "Historique des téléchargements CAO")
    })

    it('CAD Download History IT', { baseUrl: url_it }, () => {
        login.profileIconSelector().click()
        userMenu.myPageUserMenu().click()
        dashboard.chooseCADLinkFromArchive()
        cadDownload.pageTitle().should('contain.text', "Storico download dati CAD")
    })

});
