import Login from '../pom/login';
import SearchPartNumbers from '../pom/search';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const searchPN = new SearchPartNumbers()
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

const partNumber = "SFJ10-100"

describe('Part Number Suggestion - Mobile', () => {
    beforeEach(() => {
        cy.viewport('iphone-x')
        cy.visit('/')
        login.mobileSignIn(username, password)
    })

    it('Part Number Suggestion UK', { baseUrl: url_uk }, () => {
        searchPN.mobileSearch(partNumber, 0)
    })

    it('Part Number Suggestion DE', { baseUrl: url_de }, () => {
        searchPN.mobileSearch(partNumber, 0)
    })

    it('Part Number Suggestion FR', { baseUrl: url_fr }, () => {
        searchPN.mobileSearch(partNumber, 0)
    })

    it('Part Number Suggestion IT', { baseUrl: url_it }, () => {
        searchPN.mobileSearch(partNumber, 0)
    })

});
