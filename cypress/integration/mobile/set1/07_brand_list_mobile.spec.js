import Login from '../pom/login';
import MyPage from '../pom/myPage';


const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const myPage = new MyPage();
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('Brand List - Mobile', () => {
    beforeEach(() => {
        cy.viewport('iphone-x')
        cy.visit('/')
        login.mobileSignIn(username, password)
    })

    it('Brand List UK', { baseUrl: url_uk }, () => {
        myPage.allBrandsButton().click()
        myPage.viewBrandListOption().click()
        myPage.brandListHeader().should('contain.text', "Brand list")
    })

    it('Brand List DE', { baseUrl: url_de }, () => {
        myPage.allBrandsButton().click()
        myPage.viewBrandListOption().click()
        myPage.brandListHeader().should('contain.text', "Alle Marken")
    })

    it('Brand List FR', { baseUrl: url_fr }, () => {
       myPage.allBrandsButton().click()
       myPage.viewBrandListOption().click()
       myPage.brandListHeader().should('contain.text', "Toutes les marques")
    })

    it('Brand List IT', { baseUrl: url_it }, () => {
        myPage.allBrandsButton().click()
        myPage.viewBrandListOption().click()
        myPage.brandListHeader().should('contain.text', "Tutti i marchi")
    })

});
