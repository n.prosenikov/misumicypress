import Login from '../pom/login';
import HeaderOptions from '../pom/headerAndOrderHistory';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const orderHistory = new HeaderOptions()
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Order History - Mobile', () => {
    beforeEach(() => {
        cy.viewport('iphone-x')
        cy.visit('/')
        login.mobileSignIn(username, password)
        cy.wait(2000)
    })

    it('Order History UK', { baseUrl: url_uk }, () => {
        orderHistory.leftOptionButton().click()
        orderHistory.manageOrderHistoryOption().click()
        orderHistory.orderHistoryHeadLine().should('contain.text', "Order History")
    })

    it('Order History DE', { baseUrl: url_de }, () => {
        orderHistory.leftOptionButton().click()
        orderHistory.manageOrderHistoryOption().click()
        orderHistory.orderHistoryHeadLine().should('contain.text', "Bestellhistorie")
    })

    it('Order History FR', { baseUrl: url_fr }, () => {
        orderHistory.leftOptionButton().click()
        orderHistory.manageOrderHistoryOption().click()
        orderHistory.orderHistoryHeadLine().should('contain.text', "Historique des commandes")
    })

    it('Order History IT', { baseUrl: url_it }, () => {
        orderHistory.leftOptionButton().click()
        orderHistory.manageOrderHistoryOption().click()
        orderHistory.orderHistoryHeadLine().should('contain.text', "Ricerca storico ordini")
    })
});
