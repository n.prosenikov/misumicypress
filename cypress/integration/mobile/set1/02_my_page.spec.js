import Login from '../pom/login';
import UserMenu from '../pom/userMenu';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('My Page - Login Menu: Mobile', () => {

  beforeEach(() => {
    cy.viewport('iphone-x')
    cy.visit('/')
    login.mobileSignIn(username, password)
    cy.wait(2000)
  })

  it('My Page - Login Menu UK', { baseUrl: url_uk }, () => {
    login.profileIconSelector().click()
    userMenu.myPageUserMenu().click()
    cy.url().should('include', '/my/dashboard')
  })

  it('My Page - Login Menu DE', { baseUrl: url_de }, () => {
    login.profileIconSelector().click()
    userMenu.myPageUserMenu().click()
    cy.url().should('include', '/my/dashboard')
  })

  it('My Page - Login Menu FR', { baseUrl: url_fr }, () => {
    login.profileIconSelector().click()
    userMenu.myPageUserMenu().click()
    cy.url().should('include', '/my/dashboard')
  })

  it('My Page - Login Menu IT', { baseUrl: url_it }, () => {
    login.profileIconSelector().click()
    userMenu.myPageUserMenu().click()
    cy.url().should('include', '/my/dashboard')
  })

});
