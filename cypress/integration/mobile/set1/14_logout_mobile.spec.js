import Login from '../pom/login';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('Logout - Mobile Logout', () => {
    beforeEach(() => {
        cy.viewport('iphone-x')
        cy.visit('/')
        login.mobileSignIn(username, password)
        cy.wait(2000)
    })

    it('Mobile Logout UK', { baseUrl: url_uk }, () => {
        login.profileIconSelector().click()
        login.loggedUsernameSelector().should('contain.text', authUser.name)
        login.logOutButtonSelector().click()
        login.profileIconSelector().should('have.css', 'background-color', 'rgb(0, 64, 152)')
    })

    it('Mobile Logout DE', { baseUrl: url_de }, () => {
        login.profileIconSelector().click()
        login.loggedUsernameSelector().should('contain.text', authUser.name)
        login.logOutButtonSelector().click({ force: true })
        login.profileIconSelector().should('have.css', 'background-color', 'rgb(0, 64, 152)')
    })

    it('Mobile Logout FR', { baseUrl: url_fr }, () => {
        login.profileIconSelector().click()
        login.loggedUsernameSelector().should('contain.text', authUser.name)
        login.logOutButtonSelector().click({ force: true })
        login.profileIconSelector().should('have.css', 'background-color', 'rgb(0, 64, 152)')
    })

    it('Mobile Logout IT', { baseUrl: url_it }, () => {
        login.profileIconSelector().click({ force: true })
        login.loggedUsernameSelector().should('contain.text', authUser.name)
        login.logOutButtonSelector().click()
        login.profileIconSelector().should('have.css', 'background-color', 'rgb(0, 64, 152)')
    })
});
