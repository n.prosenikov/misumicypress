import Login from '../pom/login';
import UserMenu from '../pom/userMenu';
import CadDownload from '../pom/cadDownloads';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const cadDownload = new CadDownload()
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('CAD Downloads - Mobile', () => {
    beforeEach(() => {
        cy.viewport('iphone-x')
        cy.visit('/')
        login.mobileSignIn(username, password)
        cy.wait(2000)
    })

    it('CAD Downloads UK', { baseUrl: url_uk }, () => {
        login.profileIconSelector().click()
        userMenu.myCadDownloads().click()
        cadDownload.cadDownloadHeader().should('contain.text', "History of CAD data download")
    })

    it('CAD Downloads DE', { baseUrl: url_de }, () => {
        login.profileIconSelector().click()
        userMenu.myCadDownloads().click()
        cadDownload.cadDownloadHeader().should('contain.text', "CAD-Download Historie")
        
    })

    it('CAD Downloads FR', { baseUrl: url_fr }, () => {
        login.profileIconSelector().click()
        userMenu.myCadDownloads().click()
        cadDownload.cadDownloadHeader().should('contain.text', "Historique des téléchargements CAO")
    })

    it('CAD Downloads IT', { baseUrl: url_it }, () => {
        login.profileIconSelector().click()
        userMenu.myCadDownloads().click()
        cadDownload.cadDownloadHeader().should('contain.text', "Storico download dati CAD")
    })
});