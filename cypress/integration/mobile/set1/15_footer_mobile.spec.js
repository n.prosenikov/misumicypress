import Login from '../pom/login';
import Footer from '../pom/footer';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const footer = new Footer()
const {
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;


describe('Footer - Mobile', () => {
    beforeEach(() => {
        cy.viewport('iphone-x')
        cy.visit('/')
        login.mobileSignIn()
    })

    it('Footer UK', { baseUrl: url_uk }, () => {
        footer.checkFooterOptionMobileVersionUK()
    })

    it('Footer DE', { baseUrl: url_de }, () => {
        footer.checkFooterOptionMobileVersionDE()
    })

    it('Footer FR', { baseUrl: url_fr }, () => {
        footer.checkFooterOptionMobileVersionFR()
    })

    it('Footer IT', { baseUrl: url_it }, () => {
        footer.checkFooterOptionMobileVersionIT()
    })
})