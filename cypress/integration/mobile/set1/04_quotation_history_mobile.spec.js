import Login from '../pom/login';
import HeaderOptions from '../pom/headerAndOrderHistory';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const orderHistory = new HeaderOptions()
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Quotation History - Mobile', () => {
    beforeEach(() => {
        cy.viewport('iphone-x')
        cy.visit('/')
        login.mobileSignIn(username, password)
        cy.wait(2000)
    })

    it('Quotation History UK', { baseUrl: url_uk }, () => {
        orderHistory.leftOptionButton().click({force:true})
        orderHistory.manageQuoteHistoryOption().click()
        orderHistory.quoteHistoryHeadLine().should('contain.text', "Quotation History")
    })

    it('Quotation History DE', { baseUrl: url_de }, () => {
        orderHistory.leftOptionButton().click({force:true})
        orderHistory.manageQuoteHistoryOption().click()
        orderHistory.quoteHistoryHeadLine().should('contain.text', "Angebotshistorie")
    })

    it('Quotation History FR', { baseUrl: url_fr }, () => {
        orderHistory.leftOptionButton().click({force:true})
        orderHistory.manageQuoteHistoryOption().click()
        orderHistory.quoteHistoryHeadLine().should('contain.text', "Historique des devis")
    })

    it('Quotation History IT', { baseUrl: url_it }, () => {
        orderHistory.leftOptionButton().click({force:true})
        orderHistory.manageQuoteHistoryItalianVersion().click()
        orderHistory.quoteHistoryHeadLine().should('contain.text', "Storico quotazioni")
    })

});
