import Login from '../pom/login';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('Login - Mobile Login', () => {
    beforeEach(() => {
        cy.viewport('iphone-x')
        cy.visit('/')
        login.mobileSignIn(username, password)
        cy.wait(2000)
    })

    it('Mobile Login UK', { baseUrl: url_uk }, () => {
        login.profileIconSelector().click()
        login.loggedUsernameSelector().should('contain.text', authUser.name)
    })

    it('Mobile Login DE', { baseUrl: url_de }, () => {
        login.profileIconSelector().click()
        login.loggedUsernameSelector().should('contain.text', authUser.name)
    })

    it('Mobile Login FR', { baseUrl: url_fr }, () => {
        login.profileIconSelector().click()
        login.loggedUsernameSelector().should('contain.text', authUser.name)
    })

    it('Mobile Login IT', { baseUrl: url_it }, () => {
        login.profileIconSelector().click()
        login.loggedUsernameSelector().should('contain.text', authUser.name)
    })
});
