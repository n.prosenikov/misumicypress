import Login from '../pom/login';
import UserMenu from '../pom/userMenu';
import Dashboard from '../../cssp/pom/dashboard';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const dashboard = new Dashboard()
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('My Components via My MISUMI - Mobile', () => {

    beforeEach(() => {
        cy.viewport('iphone-x')
        cy.visit('/')
        login.mobileSignIn(username, password)
        cy.wait(2000)
    })

    it('My Components UK', { baseUrl: url_uk }, () => {
        login.profileIconSelector().click()
        userMenu.myPageUserMenu().click()
        dashboard.myToolsOptionsSelectors("components").invoke('removeAttr', 'target').click()
        userMenu.myComponentsHeadline().should('contain.text', "Main folder")
    })

    it('My Components DE', { baseUrl: url_de }, () => {
        login.profileIconSelector().click()
        userMenu.myPageUserMenu().click()
        dashboard.myToolsOptionsSelectors("components").invoke('removeAttr', 'target').click()
        userMenu.myComponentsHeadline().should('contain.text', "Hauptordner")
    })

    it('My Components FR', { baseUrl: url_fr }, () => {
        login.profileIconSelector().click()
        userMenu.myPageUserMenu().click()
        dashboard.myToolsOptionsSelectors("components").invoke('removeAttr', 'target').click()
        userMenu.myComponentsHeadline().should('contain.text', "Dossier principal")
    })

    it('My Components IT', { baseUrl: url_it }, () => {
        login.profileIconSelector().click()
        userMenu.myPageUserMenu().click()
        dashboard.myToolsOptionsSelectors("components").invoke('removeAttr', 'target').click()
        userMenu.myComponentsHeadline().should('contain.text', "Cartella principale")
    })

});
