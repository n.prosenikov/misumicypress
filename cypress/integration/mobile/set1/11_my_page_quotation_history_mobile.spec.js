import Login from '../pom/login';
import UserMenu from '../pom/userMenu';
import Dashboard from '../../cssp/pom/dashboard';
import HeaderOptions from '../pom/headerAndOrderHistory';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const userMenu = new UserMenu();
const dashboard = new Dashboard()
const headerOptions = new HeaderOptions()
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('My Page - Quotation History: Mobile', () => {

    beforeEach(() => {
        cy.viewport('iphone-x')
        cy.visit('/')
        login.mobileSignIn(username, password)
        cy.wait(2000)
    })

    it('My Page - Quotation History UK', { baseUrl: url_uk }, () => {
        login.profileIconSelector().click()
        userMenu.myPageUserMenu().click()
        dashboard.chooseOptionFromArchive("quotaion")
        headerOptions.quoteHistoryHeadLine().should('contain.text', "Quotation History")
    })

    it('My Page - Quotation History DE', { baseUrl: url_de }, () => {
        login.profileIconSelector().click()
        userMenu.myPageUserMenu().click()
        dashboard.chooseOptionFromArchive("quotaion")
        headerOptions.quoteHistoryHeadLine().should('contain.text', "Angebotshistorie")
    })

    it('My Page - Quotation History FR', { baseUrl: url_fr }, () => {
        login.profileIconSelector().click()
        userMenu.myPageUserMenu().click()
        dashboard.chooseOptionFromArchive("quotaion")
        headerOptions.quoteHistoryHeadLine().should('contain.text', "Historique des devis")
    })

    it('My Page - Quotation History IT', { baseUrl: url_it }, () => {
        login.profileIconSelector().click()
        userMenu.myPageUserMenu().click()
        dashboard.chooseOptionFromArchive("quotaion")
        headerOptions.quoteHistoryHeadLine().should('contain.text', "Storico quotazioni")
    })

});
