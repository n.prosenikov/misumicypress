import Login from '../pom/login';
import SearchPartNumbers from '../pom/search';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const searchPN = new SearchPartNumbers()
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

const partNumber = "SFJ10-100"

describe('Search Part - Mobile', () => {
    beforeEach(() => {
        cy.viewport('iphone-x')
        cy.visit('/')
        login.mobileSignIn(username, password)
    })

    it('Search Part UK', { baseUrl: url_uk }, () => {
        searchPN.mobileSearch(partNumber, 0)
        searchPN.productTitleSelector().should('contain.text', "Straight")
    })

    it('Search Part DE', { baseUrl: url_de }, () => {
        searchPN.mobileSearch(partNumber, 0)
        searchPN.productTitleSelector().should('contain.text', "Gerade")
    })

    it('Search Part FR', { baseUrl: url_fr }, () => {
        searchPN.mobileSearch(partNumber, 0)
        searchPN.productTitleSelector().should('contain.text', "Droit")
    })

    it('Search Part IT', { baseUrl: url_it }, () => {
        searchPN.mobileSearch(partNumber, 0)
        searchPN.productTitleSelector().should('contain.text', "Dritti")
    })

});
