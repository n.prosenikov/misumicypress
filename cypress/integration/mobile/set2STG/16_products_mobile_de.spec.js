import Login from '../pom/login';
import Product from '../pom/products';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const products = new Product()
const {
    url_de,
} = authUser;

describe('Automation Components [DE] - Mobile', () => {
    beforeEach(() => {
        cy.viewport('iphone-x')
        cy.visit('/')
        login.mobileSignIn()
    })

    it('Linear Shafts [DE]', { baseUrl: url_de }, () => {
        products.chooseComponentOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Linearwellen").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Gerade")
    })

    it('Shaft Supports [DE]', { baseUrl: url_de }, () => {
        products.chooseComponentOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Wellenhalter").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Wellenhalter/T-Form/Mittig geschlitzt")
    })

    it('Shaft Collars [DE]', { baseUrl: url_de }, () => {
        products.chooseComponentOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Stellringe").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Stellringe")
    })

    it('Linear Bushings [DE]', { baseUrl: url_de }, () => {
        products.chooseComponentOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Linearkugellager").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Linearkugellager")
    })

    it('Ball Guides - Miniature Ball Bearing Guide Sets [DE]', { baseUrl: url_de }, () => {
        products.chooseComponentOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Kugelführungen").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Miniatur-Kugelkäfigführungen")
    })

    it('Ball Guides - Rolled Ball Screws/Shaft Ends Configurable/Standard Nuts [DE]', { baseUrl: url_de }, () => {
        products.chooseComponentOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Kugelführungen").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Komponenten für Miniatur-Kugelkäfigführungen/Kugelkäfig")
    })

    it('Ball Guides - Shafts for Miniature Ball Bearing Guides/Both Ends Machined/Both Ends Tapped [DE]', { baseUrl: url_de }, () => {
        products.chooseComponentOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Kugelführungen").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Wellen für Miniatur-Kugelkäfigführungen/Beidseitig mit Innengewinde")
    })

    it('Ball Guides - Shafts for Miniature Ball Bearing Guide/with Retaining Ring Grooves [DE]', { baseUrl: url_de }, () => {
        products.chooseComponentOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Kugelführungen").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Wellen für Miniatur-Kugelkäfigführungen/Mit Sicherungsringnuten")
    })

    it('Ball Screw - Rolled Ball Screws/Dia 12 [DE]', { baseUrl: url_de }, () => {
        products.chooseComponentOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Kugelgewindetriebe").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Kugelgewindetriebe/Gerollt/Kompakte Mutter/Wellen-Ø 12/Steigung 4")
    })

    it('Ball Screw - Standard [DE]', { baseUrl: url_de }, () => {
        products.chooseComponentOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Kugelgewindetriebe").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Kugelgewindetriebe/Gerollt/Wellenenden konfigurierbar/Standardmuttern")
    })

    it('Ball Screw - Dia. 8 [DE]', { baseUrl: url_de }, () => {
        products.chooseComponentOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Kugelgewindetriebe").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Kugelgewindetriebe/Gerollt/Kompakte Muttern/Wellen-Ø 8/Steigung 2")
    })

    it('Ball Screw - Compact Nut/Shaft Dia. 15/Lead 5/10 [DE]', { baseUrl: url_de }, () => {
        products.chooseLinearMotionOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Kugelgewindetriebe").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Kugelgewindetriebe/Gerollt/Kompakte Mutter/Wellen-Ø 15/Steigung 5/10")
    })

    it('Shaft Collars [DE]', { baseUrl: url_de }, () => {
        products.chooseLinearMotionOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.chooseShaftCollardPart("Stellringe")
    })

    it('Oil Free Bushings, Washers [DE]', { baseUrl: url_de }, () => {
        products.chooseLinearMotionOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Wartungsfreie Gleitlager, Unterlegscheiben").click()
        products.bearingBoxOptionsSelector("Alle Wartungsfreie Gleitlager, Unterlegscheiben").click()
        products.itemHeaderSelector().should('contain.text', "Wartungsfreie Gleitlager, Unterlegscheiben")
    })

    it('Cable Carriers [DE]', { baseUrl: url_de }, () => {
        products.chooseLinearMotionOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Bewegliche Energieketten").click()
        products.bearingBoxOptionsSelector("Alle Bewegliche Energieketten").click()
        products.itemHeaderSelector().should('contain.text', "Bewegliche Energieketten")
    })

    it('Lead Screws, Slide Screws [DE]', { baseUrl: url_de }, () => {
        products.chooseLinearMotionOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Gewindespindeln, Miniaturgewindetriebe").click()
        products.bearingBoxOptionsSelector("Alle Gewindespindeln, Miniaturgewindetriebe").click()
        products.itemHeaderSelector().should('contain.text', "Gewindespindeln, Miniaturgewindetriebe")
    })

    it('Actuators [DE]', { baseUrl: url_de }, () => {
        products.chooseLinearMotionOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Aktuatoren").click()
        products.bearingBoxOptionsSelector("Alle Aktuatoren").click()
        products.itemHeaderSelector().should('contain.text', "Aktuatoren")
    })

    it('Single Axis Robots [DE]', { baseUrl: url_de }, () => {
        products.chooseLinearMotionOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Aktuatoren").click()
        products.bearingBoxOptionsSelector("Einspindelroboter").click()
        products.itemHeaderSelector().should('contain.text', "Einspindelroboter")
    })

    it('Single Axis Actuators [DE]', { baseUrl: url_de }, () => {
        products.chooseLinearMotionOptionFn("Mechanische Komponenten", "Linearbewegung")
        products.motionOptionsSelectors("Aktuatoren").click()
        products.bearingBoxOptionsSelector("Lineareinheiten kompakt").click()
        products.itemHeaderSelector().should('contain.text', "Lineareinheiten kompakt")
    })

    it('All Bearings [DE]', { baseUrl: url_de }, () => {
        products.chooseRotaryMotionSTGFn("Mechanische Komponenten")
        products.expandBearingSelector().invoke('removeAttr', 'target').click()
        products.bearingBoxOptionsSelector("Alle Lager").click()
        products.itemHeaderSelector().should('contain.text', "Lager")
    })

    it('Ball Bearings [DE]', { baseUrl: url_de }, () => {
        products.chooseRotaryMotionSTGFn("Mechanische Komponenten")
        products.expandBearingSelector().invoke('removeAttr', 'target').click()
        products.chooseBearingOption("Kugellager")
    })

    it('Roller Bearings [DE]', { baseUrl: url_de }, () => {
        products.chooseRotaryMotionSTGFn("Mechanische Komponenten")
        products.expandBearingSelector().invoke('removeAttr', 'target').click()
        products.chooseBearingOption("Rollenlager")
    })

    it('All Bearing Units [DE]', { baseUrl: url_de }, () => {
        products.chooseRotaryMotionSTGFn("Mechanische Komponenten")
        products.motionOptionsSelectors("Lagereinheiten").invoke('removeAttr', 'target').click()
        products.bearingBoxOptionsSelector("Alle Lagereinheiten").click()
        products.itemHeaderSelector().should('contain.text', "Lagereinheiten")
    })

    it('Pillow Type [DE]', { baseUrl: url_de }, () => {
        products.chooseRotaryMotionSTGFn("Mechanische Komponenten")
        products.motionOptionsSelectors("Lagereinheiten").invoke('removeAttr', 'target').click()
        products.chooseBearingOption("Mit Ständer")
    })

    it('Flange Type [DE]', { baseUrl: url_de }, () => {
        products.chooseRotaryMotionSTGFn("Mechanische Komponenten")
        products.motionOptionsSelectors("Lagereinheiten").invoke('removeAttr', 'target').click()
        products.chooseBearingOption("Flanschausführung")
    })

    it('Other [DE]', { baseUrl: url_de }, () => {
        products.chooseRotaryMotionSTGFn("Mechanische Komponenten")
        products.motionOptionsSelectors("Lagereinheiten").invoke('removeAttr', 'target').click()
        products.chooseBearingOption("Andere Formen")
    })

    it('Bearings with Housing [DE]', { baseUrl: url_de }, () => {
        products.chooseRotaryMotionSTGFn("Mechanische Komponenten")
        products.chooseMotionOptionWithoutSubmotions("Lager mit Gehäuse")
    })

    it('Cam Followers, Roller Followers [DE]', { baseUrl: url_de }, () => {
        products.chooseRotaryMotionSTGFn("Mechanische Komponenten")
        products.chooseMotionOptionWithoutSubmotions("Kurvenrollen mit Achsbolzen, Stützrollen")
    })

    it('Rotary Shafts, Drive Shafts [DE]', { baseUrl: url_de }, () => {
        products.chooseRotaryMotionSTGFn("Mechanische Komponenten")
        products.motionOptionsSelectors("Rotationswellen, Antriebswellen").invoke('removeAttr', 'target').click()
        products.bearingBoxOptionsSelector("Alle Rotationswellen, Antriebswellen").click()
        products.itemHeaderSelector().should('contain.text', "Rotationswellen, Antriebswellen")
    })

    it('Cantilever Shafts [DE]', { baseUrl: url_de }, () => {
        products.chooseRotaryMotionSTGFn("Mechanische Komponenten")
        products.chooseMotionOptionWithoutSubmotions("Achsbolzen")
    })

    it('Fulcrum Pins [DE]', { baseUrl: url_de }, () => {
        products.chooseRotaryMotionFn("Mechanische Komponenten")
        products.chooseConnectingPartSTG(2, "Schrauben mit Ansatz")
    })

    it('Hinge Pins [DE]', { baseUrl: url_de }, () => {
        products.chooseRotaryMotionFn("Mechanische Komponenten")
        products.chooseConnectingPartSTG(3, "Scharnierbolzen")
    })

    it('Hinge Bases, Hinge Plates, Hinge Bolt [DE]', { baseUrl: url_de }, () => {
        products.chooseRotaryMotionFn("Mechanische Komponenten")
        products.chooseConnectingPartSTG(4, "Scharnierlagerböcke, Scharnierlagerplatten, Scharnierbolzen")
    })

    it('Links [DE]', { baseUrl: url_de }, () => {
        products.chooseRotaryMotionFn("Mechanische Komponenten")
        products.chooseConnectingPartSTG(5, "Gelenkarme")
    })

    it('Rod End Bearings, Spherical Bearings [DE]', { baseUrl: url_de }, () => {
        products.chooseRotaryMotionFn("Mechanische Komponenten")
        products.chooseConnectingPartSTG(6, "Gelenkköpfe, Gelenklager")
    })

    it('Connecting Bars, Link Cables/Wires [DE]', { baseUrl: url_de }, () => {
        products.chooseRotaryMotionFn("Mechanische Komponenten")
        products.chooseConnectingPartSTG(7, "Verbindungsstäbe, Anschlusskabel/Drähte")
    })

})