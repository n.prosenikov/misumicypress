import Login from '../pom/login';
import Product from '../pom/products';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const products = new Product()
const {
    url_uk,
} = authUser;

describe('Automation Components [UK] - Mobile', () => {
    beforeEach(() => {
        cy.viewport('iphone-x')
        cy.visit('/')
        login.mobileSignIn()
    })

    it('Linear Shafts [UK]', { baseUrl: url_uk }, () => {
        products.chooseComponentOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Linear Shafts").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Straight")
    })

    it('Shaft Supports [UK]', { baseUrl: url_uk }, () => {
        products.chooseComponentOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Shaft Supports").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Shaft Supports/Flanged/Slit/Precision Cast")
    })

    it('Linear Shaft Support Flanged Mount/Thick Sleeve [UK]', { baseUrl: url_uk }, () => {
        products.chooseComponentOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Shaft Supports").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Shaft Supports/Flanged Mount/Thick Sleeve")
    })

    it('Linear Bushings/Standard/Single [UK]', { baseUrl: url_uk }, () => {
        products.chooseComponentOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Linear Bushings").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Flanged Linear Bushings/Single Type")
    })

    it('Ball Guides - Miniature Ball Bearing Guide Sets [UK]', { baseUrl: url_uk }, () => {
        products.chooseComponentOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Ball Guides").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Miniature Ball Bearing Guide Sets")
    })

    it('Ball Guides - Miniature Ball Bearing Guide Components/Ball Slider [UK]', { baseUrl: url_uk }, () => {
        products.chooseComponentOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Ball Guides").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Miniature Ball Bearing Guide Components/Ball Slider")
    })

    it('Ball Guides - Shafts for Miniature Ball Bearing Guides/Both Ends Machined/Both Ends Tapped [UK]', { baseUrl: url_uk }, () => {
        products.chooseComponentOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Ball Guides").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Shafts for Miniature Ball Bearing Guides/Both Ends Machined/Both Ends Tapped")
    })

    it('Ball Guides - Shafts for Miniature Ball Bearing Guide/with Retaining Ring Grooves [UK]', { baseUrl: url_uk }, () => {
        products.chooseComponentOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Ball Guides").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Shafts for Miniature Ball Bearing Guide/with Retaining Ring Grooves")
    })

    it('Ball Screw - Rolled Ball Screws/Shaft Ends Configurable/Standard Nuts [UK]', { baseUrl: url_uk }, () => {
        products.chooseComponentOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Ball Screw").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Rolled Ball Screws/Shaft Ends Configurable/Standard Nuts")
    })

    it('Ball Screw - Shaft Dia. 8/Lead 2/Lead 1 or 2 [UK]', { baseUrl: url_uk }, () => {
        products.chooseComponentOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Ball Screw").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Rolled Ball Screws/Shaft Dia. 8/Lead 2/Cost Efficient Product")
    })

    it('Ball Screw - Thread Diameter 10/Lead 2/4 or 10 [UK]', { baseUrl: url_uk }, () => {
        products.chooseComponentOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Ball Screw").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Rolled Ball Screws/Thread Diameter 10/Lead 2/4 or 10")
    })

    it('Ball Screw - Compact Nuts/Shaft Dia. 12/Lead 4 [UK]', { baseUrl: url_uk }, () => {
        products.chooseComponentOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Ball Screw").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Rolled Ball Screws/Compact Nut/Shaft Dia. 12/Lead 4")
    })

    it('Ball Screw - Compact Nut/Shaft Dia. 15/Lead 5/10 [UK]', { baseUrl: url_uk }, () => {
        products.chooseLinearMotionOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Ball Screw").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Rolled Ball Screws/Compact Nut/Shaft Dia. 15/Lead 5/10")
    })

    it('Set Collars / Clamping Collars [UK]', { baseUrl: url_uk }, () => {
        products.chooseLinearMotionOptionFn("Automation Components", "Linear Motion")
        products.chooseShaftCollardPart("Shaft Collars")
    })

    it('Oil Free Bushings, Washers [UK]', { baseUrl: url_uk }, () => {
        products.chooseLinearMotionOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Oil Free Bushings, Washers").click()
        products.bearingBoxOptionsSelector("All Oil Free Bushings, Washers").click()
        products.itemHeaderSelector().should('contain.text', "Oil Free Bushings, Washers")
    })

    it('Cable Carriers [UK]', { baseUrl: url_uk }, () => {
        products.chooseLinearMotionOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Cable Carriers").click()
        products.bearingBoxOptionsSelector("All Cable Carriers").click()
        products.itemHeaderSelector().should('contain.text', "Cable Carriers")
    })

    it('Lead Screws, Slide Screws [UK]', { baseUrl: url_uk }, () => {
        products.chooseLinearMotionOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Lead Screws, Slide Screws").click()
        products.bearingBoxOptionsSelector("All Lead Screws, Slide Screws").click()
        products.itemHeaderSelector().should('contain.text', "Lead Screws, Slide Screws")
    })

    it('Actuators [UK]', { baseUrl: url_uk }, () => {
        products.chooseLinearMotionOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Actuators").click()
        products.bearingBoxOptionsSelector("All Actuators").click()
        products.itemHeaderSelector().should('contain.text', "Actuators")
    })

    it('Single Axis Robots [UK]', { baseUrl: url_uk }, () => {
        products.chooseLinearMotionOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Actuators").click()
        products.bearingBoxOptionsSelector("Single Axis Robots").click()
        products.itemHeaderSelector().should('contain.text', "Single Axis Robots")
    })

    it('Single Axis Actuators [UK]', { baseUrl: url_uk }, () => {
        products.chooseLinearMotionOptionFn("Automation Components", "Linear Motion")
        products.motionOptionsSelectors("Actuators").click()
        products.bearingBoxOptionsSelector("Single Axis Actuators").click()
        products.itemHeaderSelector().should('contain.text', "Single Axis Actuators")
    })

    it('Bellows [UK]', { baseUrl: url_uk }, () => {
        products.chooseLinearMotionOptionFn("Automation Components", "Linear Motion")
        products.chooseShaftCollardPart("Bellows")
    })

    it('All Bearings [UK]', { baseUrl: url_uk }, () => {
        products.chooseRotaryMotionSTGFn("Automation Components")
        products.motionOptionsSelectors("Bearings").invoke('removeAttr', 'target').click()
        products.bearingBoxOptionsSelector("All Bearings").click()
        products.itemHeaderSelector().should('contain.text', "Bearings")
    })

    it('Ball Bearings [UK]', { baseUrl: url_uk }, () => {
        products.chooseRotaryMotionSTGFn("Automation Components")
        products.motionOptionsSelectors("Bearings").invoke('removeAttr', 'target').click()
        products.chooseBearingOption("Ball Bearings")
    })

    it('Roller Bearings [UK]', { baseUrl: url_uk }, () => {
        products.chooseRotaryMotionSTGFn("Automation Components")
        products.motionOptionsSelectors("Bearings").invoke('removeAttr', 'target').click()
        products.chooseBearingOption("Roller Bearings")
    })

    it('All Hinge Pins [UK]', { baseUrl: url_uk }, () => {
        products.chooseRotaryMotionSTGFn("Automation Components")
        products.motionOptionsSelectors("Hinge Pins").invoke('removeAttr', 'target').click()
        products.bearingBoxOptionsSelector("All Hinge Pins").click()
        products.itemHeaderSelector().should('contain.text', "Hinge Pins")
    })

    it('All Bearing Units [UK]', { baseUrl: url_uk }, () => {
        products.chooseRotaryMotionSTGFn("Automation Components")
        products.motionOptionsSelectors("Bearing Units").invoke('removeAttr', 'target').click()
        products.bearingBoxOptionsSelector("All Bearing Units").click()
        products.itemHeaderSelector().should('contain.text', "Bearing Units")
    })

    it('Pillow Type [UK]', { baseUrl: url_uk }, () => {
        products.chooseRotaryMotionSTGFn("Automation Components")
        products.motionOptionsSelectors("Bearing Units").invoke('removeAttr', 'target').click()
        products.chooseBearingOption("Pillow Type")
    })

    it('Flange Type [UK]', { baseUrl: url_uk }, () => {
        products.chooseRotaryMotionSTGFn("Automation Components")
        products.motionOptionsSelectors("Bearing Units").invoke('removeAttr', 'target').click()
        products.chooseBearingOption("Flange Type")
    })

    it('Other Shapes [UK]', { baseUrl: url_uk }, () => {
        products.chooseRotaryMotionSTGFn("Automation Components")
        products.motionOptionsSelectors("Bearing Units").invoke('removeAttr', 'target').click()
        products.chooseBearingOption("Other Shapes")
    })

    it('Bearings with Housing [UK]', { baseUrl: url_uk }, () => {
        products.chooseRotaryMotionSTGFn("Automation Components")
        products.chooseMotionOptionWithoutSubmotions("Bearings with Housing")
    })

    it('Cam Followers, Roller Followers [UK]', { baseUrl: url_uk }, () => {
        products.chooseRotaryMotionSTGFn("Automation Components")
        products.chooseMotionOptionWithoutSubmotions("Cam Followers, Roller Followers")
    })

    it('Rotary Shafts, Drive Shafts [UK]', { baseUrl: url_uk }, () => {
        products.chooseRotaryMotionSTGFn("Automation Components")
        products.motionOptionsSelectors("Rotary Shafts, Drive Shafts").invoke('removeAttr', 'target').click()
        products.bearingBoxOptionsSelector("All Rotary Shafts, Drive Shafts").click()
        products.itemHeaderSelector().should('contain.text', "Rotary Shafts, Drive Shafts")
    })

    it('Cantilever Shafts [UK]', { baseUrl: url_uk }, () => {
        products.chooseRotaryMotionSTGFn("Automation Components")
        products.chooseMotionOptionWithoutSubmotions("Cantilever Shafts")
    })

    it('Fulcrum Pins [UK]', { baseUrl: url_uk }, () => {
        products.chooseRotaryMotionFn("Automation Components")
        products.chooseConnectingPartSTG(2, "Fulcrum Pins")
    })

    it('Hinge Pins [UK]', { baseUrl: url_uk }, () => {
        products.chooseRotaryMotionFn("Automation Components")
        products.chooseConnectingPartSTG(3, "Hinge Pins")
    })

    it('Hinge Bases, Hinge Plates, Hinge Bolt [UK]', { baseUrl: url_uk }, () => {
        products.chooseRotaryMotionFn("Automation Components")
        products.chooseConnectingPartSTG(4, "Hinge Bases, Hinge Plates, Hinge Bolt")
    })

    it('Links [UK]', { baseUrl: url_uk }, () => {
        products.chooseRotaryMotionFn("Automation Components")
        products.chooseConnectingPartSTG(5, "Links")
    })

    it('Rod End Bearings, Spherical Bearings [UK]', { baseUrl: url_uk }, () => {
        products.chooseRotaryMotionFn("Automation Components")
        products.chooseConnectingPartSTG(6, "Rod End Bearings, Spherical Bearings")
    })

    it('Connecting Bars, Link Cables/Wires [UK]', { baseUrl: url_uk }, () => {
        products.chooseRotaryMotionFn("Automation Components")
        products.chooseConnectingPartSTG(7, "Connecting Bars, Link Cables/Wires")
    })

})