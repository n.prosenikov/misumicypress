import Login from '../pom/login';
import Product from '../pom/products';

const authUser = require('../../../fixtures/auth-user.json');
const login = new Login();
const products = new Product()
const {
    url_it,
} = authUser;

describe('Automation Components [IT] - Mobile', () => {
    beforeEach(() => {
        cy.viewport('iphone-x')
        cy.visit('/')
        login.mobileSignIn()
    })

    it('Linear Shafts [IT]', { baseUrl: url_it }, () => {
        products.chooseComponentOptionFn("Componenti meccanici", "Movimentazione lineare")
        products.motionOptionsSelectors("Alberi lineari").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Dritti")
    })

    it('Shaft Supports [IT]', { baseUrl: url_it }, () => {
        products.chooseComponentOptionFn("Componenti meccanici", "Movimentazione lineare")
        products.motionOptionsSelectors("Supporti per alberi").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Supporti albero/Attacco flangiato con spacchi")
    })

    it('Linear Shaft Support Flanged Mount/Thick Sleeve [IT]', { baseUrl: url_it }, () => {
        products.chooseComponentOptionFn("Componenti meccanici", "Movimentazione lineare")
        products.motionOptionsSelectors("Supporti per alberi").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Supporti albero/Attacco con flangia/Manicotto spesso")
    })

    it('Ball Guides - Miniature Ball Bearing Guide Sets [IT]', { baseUrl: url_it }, () => {
        products.chooseComponentOptionFn("Componenti meccanici", "Movimentazione lineare")
        products.motionOptionsSelectors("Guide a sfere").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Kit guide per cuscinetti a sfere in miniatura")
    })

    it('Ball Guides - Miniature Ball Bearing Guide Components/Ball Slider [IT]', { baseUrl: url_it }, () => {
        products.chooseComponentOptionFn("Componenti meccanici", "Movimentazione lineare")
        products.motionOptionsSelectors("Guide a sfere").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Componenti per guide a sfere in miniatura/Cursore a sfere")
    })

    it('Ball Guides - Shafts for Miniature Ball Bearing Guides/Both Ends Machined/Both Ends Tapped [IT]', { baseUrl: url_it }, () => {
        products.chooseComponentOptionFn("Componenti meccanici", "Movimentazione lineare")
        products.motionOptionsSelectors("Guide a sfere").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Alberi per guide a sfere in miniatura/Lavorati e maschiati sui due lati")
    })

    it('Ball Guides - Shafts for Miniature Ball Bearing Guide/with Retaining Ring Grooves [IT]', { baseUrl: url_it }, () => {
        products.chooseComponentOptionFn("Componenti meccanici", "Movimentazione lineare")
        products.motionOptionsSelectors("Guide a sfere").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Alberi porta-guide a sfere in miniatura/Con gole per anello di sicurezza")
    })

    it('Ball Screw - Rolled Ball Screws/Shaft Ends Configurable/Standard Nuts [IT]', { baseUrl: url_it }, () => {
        products.chooseComponentOptionFn("Componenti meccanici", "Movimentazione lineare")
        products.motionOptionsSelectors("Viti a ricircolo di sfere").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Viti a ricircolo di sfere rullate/Estremità albero configurabili/Chiocciole standard")
    })

    it('Ball Screw - Shaft Dia. 8/Lead 2/Cost Efficient Product [IT]', { baseUrl: url_it }, () => {
        products.chooseComponentOptionFn("Componenti meccanici", "Movimentazione lineare")
        products.motionOptionsSelectors("Viti a ricircolo di sfere").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Viti a ricircolo di sfere rullate/Diametro albero 8/Passo 2/Prodotto economico")
    })

    it('Ball Screw - Thread Diameter 10/Lead 2/4 or 10 [IT]', { baseUrl: url_it }, () => {
        products.chooseComponentOptionFn("Componenti meccanici", "Movimentazione lineare")
        products.motionOptionsSelectors("Viti a ricircolo di sfere").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Viti a ricircolo di sfere di precisione/Diametro filettatura 10/Passo 2, 4, 10")
    })

    it('Ball Screw - Compact Nut/Shaft Dia. 20/Lead 5/10 [IT]', { baseUrl: url_it }, () => {
        products.chooseComponentOptionFn("Componenti meccanici", "Movimentazione lineare")
        products.motionOptionsSelectors("Viti a ricircolo di sfere").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Viti a ricircolo di sfere rullate/Chiocciola compatta/Diametro albero 20/Passo 5, 10")
    })

    it('Ball Screw - Compact Nut/Shaft Dia. 15/Lead 5/10 [IT]', { baseUrl: url_it }, () => {
        products.chooseLinearMotionOptionFn("Componenti meccanici", "Movimentazione lineare")
        products.motionOptionsSelectors("Viti a ricircolo di sfere").invoke('removeAttr', 'target').click()
        products.clickOnAnItemFn("Viti a ricircolo di sfere rullate/Chiocciola compatta/Diametro albero 15/Passo 5, 10")
    })

    it('Shaft Collars [IT]', { baseUrl: url_it }, () => {
        products.chooseLinearMotionOptionFn("Componenti meccanici", "Movimentazione lineare")
        products.chooseShaftCollardPart("Supporti per alberi")
    })

    it('Oil Free Bushings, Washers [IT]', { baseUrl: url_it }, () => {
        products.chooseLinearMotionOptionFn("Componenti meccanici", "Movimentazione lineare")
        products.motionOptionsSelectors("Boccole e rondelle autolubrificanti").click()
        products.bearingBoxOptionsSelector("Tutti Boccole e rondelle autolubrificanti").click()
        products.itemHeaderSelector().should('contain.text', "Boccole e rondelle autolubrificanti")
    })

    it('Cable Carriers [IT]', { baseUrl: url_it }, () => {
        products.chooseLinearMotionOptionFn("Componenti meccanici", "Movimentazione lineare")
        products.motionOptionsSelectors("Catene portacavi").click()
        products.bearingBoxOptionsSelector("Tutti Catene portacavi").click()
        products.itemHeaderSelector().should('contain.text', "Catene portacavi")
    })

    it('Lead Screws, Slide Screws [IT]', { baseUrl: url_it }, () => {
        products.chooseLinearMotionOptionFn("Componenti meccanici", "Movimentazione lineare")
        products.motionOptionsSelectors("Viti di trasmissione, viti a strisciamento").click()
        products.bearingBoxOptionsSelector("Tutti Viti di trasmissione, viti a strisciamento").click()
        products.itemHeaderSelector().should('contain.text', "Viti di trasmissione, viti a strisciamento")
    })

    it('Actutators [IT]', { baseUrl: url_it }, () => {
        products.chooseLinearMotionOptionFn("Componenti meccanici", "Movimentazione lineare")
        products.motionOptionsSelectors("Attuatori").click()
        products.bearingBoxOptionsSelector("Tutti Attuatori").click()
        products.itemHeaderSelector().should('contain.text', "Attuatori")
    })

    it('Single Axis Robots [IT]', { baseUrl: url_it }, () => {
        products.chooseLinearMotionOptionFn("Componenti meccanici", "Movimentazione lineare")
        products.motionOptionsSelectors("Attuatori").click()
        products.bearingBoxOptionsSelector("Robot ad asse singolo").click()
        products.itemHeaderSelector().should('contain.text', "Robot ad asse singolo")
    })

    it('Single Axis Attuatori [IT]', { baseUrl: url_it }, () => {
        products.chooseLinearMotionOptionFn("Componenti meccanici", "Movimentazione lineare")
        products.motionOptionsSelectors("Attuatori").click()
        products.bearingBoxOptionsSelector("Attuatori ad asse singolo").click()
        products.itemHeaderSelector().should('contain.text', "Attuatori ad asse singolo")
    })

    it('Bellows [IT]', { baseUrl: url_it }, () => {
        products.chooseLinearMotionOptionFn("Componenti meccanici", "Movimentazione lineare")
        products.chooseShaftCollardPart("Soffietti")
    })

    it('All Bearings [IT]', { baseUrl: url_it }, () => {
        products.chooseRotaryMotionFn("Componenti meccanici")
        products.italianExpandBearingSelector().invoke('removeAttr', 'target').click()
        products.bearingBoxOptionsSelector("Tutti Cuscinetti").click()
        products.itemHeaderSelector().should('contain.text', "Cuscinetti")
    })

    it('Ball Bearings [IT]', { baseUrl: url_it }, () => {
        products.chooseRotaryMotionFn("Componenti meccanici")
        products.italianExpandBearingSelector().invoke('removeAttr', 'target').click()
        products.chooseBearingOption("Cuscinetti a sfere")
    })

    it('Roller Bearings [IT]', { baseUrl: url_it }, () => {
        products.chooseRotaryMotionFn("Componenti meccanici")
        products.italianExpandBearingSelector().invoke('removeAttr', 'target').click()
        products.chooseBearingOption("Cuscinetti a rullini")
    })

    it('All Hinge Pins [IT]', { baseUrl: url_it }, () => {
        products.chooseRotaryMotionFn("Componenti meccanici")
        products.motionOptionsSelectors("Accessori per cuscinetti").invoke('removeAttr', 'target').click()
        products.bearingBoxOptionsSelector("Tutti Accessori per cuscinetti").click()
        products.itemHeaderSelector().should('contain.text', "Accessori per cuscinetti")
    })

    it('All Bearing Units [IT]', { baseUrl: url_it }, () => {
        products.chooseRotaryMotionFn("Componenti meccanici")
        products.motionOptionsSelectors("Gruppi cuscinetto").invoke('removeAttr', 'target').click()
        products.bearingBoxOptionsSelector("Tutti Gruppi cuscinetto").click()
        products.itemHeaderSelector().should('contain.text', "Gruppi cuscinetto")
    })

    it('Pillow Type [IT]', { baseUrl: url_it }, () => {
        products.chooseRotaryMotionFn("Componenti meccanici")
        products.motionOptionsSelectors("Gruppi cuscinetto").invoke('removeAttr', 'target').click()
        products.chooseBearingOption("Supporti dritti")
    })

    it('Flange Type [IT]', { baseUrl: url_it }, () => {
        products.chooseRotaryMotionFn("Componenti meccanici")
        products.motionOptionsSelectors("Gruppi cuscinetto").invoke('removeAttr', 'target').click()
        products.chooseBearingOption("Unità flangiate")
    })

    it('Other Shapes [IT]', { baseUrl: url_it }, () => {
        products.chooseRotaryMotionFn("Componenti meccanici")
        products.motionOptionsSelectors("Gruppi cuscinetto").invoke('removeAttr', 'target').click()
        products.chooseBearingOption("Altre forme")
    })

    it('Bearings with Housing [IT]', { baseUrl: url_it }, () => {
        products.chooseRotaryMotionFn("Componenti meccanici")
        products.chooseMotionOptionWithoutSubmotions("Sedi cuscinetto")
    })

    it('Cam Followers, Roller Followers [IT]', { baseUrl: url_it }, () => {
        products.chooseRotaryMotionFn("Componenti meccanici")
        products.chooseMotionOptionWithoutSubmotions("Perni folli, rotelle")
    })

    it('Rotary Shafts, Drive Shafts [IT]', { baseUrl: url_it }, () => {
        products.chooseRotaryMotionFn("Componenti meccanici")
        products.motionOptionsSelectors("Alberi rotanti, alberi motore").invoke('removeAttr', 'target').click()
        products.bearingBoxOptionsSelector("Tutti Alberi rotanti, alberi motore").click()
        products.itemHeaderSelector().should('contain.text', "Alberi rotanti, alberi motore")
    })

    it('Cantilever Shafts [IT]', { baseUrl: url_it }, () => {
        products.chooseRotaryMotionFn("Componenti meccanici")
        products.chooseMotionOptionWithoutSubmotions("Alberi a sbalzo")
    })

    it('Fulcrum Pins [IT]', { baseUrl: url_it }, () => {
        products.chooseItalianConnectingPart("Componenti meccanici", "Componenti con meccanismo consolidato e ad anello")
        products.italianChooseConnectingPart(2, "Perni fulcro")
    })

    it('Hinge Pins [IT]', { baseUrl: url_it }, () => {
        products.chooseItalianConnectingPart("Componenti meccanici", "Componenti con meccanismo consolidato e ad anello")
        products.italianChooseConnectingPart(3, "Perni girevoli")
    })

    it('Hinge Bases, Hinge Plates, Hinge Bolt [IT]', { baseUrl: url_it }, () => {
        products.chooseItalianConnectingPart("Componenti meccanici", "Componenti con meccanismo consolidato e ad anello")
        products.italianChooseConnectingPart(4, "Basi cerniera, bulloni per cerniera")
    })

    it('Links [IT]', { baseUrl: url_it }, () => {
        products.chooseItalianConnectingPart("Componenti meccanici", "Componenti con meccanismo consolidato e ad anello")
        products.italianChooseConnectingPart(5, "Tiranti")
    })

    it('Rod End Bearings, Spherical Bearings [IT]', { baseUrl: url_it }, () => {
        products.chooseItalianConnectingPart("Componenti meccanici", "Componenti con meccanismo consolidato e ad anello")
        products.italianChooseConnectingPart(6, "Cuscinetti teste a snodo, cuscinetti sferici")
    })

    it('Connecting Bars, Link Cables/Wires [IT]', { baseUrl: url_it }, () => {
        products.chooseItalianConnectingPart("Componenti meccanici", "Componenti con meccanismo consolidato e ad anello")
        products.italianChooseConnectingPart(7, "Barre, cavi e fili di collegamento")
    })

})