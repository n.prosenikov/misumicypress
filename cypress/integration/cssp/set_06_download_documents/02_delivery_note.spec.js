import Login from "../../boc/pom/login"
import Dashboard from '../pom/dashboard';
import OrderAndFilter from "../pom/orderAndFilter";
import ClaimsPage from "../pom/claims";
import { deleteDownloadsFolder } from "../pom/orderAndFilter";

const loginPage = new Login()
const dashboard = new Dashboard()
const claimsPage = new ClaimsPage()
const orderFilter = new OrderAndFilter()

const filterOpt = "invoice-due"
const typeDocument = "delivery"
const nameOfDocument = "Delivery-Note"

const authUser = require('../../../fixtures/auth-user.json');
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('Download Delivery Note]', () => {
    beforeEach(() => {
        deleteDownloadsFolder()
        loginPage.login(username, password)
        dashboard.goToTheLink()
        claimsPage.ordersOverviewBtnSelector().click()
    });

    it('Download Delivery Note Document [UK]', { baseUrl: url_uk }, () => {
        orderFilter.chooseFilterOptions(filterOpt)
        orderFilter.checkInvoiceOverdue(typeDocument, nameOfDocument)
    })

    it('Download Delivery Note Document [DE]', { baseUrl: url_de }, () => {
        orderFilter.chooseFilterOptions(filterOpt)
        orderFilter.checkInvoiceOverdue(typeDocument, nameOfDocument)
    })

    it('Download Delivery Note Document [FR]', { baseUrl: url_fr }, () => {
        orderFilter.chooseFilterOptions(filterOpt)
        orderFilter.checkInvoiceOverdue(typeDocument, nameOfDocument)
    })

    it('Download Delivery Note Document [IT]', { baseUrl: url_it }, () => {
        orderFilter.chooseFilterOptions(filterOpt)
        orderFilter.checkInvoiceOverdue(typeDocument, nameOfDocument)
    })

})