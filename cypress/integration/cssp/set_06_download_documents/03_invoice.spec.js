import Login from "../../boc/pom/login"
import OrderAndFilter from "../pom/orderAndFilter";
import Dashboard from '../pom/dashboard';
import ClaimsPage from "../pom/claims";
import { deleteDownloadsFolder } from "../pom/orderAndFilter";



const loginPage = new Login()
const orderFilter = new OrderAndFilter()
const dashboard = new Dashboard()
const claimsPage = new ClaimsPage()


const typeDocument = "invoice"
const nameOfDocument = "Invoice-"

const authUser = require('../../../fixtures/auth-user.json');
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('Download Invocie Document', () => {
    beforeEach(() => {
        deleteDownloadsFolder()
        loginPage.login(username, password)
        dashboard.goToTheLink()
        claimsPage.ordersOverviewBtnSelector().click()
    });

    it('Download Invoice Document [UK]', { baseUrl: url_uk }, () => {
        orderFilter.downloadDocument(20, typeDocument)
        orderFilter.verifyDownloadIsSuccessful(nameOfDocument)
    })

    it('Download Invoice Document [DE]', { baseUrl: url_de }, () => {
        orderFilter.downloadDocument(20, typeDocument)
        orderFilter.verifyDownloadIsSuccessful(nameOfDocument)
    })

    it('Download Invoice Document [FR]', { baseUrl: url_fr }, () => {
        orderFilter.downloadDocument(20, typeDocument)
        orderFilter.verifyDownloadIsSuccessful(nameOfDocument)
    })

    it('Download Invoice Document [IT]', { baseUrl: url_it }, () => {
        orderFilter.downloadDocument(20, typeDocument)
        orderFilter.verifyDownloadIsSuccessful(nameOfDocument)
    })

})