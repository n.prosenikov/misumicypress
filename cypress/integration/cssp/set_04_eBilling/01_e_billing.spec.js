import Login from "../../boc/pom/login"
import Dashboard from '../pom/dashboard';
import CsspPage from "../pom/cssp";
import EBilling from "../pom/e_billing";

const loginPage = new Login()
const dashboard = new Dashboard()
const csspPage = new CsspPage()
const ebilling = new EBilling()

const authUser = require('../../../fixtures/auth-user.json');
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('CSSP E-Billing [UK]', () => {
  beforeEach(() => {
    loginPage.login(username, password)
    cy.visit('/')
  });
  const randInt = csspPage.getRandomInt(777)

  /*
  Description: Login onto the FR server using the provided credentials and open CSSP page
  Test Steps:
    1. eCatalog top page - Click the [Login] button in the upper right corner of the page and log in with a common account to all local offices.
    2. Assert logged with correct username is displayed
    3. Open CSSP page and confirm the page is loaded
  */
  it('CSSP - e-Billing subscription default checked [UK]', { baseUrl: url_uk }, () => {
    dashboard.visitCSSPPage()
    ebilling.visitEbillingFn()
    ebilling.allElementsAreVisibleFn(0) //0-UK, 1-DE, 2-FR, 3-IT
  })

  it('CSSP - e-Billing subscription default checked [DE]', { baseUrl: url_de }, () => {
    dashboard.visitCSSPPage()
    ebilling.visitEbillingFn()
    ebilling.allElementsAreVisibleFn(1) //0-UK, 1-DE, 2-FR, 3-IT
  })

  it('CSSP - e-Billing subscription default checked [FR]', { baseUrl: url_fr }, () => {
    dashboard.visitCSSPPage()
    ebilling.visitEbillingFn()
    ebilling.allElementsAreVisibleFn(2) //0-UK, 1-DE, 2-FR, 3-IT
  })

  it('CSSP - e-Billing subscription default checked [IT]', { baseUrl: url_it }, () => {
    dashboard.visitCSSPPage()
    ebilling.visitEbillingFn()
    ebilling.allElementsAreVisibleFn(3) //0-UK, 1-DE, 2-FR, 3-IT
  })

  /*
  Description: Login onto the FR server using the provided credentials and open CSSP page
  Test Steps:
    1. eCatalog top page - Click the [Login] button in the upper right corner of the page and log in with a common account to all local offices.
    2. Assert logged with correct username is displayed
    3. Open CSSP page and confirm the page is loaded
  */

  it('CSSP - e-Billing [Email subscription list entry - UK]', { baseUrl: url_uk + '/my/dashboard/e-billing.html' }, () => {
    ebilling.clearEmailFieldAtTheBeginning()
    ebilling.emailInputField().type(`test${randInt}@blubito.com {enter}`)
    cy.get('div').should('contain.text', `test${randInt}@blubito.com`)
    ebilling.saveEBilling(0, 'be.visible') //0-UK, 1-DE, 2-FR, 3-IT
  })

  it('CSSP - e-Billing [Email subscription list entry - DE]', { baseUrl: url_de + '/my/dashboard/e-billing.html' }, () => {
    ebilling.clearEmailFieldAtTheBeginning()
    ebilling.emailInputField().type(`test${randInt}@blubito.com {enter}`)
    cy.get('div').should('contain.text', `test${randInt}@blubito.com`)
    ebilling.saveEBilling(1, 'be.visible') //0-UK, 1-DE, 2-FR, 3-IT
  })

  it('CSSP - e-Billing [Email subscription list entry - FR]', { baseUrl: url_fr + '/my/dashboard/e-billing.html' }, () => {
    ebilling.clearEmailFieldAtTheBeginning()
    ebilling.emailInputField().type(`test${randInt}@blubito.com {enter}`)
    cy.get('div').should('contain.text', `test${randInt}@blubito.com`)
    ebilling.saveEBilling(2, 'be.visible') //0-UK, 1-DE, 2-FR, 3-IT
  })

  it('CSSP - e-Billing [Email subscription list entry - IT]', { baseUrl: url_it + '/my/dashboard/e-billing.html' }, () => {
    ebilling.clearEmailFieldAtTheBeginning()
    ebilling.emailInputField().type(`test${randInt}@blubito.com {enter}`)
    cy.get('div').should('contain.text', `test${randInt}@blubito.com`)
    ebilling.saveEBilling(3, 'be.visible') //0-UK, 1-DE, 2-FR, 3-IT
  })

  /*
  Description: Login onto the FR server using the provided credentials and open CSSP page
  Test Steps:
  1. eCatalog top page - Click the [Login] button in the upper right corner of the page and log in with a common account to all local offices.
  2. Assert logged with correct username is displayed
  3. Open CSSP page and confirm the page is loaded
  */
  it('CSSP - e-Billing [Email subscription list max entries - UK]', { baseUrl: url_uk + '/my/dashboard/e-billing.html' }, () => {
    ebilling.enterMoreThanThreeEmails(0, 'exist') //0-UK, 1-DE, 2-FR, 3-IT
    //ebilling.saveEBilling(0, 'exist')
  })

  it('CSSP - e-Billing [Email subscription list max entries - DE]', { baseUrl: url_de + '/my/dashboard/e-billing.html' }, () => {
    ebilling.enterMoreThanThreeEmails(1, 'exist') //0-UK, 1-DE, 2-FR, 3-IT
    //ebilling.saveEBilling(1, 'exist')
  })

  it('CSSP - e-Billing [Email subscription list max entries - FR]', { baseUrl: url_fr + '/my/dashboard/e-billing.html' }, () => {
    ebilling.enterMoreThanThreeEmails(2, 'exist') //0-UK, 1-DE, 2-FR, 3-IT
    //ebilling.saveEBilling(2, 'exist')
  })

  it('CSSP - e-Billing [Email subscription list max entries - IT]', { baseUrl: url_it + '/my/dashboard/e-billing.html' }, () => {
    ebilling.enterMoreThanThreeEmails(3, 'exist') //0-UK, 1-DE, 2-FR, 3-IT
    //ebilling.saveEBilling(3, 'exist')
  })

  /*
  Description: Login onto the FR server using the provided credentials and open CSSP page
  Test Steps:
    1. eCatalog top page - Click the [Login] button in the upper right corner of the page and log in with a common account to all local offices.
    2. Assert logged with correct username is displayed
    3. Open CSSP page and confirm the page is loaded
  */

  it('CSSP - e-Billing [Email subscription list remove entry - UK]', { baseUrl: url_uk + '/my/dashboard/e-billing.html' }, () => {
    ebilling.enterMoreThanOneEmail(2)
    ebilling.removeEmails(2) //the number in the () must be the same as in the above function
    ebilling.saveEBilling(0, 'be.visible') //0-UK, 1-DE, 2-FR, 3-IT
  })

  it('CSSP - e-Billing [Email subscription list remove entry - DE]', { baseUrl: url_de + '/my/dashboard/e-billing.html' }, () => {
    ebilling.enterMoreThanOneEmail(2)
    ebilling.removeEmails(2) //the number in the () must be the same as in the above function
    ebilling.saveEBilling(1, 'be.visible') //0-UK, 1-DE, 2-FR, 3-IT
  })

  it('CSSP - e-Billing [Email subscription list remove entry - FR]', { baseUrl: url_fr + '/my/dashboard/e-billing.html' }, () => {
    ebilling.enterMoreThanOneEmail(2)
    ebilling.removeEmails(2) //the number in the () must be the same as in the above function
    ebilling.saveEBilling(2, 'be.visible') //0-UK, 1-DE, 2-FR, 3-IT
  })

  it('CSSP - e-Billing [Email subscription list remove entry - IT]', { baseUrl: url_it + '/my/dashboard/e-billing.html' }, () => {
    ebilling.enterMoreThanOneEmail(2)
    ebilling.removeEmails(2) //the number in the () must be the same as in the above function
    ebilling.saveEBilling(3, 'be.visible') //0-UK, 1-DE, 2-FR, 3-IT
  })

  /*
  Description: Login onto the FR server using the provided credentials and open CSSP page
  Test Steps:
    1. eCatalog top page - Click the [Login] button in the upper right corner of the page and log in with a common account to all local offices.
    2. Assert logged with correct username is displayed
    3. Open CSSP page and confirm the page is loaded
  */

  it('CSSP - e-Billing [Email subscription list validation - UK]', { baseUrl: url_uk + '/my/dashboard/e-billing.html' }, () => {
    ebilling.clearEmailFieldAtTheBeginning()
    ebilling.emailInputField().type(`testauto {enter}`)
    ebilling.invalidEmailMsgFn(0) //0-UK, 1-DE, 2-FR, 3-IT
  })

  it('CSSP - e-Billing [Email subscription list validation - DE]', { baseUrl: url_de + '/my/dashboard/e-billing.html' }, () => {
    ebilling.clearEmailFieldAtTheBeginning()
    ebilling.emailInputField().type(`testauto {enter}`)
    ebilling.invalidEmailMsgFn(1) //0-UK, 1-DE, 2-FR, 3-IT
  })

  it('CSSP - e-Billing [Email subscription list validation - FR]', { baseUrl: url_fr + '/my/dashboard/e-billing.html' }, () => {
    ebilling.clearEmailFieldAtTheBeginning()
    ebilling.emailInputField().type(`testauto {enter}`)
    ebilling.invalidEmailMsgFn(2) //0-UK, 1-DE, 2-FR, 3-IT
  })

  it('CSSP - e-Billing [Email subscription list validation - IT]', { baseUrl: url_it + '/my/dashboard/e-billing.html' }, () => {
    ebilling.clearEmailFieldAtTheBeginning()
    ebilling.emailInputField().type(`testauto {enter}`)
    ebilling.invalidEmailMsgFn(3) //0-UK, 1-DE, 2-FR, 3-IT
  })

  /*
  Description: Login onto the FR server using the provided credentials and open CSSP page
  Test Steps:
    1. eCatalog top page - Click the [Login] button in the upper right corner of the page and log in with a common account to all local offices.
    2. Assert logged with correct username is displayed
    3. Open CSSP page and confirm the page is loaded
  */

  it('CSSP - e-Billing [Cancel changes - UK]', { baseUrl: url_uk + '/my/dashboard/e-billing.html' }, () => {
    ebilling.clearEmailFieldAtTheBeginning()
    ebilling.enterMoreThanOneEmail(2)
    ebilling.cancelSubscriptionBtn().click()
    ebilling.existingEmailInTheField()
  })

  it('CSSP - e-Billing [Cancel changes - DE]', { baseUrl: url_de + '/my/dashboard/e-billing.html' }, () => {
    ebilling.clearEmailFieldAtTheBeginning()
    ebilling.enterMoreThanOneEmail(2)
    ebilling.cancelSubscriptionBtn().click()
    ebilling.existingEmailInTheField()
  })

  it('CSSP - e-Billing [Cancel changes - FR]', { baseUrl: url_fr + '/my/dashboard/e-billing.html' }, () => {
    ebilling.clearEmailFieldAtTheBeginning()
    ebilling.enterMoreThanOneEmail(2)
    ebilling.cancelSubscriptionBtn().click()
    ebilling.existingEmailInTheField()
  })

  it('CSSP - e-Billing [Cancel changes - IT]', { baseUrl: url_it + '/my/dashboard/e-billing.html' }, () => {
    ebilling.clearEmailFieldAtTheBeginning()
    ebilling.enterMoreThanOneEmail(2)
    ebilling.cancelSubscriptionBtn().click()
    ebilling.existingEmailInTheField()
  })

  /*
  Description: Login onto the FR server using the provided credentials and open CSSP page
  Test Steps:
    1. eCatalog top page - Click the [Login] button in the upper right corner of the page and log in with a common account to all local offices.
    2. Assert logged with correct username is displayed
    3. Open CSSP page and confirm the page is loaded
  */

  it('CSSP - e-Billing [Disable subscription - UK]', { baseUrl: url_uk + '/my/dashboard/e-billing.html' }, () => {
    ebilling.noSubscriptionBtn().click()
    ebilling.noSubscriptionBtn().should("have.css", "background-color", "rgb(37, 74, 149)")
    ebilling.emailInputField().parent().should("have.css", "background-color", "rgb(239, 239, 239)")
  })

  it('CSSP - e-Billing [Disable subscription - DE]', { baseUrl: url_de + '/my/dashboard/e-billing.html' }, () => {
    ebilling.noSubscriptionBtn().click()
    ebilling.noSubscriptionBtn().should("have.css", "background-color", "rgb(37, 74, 149)")
    ebilling.emailInputField().parent().should("have.css", "background-color", "rgb(239, 239, 239)")
  })

  it('CSSP - e-Billing [Disable subscription - FR]', { baseUrl: url_fr + '/my/dashboard/e-billing.html' }, () => {
    ebilling.noSubscriptionBtn().click()
    ebilling.noSubscriptionBtn().should("have.css", "background-color", "rgb(37, 74, 149)")
    ebilling.emailInputField().parent().should("have.css", "background-color", "rgb(239, 239, 239)")
  })

  it('CSSP - e-Billing [Disable subscription - IT]', { baseUrl: url_it + '/my/dashboard/e-billing.html' }, () => {
    ebilling.noSubscriptionBtn().click()
    ebilling.noSubscriptionBtn().should("have.css", "background-color", "rgb(37, 74, 149)")
    ebilling.emailInputField().parent().should("have.css", "background-color", "rgb(239, 239, 239)")
    ebilling.clearEmailFieldAtTheBeginning()
  })

});
