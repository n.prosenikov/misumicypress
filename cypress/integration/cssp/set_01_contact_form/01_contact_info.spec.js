import Login from "../../boc/pom/login"
import Dashboard from '../pom/dashboard';
import ClaimsPage from "../pom/claims";
import CsspPage from "../pom/cssp";
import MyPage from "../../boc/pom/myPage";

const loginPage = new Login()
const dashboard = new Dashboard()
const claims = new ClaimsPage()
const csspPage = new CsspPage()
const myPage = new MyPage()

const authUser = require('../../../fixtures/auth-user.json');
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('Contact Phone/Email information', () => {
    beforeEach(() => {
        loginPage.login(username, password)
        dashboard.goToTheLink()
        claims.contactBtnSelector().click()
    });

    it('Contact Form - Email [UK]', { baseUrl: url_uk }, () => {
        csspPage.contactServiceEmailBtn().should('be.visible')
        csspPage.contactServiceEmailBtn().should("have.css", "background-color", "rgb(37, 74, 149)")

        csspPage.goToHomeBtn().click()
        myPage.headerTitle().should('contain.text', "Welcome") //check if I am returned correctly back
    })

    it('Contact Form - Email [DE]', { baseUrl: url_de }, () => {
        csspPage.contactServiceEmailBtn().should('be.visible')
        csspPage.contactServiceEmailBtn().should("have.css", "background-color", "rgb(37, 74, 149)")

        csspPage.goToHomeBtn().click()
        myPage.headerTitle().should('contain.text', "Willkommen") //check if I am returned correctly back
    })

    it('Contact Form - Email [FR]', { baseUrl: url_fr }, () => {
        csspPage.contactServiceEmailBtn().should('be.visible')
        csspPage.contactServiceEmailBtn().should("have.css", "background-color", "rgb(37, 74, 149)")

        csspPage.goToHomeBtn().click()
        myPage.headerTitle().should('contain.text', "Bienvenue") //check if I am returned correctly back
    })

    it('Contact Form - Email [IT]', { baseUrl: url_it }, () => {
        csspPage.contactServiceEmailBtn().should('be.visible')
        csspPage.contactServiceEmailBtn().should("have.css", "background-color", "rgb(37, 74, 149)")

        csspPage.goToHomeBtn().click()
        myPage.headerTitle().should('contain.text', "Benvenuti") //check if I am returned correctly back
    })

    it('Contact Form - Phone [UK]', { baseUrl: url_uk }, () => {
        csspPage.phoneBtnSelector().click()
        csspPage.phoneBtnSelector().should("have.css", "background-color", "rgb(37, 74, 149)")
        csspPage.labelOfInputFieldSelector().should('contain.text', "Enter your phone number")
    })

    it('Contact Form - Phone [DE]', { baseUrl: url_de }, () => {
        csspPage.phoneBtnSelector().click()
        csspPage.phoneBtnSelector().should("have.css", "background-color", "rgb(37, 74, 149)")
        csspPage.labelOfInputFieldSelector().should('contain.text', "Geben Sie Ihre Telefonnummer")
    })

    it('Contact Form - Phone [FR]', { baseUrl: url_fr }, () => {
        csspPage.phoneBtnSelector().click()
        csspPage.phoneBtnSelector().should("have.css", "background-color", "rgb(37, 74, 149)")
        csspPage.labelOfInputFieldSelector().should('contain.text', "Saisissez votre numéro de téléphone")
    })

    it('Contact Form - Phone [IT]', { baseUrl: url_it }, () => {
        csspPage.phoneBtnSelector().click()
        csspPage.phoneBtnSelector().should("have.css", "background-color", "rgb(37, 74, 149)")
        csspPage.labelOfInputFieldSelector().should('contain.text', "Inserire il proprio numero di telefono")
    })

    it('Contact Form - Contact Information  [UK]', { baseUrl: url_uk }, () => {
        csspPage.contactServiceEmailBtn().should('be.visible')
        csspPage.phoneBtnSelector().should('be.visible')
        csspPage.contactInformationUserId().should('contain.text', authUser.name)
    })

    it('Contact Form - Contact Information  [DE]', { baseUrl: url_de }, () => {
        csspPage.contactServiceEmailBtn().should('be.visible')
        csspPage.phoneBtnSelector().should('be.visible')
        csspPage.contactInformationUserId().should('contain.text', authUser.name)
    })

    it('Contact Form - Contact Information  [FR]', { baseUrl: url_fr }, () => {
        csspPage.contactServiceEmailBtn().should('be.visible')
        csspPage.phoneBtnSelector().should('be.visible')
        csspPage.contactInformationUserId().should('contain.text', authUser.name)
    })

    it('Contact Form - Contact Information  [IT]', { baseUrl: url_it }, () => {
        csspPage.contactServiceEmailBtn().should('be.visible')
        csspPage.phoneBtnSelector().should('be.visible')
        csspPage.contactInformationUserId().should('contain.text', authUser.name)
    })

    it('Contact Form - Back to Dashboard [UK]', { baseUrl: url_uk }, () => {
        csspPage.contactServiceEmailBtn().should('be.visible')
        csspPage.phoneBtnSelector().should('be.visible')

        csspPage.goToHomeBtn().click()
        myPage.headerTitle().should('contain.text', "Welcome") //check if I am returned correctly back
    })

    it('Contact Form - Back to Dashboard [DE]', { baseUrl: url_de }, () => {
        csspPage.contactServiceEmailBtn().should('be.visible')
        csspPage.phoneBtnSelector().should('be.visible')

        csspPage.goToHomeBtn().click()
        myPage.headerTitle().should('contain.text', "Willkommen") //check if I am returned correctly back
    })

    it('Contact Form - Back to Dashboard [FR]', { baseUrl: url_fr }, () => {
        csspPage.contactServiceEmailBtn().should('be.visible')
        csspPage.phoneBtnSelector().should('be.visible')

        csspPage.goToHomeBtn().click()
        myPage.headerTitle().should('contain.text', "Bienvenue") //check if I am returned correctly back
    })

    it('Contact Form - Back to Dashboard [IT]', { baseUrl: url_it }, () => {
        csspPage.contactServiceEmailBtn().should('be.visible')
        csspPage.phoneBtnSelector().should('be.visible')

        csspPage.goToHomeBtn().click()
        myPage.headerTitle().should('contain.text', "Benvenuti") //check if I am returned correctly back
    })
})