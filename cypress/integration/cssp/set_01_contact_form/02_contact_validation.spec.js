import Login from "../../boc/pom/login"
import Dashboard from '../pom/dashboard';
import ClaimsPage from "../pom/claims";

const loginPage = new Login()
const dashboard = new Dashboard()
const claims = new ClaimsPage()

const authUser = require('../../../fixtures/auth-user.json');
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('Contact Phone/Email Validation', () => {
    beforeEach(() => {
        loginPage.login(username, password)
        dashboard.goToTheLink()
        claims.contactBtnSelector().click()
    });

    it('Contact Form - Phone validation [UK]', { baseUrl: url_uk }, () => {
        claims.phoneBtnSelector().click()
        claims.typeEmailOrPhoneNumberFn("+44 111 111")

        claims.confirmIconSelector().should("be.visible") //valid phone ed option
        claims.invalidEmailTooltip("Please enter a valid phone number").should('not.exist')
    })

    it('Contact Form - Phone validation [DE]', { baseUrl: url_de }, () => {
        claims.phoneBtnSelector().click()
        claims.typeEmailOrPhoneNumberFn("+49 111 111")

        claims.confirmIconSelector().should("be.visible") //valid phone ed option
        claims.invalidEmailTooltip("Bitte geben Sie eine gültige Telefonnummer ein").should('not.exist')
    })

    it('Contact Form - Phone validation [FR]', { baseUrl: url_fr }, () => {
        claims.phoneBtnSelector().click()
        claims.typeEmailOrPhoneNumberFn("+33 111 111")

        claims.confirmIconSelector().should("be.visible") //valid phone ed option
        claims.invalidEmailTooltip("Veuillez saisir un numéro de téléphone valide").should('not.exist')
    })

    it('Contact Form - Phone validation [IT]', { baseUrl: url_it }, () => {
        claims.phoneBtnSelector().click()
        claims.typeEmailOrPhoneNumberFn("+39 111 111")

        claims.confirmIconSelector().should("be.visible") //valid phone ed option
        claims.invalidEmailTooltip("Inserire un numero di telefono valido").should('not.exist')
    })

    it('Contact Form - Phone invalid [UK]', { baseUrl: url_uk }, () => {
        claims.phoneBtnSelector().click()
        claims.typeEmailOrPhoneNumberFn("+44 111Test")

        claims.confirmIconSelector().should("not.exist") //valid phone ed option
        claims.invalidEmailTooltip("Please enter a valid phone number").should('be.visible')
    })

    it('Contact Form - Phone invalid [DE]', { baseUrl: url_de }, () => {
        claims.phoneBtnSelector().click()
        claims.typeEmailOrPhoneNumberFn("+49 111Test")

        claims.confirmIconSelector().should("not.exist") //valid phone ed option
        claims.invalidEmailTooltip("Bitte geben Sie eine gültige Telefonnummer ein").should('be.visible')
    })

    it('Contact Form - Phone invalid [FR]', { baseUrl: url_fr }, () => {
        claims.phoneBtnSelector().click()
        claims.typeEmailOrPhoneNumberFn("+33 111Test")

        claims.confirmIconSelector().should("not.exist") //valid phone ed option
        claims.invalidEmailTooltip("Veuillez saisir un numéro de téléphone valide").should('be.visible')
    })

    it('Contact Form - Phone invalid [IT]', { baseUrl: url_it }, () => {
        claims.phoneBtnSelector().click()
        claims.typeEmailOrPhoneNumberFn("+39 111Test")

        claims.confirmIconSelector().should("not.exist") //valid phone ed option
        claims.invalidEmailTooltip("Inserire un numero di telefono valido").should('be.visible')
    })

    it('Contact Form - Email validation [UK]', { baseUrl: url_uk }, () => {
        claims.typeEmailOrPhoneNumberFn("test@test.com")

        claims.confirmIconSelector().should("be.visible") //valid phone ed option
        claims.invalidEmailTooltip("Please enter a valid e-mail address.").should('not.exist')
    })

    it('Contact Form - Email validation [DE]', { baseUrl: url_de }, () => {
        claims.typeEmailOrPhoneNumberFn("test@test.com")

        claims.confirmIconSelector().should("be.visible") //valid phone ed option
        claims.invalidEmailTooltip("Bitte geben Sie eine gültige E-Mail Adresse ein.").should('not.exist')
    })

    it('Contact Form - Email validation [FR]', { baseUrl: url_fr }, () => {
        claims.typeEmailOrPhoneNumberFn("test@test.com")

        claims.confirmIconSelector().should("be.visible") //valid phone ed option
        claims.invalidEmailTooltip("Veuillez saisir une adresse e-mail valide.").should('not.exist')
    })

    it('Contact Form - Email validation [IT]', { baseUrl: url_it }, () => {
        claims.typeEmailOrPhoneNumberFn("test@test.com")

        claims.confirmIconSelector().should("be.visible") //valid phone ed option
        claims.invalidEmailTooltip("Inserire un indirizzo e-mail valido.").should('not.exist')
    })

    it('Contact Form - Email invalid [UK]', { baseUrl: url_uk }, () => {
        claims.typeEmailOrPhoneNumberFn("test@test.comTest")

        claims.confirmIconSelector().should("not.exist") //valid phone ed option
        claims.invalidEmailTooltip("Please enter a valid e-mail address.").should('be.visible')
    })

    it('Contact Form - Email invalid [DE]', { baseUrl: url_de }, () => {
        claims.typeEmailOrPhoneNumberFn("test@test.comTest")

        claims.confirmIconSelector().should("not.exist") //valid phone ed option
        claims.invalidEmailTooltip("Bitte geben Sie eine gültige E-Mail Adresse ein.").should('be.visible')
    })

    it('Contact Form - Email invalid [FR]', { baseUrl: url_fr }, () => {
        claims.typeEmailOrPhoneNumberFn("test@test.comTest")

        claims.confirmIconSelector().should("not.exist") //valid phone ed option
        claims.invalidEmailTooltip("Veuillez saisir une adresse e-mail valide.").should('be.visible')
    })

    it('Contact Form - Email invalid [IT]', { baseUrl: url_it }, () => {
        claims.typeEmailOrPhoneNumberFn("test@test.comTest")

        claims.confirmIconSelector().should("not.exist") //valid phone ed option
        claims.invalidEmailTooltip("Inserire un indirizzo e-mail valido.").should('be.visible')
    })
})