import CsspPage from '../pom/cssp';
import Login from "../../boc/pom/login"
import ClaimsPage from '../pom/claims';

const loginPage = new Login()
const csspPage = new CsspPage()
const claimsPage = new ClaimsPage()


const authUser = require('../../../fixtures/auth-user.json');
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('General Documents ', () => {
    beforeEach(() => {
        loginPage.login(username, password)
    });

    it('Open First General Document in a new tab [UK]', { baseUrl: url_uk }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.checkDocumentOpenedInNewTab(1) //check if the document will be opened in a ne w tab+ if the link is live 
    })

    it('Open First General Document in a new tab [DE]', { baseUrl: url_de }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.checkDocumentOpenedInNewTab(1) //check if the document will be opened in a new tab + if the link is live 

    })

    it('Open First General Document in a new tab [FR]', { baseUrl: url_fr }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.checkDocumentOpenedInNewTab(1) //check if the document will be opened in a new tab + if the link is live 

    })

    it('Open First General Document in a new tab [IT]', { baseUrl: url_it }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.checkDocumentOpenedInNewTab(1) //check if the document will be opened in a new tab + if the link is live 

    })

    it('Open Second General Document in a new tab [UK]', { baseUrl: url_uk }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.checkDocumentOpenedInNewTab(2)//check if the document will be opened in a new tab + if the link is live
    })

    it('Open Second General Document in a new tab [DE]', { baseUrl: url_de }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.checkDocumentOpenedInNewTab(2)//check if the document will be opened in a new tab + if the link is live
    })

    it('Open Second General Document in a new tab [FR]', { baseUrl: url_fr }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.checkDocumentOpenedInNewTab(2)//check if the document will be opened in a new tab + if the link is live
    })

    it('Open Second General Document in a new tab [IT]', { baseUrl: url_it }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.checkDocumentOpenedInNewTab(2)//check if the document will be opened in a new tab + if the link is live
    })

    it('Open Third General Document in a new tab [UK]', { baseUrl: url_uk }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.checkDocumentOpenedInNewTab(3)//check if the document will be opened in a new tab + if the link is live
    })

    it('Open Third General Document in a new tab [DE]', { baseUrl: url_de }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.checkDocumentOpenedInNewTab(3)//check if the document will be opened in a new tab + if the link is live
    })

    it('Open Third General Document in a new tab [FR]', { baseUrl: url_fr }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.checkDocumentOpenedInNewTab(3)//check if the document will be opened in a new tab + if the link is live
    })

    it('Open Third General Document in a new tab [IT]', { baseUrl: url_it }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.checkDocumentOpenedInNewTab(3)//check if the document will be opened in a new tab + if the link is live
    })
    
    it('Open Fourth General Document in a new tab [UK]', { baseUrl: url_uk }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.checkDocumentOpenedInNewTab(4)//check if the document will be opened in a new tab + if the link is live
    })

    it('Open Fourth General Document in a new tab [DE]', { baseUrl: url_de }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.checkDocumentOpenedInNewTab(4)//check if the document will be opened in a new tab + if the link is live
    })

    it('Open Fourth General Document in a new tab [FR]', { baseUrl: url_fr }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.checkDocumentOpenedInNewTab(4)//check if the document will be opened in a new tab + if the link is live
    })

    it('Open Fourth General Document in a new tab [IT]', { baseUrl: url_it }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.checkDocumentOpenedInNewTab(4)//check if the document will be opened in a new tab + if the link is live
    })

})