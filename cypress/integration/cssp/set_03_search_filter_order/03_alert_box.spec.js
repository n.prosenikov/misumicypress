import Login from "../../boc/pom/login"
import OrderAndFilter from "../pom/orderAndFilter";

const loginPage = new Login()
const orderFilter = new OrderAndFilter()

const authUser = require('../../../fixtures/auth-user.json');
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('Alert button and message by Order Overview', () => {
    beforeEach(() => {
        loginPage.login(username, password)
        cy.visit('/my/dashboard/orders.html')
    });

    it('Alert Box by first opening of the page [UK]', { baseUrl: url_uk }, () => {
        orderFilter.openedAlertBoxClose(orderFilter.alertOrderOverviewHeaders[0]) //0-UK, 1-DE, 2-FR, 3-IT
    })

    it('Alert Box by first opening of the page [DE]', { baseUrl: url_de }, () => {
        orderFilter.openedAlertBoxClose(orderFilter.alertOrderOverviewHeaders[1]) //0-UK, 1-DE, 2-FR, 3-IT
    })

    it('Alert Box by first opening of the page [FR]', { baseUrl: url_fr }, () => {
        orderFilter.openedAlertBoxClose(orderFilter.alertOrderOverviewHeaders[2]) //0-UK, 1-DE, 2-FR, 3-IT
    })

    it('Alert Box by first opening of the page [IT]', { baseUrl: url_it }, () => {
        orderFilter.openedAlertBoxClose(orderFilter.alertOrderOverviewHeaders[3]) //0-UK, 1-DE, 2-FR, 3-IT
    })


    it('Alert Box by refresh [UK]', { baseUrl: url_uk }, () => {
        orderFilter.openedAlertBoxClose(orderFilter.alertOrderOverviewHeaders[0]) //0-UK, 1-DE, 2-FR, 3-IT
        cy.reload(true)
        orderFilter.alertTooltipMsgBox().should('be.visible').and('contain.text', orderFilter.alertOrderOverviewHeaders[0])
    })

    it('Alert Box by refresh [DE]', { baseUrl: url_de }, () => {
        orderFilter.openedAlertBoxClose(orderFilter.alertOrderOverviewHeaders[1]) //0-UK, 1-DE, 2-FR, 3-IT
        cy.reload(true)
        orderFilter.alertTooltipMsgBox().should('be.visible').and('contain.text', orderFilter.alertOrderOverviewHeaders[1])
    })

    it('Alert Box by refresh [FR]', { baseUrl: url_fr }, () => {
        orderFilter.openedAlertBoxClose(orderFilter.alertOrderOverviewHeaders[2]) //0-UK, 1-DE, 2-FR, 3-IT
        cy.reload(true)
        orderFilter.alertTooltipMsgBox().should('be.visible').and('contain.text', orderFilter.alertOrderOverviewHeaders[2])
    })

    it('Alert Box by refresh [IT]', { baseUrl: url_it }, () => {
        orderFilter.openedAlertBoxClose(orderFilter.alertOrderOverviewHeaders[3]) //0-UK, 1-DE, 2-FR, 3-IT
        cy.reload(true)
        orderFilter.alertTooltipMsgBox().should('be.visible').and('contain.text', orderFilter.alertOrderOverviewHeaders[3])
    })


    it('Open Alert Box and check link in Tooltip [UK]', { baseUrl: url_uk }, () => {
        orderFilter.alertTooltipLink().invoke('attr', 'href').then(href => {
            cy.visit(href)
            cy.url().should('include', "TEMP_AUTH_SO_HISTORY")
        })
    })

    it('Open Alert Box and check link in Tooltip [DE]', { baseUrl: url_de }, () => {
        orderFilter.alertTooltipLink().invoke('attr', 'href').then(href => {
            cy.visit(href)
            cy.url().should('include', "TEMP_AUTH_SO_HISTORY")
        })
    })

    it('Open Alert Box and check link in Tooltip [FR]', { baseUrl: url_fr }, () => {
        orderFilter.alertTooltipLink().invoke('attr', 'href').then(href => {
            cy.visit(href)
            cy.url().should('include', "TEMP_AUTH_SO_HISTORY")
        })
    })

    it('Open Alert Box and check link in Tooltip [IT]', { baseUrl: url_it }, () => {
        orderFilter.alertTooltipLink().invoke('attr', 'href').then(href => {
            cy.visit(href)
            cy.url().should('include', "TEMP_AUTH_SO_HISTORY")
        })
    })

})