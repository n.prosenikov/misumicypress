import Login from "../../boc/pom/login"
import Dashboard from '../pom/dashboard';
import ClaimsPage from "../pom/claims";
import OrderAndFilter from "../pom/orderAndFilter";

const dashboard = new Dashboard()
const loginPage = new Login()
const claimsPage = new ClaimsPage()
const orderFilter = new OrderAndFilter()


const authUser = require('../../../fixtures/auth-user.json');
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('Search with none filter', () => {
    beforeEach(() => {
        loginPage.login(username, password)
        dashboard.goToTheLink()
        claimsPage.ordersOverviewBtnSelector().click()
    });

    it('Orders - Search with none option [UK]', { baseUrl: url_uk }, () => {
        orderFilter.searchAndFilterFn(20)
    })

    it('Orders - Search with none option [DE]', { baseUrl: url_de }, () => {
        orderFilter.searchAndFilterFn(20)
    })

    it('Orders - Search with none option [FR]', { baseUrl: url_fr }, () => {
        orderFilter.searchAndFilterFn(20)
    })

    it('Orders - Search with none option [IT]', { baseUrl: url_it }, () => {
        orderFilter.searchAndFilterFn(20)
    })

})