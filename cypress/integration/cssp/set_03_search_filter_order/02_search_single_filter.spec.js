import Login from "../../boc/pom/login"
import Dashboard from '../pom/dashboard';
import ClaimsPage from "../pom/claims";
import OrderAndFilter from "../pom/orderAndFilter";

const dashboard = new Dashboard()
const loginPage = new Login()
const claimsPage = new ClaimsPage()
const orderFilter = new OrderAndFilter()

const numberOfEntities = 20 //number of elements per page

const authUser = require('../../../fixtures/auth-user.json');
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('Search with single filter', () => {
    beforeEach(() => {
        loginPage.login(username, password)
        dashboard.goToTheLink()
        claimsPage.ordersOverviewBtnSelector().click()
    });

    it('Orders - Search with single option [UK]', { baseUrl: url_uk }, () => {
        orderFilter.chooseFilterFn("complete", "Complete")
    })

    it('Orders - Search with single option [DE]', { baseUrl: url_de }, () => {
        orderFilter.chooseFilterFn('complete', "Complete")
    })

    it('Orders - Search with single option [FR]', { baseUrl: url_fr }, () => {
        orderFilter.chooseFilterFn('complete', "Complete")
    })

    it('Orders - Search with single option [IT]', { baseUrl: url_it }, () => {
        orderFilter.chooseFilterFn('complete', "Complete")
    })

    it('Orders - Search with selected date option [UK]', { baseUrl: url_uk }, () => {
        orderFilter.chooseDateFn(numberOfEntities)
        orderFilter.checkBoxOption(1).click()
        orderFilter.downloadSelectedInvoicesBtn().should("have.css", "background-color", "rgb(255, 255, 255)")
        claimsPage.openClaimsBtnSelector().should("have.css", "background-color", "rgb(255, 255, 255)")
    })

    it('Orders - Search with selected date option [DE]', { baseUrl: url_de }, () => {
        orderFilter.chooseDateFn(numberOfEntities)
        orderFilter.checkBoxOption(1).click()
        orderFilter.downloadSelectedInvoicesBtn().should("have.css", "background-color", "rgb(255, 255, 255)")
        claimsPage.openClaimsBtnSelector().should("have.css", "background-color", "rgb(255, 255, 255)")
    })

    it('Orders - Search with selected date option [FR]', { baseUrl: url_fr }, () => {
        orderFilter.chooseDateFn(numberOfEntities)
        orderFilter.checkBoxOption(1).click()
        orderFilter.downloadSelectedInvoicesBtn().should("have.css", "background-color", "rgb(255, 255, 255)")
        //claimsPage.openClaimsBtnSelector().should("have.css", "background-color", "rgb(255, 255, 255)")
    })

    it('Orders - Search with selected date option [IT]', { baseUrl: url_it }, () => {
        orderFilter.chooseDateFn(numberOfEntities)
        orderFilter.checkBoxOption(1).click()
        orderFilter.downloadSelectedInvoicesBtn().should("have.css", "background-color", "rgb(255, 255, 255)")
        //claimsPage.openClaimsBtnSelector().should("have.css", "background-color", "rgb(255, 255, 255)")
    })

})