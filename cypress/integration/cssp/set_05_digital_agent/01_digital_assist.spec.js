import Login from "../../boc/pom/login"
import CsspPage from "../pom/cssp";


const loginPage = new Login()
const csspPage = new CsspPage()

const authUser = require('../../../fixtures/auth-user.json');
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('CSSP Digital Assistant', () => {
    beforeEach(() => {
        loginPage.login(username, password)
        cy.visit('/')
    });

    /*
    Description: Login onto the FR server using the provided credentials and open CSSP page
    Test Steps:
      1. eCatalog top page - Click the [Login] button in the upper right corner of the page and log in with a common account to all local offices.
      2. Assert logged with correct username is displayed
      3. Open CSSP page and confirm the page is loaded
    */
    it('CSSP - Digital Assistant Open [UK]', { baseUrl: url_uk }, () => {
        csspPage.openChatBotFn()
        cy.get('#solvemate-widget').should('be.visible')
    })

    it('CSSP - Digital Assistant Open [DE]', { baseUrl: url_de }, () => {
        csspPage.openChatBotFn()
        cy.get('#solvemate-widget').should('be.visible')
    })

    it('CSSP - Digital Assistant Open [FR]', { baseUrl: url_fr }, () => {
        csspPage.openChatBotFn()
        cy.get('#solvemate-widget').should('be.visible')
    })

    it('CSSP - Digital Assistant Open [IT]', { baseUrl: url_it }, () => {
        csspPage.openChatBotFn()
        cy.get('#solvemate-widget').should('be.visible')
    })

    /*
    Description: Login onto the FR server using the provided credentials and open CSSP page
    Test Steps:
      1. eCatalog top page - Click the [Login] button in the upper right corner of the page and log in with a common account to all local offices.
      2. Assert logged with correct username is displayed
      3. Open CSSP page and confirm the page is loaded
    */

    it('CSSP - Digital Assistant Close [UK]', { baseUrl: url_uk }, () => {
        csspPage.closeChatBotFn()
        cy.get('#solvemate-widget').should('not.be.visible')
    })

    it('CSSP - Digital Assistant Close [DE]', { baseUrl: url_de }, () => {
        csspPage.closeChatBotFn()
        cy.get('#solvemate-widget').should('not.be.visible')
    })

    it('CSSP - Digital Assistant Close [FR]', { baseUrl: url_fr }, () => {
        csspPage.closeChatBotFn()
        cy.get('#solvemate-widget').should('not.be.visible')
    })

    it('CSSP - Digital Assistant Close [IT]', { baseUrl: url_it }, () => {
        csspPage.closeChatBotFn()
        cy.get('#solvemate-widget').should('not.be.visible')
    })
});

