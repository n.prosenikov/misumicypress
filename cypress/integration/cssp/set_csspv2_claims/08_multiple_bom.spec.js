import CsspPage from '../pom/cssp';
import Login from "../../boc/pom/login"
import ClaimsPage from '../pom/claims';

const loginPage = new Login()
const csspPage = new CsspPage()
const claimsPage = new ClaimsPage()

const authUser = require('../../../fixtures/auth-user.json');
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;


describe('Claim with multiple selected BOM and one attached file option', () => {
    beforeEach(() => {
        loginPage.login(username, password)
    });

    it('Web to case without selected BOM w/ single attachment [UK]', { baseUrl: url_uk }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        //path via Order Overview, select an order, press Open Claim button
        claimsPage.searchOrderFn('LN23204420G2')

        claimsPage.checkBoxSelector().click()
        claimsPage.openClaimsBtnSelector().click()

        claimsPage.typeEmailAddress("testautomation@test.com")

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()

        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //check if the right claim reason is selected

        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Your request was successfully sent') //Success Message by submit
    })

    it('Web to case without selected BOM w/ single attachment [DE]', { baseUrl: url_de }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        //path via Order Overview, select an order, press Open Claim button
        claimsPage.searchOrderFn('LN23204420G2')

        claimsPage.checkBoxSelector().click()
        claimsPage.openClaimsBtnSelector().click()

        claimsPage.typeEmailAddress("testautomation@test.com")

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //check if the right claim reason is selected

        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Ihre Anfrage wurde erfolgreich übermittelt') //Success Message by submit
    })

    it('Web to case without selected BOM w/ single attachment [FR]', { baseUrl: url_fr }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        //path via Order Overview, select an order, press Open Claim button
        claimsPage.searchOrderFn('LN23204420G2')

        claimsPage.checkBoxSelector().click()
        claimsPage.openClaimsBtnSelector().click()

        claimsPage.typeEmailAddress("testautomation@test.com")

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //check if the right claim reason is selected

        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Votre demande a été transmise avec succès') //Success Message by submit
    })

    it('Web to case without selected BOM w/ single attachment [IT]', { baseUrl: url_it }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        //path via Order Overview, select an order, press Open Claim button
        claimsPage.searchOrderFn('LN23204420G2')

        claimsPage.checkBoxSelector().click()
        claimsPage.openClaimsBtnSelector().click()

        claimsPage.typeEmailAddress("testautomation@test.com")

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //check if the right claim reason is selected

        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('La sua richiesta è stata inviata') //Success Message by submit
    })

    it('Web to case with two selected BOM w/ single attachment [UK]', { baseUrl: url_uk }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        //path via Order Overview, detail view of an order, choose a BOM, Open Product Claim
        claimsPage.searchOrderFn('LN23204420G2')
        claimsPage.orderDetailsViewBtnSelector('LN23204420G2').click()
        claimsPage.detailsRowCheckBox('GA000MIS060836').click()
        claimsPage.detailsRowCheckBox('GA000MIS060837').click()
        claimsPage.openProductClaimSelector().click()


        claimsPage.rowCheckBox('GA000MIS060836').find('path').should('have.css', 'color', 'rgb(37, 74, 149)')//check BOM still selected
        claimsPage.rowCheckBox('GA000MIS060837').find('path').should('have.css', 'color', 'rgb(37, 74, 149)')//check 2nd BOM still selected

        claimsPage.typeEmailAddress("testautomation@test.com")

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //check if the right claim reason is selected

        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Your request was successfully sent') //Success Message by submit
    })

    it('Web to case with two selected BOM w/ single attachment [DE]', { baseUrl: url_de }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        //path via Order Overview, detail view of an order, choose a BOM, Open Product Claim
        claimsPage.searchOrderFn('LN23204420G2')
        claimsPage.orderDetailsViewBtnSelector('LN23204420G2').click()
        claimsPage.detailsRowCheckBox('GA000MIS060836').click()
        claimsPage.detailsRowCheckBox('GA000MIS060837').click()
        claimsPage.openProductClaimSelector().click()

        claimsPage.rowCheckBox('GA000MIS060836').find('path').should('have.css', 'color', 'rgb(37, 74, 149)')//check BOM still selected
        claimsPage.rowCheckBox('GA000MIS060837').find('path').should('have.css', 'color', 'rgb(37, 74, 149)')//check 2nd BOM still selected

        claimsPage.typeEmailAddress("testautomation@test.com")

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //check if the right claim reason is selected

        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Ihre Anfrage wurde erfolgreich übermittelt') //Success Message by submit
    })

    it('Web to case with two selected BOM w/ single attachment [FR]', { baseUrl: url_fr }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        //path via Order Overview, detail view of an order, choose a BOM, Open Product Claim
        claimsPage.searchOrderFn('LN23204420G2')
        claimsPage.orderDetailsViewBtnSelector('LN23204420G2').click()
        claimsPage.detailsRowCheckBox('GA000MIS060836').click()
        claimsPage.detailsRowCheckBox('GA000MIS060837').click()
        claimsPage.openProductClaimSelector().click()


        claimsPage.rowCheckBox('GA000MIS060836').find('path').should('have.css', 'color', 'rgb(37, 74, 149)')//check BOM still selected
        claimsPage.rowCheckBox('GA000MIS060837').find('path').should('have.css', 'color', 'rgb(37, 74, 149)')//check 2nd BOM still selected

        claimsPage.typeEmailAddress("testautomation@test.com")

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //check if the right claim reason is selected

        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Votre demande a été transmise avec succès') //Success Message by submit
    })

    it('Web to case with two selected BOM w/ single attachment [IT]', { baseUrl: url_it }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        //path via Order Overview, detail view of an order, choose a BOM, Open Product Claim
        claimsPage.searchOrderFn('LN23204420G2')
        claimsPage.orderDetailsViewBtnSelector('LN23204420G2').click()
        claimsPage.detailsRowCheckBox('GA000MIS060836').click()
        claimsPage.detailsRowCheckBox('GA000MIS060837').click()
        claimsPage.openProductClaimSelector().click()

        claimsPage.rowCheckBox('GA000MIS060836').find('path').should('have.css', 'color', 'rgb(37, 74, 149)')//check BOM still selected
        claimsPage.rowCheckBox('GA000MIS060837').find('path').should('have.css', 'color', 'rgb(37, 74, 149)')//check 2nd BOM still selected

        claimsPage.typeEmailAddress("testautomation@test.com")

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //check if the right claim reason is selected

        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('La sua richiesta è stata inviata') //Success Message by submit
    })

    it('Web to case with three selected BOM w/ single attachment [UK]', { baseUrl: url_uk }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        //path via Raise claims
        claimsPage.raiseClaimBtnSelector().click()

        claimsPage.typeEmailAddress("testautomation@test.com")

        claimsPage.chooseOrderFieldSelector().type('LN23204420G2')
        claimsPage.chooseFirstOrderSelector(0).click()

        claimsPage.typeMessage()

        claimsPage.detailsRowCheckBox('GA000MIS060836').click()
        claimsPage.detailsRowCheckBox('GA000MIS060837').click()
        claimsPage.detailsRowCheckBox('GA000MIS060838').click()

        claimsPage.rowCheckBox('GA000MIS060836').find('path').should('have.css', 'color', 'rgb(37, 74, 149)')//check BOM still selected
        claimsPage.rowCheckBox('GA000MIS060837').find('path').should('have.css', 'color', 'rgb(37, 74, 149)')//check 2nd BOM still selected
        claimsPage.rowCheckBox('GA000MIS060838').find('path').should('have.css', 'color', 'rgb(37, 74, 149)')//check 3rd BOM still selected

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //check if the right claim reason is selected

        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Your request was successfully sent') //Success Message by submit
    })

    it('Web to case with three selected BOM w/ single attachment [DE]', { baseUrl: url_de }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        //path via Raise claims
        claimsPage.raiseClaimBtnSelector().click()

        claimsPage.typeEmailAddress("testautomation@test.com")

        claimsPage.chooseOrderFieldSelector().type('LN23204420G2')
        claimsPage.chooseFirstOrderSelector(0).click()

        claimsPage.typeMessage()

        claimsPage.detailsRowCheckBox('GA000MIS060836').click()
        claimsPage.detailsRowCheckBox('GA000MIS060837').click()
        claimsPage.detailsRowCheckBox('GA000MIS060838').click()

        claimsPage.rowCheckBox('GA000MIS060836').find('path').should('have.css', 'color', 'rgb(37, 74, 149)')//check BOM still selected
        claimsPage.rowCheckBox('GA000MIS060837').find('path').should('have.css', 'color', 'rgb(37, 74, 149)')//check 2nd BOM still selected
        claimsPage.rowCheckBox('GA000MIS060838').find('path').should('have.css', 'color', 'rgb(37, 74, 149)')//check 3rd BOM still selected

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //check if the right claim reason is selected

        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Ihre Anfrage wurde erfolgreich übermittelt') //Success Message by submit
    })

    it('Web to case with three selected BOM w/ single attachment [FR]', { baseUrl: url_fr }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        //path via Raise claims
        claimsPage.raiseClaimBtnSelector().click()

        claimsPage.typeEmailAddress("testautomation@test.com")

        claimsPage.chooseOrderFieldSelector().type('LN23204420G2')
        claimsPage.chooseFirstOrderSelector(0).click()

        claimsPage.typeMessage()

        claimsPage.detailsRowCheckBox('GA000MIS060836').click()
        claimsPage.detailsRowCheckBox('GA000MIS060837').click()
        claimsPage.detailsRowCheckBox('GA000MIS060838').click()

        claimsPage.rowCheckBox('GA000MIS060836').find('path').should('have.css', 'color', 'rgb(37, 74, 149)')//check BOM still selected
        claimsPage.rowCheckBox('GA000MIS060837').find('path').should('have.css', 'color', 'rgb(37, 74, 149)')//check 2nd BOM still selected
        claimsPage.rowCheckBox('GA000MIS060838').find('path').should('have.css', 'color', 'rgb(37, 74, 149)')//check 3rd BOM still selected

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //check if the right claim reason is selected

        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Votre demande a été transmise avec succès') //Success Message by submit
    })

    it('Web to case with three selected BOM w/ single attachment [IT]', { baseUrl: url_it }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        //path via Raise claims
        claimsPage.raiseClaimBtnSelector().click()

        claimsPage.typeEmailAddress("testautomation@test.com")

        claimsPage.chooseOrderFieldSelector().type('LN23204420G2')
        claimsPage.chooseFirstOrderSelector(0).click()

        claimsPage.typeMessage()

        claimsPage.detailsRowCheckBox('GA000MIS060836').click()
        claimsPage.detailsRowCheckBox('GA000MIS060837').click()
        claimsPage.detailsRowCheckBox('GA000MIS060838').click()

        claimsPage.rowCheckBox('GA000MIS060836').find('path').should('have.css', 'color', 'rgb(37, 74, 149)')//check BOM still selected
        claimsPage.rowCheckBox('GA000MIS060837').find('path').should('have.css', 'color', 'rgb(37, 74, 149)')//check 2nd BOM still selected
        claimsPage.rowCheckBox('GA000MIS060838').find('path').should('have.css', 'color', 'rgb(37, 74, 149)')//check 3rd BOM still selected

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //check if the right claim reason is selected

        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('La sua richiesta è stata inviata') //Success Message by submit
    })

})