import Login from "../../boc/pom/login"
import ClaimsPage from '../pom/claims';

const loginPage = new Login()
const claimsPage = new ClaimsPage()

const orderNumber = "LN23243881G2"
const requestURL = "https://cssp-api.preprod.misumi-europe.com/api/v1/icf"

const authUser = require('../../../fixtures/auth-user.json');
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;


describe('Service Request check', () => {
    beforeEach(() => {
        loginPage.login(username, password)
        cy.visit('/my/dashboard/contact-us.html')
    });

    it('Check Service Requests information [UK]', { baseUrl: url_uk }, () => {
        claimsPage.typeEmailAddress("testautomation@test.com")

        claimsPage.chooseClaim()
        claimsPage.chooseOrderFieldSelector().type(orderNumber)
        claimsPage.chooseFirstOrderSelector(0).click()
        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.typeMessage()

        const requestInfo = claimsPage.takeRequestInfo(3) //collect info for the request
        cy.intercept({
            method: 'POST',
            url: requestURL
        }).as('apiCheck') //define service request

        claimsPage.submitRequestBtn().click()

        cy.wait('@apiCheck').its('request.body').should('include', requestInfo.bomSelectedRows, requestInfo.bomSelectedRowsProductCodes, requestInfo.bomSelectedRowsQuantities)//all collected info must be there
    })

    it('Check Service Requests information [DE]', { baseUrl: url_de }, () => {
        claimsPage.typeEmailAddress("testautomation@test.com")

        claimsPage.chooseClaim()
        claimsPage.chooseOrderFieldSelector().type(orderNumber)
        claimsPage.chooseFirstOrderSelector(0).click()
        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.typeMessage()

        const requestInfo = claimsPage.takeRequestInfo(3) //collect info for the request
        cy.intercept({
            method: 'POST',
            url: requestURL
        }).as('apiCheck') //define service request

        claimsPage.submitRequestBtn().click()

        cy.wait('@apiCheck').its('request.body').should('include', requestInfo.bomSelectedRows, requestInfo.bomSelectedRowsProductCodes, requestInfo.bomSelectedRowsQuantities)//all collected info must be there
    })

    it('Check Service Requests information [FR]', { baseUrl: url_fr }, () => {
        claimsPage.typeEmailAddress("testautomation@test.com")

        claimsPage.chooseClaim()
        claimsPage.chooseOrderFieldSelector().type(orderNumber)
        claimsPage.chooseFirstOrderSelector(0).click()
        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.typeMessage()

        const requestInfo = claimsPage.takeRequestInfo(3) //collect info for the request
        cy.intercept({
            method: 'POST',
            url: requestURL
        }).as('apiCheck') //define service request

        claimsPage.submitRequestBtn().click()

        cy.wait('@apiCheck').its('request.body').should('include', requestInfo.bomSelectedRows, requestInfo.bomSelectedRowsProductCodes, requestInfo.bomSelectedRowsQuantities)//all collected info must be there
    })

    it('Check Service Requests information [IT]', { baseUrl: url_it }, () => {
        claimsPage.typeEmailAddress("testautomation@test.com")

        claimsPage.chooseClaim()
        claimsPage.chooseOrderFieldSelector().type(orderNumber)
        claimsPage.chooseFirstOrderSelector(0).click()
        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.typeMessage()

        const requestInfo = claimsPage.takeRequestInfo(3) //collect info for the request
        cy.intercept({
            method: 'POST',
            url: requestURL
        }).as('apiCheck') //define service request

        claimsPage.submitRequestBtn().click()

        cy.wait('@apiCheck').its('request.body').should('include', requestInfo.bomSelectedRows, requestInfo.bomSelectedRowsProductCodes, requestInfo.bomSelectedRowsQuantities)//all collected info must be there
    })

})