import CsspPage from '../pom/cssp';
import Login from "../../boc/pom/login"
import ClaimsPage from '../pom/claims';

const loginPage = new Login()
const csspPage = new CsspPage()
const claimsPage = new ClaimsPage()


const authUser = require('../../../fixtures/auth-user.json');
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('Claims with chosen Phone option as contact', () => {
    beforeEach(() => {
        loginPage.login(username, password)
    });

    it('Web to case quality related with phone option as contact and 0 BOM and 0 files [UK]', { baseUrl: url_uk }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.phoneBtnSelector().click()
        claimsPage.emailFieldSelector().type('+11111111111')

        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid phone number

        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //check if the right claim reason is selected

        claimsPage.checkNumberOfUploadedFiles(0)
        claimsPage.typeMessage()

        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()
        claimsPage.checkIfRequestIsSucc('Your request was successfully sent') //Success Message by submit
    })

    it('Web to case quality related with phone option as contact and 0 BOM and 0 files [DE]', { baseUrl: url_de }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.phoneBtnSelector().click()
        claimsPage.emailFieldSelector().type('+49111111111')

        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid phone number

        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //check if the right claim reason is selected

        claimsPage.checkNumberOfUploadedFiles(0)
        claimsPage.typeMessage()

        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()
        claimsPage.checkIfRequestIsSucc('Ihre Anfrage wurde erfolgreich übermittelt') //Success Message by submit
    })

    it('Web to case quality related with phone option as contact and 0 BOM and 0 files [FR]', { baseUrl: url_fr }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.phoneBtnSelector().click()
        claimsPage.emailFieldSelector().type('+49111111111')

        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid phone number

        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //check if the right claim reason is selected

        claimsPage.checkNumberOfUploadedFiles(0)
        claimsPage.typeMessage()

        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()
        claimsPage.checkIfRequestIsSucc('Votre demande a été transmise avec succès') //Success Message by submit
    })

    it('Web to case quality related with phone option as contact and 0 BOM and 0 files [IT]', { baseUrl: url_it }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.phoneBtnSelector().click()
        claimsPage.emailFieldSelector().type('+49111111111')

        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid phone number

        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //check if the right claim reason is selected

        claimsPage.checkNumberOfUploadedFiles(0)
        claimsPage.typeMessage()

        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()
        claimsPage.checkIfRequestIsSucc('La sua richiesta è stata inviata') //Success Message by submit
    })

    it('Web to case quality related with phone option as contact, 1 BOM and 1 file [UK]', { baseUrl: url_uk }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.phoneBtnSelector().click()
        claimsPage.emailFieldSelector().type('+11111111111')

        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid phone number

        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //check if the right claim reason is selected

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()
        claimsPage.checkBoxSelector().click()

        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()
        claimsPage.checkIfRequestIsSucc('Your request was successfully sent') //Success Message by submit
    })

    it('Web to case quality related with phone option as contact, 1 BOM and 1 file [DE]', { baseUrl: url_de }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.phoneBtnSelector().click()
        claimsPage.emailFieldSelector().type('+49111111111')
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid phone number

        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //check if the right claim reason is selected

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()
        claimsPage.checkBoxSelector().click()

        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()
        claimsPage.checkIfRequestIsSucc('Ihre Anfrage wurde erfolgreich übermittelt') //Success Message by submit
    })

    it('Web to case quality related with phone option as contact, 1 BOM and 1 file [FR]', { baseUrl: url_fr }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.phoneBtnSelector().click()
        claimsPage.emailFieldSelector().type('+49111111111')
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid phone number

        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //check if the right claim reason is selected

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()
        claimsPage.checkBoxSelector().click()

        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()
        claimsPage.checkIfRequestIsSucc('Votre demande a été transmise avec succès') //Success Message by submit
    })

    it('Web to case quality related with phone option as contact, 1 BOM and 1 file [IT]', { baseUrl: url_it }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.phoneBtnSelector().click()
        claimsPage.emailFieldSelector().type('+49111111111')
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid phone number

        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //check if the right claim reason is selected

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()
        claimsPage.checkBoxSelector().click()

        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()
        claimsPage.checkIfRequestIsSucc('La sua richiesta è stata inviata') //Success Message by submit
    })

    it('Web to case quality related with phone option as contact and Shipment not Received option [UK]', { baseUrl: url_uk }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.phoneBtnSelector().click()
        claimsPage.emailFieldSelector().type('+11111111111')

        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid phone number

        claimsPage.selectClaimReason('Shipment not received') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Shipment not received') //check if the right claim reason is selected
        claimsPage.tableBOMSelector().should('not.be.visible') //check if BOM table gets hidden

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()

        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()
        claimsPage.checkIfRequestIsSucc('Your request was successfully sent') //Success Message by submit
    })

    it('Web to case quality related with phone option as contact and Shipment not Received option [DE]', { baseUrl: url_de }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.phoneBtnSelector().click()
        claimsPage.emailFieldSelector().type('+49111111111')
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid phone number

        claimsPage.selectClaimReason('Shipment not received') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Shipment not received') //check if the right claim reason is selected
        claimsPage.tableBOMSelector().should('not.be.visible') //check if BOM table gets hidden

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()

        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()
        claimsPage.checkIfRequestIsSucc('Ihre Anfrage wurde erfolgreich übermittelt') //Success Message by submit
    })

    it('Web to case quality related with phone option as contact and Shipment not Received option [FR]', { baseUrl: url_fr }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.phoneBtnSelector().click()
        claimsPage.emailFieldSelector().type('+49111111111')
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid phone number

        claimsPage.selectClaimReason('Shipment not received') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Shipment not received') //check if the right claim reason is selected
        claimsPage.tableBOMSelector().should('not.be.visible') //check if BOM table gets hidden

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()

        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()
        claimsPage.checkIfRequestIsSucc('Votre demande a été transmise avec succès') //Success Message by submit
    })

    it('Web to case quality related with phone option as contact and Shipment not Received option [IT]', { baseUrl: url_it }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.phoneBtnSelector().click()
        claimsPage.emailFieldSelector().type('+49111111111')
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid phone number

        claimsPage.selectClaimReason('Shipment not received') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Shipment not received') //check if the right claim reason is selected
        claimsPage.tableBOMSelector().should('not.be.visible') //check if BOM table gets hidden

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)
        
        claimsPage.typeMessage()

        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()
        claimsPage.checkIfRequestIsSucc('La sua richiesta è stata inviata') //Success Message by submit
    })

})