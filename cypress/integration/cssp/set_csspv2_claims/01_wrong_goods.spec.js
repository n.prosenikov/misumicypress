import CsspPage from '../pom/cssp';
import Login from "../../boc/pom/login"
import ClaimsPage from '../pom/claims';

const loginPage = new Login()
const csspPage = new CsspPage()
const claimsPage = new ClaimsPage()


const authUser = require('../../../fixtures/auth-user.json');
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('Wrong goods related', () => {
    beforeEach(() => {
        loginPage.login(username, password)
    });

    it('Web to case wrong goods w/ single attachement [UK]', { baseUrl: url_uk }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid E-mail address

        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //check if the right claim reason is selected

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Your request was successfully sent') //Success Message by submit
    })

    it('Web to case wrong goods w/ single attachement [DE]', { baseUrl: url_de }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address
        
        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //check if the right claim reason is selected

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Ihre Anfrage wurde erfolgreich übermittelt') //Success Message by submit
    })

    it('Web to case wrong goods w/ single attachement [FR]', { baseUrl: url_fr }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //check if the right claim reason is selected

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Votre demande a été transmise avec succès') //Success Message by submit
    })

    it('Web to case wrong goods w/ single attachement [IT]', { baseUrl: url_it }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address
        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //check if the right claim reason is selected

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('La sua richiesta è stata inviata') //Success Message by submit
    })

    it('Web to case wrong goods w/ multiple attachments [UK]', { baseUrl: url_uk }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address
        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //check if the right claim reason is selected

        claimsPage.fileUpload(['images/misumi_logo.png', 'images/image1.png', 'images/image2.png', 'images/image3.png', 'images/image4.png'])
        claimsPage.checkNumberOfUploadedFiles(5)

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Your request was successfully sent') //Success Message by submit
    })

    it('Web to case wrong goods w/ multiple attachments [DE]', { baseUrl: url_de }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address
        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //check if the right claim reason is selected

        claimsPage.fileUpload(['images/misumi_logo.png', 'images/image1.png', 'images/image2.png', 'images/image3.png', 'images/image4.png'])
        claimsPage.checkNumberOfUploadedFiles(5)

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Ihre Anfrage wurde erfolgreich übermittelt') //Success Message by submit
    })

    it('Web to case wrong goods w/ multiple attachments [FR]', { baseUrl: url_fr }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address
        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //check if the right claim reason is selected

        claimsPage.fileUpload(['images/misumi_logo.png', 'images/image1.png', 'images/image2.png', 'images/image3.png', 'images/image4.png'])
        claimsPage.checkNumberOfUploadedFiles(5)

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Votre demande a été transmise avec succès') //Success Message by submit
    })

    it('Web to case wrong goods w/ multiple attachments [IT]', { baseUrl: url_it }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address
        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //check if the right claim reason is selected

        claimsPage.fileUpload(['images/misumi_logo.png', 'images/image1.png', 'images/image2.png', 'images/image3.png', 'images/image4.png'])
        claimsPage.checkNumberOfUploadedFiles(5)

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('La sua richiesta è stata inviata') //Success Message by submit
    })

    it('Web to case wrong goods w/o attachments [UK]', { baseUrl: url_uk }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address
        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //check if the right claim reason is selected

        claimsPage.checkNumberOfUploadedFiles(0)

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Your request was successfully sent') //Success Message by submit
    })

    it('Web to case wrong goods w/o attachments [DE]', { baseUrl: url_de }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address
        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //check if the right claim reason is selected

        claimsPage.checkNumberOfUploadedFiles(0)

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Ihre Anfrage wurde erfolgreich übermittelt') //Success Message by submit
    })

    it('Web to case wrong goods w/o attachments [FR]', { baseUrl: url_fr }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address
        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //wrong goods must be selected

        claimsPage.checkNumberOfUploadedFiles(0)

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Votre demande a été transmise avec succès') //Success Message by submit
    })

    it('Web to case wrong goods w/o attachments [IT]', { baseUrl: url_it }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //wrong goods must be selected

        claimsPage.checkNumberOfUploadedFiles(0)

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('La sua richiesta è stata inviata') //Success Message by submit
    })

})