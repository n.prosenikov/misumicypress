import CsspPage from '../pom/cssp';
import Login from "../../boc/pom/login"
import ClaimsPage from '../pom/claims';

const loginPage = new Login()
const csspPage = new CsspPage()
const claimsPage = new ClaimsPage()


const authUser = require('../../../fixtures/auth-user.json');
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('Quality related', () => {
    beforeEach(() => {
        loginPage.login(username, password)
    });

    it('Web to case quality related and Material w/o attachments [UK]', { baseUrl: url_uk }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address


        claimsPage.selectClaimReason('Quality related') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Quality related') //check if the right claim reason is selected

        claimsPage.selectQualityRelatedOption('Material') //choose quality related option
        claimsPage.qualityRelatedOptionsSelector('Material').should('have.css', 'background-color', 'rgb(37, 74, 149)')

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Your request was successfully sent') //Success Message by submit
    })

    it('Web to case quality related and Material w/o attachments [DE]', { baseUrl: url_de }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Quality related') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Quality related') //check if the right claim reason is selected

        claimsPage.selectQualityRelatedOption('Material') //choose quality related option
        claimsPage.qualityRelatedOptionsSelector('Material').should('have.css', 'background-color', 'rgb(37, 74, 149)')

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Ihre Anfrage wurde erfolgreich übermittelt') //Success Message by submit
    })

    it('Web to case quality related and Material w/o attachments [FR]', { baseUrl: url_fr }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Quality related') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Quality related') //check if the right claim reason is selected

        claimsPage.selectQualityRelatedOption('Material') //choose quality related option
        claimsPage.qualityRelatedOptionsSelector('Material').should('have.css', 'background-color', 'rgb(37, 74, 149)')

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Votre demande a été transmise avec succès') //Success Message by submit
    })

    it('Web to case quality related and Material w/o attachments [IT]', { baseUrl: url_it }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Quality related') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Quality related') //check if the right claim reason is selected

        claimsPage.selectQualityRelatedOption('Material') //choose qulaty related option
        claimsPage.qualityRelatedOptionsSelector('Material').should('have.css', 'background-color', 'rgb(37, 74, 149)')

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('La sua richiesta è stata inviata') //Success Message by submit
    })

    it('Web to case quality related and Dimension w/ single attachment [UK]', { baseUrl: url_uk }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address


        claimsPage.selectClaimReason('Quality related') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Quality related') //check if the right claim reason is selected

        claimsPage.selectQualityRelatedOption('Dimension') //choose quality related option
        claimsPage.qualityRelatedOptionsSelector('Dimension').should('have.css', 'background-color', 'rgb(37, 74, 149)')

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)
        
        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Your request was successfully sent') //Success Message by submit
    })

    it('Web to case quality related and Dimension w/ single attachment [DE]', { baseUrl: url_de }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Quality related') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Quality related') //check if the right claim reason is selected

        claimsPage.selectQualityRelatedOption('Dimension') //choose quality related option
        claimsPage.qualityRelatedOptionsSelector('Dimension').should('have.css', 'background-color', 'rgb(37, 74, 149)')

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)
        
        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Ihre Anfrage wurde erfolgreich übermittelt') //Success Message by submit
    })

    it('Web to case quality related and Dimension w/ single attachment [FR]', { baseUrl: url_fr }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Quality related') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Quality related') //check if the right claim reason is selected

        claimsPage.selectQualityRelatedOption('Dimension') //choose quality related option
        claimsPage.qualityRelatedOptionsSelector('Dimension').should('have.css', 'background-color', 'rgb(37, 74, 149)')

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)
        
        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Votre demande a été transmise avec succès') //Success Message by submit
    })

    it('Web to case quality related and Dimension w/ single attachment [IT]', { baseUrl: url_it }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Quality related') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Quality related') //check if the right claim reason is selected

        claimsPage.selectQualityRelatedOption('Dimension') //choose qulaty related option
        claimsPage.qualityRelatedOptionsSelector('Dimension').should('have.css', 'background-color', 'rgb(37, 74, 149)')

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)
        
        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('La sua richiesta è stata inviata') //Success Message by submit
    })

    it('Web to case quality related and Tolerance w/ single attachment [UK]', { baseUrl: url_uk }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address


        claimsPage.selectClaimReason('Quality related') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Quality related') //check if the right claim reason is selected

        claimsPage.selectQualityRelatedOption('Tolerance') //choose quality related option
        claimsPage.qualityRelatedOptionsSelector('Tolerance').should('have.css', 'background-color', 'rgb(37, 74, 149)')

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Your request was successfully sent') //Success Message by submit
    })

    it('Web to case quality related and Tolerance w/ single attachment [DE]', { baseUrl: url_de }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Quality related') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Quality related') //check if the right claim reason is selected

        claimsPage.selectQualityRelatedOption('Tolerance') //choose quality related option
        claimsPage.qualityRelatedOptionsSelector('Tolerance').should('have.css', 'background-color', 'rgb(37, 74, 149)')

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Ihre Anfrage wurde erfolgreich übermittelt') //Success Message by submit
    })

    it('Web to case quality related and Tolerance w/ single attachment [FR]', { baseUrl: url_fr }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Quality related') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Quality related') //check if the right claim reason is selected

        claimsPage.selectQualityRelatedOption('Tolerance') //choose quality related option
        claimsPage.qualityRelatedOptionsSelector('Tolerance').should('have.css', 'background-color', 'rgb(37, 74, 149)')

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Votre demande a été transmise avec succès') //Success Message by submit
    })

    it('Web to case quality related and Tolerance w/ single attachment [IT]', { baseUrl: url_it }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Quality related') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Quality related') //check if the right claim reason is selected

        claimsPage.selectQualityRelatedOption('Tolerance') //choose qulaty related option
        claimsPage.qualityRelatedOptionsSelector('Tolerance').should('have.css', 'background-color', 'rgb(37, 74, 149)')

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('La sua richiesta è stata inviata') //Success Message by submit
    })

    it('Web to case quality related and Surface w/ single attachment [UK]', { baseUrl: url_uk }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address


        claimsPage.selectClaimReason('Quality related') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Quality related') //check if the right claim reason is selected

        claimsPage.selectQualityRelatedOption('Surface') //choose quality related option
        claimsPage.qualityRelatedOptionsSelector('Surface').should('have.css', 'background-color', 'rgb(37, 74, 149)')

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Your request was successfully sent') //Success Message by submit
    })

    it('Web to case quality related and Surface w/ single attachment [DE]', { baseUrl: url_de }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Quality related') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Quality related') //check if the right claim reason is selected

        claimsPage.selectQualityRelatedOption('Surface') //choose quality related option
        claimsPage.qualityRelatedOptionsSelector('Surface').should('have.css', 'background-color', 'rgb(37, 74, 149)')

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Ihre Anfrage wurde erfolgreich übermittelt') //Success Message by submit
    })

    it('Web to case quality related and Surface w/ single attachment [FR]', { baseUrl: url_fr }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Quality related') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Quality related') //check if the right claim reason is selected

        claimsPage.selectQualityRelatedOption('Surface') //choose quality related option
        claimsPage.qualityRelatedOptionsSelector('Surface').should('have.css', 'background-color', 'rgb(37, 74, 149)')

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Votre demande a été transmise avec succès') //Success Message by submit
    })

    it('Web to case quality related and Surface w/ single attachment [IT]', { baseUrl: url_it }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Quality related') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Quality related') //check if the right claim reason is selected

        claimsPage.selectQualityRelatedOption('Surface') //choose qulaty related option
        claimsPage.qualityRelatedOptionsSelector('Surface').should('have.css', 'background-color', 'rgb(37, 74, 149)')

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('La sua richiesta è stata inviata') //Success Message by submit
    })

    it('Web to case quality related and Heat treatment w/ single attachment [UK]', { baseUrl: url_uk }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address


        claimsPage.selectClaimReason('Quality related') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Quality related') //check if the right claim reason is selected

        claimsPage.selectQualityRelatedOption('Heat') //choose quality related option
        claimsPage.qualityRelatedOptionsSelector('Heat').should('have.css', 'background-color', 'rgb(37, 74, 149)')

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Your request was successfully sent') //Success Message by submit
    })

    it('Web to case quality related and Heat treatment w/ single attachment [DE]', { baseUrl: url_de }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Quality related') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Quality related') //check if the right claim reason is selected

        claimsPage.selectQualityRelatedOption('Heat treatment') //choose quality related option
        claimsPage.qualityRelatedOptionsSelector('Heat treatment').should('have.css', 'background-color', 'rgb(37, 74, 149)')

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Ihre Anfrage wurde erfolgreich übermittelt') //Success Message by submit
    })

    it('Web to case quality related and Heat treatment w/ single attachment [FR]', { baseUrl: url_fr }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Quality related') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Quality related') //check if the right claim reason is selected

        claimsPage.selectQualityRelatedOption('Heat treatment') //choose quality related option
        claimsPage.qualityRelatedOptionsSelector('Heat treatment').should('have.css', 'background-color', 'rgb(37, 74, 149)')

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Votre demande a été transmise avec succès') //Success Message by submit
    })

    it('Web to case quality related and Heat treatment w/ single attachment [IT]', { baseUrl: url_it }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Quality related') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Quality related') //check if the right claim reason is selected

        claimsPage.selectQualityRelatedOption('Heat treatment') //choose qulaty related option
        claimsPage.qualityRelatedOptionsSelector('Heat treatment').should('have.css', 'background-color', 'rgb(37, 74, 149)')

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('La sua richiesta è stata inviata') //Success Message by submit
    })

    it('Web to case quality related and Function w/ single attachment [UK]', { baseUrl: url_uk }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address


        claimsPage.selectClaimReason('Quality related') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Quality related') //check if the right claim reason is selected

        claimsPage.selectQualityRelatedOption('Function') //choose quality related option
        claimsPage.qualityRelatedOptionsSelector('Function').should('have.css', 'background-color', 'rgb(37, 74, 149)')

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Your request was successfully sent') //Success Message by submit
    })

    it('Web to case quality related and Function w/ single attachment [DE]', { baseUrl: url_de }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Quality related') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Quality related') //check if the right claim reason is selected

        claimsPage.selectQualityRelatedOption('Function') //choose quality related option
        claimsPage.qualityRelatedOptionsSelector('Function').should('have.css', 'background-color', 'rgb(37, 74, 149)')

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Ihre Anfrage wurde erfolgreich übermittelt') //Success Message by submit
    })

    it('Web to case quality related and Function w/ single attachment [FR]', { baseUrl: url_fr }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Quality related') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Quality related') //check if the right claim reason is selected

        claimsPage.selectQualityRelatedOption('Function') //choose quality related option
        claimsPage.qualityRelatedOptionsSelector('Function').should('have.css', 'background-color', 'rgb(37, 74, 149)')

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('Votre demande a été transmise avec succès') //Success Message by submit
    })

    it('Web to case quality related and Function w/ single attachment [IT]', { baseUrl: url_it }, () => {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')

        claimsPage.pressContactButton()
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Quality related') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Quality related') //check if the right claim reason is selected

        claimsPage.selectQualityRelatedOption('Function') //choose qulaty related option
        claimsPage.qualityRelatedOptionsSelector('Function').should('have.css', 'background-color', 'rgb(37, 74, 149)')

        claimsPage.fileUpload('images/' + ['misumi_logo.png'])
        claimsPage.checkNumberOfUploadedFiles(1)

        claimsPage.typeMessage()
        claimsPage.submitRequestBtn().should('not.be.disabled') //check if the request button is active
        claimsPage.submitRequestBtn().click()

        claimsPage.checkIfRequestIsSucc('La sua richiesta è stata inviata') //Success Message by submit
    })

})