import Login from "../../boc/pom/login"
import ClaimsPage from '../pom/claims';
import AppErrors from '../pom/applicationErrors';

const loginPage = new Login()
const claimsPage = new ClaimsPage()
const appErrors = new AppErrors()

const requestedURL = " https://cssp-api.preprod.misumi-europe.com/api/v1/icf"
const value4XXMsg = "Error connecting to claim creation service, 4XX error"
const value5XXMsg = "Error connecting to claim creation service, 5XX error"

const authUser = require('../../../fixtures/auth-user.json');
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('Application errors number 4XX and 5XX', () => {
    beforeEach(() => {
        loginPage.login(username, password)
        cy.visit('/my/dashboard/contact-us.html')
        appErrors.chooseDefaultClaimSettings()
    });

    it('4XX error message [UK]', { baseUrl: url_uk }, () => {
        appErrors.chooseInputFieldValue(1) //1-> 4XX; 2-> 5XX
        cy.intercept({ method: 'POST', url: requestedURL }).as('responseCheck')
        claimsPage.submitRequestBtn().click()
        cy.wait('@responseCheck').its('response.body.external_error.external_error').should('eql', value4XXMsg)
    })

    it('4XX error message [DE]', { baseUrl: url_de }, () => {
        appErrors.chooseInputFieldValue(1) //1-> 4XX; 2-> 5XX
        cy.intercept({ method: 'POST', url: requestedURL }).as('responseCheck')
        claimsPage.submitRequestBtn().click()
        cy.wait('@responseCheck').its('response.body.external_error.external_error').should('eql', value4XXMsg)
    })

    it('4XX error message [FR]', { baseUrl: url_fr }, () => {
        appErrors.chooseInputFieldValue(1) //1-> 4XX; 2-> 5XX
        cy.intercept({ method: 'POST', url: requestedURL }).as('responseCheck')
        claimsPage.submitRequestBtn().click()
        cy.wait('@responseCheck').its('response.body.external_error.external_error').should('eql', value4XXMsg)
    })

    it('4XX error message [IT]', { baseUrl: url_it }, () => {
        appErrors.chooseInputFieldValue(1) //1-> 4XX; 2-> 5XX
        cy.intercept({ method: 'POST', url: requestedURL }).as('responseCheck')
        claimsPage.submitRequestBtn().click()
        cy.wait('@responseCheck').its('response.body.external_error.external_error').should('eql', value4XXMsg)
    })

    it('5XX error message [UK]', { baseUrl: url_uk }, () => {
        appErrors.chooseInputFieldValue(2) //1-> 4XX; 2-> 5XX
        cy.intercept({ method: 'POST', url: requestedURL }).as('responseCheck')
        claimsPage.submitRequestBtn().click()
        cy.wait('@responseCheck').its('response.body.external_error.external_error').should('eql', value5XXMsg)
    })

    it('5XX error message [DE]', { baseUrl: url_de }, () => {
        appErrors.chooseInputFieldValue(2) //1-> 4XX; 2-> 5XX
        cy.intercept({ method: 'POST', url: requestedURL }).as('responseCheck')
        claimsPage.submitRequestBtn().click()
        cy.wait('@responseCheck').its('response.body.external_error.external_error').should('eql', value5XXMsg)
    })

    it('5XX error message [FR]', { baseUrl: url_fr }, () => {
        appErrors.chooseInputFieldValue(2) //1-> 4XX; 2-> 5XX
        cy.intercept({ method: 'POST', url: requestedURL }).as('responseCheck')
        claimsPage.submitRequestBtn().click()
        cy.wait('@responseCheck').its('response.body.external_error.external_error').should('eql', value5XXMsg)
    })

    it('5XX error message [IT]', { baseUrl: url_it }, () => {
        appErrors.chooseInputFieldValue(2) //1-> 4XX; 2-> 5XX
        cy.intercept({ method: 'POST', url: requestedURL }).as('responseCheck')
        claimsPage.submitRequestBtn().click()
        cy.wait('@responseCheck').its('response.body.external_error.external_error').should('eql', value5XXMsg)
    })

})


