import CsspPage from "./cssp"
import OrderAndFilter from "./orderAndFilter"

const csspPage = new CsspPage()
const orderAndFilter = new OrderAndFilter()

const randInt = csspPage.getRandomInt(777)

const subscriptionInfoStrings = [
    "Please confirm your entered email address with the enter key or a space. You can enter up to three e-mail addresses",
    "Bestätigen Sie die eingegebene Emailadresse mit der Entertaste oder einem Leerzeichen. Sie können bis zu drei E-Mail-Adressen eingeben",
    "Merci de cnfirmer l'adresse email avec le touche enter ou une space. Vous pouvez saisir jusqu’à trois adresses e-mail",
    "Conferma l'indirizzo email inserito con il tasto enter o un spazio. Le fatture possono essere inviate a fino a tre indirizzi e-mail"
]

const rightSectionHeadlineStrings = [
    "Registration for E-Billing",
    "Registrierung für den elektronischen PDF Rechnungsversand",
    "Inscription à l’e-facture",
    "Registrazione per e-billing"
]

const successfullMsgStrings = [
    "Changes have been successfully saved!",
    "Änderungen wurden erfolgreich gespeichert!",
    "Les modifications ont été enregistrées avec succès !",
    "Le modifiche sono state salvate correttamente!"
]

const errorMsgByMoreEmailsStrings = [
    "You can add maximum three emails to your subscription list!",
    "Sie können maximal drei E-Mails zu Ihrer Abonnementliste hinzufügen!",
    "Vous pouvez ajouter au maximum trois adresses e-mails à votre liste d'inscription",
    "È possibile aggiungere fino a tre indirizzi e-mail!"
]

const errorMsgByInvalidEmail = [
    "Please enter a valid e-mail address",
    "Bitte geben Sie eine gültige E-Mail Adresse ein",
    "Veuillez saisir une adresse e-mail valide",
    "Inserire un indirizzo e-mail valido"
]

export default class EBilling {
    //Selectors

    linkToEBbilingBtmSection() {
        return cy.get('#bottom-section-settings-ebilling-link')
    }

    yesSubscriptionBtn() {
        return cy.get('#subscription-section-yes-button')
    }

    noSubscriptionBtn() {
        return cy.get('#subscription-section-no-button')
    }

    emailInputField() {
        return cy.get('#subscription-section-input')
    }

    subscriptionInfoText() {
        return cy.get('#subscription-section-input-infotext')
    }

    cancelSubscriptionBtn() {
        return cy.get('#subscription-section-input-cancel-button')
    }

    saveSubscriptionBtn() {
        return cy.get('#subscription-section-input-save-button')
    }

    rightSectionHeadline() {
        return cy.get('#headline-section-title')
    }

    xBtnbyEmails() {
        return cy.get('.fa-w-11')
    }

    //Functions
    visitEbillingFn() {
        this.linkToEBbilingBtmSection().click()
        cy.location('pathname').should('eq', '/my/dashboard/e-billing.html')
    }

    allElementsAreVisibleFn(language) {
        this.yesSubscriptionBtn().should('be.visible')
        this.noSubscriptionBtn().should('be.visible')
        this.emailInputField().should('be.visible')
        this.subscriptionInfoText().should('contain.text', subscriptionInfoStrings[language])
        this.cancelSubscriptionBtn().should('be.visible')
        this.saveSubscriptionBtn().should('be.visible')
        this.rightSectionHeadline().should('contain.text', rightSectionHeadlineStrings[language])
    }

    saveEBilling(language, visibleOrNot) {
        this.saveSubscriptionBtn().click()
        cy.waitFor(cy.get('div').contains(successfullMsgStrings[language]).should(visibleOrNot))
    }

    enterMoreThanOneEmail(number) {
        this.clearEmailFieldAtTheBeginning()
        for (let i = 0; i < `${number}`; i++) {
            this.emailInputField().type(`test${csspPage.getRandomInt(777)}@blubito.com {enter}`)
        }
    }

    enterMoreThanThreeEmails(language, visibleOrNot) {
        this.clearEmailFieldAtTheBeginning()
        for (let i = 0; i < 2; i++) {
            this.emailInputField().type(`test${csspPage.getRandomInt(777)}@blubito.com {enter}`)
        }
        this.emailInputField().type(`test${csspPage.getRandomInt(777)}@blubito.com`)
        this.saveSubscriptionBtn().click()
        this.emailInputField().should('be.disabled')
        cy.waitFor(cy.get('div').contains(successfullMsgStrings[language]).should(visibleOrNot))
        //cy.contains(errorMsgByMoreEmailsStrings[language])
    }

    clearEmailFieldAtTheBeginning() {
        this.yesSubscriptionBtn().click()
        this.xBtnbyEmails().then(number => {
            const counter = Cypress.$(number).length.toString()
            this.removeEmails(counter - 1)
        })
    }

    removeEmails(number) {
        for (let i = `${number}`; i > 0; i--) {
            this.xBtnbyEmails().eq(i).click()
        }
        cy.contains(`test${randInt}@blubito.com`).should('not.exist')
    }

    invalidEmailMsgFn(language) {
        cy.contains(errorMsgByInvalidEmail[language]).should('exist')
    }

    existingEmailInTheField() {
        cy.get('body').then(inputField => {
            const xCounter = inputField.find('.fa-w-11').length
            if (xCounter > 1) {
                cy.contains('@blubito.com').should('exist')
                cy.log("The changes are on Stg0")
            } else {
                cy.contains('@blubito.com').should('not.exist')
                cy.log("The changes are on LIVE")
            }
        })
    }

    deactivateEbilling() {
        this.noSubscriptionBtn().click()
        this.saveSubscriptionBtn().click()
        orderAndFilter.backToDashboardBtn().click()
    }
}