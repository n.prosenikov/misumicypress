import ClaimsPage from "./claims";

const claimsPage = new ClaimsPage()

export default class AppErrors {
    //Selectors
    hiddenInputFieldSelector() {
        return cy.get('#sf_ff')
    }


    //Functions
    addElement() { //add a new HTML element while a test is running
        // create a new div element
        const newDiv = document.createElement("input");

        const typeTag = document.createAttribute("type");
        const nameTag = document.createAttribute("name");
        const valueTag = document.createAttribute("value");

        typeTag.value = "text";
        nameTag.value = "sf_ff";
        valueTag.value = "1";

        // and give it some content
        const newContent = document.createTextNode("TestVasil");

        // add the text node to the newly created div
        newDiv.append(newContent);
        newDiv.setAttributeNode(typeTag)
        newDiv.setAttributeNode(nameTag)
        newDiv.setAttributeNode(valueTag)

        // add the newly created element and its content into the DOM
        const currentDiv = document.getElementById("userdata-section-phone-button");

        //var bodyDocument = document.getElementsByClassName("page2")

        parent.document.getElementById("Your App: 'misumicypress'").contentWindow.document.body.insertBefore(newDiv, currentDiv);
    }

    chooseDefaultClaimSettings() {
        claimsPage.typeEmailAddress("testautomation@test.com")
        claimsPage.chooseOrder(0)
        claimsPage.confirmIconSelector().should("be.visible") //valid e-mail address

        claimsPage.selectClaimReason('Wrong goods') //chose reason for claim
        claimsPage.checkWhichClaimReasonIsSelected('Wrong goods') //wrong goods must be selected

        claimsPage.typeMessage()
    }

    chooseInputFieldValue(value) {
        this.hiddenInputFieldSelector().invoke('show')
        this.hiddenInputFieldSelector().invoke('val', value)
    }
}