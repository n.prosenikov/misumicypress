export default class LoginButton {
    //Selectors
    loginBtnSelector() {
        return cy.get('[data-component="head_navi_aside"]')
    }

    topPartOfUserMenu() {
        return cy.get('.l-header__order__function')
    }

    myMisumiBtn() {
        return cy.get('[data-common-userlink="mypagetop"]').first()
    }

    myFavoritesSection() {
        return cy.get('[class="l-header__balloonBoxInner"]').eq(1)
    }

    mySettingsSection() {
        return cy.get('[class="l-header__balloonBoxInner"]').eq(2)
    }

    logOutBtnSelector() {
        return cy.get('.l-header__menu--logout')
    }

    welcomeHeaderSelector() {
        return cy.get('#header-title')
    }

    loginRegisterBtn(){
        return cy.get('[data-header-func="login"]')
    }

    //Functions

    allInfoShouldBeVisible() {
        this.loginBtnSelector().should('be.visible')
        this.topPartOfUserMenu().should('be.visible')
        this.myMisumiBtn().should('be.visible')
        this.myFavoritesSection().should('be.visible')
        this.mySettingsSection().should('be.visible')
        this.logOutBtnSelector().should('be.visible')
    }
}