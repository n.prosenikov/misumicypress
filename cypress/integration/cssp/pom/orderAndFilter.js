export const deleteDownloadsFolder = () => {
    const downloadsFolder = Cypress.config('downloadsFolder')
    cy.task('deleteFolder', downloadsFolder)
}
export default class OrderAndFilter {
    alertOrderOverviewHeaders = [
        "You do not see your data from today?",
        "Sie sehen Ihre Daten von heute nicht?",
        "Vous ne voyez pas vos données d'aujourd'hui?",
        "Non vede i vostri dati di oggi?"
    ]

    alertDashboardHeaders = [
        "Activate E-Billing today to get invoices digitally",
        "",
        "",
        ""
    ]

    //Selectors

    filterOptionFieldSelector() {
        return cy.get('#orders-overview-status-filter-select')
    }

    filterOptionsSelectors(filterOption) {
        return cy.get(`#orders-overview-status-filter-select-${filterOption}`)
    }

    dateInputField() {
        return cy.get('#mui-5')
    }

    orderDateInTable() {
        return cy.get('[data-field="vsd_date"]')
    }

    checkBoxOption(orderNumber) {
        return cy.get('[type="checkbox"]').eq(orderNumber)
    }

    downloadSelectedInvoicesBtn() {
        return cy.get('#orders-overview-download-selected-button')
    }

    orderInputFieldSelector() {
        return cy.get('#orders-overview-search-numbers-input')
    }

    tableOrderResult(orderNUmber) {
        return cy.get(`[data-id=${orderNUmber}]`)
    }

    statusOfAnOrder() {
        return cy.get('[id*="order-details-invoice-status"]')
    }

    documentDownloadBtnSelector(invoiceNumber) {
        return cy.get(`#order-details-pdf-select-${invoiceNumber}`)
    }

    typeDocumentSelector(typeDocument, invoiceNumber) {
        return cy.get(`#order-details-pdf-select-${typeDocument}-${invoiceNumber}`)
    }

    entriesPerPageSelector() {
        return cy.get('[aria-labelledby*="orders-overview-page-entries-select-label"]')
    }

    loadingDialogueContainer() {
        return cy.get('[aria-labelledby="loading-dialog-container"]')
    }

    alertBellBtn() {
        return cy.get('#alert-closed-bell-icon-button')
    }

    alertBoxMessage() {
        return cy.get('#alert-closed-chatbox-button')
    }

    alertTooltipMsgBox() {
        return cy.get('#alert-open-tooltip-content-wrapper')
    }

    alertTooltipLink(){
        return cy.get('#alert-open-tooltip-link')
    }

    alertTooltipMsgHeader() {
        return cy.get('#alert-open-tooltip-heading')
    }

    alertToolTipCloseBtn() {
        return cy.get('#alert-open-tooltip-close-button')
    }

    backToDashboardBtn(){
        return cy.get('#navigation-dashboard-button')
    }

    //Functions

    chooseFilterOptions(selectOption) {
        this.filterOptionFieldSelector().click()
        this.filterOptionsSelectors(selectOption).click()
    }

    typeAnOrderNumber(orderNumber) {
        this.orderInputFieldSelector().type(orderNumber)
    }

    changeNumberOfEntities(number) {
        this.entriesPerPageSelector().click()
        cy.get(`[data-value="${number}"]`).click() //choose 20 entries per page
    }

    searchAndFilterFn(numberOfEntities) {
        this.changeNumberOfEntities(numberOfEntities) //entities per page
        cy.get('[data-id*=LN]').eq(0).invoke('attr', 'data-id').then($invoice => {
            this.typeAnOrderNumber($invoice)
            this.tableOrderResult($invoice).should('be.visible')
        })
    }

    downloadDocument(numberOfEntities, typeDocument) {
        this.changeNumberOfEntities(numberOfEntities) //entities per page
        cy.get('[data-id*=LN]').eq(0).invoke('attr', 'data-id').then($invoice => {
            this.typeAnOrderNumber($invoice)
            this.documentDownloadBtnSelector($invoice).click()
            this.typeDocumentSelector(typeDocument, $invoice).click()
        })
        cy.waitFor(this.loadingDialogueContainer().should('not.exist'))
    }

    downloadDeliveryNoteDocument(typeDocument) {
        cy.get('[data-id*=LN]').first().invoke('attr', 'data-id').then($invoice => {
            this.documentDownloadBtnSelector($invoice).click()
            this.typeDocumentSelector(typeDocument, $invoice).click()
        })

        cy.waitFor(this.loadingDialogueContainer().should('not.exist'))
    }

    chooseFilterFn(selectOption, displayedOption) {
        this.chooseFilterOptions(selectOption)
        this.statusOfAnOrder().should('contain.text', displayedOption)
    }

    chooseDateFn(numberOfEntities) {
        this.changeNumberOfEntities(numberOfEntities) //entities per page
        cy.get('[data-id*=LN]').first().invoke('attr', 'data-id').as("invoiceNumber").then(($invoice) => {
            cy.get(`[data-id="${$invoice}"]`).find('[data-field="vsd_date"]').as("shipDate").then(($date) => {
                const shipDate = $date.text().toString();
                this.dateInputField().type(shipDate)
                this.orderDateInTable().should('contain.text', shipDate)
                //partNumberArray.push($date.text().toString()) //get part number
            })
        })
    }

    verifyDownloadIsSuccessful(nameOfDocument) {
        cy.get('body').then($element => {
            if ($element.find('#document-not-found-dialog-title').length) {
                cy.log("The Document does not exist and can not be downloaded")
            } else {
                cy.verifyDownload(nameOfDocument, { contains: true, timeout: 25000 })
            }
        })
    }

    checkInvoiceOverdue(typeDocument, nameOfDocument) {
        cy.get('body').then($element => {
            if ($element.find('[data-id*="LN"]').length) {
                this.downloadDeliveryNoteDocument(typeDocument)
                this.verifyDownloadIsSuccessful(nameOfDocument)
            } else {
                cy.log("The are no orders with status: 'Invoices Overdue' ")
            }
        })
    }

    openedAlertBoxClose(headerString) {
        this.alertTooltipMsgBox().should('be.visible')
        this.alertTooltipMsgHeader().should('contain.text', headerString)
        this.alertToolTipCloseBtn().click({ force: true })
    }

    closedAlertBoxCheckOpen(headerString) {
        this.alertBoxMessage().should('be.visible').and('contain.text', headerString)
        this.alertBellBtn().should('be.visible')
        this.alertBellBtn().click()
    }

    checkAndCloseAlertTooltip(language) {
        cy.get('body').then($body => {
            if ($body.find(this.alertTooltipMsgBox()).length) {
                this.openedAlertBoxClose(language)
                this.closedAlertBoxCheckOpen(language)
            } else {
                this.closedAlertBoxCheckOpen(language)
                this.openedAlertBoxClose(language)
            }
        })
    }
}