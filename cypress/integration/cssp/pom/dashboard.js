import CsspPage from './cssp';
const csspPage = new CsspPage()

const myPersonalInfoOptions = ['customer-code', 'customer-name', 'userid', 'acc-type']
const userSettingsOptions = ['ebilling', 'change-userdata', 'change-password', 'corporate-info', 'change-corporate-info']
const myTools = ['parnum', 'partnames', 'components']
const myOrderManagement = ['orders', 'order-confirmation', 'delivery-note', 'invoices', 'customer-statement', 'delivery'/*, 'raise-claim', 'claims'*/]
const myArchive = ['order', 'quotaion', 'delivery']

export default class Dashboard {
    //Selectors

    topOrdersBoxSelector() {
        return cy.get('#top-section-open-orders-box').find('h3')
    }

    topOrdersCounter() {
        return cy.get('#top-section-open-orders-counter')
    }

    topDeliveryBoxSelector() {
        return cy.get('#top-section-open-ondelivery-box').find('h3')
    }

    topInvoicesBoxSelector() {
        return cy.get('#top-section-open-invoices-box').find('h3')
    }

    topInvoicesCounter() {
        return cy.get('#top-section-due-counter')
    }

    myPersonalInfoSelectors(option) {
        return cy.get(`#top-section-person-info-${option}`)
    }

    userSettingsOptionsSelectors(option) {
        return cy.get(`#bottom-section-settings-${option}-link`)
    }

    myToolsOptionsSelectors(option) {
        return cy.get(`#bottom-section-tools-${option}-link`)
    }

    myArchiveSelector() {
        return cy.get('#composition-button')
    }

    myArchiveOptionsSelectors(option) {
        return cy.get(`#bottom-section-archive-${option}-history-link`)
    }

    infoCircleIcon(optionName) {
        return cy.get(`#bottom-section-${optionName}-info-icon`)
    }

    infoToolsTooltip(optionName) {
        return cy.get(`#bottom-section-${optionName}-tootip`)
    }

    infoTooltip(optionName) {
        return cy.get(`#bottom-section-${optionName}-tooltip`)
    }

    dataCADDownloadSelector() {
        return cy.get(`#bottom-section-archive-cad-link`)
    }

    myOrderManagementOptionsSelectors(option) {
        return cy.get(`#bottom-section-management-${option}-link`)
    }

    backToOldMyPageSelector() {
        return cy.get('#top-section-options-oldmypage-button')
    }

    oldPageCustomerNumberSelector() {
        return cy.get('.customerCode_Text')
    }


    //Functions
    goToTheLink() {
        cy.visit('/')
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')
    }

    visitCSSPPage() {
        csspPage.goTomyPageCSSP().click()
        cy.location('pathname').should('eq', '/my/dashboard/')
    }

    myPersonalInfoOptionsExist() {
        myPersonalInfoOptions.forEach((el) => {
            this.myPersonalInfoSelectors(el).should('be.visible')
        })
    }

    userSettingsOptionsExist() {
        userSettingsOptions.forEach((el) => {
            this.userSettingsOptionsSelectors(el).should('be.visible')
        })
    }

    myToolsOptionsExist() {
        myTools.forEach((el) => {
            this.myToolsOptionsSelectors(el).should('be.visible')
        })
    }

    myOrderManagementOptionsExist() {
        myOrderManagement.forEach((el) => {
            this.myOrderManagementOptionsSelectors(el).should('be.visible')
        })
    }

    myArchiveOptionsExist() {
        this.myArchiveSelector().click()
        myArchive.forEach((el) => {
            this.myArchiveOptionsSelectors(el).should('be.visible')
        })
    }

    oldPageCustomerNumberCheckFn(text) {
        this.oldPageCustomerNumberSelector().should('contain.text', text)
    }

    chooseOptionFromArchive(option) {
        this.myArchiveSelector().click()
        this.myArchiveOptionsSelectors(option).invoke('removeAttr', 'target').click()
    }

    chooseCADLinkFromArchive() {
        this.myArchiveSelector().click()
        this.dataCADDownloadSelector().invoke('removeAttr', 'target').click()
    }


    checkOrdersCounterExist() {
        cy.get('#top-section-open-orders-box').then($order => {
            if ($order.find('#top-section-open-orders-counter').length) {
                this.topOrdersBoxSelector().click()
                cy.url().should('include', "orders.html")
            } else {
                cy.log("There are no orders")
            }
        })
    }


    checkDeliveryCounterExist() {
        cy.get('#top-section-open-ondelivery-box').then($delivery => {
            if ($delivery.find('#top-section-ondelivery-counter').length) {
                this.topDeliveryBoxSelector().click()
                cy.url().should('include', "orders.html")
            } else {
                cy.log("There are no delivery-orders")
            }
        })
    }

    checkInvoicesCounterExist() {
        cy.get('#top-section-open-invoices-box').then($invoice => {
            if ($invoice.find('#top-section-due-counter').length) {
                this.topInvoicesBoxSelector().click()
                cy.url().should('include', "orders.html")
            } else {
                cy.log("There are no invoices")
            }
        })
    }

    hoverOverInfoCircle(option) {
        this.infoCircleIcon(option).trigger('mouseover')
        this.infoTooltip(option).should('be.visible')
    }

    hoverOverToolsInfoCircle(option) {
        this.infoCircleIcon(option).trigger('mouseover')
        this.infoToolsTooltip(option).should('be.visible')
    }

}