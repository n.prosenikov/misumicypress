


export default class CsspPage {

    goTomyPageCSSP() {
        return cy.get('[data-common-userlink="mypagetop"]')
    }

    goToHomeBtn() {
        return cy.get('#navigation-go-back-button')
    }

    contactServiceTeamBtn() {
        return cy.get('#navigation-contact-service-team-button')
    }

    phoneBtnSelector() {
        return cy.get('#userdata-section-phone-button')
    }

    contactServiceEmailBtn() {
        return cy.get('#userdata-section-email-button')
    }

    labelOfInputFieldSelector() {
        return cy.get('#userdata-section-input-title')
    }

    contactServiceEmailInputFiled() {
        return cy.get("input").eq(3)
    }

    contactInformationUserId() {
        return cy.get('#userdata-section-userdata-userid-value')
    }

    contactServiceRequestSelectionDrowdown() {
        return cy.get('#request-section-dropdown')
    }

    contactServiceRequestSelectionTech() {
        return cy.get('#request-section-select-technical')
    }

    contactServiceRequestTextArea() {
        return cy.get("#request-section-textarea")
    }

    contactServiceRequestSubmitBtn() {
        return cy.get("#request-section-submit-button")
    }

    chatBotSelector() {
        return cy.get('#solvemate-widget-button')
    }

    chatBotGreetings(greetingsMessage) {
        return cy.get('p').contains(greetingsMessage)
    }

    openChatBotSelector() {
        return cy.get('#area-to-open')
    }

    //Functions
    getRandomInt(max) {
        return Math.floor(Math.random() * max);
    }

    getIframeDocument = () => {
        return cy
            .get('#solvemate-widget-button')
            .its('0.contentDocument')
            .should('exist')
    }

    getIframeBody = () => {
        return this.getIframeDocument()
            .its('body')
            .should('not.be.undefined')
            .then(cy.wrap)
    }

    openChatBotFn() {
        cy.waitFor(this.chatBotSelector().should('be.visible'))
        cy.wait(1500)
        return this.getIframeBody().find('#area-to-open').click({force:true})
    }

    closeChatBotFn() {
        this.openChatBotFn()
        return this.getIframeBody().find('#area-to-close').click({force:true})
    }
}
