const openViewString = ["Open view", "Öffnen", "Ouvrir la vue", "Espandi"]
const orderOverviewStrg = ["Order Overview", "Bestellübersicht", "Aperçu des commandes", "Panoramica degli ordini"]

export default class ClaimsPage {
    //selectors
    contactBtnSelector() {
        return cy.get('#navigation-contact-service-team-button')
    }

    ordersOverviewBtnSelector() {
        return cy.get('#bottom-section-management-orders-link')
    }

    raiseClaimBtnSelector() {
        return cy.get('#bottom-section-management-raise-claim-link')
    }

    searchOrderByNumberInputFieldSelector() {
        return cy.get('#orders-overview-search-numbers-input')
    }

    openClaimsBtnSelector() {
        return cy.get('#orders-overview-open-claim-button')
    }

    orderDetailsViewBtnSelector(invoiceNumber) {
        return cy.get(`#order-details-invoice-details-button-${invoiceNumber}`)
    }

    rowCheckBox(globalnumber) {
        return cy.get(`[data-id="${globalnumber}"]`)
    }

    detailsRowCheckBox(globalnumber) {
        return this.rowCheckBox(globalnumber).find('[aria-label="Select row"]')
    }

    openOrderClaimBtnSelector() {
        return cy.get('#orders-details-open-order-claim-link')
    }

    openProductClaimSelector() {
        return cy.get('#orders-details-open-product-claim-link')
    }

    phoneBtnSelector() {
        return cy.get('#userdata-section-phone-button')
    }

    emailFieldSelector() {
        return cy.get('#userdata-section-input-field')
    }

    confirmIconSelector() {
        return cy.get('[data-testid="DoneIcon"]')
    }

    invalidEmailTooltip(errorMessage) {
        return cy.get('span').contains(errorMessage)
    }

    chooseOrderFieldSelector() {
        return cy.get('#orders-autocomplete')
    }
    chooseFirstOrderSelector(option) {
        return cy.get(`[id="orders-autocomplete-option-${option}"]`)
    }

    topicFieldSelector() {
        return cy.get('#request-section-select')
    }
    claimsOptionSelector() {
        return cy.get('[data-value="7"]')
    }

    tableBOMSelector() {
        return cy.get('.MuiDataGrid-row')
    }

    uploadImageSelector() {
        return cy.get('#claim-section-file-uploader-right-section')
    }
    uploadFilesCounterSelector() {
        return cy.get('#claim-section-file-uploader-left-section').find('h5')
    }

    messageInputFieldSelector() {
        return cy.get('#claim-section-text-area')
    }

    submitRequestBtn() {
        return cy.get('#claim-section-submit-button')
    }

    successfulMsgSelector() {
        return cy.get('[role="dialog"]').find('span')
    }

    reasonForClaimSelector(reason) {
        return cy.get(`[value="${reason}"]`)
    }

    qualityRelatedContainerSelector() {
        return cy.get('#claim-section-quality-related-button-group')
    }

    editPenQualityIcon() {
        return cy.get('[data-icon="pen"]').last()
    }

    quantityNumberField() {
        return cy.get('[type="number"]')
    }

    saveBtnByQuantity() {
        return cy.get('button').last()
    }

    qualityRelatedOptionsSelector(optionName) {
        return cy.get(`[id*="claim-section-quality-related-button-${optionName}"]`)
    }

    reorderBtnSelector() {
        return cy.get('#claim-section-preferred-solution-reorder-button')
    }

    checkBoxSelector() {
        return cy.get('[aria-label="Select row"]').first()
    }

    invoicesOverdueTooltipSelector() {
        return cy.get('#top-section-overdue-teaser-tooltip')
    }

    overdueTooltipTitleSelector() {
        return cy.get('#top-section-overdue-teaser-tooltip-title')
    }

    overdueTooltipOpenOverviewSelector() {
        return cy.get('[href="/my/dashboard/orders.html"]').eq(1)
    }

    overdueOpenViewBtnSelector(openViewOption) {
        return cy.get('#top-section-open-orders-box').find('span').contains(openViewOption)
    }

    orderOverviewHeaderTitleSelector() {
        return cy.get('#header-title')
    }

    firstGeneralDocumentSelector() {
        return cy.get('#bottom-section-general-docs-iso9001')
    }
    secondGeneralDocumentSelector() {
        return cy.get('#bottom-section-general-docs-iso14001')
    }
    thirdGeneralDocumentSelector() {
        return cy.get('#bottom-section-general-docs-rohs-reach')
    }
    fourthGeneralDocumentSelector() {
        return cy.get('#bottom-section-general-docs-supplier')
    }

    //functions
    pressContactButton() {
        this.contactBtnSelector().click()
    }

    pressOrdersOverviewBtn() {
        this.ordersOverviewBtnSelector().click()
    }

    searchOrderByNumber(orderNumber) {
        this.searchOrderByNumberInputFieldSelector().type(orderNumber)
    }

    searchOrderFn(numberOfOrder) {
        this.pressOrdersOverviewBtn()
        this.searchOrderByNumber(numberOfOrder)
    }

    typeEmailAddress(emailAddress) {
        this.emailFieldSelector().type(emailAddress)
    }

    chooseOrder(option) {
        this.topicFieldSelector().click()
        this.claimsOptionSelector().click()
        this.chooseOrderFieldSelector().click()
        this.chooseFirstOrderSelector(option).click()
    }

    chooseClaim() {
        this.topicFieldSelector().click()
        this.claimsOptionSelector().click()
    }

    fileUpload(filePath) {
        return this.uploadImageSelector().attachFile(filePath, { subjectType: 'drag-n-drop' })
    }

    checkNumberOfUploadedFiles(numberOfFiles) {
        this.uploadFilesCounterSelector().should('contain.text', `(${numberOfFiles}/5)`)
    }

    typeMessage() {
        this.messageInputFieldSelector().type('Claim by Test automation')
    }

    checkIfRequestIsSucc(successfulMessage) {
        this.successfulMsgSelector().should('contain.text', successfulMessage)
    }

    checkWhichClaimReasonIsSelected(reason) {
        this.reasonForClaimSelector(reason).should('have.css', 'background-color', 'rgb(37, 74, 149)')
    }

    selectClaimReason(reason) {
        this.reasonForClaimSelector(reason).click()
    }

    selectQualityRelatedOption(option) {
        this.qualityRelatedContainerSelector().should('be.visible')
        this.qualityRelatedOptionsSelector(option).click()
    }

    addQuantity(qty) {
        cy.get('body').then($body => {
            if ($body.find('[role="grid"]').length) {
                this.editPenQualityIcon().click()
                this.quantityNumberField().type(qty)
                this.saveBtnByQuantity().click()
            } else {
                cy.log("There are no BOMs to this order")
            }
        })

    }

    checkDocumentOpenedInNewTab(order) {
        switch (order) {
            case 1:
                this.firstGeneralDocumentSelector().should('have.attr', 'target', '_blank')
                this.firstGeneralDocumentSelector().then(link => {
                    cy.request(link.prop('href')).its('status').should('eq', 200);
                })
                break;
            case 2:
                this.secondGeneralDocumentSelector().should('have.attr', 'target', '_blank')
                this.secondGeneralDocumentSelector().then(link => {
                    cy.request(link.prop('href')).its('status').should('eq', 200);
                })
                break;
            case 3:
                this.thirdGeneralDocumentSelector().should('have.attr', 'target', '_blank')
                this.thirdGeneralDocumentSelector().then(link => {
                    cy.request(link.prop('href')).its('status').should('eq', 200);
                })
                break;
            case 4:
                this.fourthGeneralDocumentSelector().should('have.attr', 'target', '_blank')
                this.fourthGeneralDocumentSelector().then(link => {
                    cy.request(link.prop('href')).its('status').should('eq', 200);
                })
                break;
        }
    }

    orderOverviewHeaderTitleFn(headerTitle) {
        this.orderOverviewHeaderTitleSelector().should('contain.text', `${headerTitle}`)
    }

    typeEmailOrPhoneNumberFn(phoneOrEmail) {
        this.emailFieldSelector().type(phoneOrEmail)
        this.topicFieldSelector().dblclick()
    }

    takeRequestInfo(numberOfBOC) {
        let globalNumbersArray = []
        let partNumberArray = []
        let quantityArray = []

        let requestInfo = {
            bomSelectedRows: [],
            bomSelectedRowsProductCodes: [],
            bomSelectedRowsQuantities: []
        }

        for (let i = 0; i < numberOfBOC; i++) {
            cy.get('[data-id*=G]').eq(i).invoke('attr', 'data-id').as("invoice").then(($invoice) => {
                cy.get(`[data-id="${$invoice}"]`).find('[type="checkbox"]').click()
                globalNumbersArray.push($invoice) //get global number

                cy.get(`[data-id="${$invoice}"]`).find('[data-field="product_cd"]').as("partNum").then(($pn) => {
                    partNumberArray.push($pn.text().toString()) //get part number
                })

                cy.get(`[data-id="${$invoice}"]`).find('[data-field="so_qty"]').as("quantity").then(($qt) => {
                    quantityArray.push(`${$qt.text()}.0`) //get quantity
                })
            })
        }

        cy.get('@invoice').then(() => requestInfo.bomSelectedRows.push(globalNumbersArray)) //push globalArray in object
        cy.get('@partNum').then(() => requestInfo.bomSelectedRowsProductCodes.push(partNumberArray)) //push PNArray in object
        cy.get('@quantity').then(() => requestInfo.bomSelectedRowsQuantities.push(quantityArray)) //push QuantityArray in object

        cy.log(requestInfo)

        return requestInfo;
    }

    openOverviewFn(openView, orderOverView) {
        cy.get('#top-section-open-orders-box').then($order => {
            if ($order.find('#top-section-open-orders-counter').length) {
                this.overdueOpenViewBtnSelector(openViewString[openView]).click()
                cy.url().should('contain', '/my/dashboard/orders.html') //check if the URL is correct
                this.orderOverviewHeaderTitleFn(orderOverviewStrg[orderOverView])//check if the header title is visibleF
            } else {
                cy.log("There is no counter")
            }
        })
    }

    hoverOverOverdueInvoicesBtn(int) {
        cy.get('#top-section-open-invoices-box').then($invoice => {
            if ($invoice.find('#top-section-due-counter').length) {
                this.invoicesOverdueTooltipSelector().trigger('mouseover') //hover over the tooltip
                this.invoicesOverdueTooltipSelector().should('be.visible') //check if the tooltip is opened
                if(int==1){
                    this.orderOverviewHeaderTitleSelector().should('be.visible')
                }
            } else {
                cy.log("There are no invoices and no tooltip")
            }
        })
    }


}
