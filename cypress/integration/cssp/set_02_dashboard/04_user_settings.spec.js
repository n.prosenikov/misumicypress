import Login from "../../boc/pom/login"
import Dashboard from '../pom/dashboard';

const dashboard = new Dashboard()
const loginPage = new Login()

const mySettingsInfo = "settings"

const authUser = require('../../../fixtures/auth-user.json');
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('Dashboard - User Settings', () => {
    beforeEach(() => {
        loginPage.login(username, password)
        dashboard.goToTheLink()
    });

    it('User Settings options exist [UK]', { baseUrl: url_uk }, () => {
        dashboard.userSettingsOptionsExist() //check if all options are visible
    })

    it('User Settings options exist [DE]', { baseUrl: url_de }, () => {
        dashboard.userSettingsOptionsExist()
    })

    it('User Settings options exist [FR]', { baseUrl: url_fr }, () => {
        dashboard.userSettingsOptionsExist()
    })

    it('User Settings options exist [IT]', { baseUrl: url_it }, () => {
        dashboard.userSettingsOptionsExist()
    })

    it('Hover over "My User Settings" Info Circle button [UK]', { baseUrl: url_uk }, () => {
        dashboard.hoverOverInfoCircle(mySettingsInfo)
    })

    it('Hover over "My User Settings" Info Circle button [DE]', { baseUrl: url_de }, () => {
        dashboard.hoverOverInfoCircle(mySettingsInfo)
    })

    it('Hover over "My User Settings" Info Circle button [FR]', { baseUrl: url_fr }, () => {
        dashboard.hoverOverInfoCircle(mySettingsInfo)
    })

    it('Hover over "My User Settings" Info Circle button [IT]', { baseUrl: url_it }, () => {
        dashboard.hoverOverInfoCircle(mySettingsInfo)
    })

})