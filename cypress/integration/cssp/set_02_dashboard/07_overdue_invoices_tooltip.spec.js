import CsspPage from '../pom/cssp';
import Login from "../../boc/pom/login"
import ClaimsPage from '../pom/claims';
import OrderAndFilter from '../pom/orderAndFilter';

const loginPage = new Login()
const orderFilter = new OrderAndFilter()
const claimsPage = new ClaimsPage()


const authUser = require('../../../fixtures/auth-user.json');
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('Overdue Invoices information by hover', () => {
    beforeEach(() => {
        loginPage.login(username, password)
        cy.visit('/my/dashboard/orders.html')
    });

    it('Hover over Overdue Invoices button [UK]', { baseUrl: url_uk }, () => {
        orderFilter.chooseFilterOptions("invoice-due")
        cy.get('body').then(element => {
            if (element.find('[data-id*="LN"]').length) {
                orderFilter.backToDashboardBtn().click()
                claimsPage.hoverOverOverdueInvoicesBtn(0)
            } else {
                cy.log("There are no invoice overdues")
            }
        })
    })

    it('Hover over Overdue Invoices button [DE]', { baseUrl: url_de }, () => {
        orderFilter.chooseFilterOptions("invoice-due")
        cy.get('body').then(element => {
            if (element.find('[data-id*="LN"]').length) {
                orderFilter.backToDashboardBtn().click()
                claimsPage.hoverOverOverdueInvoicesBtn(0)
            } else {
                cy.log("There are no invoice overdues")
            }
        })
    })

    it('Hover over Overdue Invoices button [FR]', { baseUrl: url_fr }, () => {
        orderFilter.chooseFilterOptions("invoice-due")
        cy.get('body').then(element => {
            if (element.find('[data-id*="LN"]').length) {
                orderFilter.backToDashboardBtn().click()
                claimsPage.hoverOverOverdueInvoicesBtn(0)
            } else {
                cy.log("There are no invoice overdues")
            }
        })
    })

    it('Hover over Overdue Invoices button [IT]', { baseUrl: url_it }, () => {
        orderFilter.chooseFilterOptions("invoice-due")
        cy.get('body').then(element => {
            if (element.find('[data-id*="LN"]').length) {
                orderFilter.backToDashboardBtn().click()
                claimsPage.hoverOverOverdueInvoicesBtn(0)
            } else {
                cy.log("There are no invoice overdues")
            }
        })
    })

    it('Press open view button [UK]', { baseUrl: url_uk }, () => {
        orderFilter.backToDashboardBtn().click()
        claimsPage.openOverviewFn(0, 0) //0-UK, 1-DE, 2-FR, 3-IT
    })

    it('Press open view button [DE]', { baseUrl: url_de }, () => {
        orderFilter.backToDashboardBtn().click()
        claimsPage.openOverviewFn(1, 1) //0-UK, 1-DE, 2-FR, 3-IT
    })

    it('Press open view button [FR]', { baseUrl: url_fr }, () => {
        orderFilter.backToDashboardBtn().click()
        claimsPage.openOverviewFn(2, 2) //0-UK, 1-DE, 2-FR, 3-IT
    })

    it('Press open view button [IT]', { baseUrl: url_it }, () => {
        orderFilter.backToDashboardBtn().click()
        claimsPage.openOverviewFn(3, 3) //0-UK, 1-DE, 2-FR, 3-IT
    })

    it('Open Overview button in the tooltip visible [UK]', { baseUrl: url_uk }, () => {
        orderFilter.chooseFilterOptions("invoice-due")
        cy.get('body').then(element => {
            if (element.find('[data-id*="LN"]').length) {
                orderFilter.backToDashboardBtn().click()
                claimsPage.hoverOverOverdueInvoicesBtn(1)
            } else {
                cy.log("There are no invoice overdues")
            }
        })
    })

    it('Open Overview button in the tooltip visible [DE]', { baseUrl: url_de }, () => {
        orderFilter.chooseFilterOptions("invoice-due")
        cy.get('body').then(element => {
            if (element.find('[data-id*="LN"]').length) {
                orderFilter.backToDashboardBtn().click()
                claimsPage.hoverOverOverdueInvoicesBtn(1)
            } else {
                cy.log("There are no invoice overdues")
            }
        })
    })

    it('Open Overview button in the tooltip visible [FR]', { baseUrl: url_fr }, () => {
        orderFilter.chooseFilterOptions("invoice-due")
        cy.get('body').then(element => {
            if (element.find('[data-id*="LN"]').length) {
                orderFilter.backToDashboardBtn().click()
                claimsPage.hoverOverOverdueInvoicesBtn(1)
            } else {
                cy.log("There are no invoice overdues")
            }
        })
    })

    it('Open Overview button in the tooltip visible [IT]', { baseUrl: url_it }, () => {
        orderFilter.chooseFilterOptions("invoice-due")
        cy.get('body').then(element => {
            if (element.find('[data-id*="LN"]').length) {
                orderFilter.backToDashboardBtn().click()
                claimsPage.hoverOverOverdueInvoicesBtn(1)
            } else {
                cy.log("There are no invoice overdues")
            }
        })
    })

})