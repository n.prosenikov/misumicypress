import Login from "../../boc/pom/login"
import Dashboard from '../pom/dashboard';

const loginPage = new Login()
const dashboard = new Dashboard()


const authUser = require('../../../fixtures/auth-user.json');
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('Dashboard - My Personal Info', () => {
    beforeEach(() => {
        loginPage.login(username, password)
        dashboard.goToTheLink()
    });

    it('My Personal Info options exist [UK]', { baseUrl: url_uk }, () => {
        dashboard.myPersonalInfoOptionsExist() //check if all options are visible
    })

    it('My Personal Info options exist [DE]', { baseUrl: url_de }, () => {
        dashboard.myPersonalInfoOptionsExist()
    })

    it('My Personal Info options exist [FR]', { baseUrl: url_fr }, () => {
        dashboard.myPersonalInfoOptionsExist()
    })

    it('My Personal Info options exist [IT]', { baseUrl: url_it }, () => {
        dashboard.myPersonalInfoOptionsExist()
    })

    
})