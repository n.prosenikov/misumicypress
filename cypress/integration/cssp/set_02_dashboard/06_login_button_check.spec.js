import Login from "../../boc/pom/login"
import LoginButton from "../pom/login_button";

const loginPage = new Login()
const loginBtn = new LoginButton

const authUser = require('../../../fixtures/auth-user.json');
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('Login Button Functionality', () => {
    beforeEach(() => {
        loginPage.login(username, password)
    });

    it('Login Btn and all options in the menu exist [UK]', { baseUrl: url_uk }, () => {
        cy.visit('/my/dashboard/')
        loginBtn.loginBtnSelector().trigger('mouseover')
        loginBtn.allInfoShouldBeVisible()
    })

    it('Login Btn and all options in the menu exist [DE]', { baseUrl: url_de }, () => {
        cy.visit('/my/dashboard/')
        loginBtn.loginBtnSelector().trigger('mouseover')
        loginBtn.allInfoShouldBeVisible()
    })

    it('Login Btn and all options in the menu exist [FR]', { baseUrl: url_fr }, () => {
        cy.visit('/my/dashboard/')
        loginBtn.loginBtnSelector().trigger('mouseover')
        loginBtn.allInfoShouldBeVisible()
    })

    it('Login Btn and all options in the menu exist [IT]', { baseUrl: url_it }, () => {
        cy.visit('/my/dashboard/')
        loginBtn.loginBtnSelector().trigger('mouseover')
        loginBtn.allInfoShouldBeVisible()
    })

    it('"My Misumi" Button functionaluty [UK]', { baseUrl: url_uk }, () => {
        cy.visit('/')
        loginBtn.loginBtnSelector().trigger('mouseover')
        loginBtn.myMisumiBtn().click()
        cy.url().should('include', '/my/dashboard/')
        loginBtn.welcomeHeaderSelector().should('be.visible')
    })

    it('"My Misumi" Button functionaluty [DE]', { baseUrl: url_de }, () => {
        cy.visit('/')
        loginBtn.loginBtnSelector().trigger('mouseover')
        loginBtn.myMisumiBtn().click()
        cy.url().should('include', '/my/dashboard/')
        loginBtn.welcomeHeaderSelector().should('be.visible')
    })

    it('"My Misumi" Button functionaluty [FR]', { baseUrl: url_fr }, () => {
        cy.visit('/')
        loginBtn.loginBtnSelector().trigger('mouseover')
        loginBtn.myMisumiBtn().click()
        cy.url().should('include', '/my/dashboard/')
        loginBtn.welcomeHeaderSelector().should('be.visible')
    })

    it('"My Misumi" Button functionaluty [IT]', { baseUrl: url_it }, () => {
        cy.visit('/')
        loginBtn.loginBtnSelector().trigger('mouseover')
        loginBtn.myMisumiBtn().click()
        cy.url().should('include', '/my/dashboard/')
        loginBtn.welcomeHeaderSelector().should('be.visible')
    })

    it('Logout Button functionality [UK]', { baseUrl: url_uk }, () => {
        cy.visit('/')
        loginBtn.loginBtnSelector().trigger('mouseover')
        loginBtn.myMisumiBtn().click()
        loginBtn.welcomeHeaderSelector().should('be.visible')
        loginBtn.loginBtnSelector().trigger('mouseover')
        loginBtn.logOutBtnSelector().click()
        loginBtn.loginRegisterBtn().should('be.visible')
    })

    it('Logout Button functionality [DE]', { baseUrl: url_de }, () => {
        cy.visit('/')
        loginBtn.loginBtnSelector().trigger('mouseover')
        loginBtn.myMisumiBtn().click()
        loginBtn.welcomeHeaderSelector().should('be.visible')
        loginBtn.loginBtnSelector().trigger('mouseover')
        loginBtn.logOutBtnSelector().click()
        loginBtn.loginRegisterBtn().should('be.visible')
    })

    it('Logout Button functionality [FR]', { baseUrl: url_fr }, () => {
        cy.visit('/')
        loginBtn.loginBtnSelector().trigger('mouseover')
        cy.wait(1000)
        loginBtn.myMisumiBtn().click()
        loginBtn.welcomeHeaderSelector().should('be.visible')
        loginBtn.loginBtnSelector().trigger('mouseover')
        loginBtn.logOutBtnSelector().click()
        loginBtn.loginRegisterBtn().should('be.visible')
    })

    it('Logout Button functionality [IT]', { baseUrl: url_it }, () => {
        cy.visit('/')
        loginBtn.loginBtnSelector().trigger('mouseover')
        loginBtn.myMisumiBtn().click()
        loginBtn.welcomeHeaderSelector().should('be.visible')
        loginBtn.loginBtnSelector().trigger('mouseover')
        loginBtn.logOutBtnSelector().click()
        loginBtn.loginRegisterBtn().should('be.visible')
    })


})