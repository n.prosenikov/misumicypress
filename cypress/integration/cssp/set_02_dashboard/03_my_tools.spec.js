import Login from "../../boc/pom/login"
import Dashboard from '../pom/dashboard';

const dashboard = new Dashboard()
const loginPage = new Login()

const myToolsInfo = "tools"

const authUser = require('../../../fixtures/auth-user.json');
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('Dashboard - My Tools', () => {
    beforeEach(() => {
        loginPage.login(username, password)
        dashboard.goToTheLink()
    });

    it('My Tools options exist [UK]', { baseUrl: url_uk }, () => {
        dashboard.myToolsOptionsExist() //check if all options are visible
    })

    it('My Tools options exist [DE]', { baseUrl: url_de }, () => {
        dashboard.myToolsOptionsExist()
    })

    it('My Tools options exist [FR]', { baseUrl: url_fr }, () => {
        dashboard.myToolsOptionsExist()
    })

    it('My Tools options exist [IT]', { baseUrl: url_it }, () => {
        dashboard.myToolsOptionsExist()
    })

    it('My Archive options exist [UK]', { baseUrl: url_uk }, () => {
        dashboard.myArchiveOptionsExist() //check if all options in the archove menu are visible
    })

    it('My Archive options exist [DE]', { baseUrl: url_de }, () => {
        dashboard.myArchiveOptionsExist()
    })

    it('My Archive options exist [FR]', { baseUrl: url_fr }, () => {
        dashboard.myArchiveOptionsExist()
    })

    it('My Archive options exist [IT]', { baseUrl: url_it }, () => {
        dashboard.myArchiveOptionsExist()
    })

    it('Hover over "My Tools" Info Circle button [UK]', { baseUrl: url_uk }, () => {
        dashboard.hoverOverToolsInfoCircle(myToolsInfo)
    })

    it('Hover over "My Tools" Info Circle button [DE]', { baseUrl: url_de }, () => {
       dashboard.hoverOverToolsInfoCircle(myToolsInfo)
    })

    it('Hover over "My Tools" Info Circle button [FR]', { baseUrl: url_fr }, () => {
       dashboard.hoverOverToolsInfoCircle(myToolsInfo)
    })

    it('Hover over "My Tools" Info Circle button [IT]', { baseUrl: url_it }, () => {
       dashboard.hoverOverToolsInfoCircle(myToolsInfo)
    })

})