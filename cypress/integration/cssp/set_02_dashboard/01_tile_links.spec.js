import Login from "../../boc/pom/login"
import Dashboard from '../pom/dashboard';

const loginPage = new Login()
const dashboard = new Dashboard()


const authUser = require('../../../fixtures/auth-user.json');
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('Dashboard Tile Links', () => {
    beforeEach(() => {
        loginPage.login(username, password)
        dashboard.goToTheLink()
    });

    it('Orders box visible [UK]', { baseUrl: url_uk }, () => {
        dashboard.topOrdersBoxSelector().should('contain.text', 'Orders')
        dashboard.checkOrdersCounterExist()
    })

    it('Orders box visible [DE]', { baseUrl: url_de }, () => {
        dashboard.topOrdersBoxSelector().should('contain.text', 'Bestellungen')
        dashboard.checkOrdersCounterExist()
    })

    it('Orders box visible [FR]', { baseUrl: url_fr }, () => {
        dashboard.topOrdersBoxSelector().should('contain.text', 'Commandes')
        dashboard.checkOrdersCounterExist()
    })

    it('Orders box visible [IT]', { baseUrl: url_it }, () => {
        dashboard.topOrdersBoxSelector().should('contain.text', 'Ordini')
        dashboard.checkOrdersCounterExist()
    })

    it('Delivery box visible [UK]', { baseUrl: url_uk }, () => {
        dashboard.topDeliveryBoxSelector().should('contain.text', 'Delivery')
        dashboard.checkDeliveryCounterExist()
    })

    it('Delivery box visible [DE]', { baseUrl: url_de }, () => {
        dashboard.topDeliveryBoxSelector().should('contain.text', 'Lieferung')
        dashboard.checkDeliveryCounterExist()
    })

    it('Delivery box visible [FR]', { baseUrl: url_fr }, () => {
        dashboard.topDeliveryBoxSelector().should('contain.text', 'Livraison')
        dashboard.checkDeliveryCounterExist()
    })

    it('Delivery box visible [IT]', { baseUrl: url_it }, () => {
        dashboard.topDeliveryBoxSelector().should('contain.text', 'Consegna')
        dashboard.checkDeliveryCounterExist()
    })

    it('Invoices box visible [UK]', { baseUrl: url_uk }, () => {
        dashboard.topInvoicesBoxSelector().should('contain.text', 'Invoices')
        dashboard.checkInvoicesCounterExist()
    })

    it('Invoices box visible [DE]', { baseUrl: url_de }, () => {
        dashboard.topInvoicesBoxSelector().should('contain.text', 'Rechnungen')
        dashboard.checkInvoicesCounterExist()
    })

    it('Invoices box visible [FR]', { baseUrl: url_fr }, () => {
        dashboard.topInvoicesBoxSelector().should('contain.text', 'Factures')
        dashboard.checkInvoicesCounterExist()
    })

    it('Invoices box visible [IT]', { baseUrl: url_it }, () => {
        dashboard.topInvoicesBoxSelector().should('contain.text', 'Fatture')
        dashboard.checkInvoicesCounterExist()
    })

})