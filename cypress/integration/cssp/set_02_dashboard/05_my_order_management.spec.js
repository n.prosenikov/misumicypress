import Login from "../../boc/pom/login"
import Dashboard from '../pom/dashboard';

const dashboard = new Dashboard()
const loginPage = new Login()

const myManagementInfo = "management"

const authUser = require('../../../fixtures/auth-user.json');
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('Dashboard - My Order Management', () => {
    beforeEach(() => {
        loginPage.login(username, password)
        dashboard.goToTheLink()
    });

    it('My Order Management options exist [UK]', { baseUrl: url_uk }, () => {
        dashboard.myOrderManagementOptionsExist() //check if all options are visible
    })

    it('My Order Management options exist [DE]', { baseUrl: url_de }, () => {
        dashboard.myOrderManagementOptionsExist()
    })

    it('My Order Management options exist [FR]', { baseUrl: url_fr }, () => {
        dashboard.myOrderManagementOptionsExist()
    })

    it('My Order Management options exist [IT]', { baseUrl: url_it }, () => {
        dashboard.myOrderManagementOptionsExist()
    })

    it('Hover over "My Order Management" Info Circle button [UK]', { baseUrl: url_uk }, () => {
        dashboard.hoverOverInfoCircle(myManagementInfo)
    })

    it('Hover over "My Order Management" Info Circle button [DE]', { baseUrl: url_de }, () => {
        dashboard.hoverOverInfoCircle(myManagementInfo)
    })

    it('Hover over "My Order Management" Info Circle button [FR]', { baseUrl: url_fr }, () => {
        dashboard.hoverOverInfoCircle(myManagementInfo)
    })

    it('Hover over "My Order Management" Info Circle button [IT]', { baseUrl: url_it }, () => {
        dashboard.hoverOverInfoCircle(myManagementInfo)
    })

})