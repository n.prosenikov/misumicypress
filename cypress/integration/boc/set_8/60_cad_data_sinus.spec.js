import Login from '../pom/login';

const loginPage = new Login()

const authUser = require('../../../fixtures/auth-user.json');
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('CAD data', () => {

  beforeEach(() => {
    loginPage.login(username, password)
    cy.visit('/')
    cy.wait(1500)
  });


  it('UK SSTHWR8- normal CADENAS flow', { baseUrl: url_uk }, () => {
    cy.intercept('POST', '**/vona2/token/check/**').as('partLoad')
    cy.searchPart('SSTHWR8')
    cy.wait(1000)
    cy.wait("@partLoad")
    cy.intercept('POST', '**/sinus/api/v1/cad/preview**').as('sinusLoad')
    cy.get('#Tab_preview').click()
    cy.get('#Tab_preview').should("have.css", "background-color", "rgb(116, 157, 207)")

    //cy.wait("@sinusLoad")

    // cy.wait(10000)

    // cy.get('#preview_iframe').then($iframe => {
    //   const $body = $iframe.contents().find('body')
    //   cy.wrap($body)
    //     .find('#c-canvas--sinus')
    //     .should("have.css", "background-color", "rgb(116, 157, 207)")
    // })
  })

  it('DE SSTHWR8', { baseUrl: url_de }, () => {

    cy.intercept('POST', '**/vona2/token/check/**').as('partLoad')
    cy.searchPart('SSTHWR8')
    cy.wait(1000)
    cy.wait("@partLoad")
    cy.intercept('POST', '**/sinus/api/v1/cad/preview**').as('sinusLoad')
    cy.get('#Tab_preview').click()
    cy.get('#Tab_preview').should("have.css", "background-color", "rgb(116, 157, 207)")

  })

  it('IT SSTHWR8 - normal CADENAS flow', { baseUrl: url_it }, () => {
    cy.intercept('POST', '**/vona2/token/check/**').as('partLoad')
    cy.searchPart('SSTHWR8')
    cy.wait(1000)
    cy.wait("@partLoad")
    cy.intercept('POST', '**/sinus/api/v1/cad/preview**').as('sinusLoad')
    cy.get('#Tab_preview').click()
    cy.get('#Tab_preview').should("have.css", "background-color", "rgb(116, 157, 207)")
  })

  it('FR SSTHWR8 - normal CADENAS flow', { baseUrl: url_fr }, () => {
    cy.intercept('POST', '**/vona2/token/check/**').as('partLoad')
    cy.searchPart('SSTHWR8')
    cy.wait(1000)
    cy.wait("@partLoad")
    cy.intercept('POST', '**/sinus/api/v1/cad/preview**').as('sinusLoad')
    cy.get('#Tab_preview').click()
    cy.get('#Tab_preview').should("have.css", "background-color", "rgb(116, 157, 207)")
  })

  it('FR SCS12-8 - normal CADENAS flow', { baseUrl: url_fr }, () => {
    cy.intercept('POST', '**/vona2/token/check/**').as('partLoad')
    cy.searchPart('SCS12-8')
    cy.wait(1000)
    cy.wait("@partLoad")
    cy.intercept('POST', '**/sinus/api/v1/cad/preview**').as('sinusLoad')
    cy.get('#Tab_preview').click()
    cy.contains("Standard")
  })

  it('Add to cart SINUS CAD', { baseUrl: url_fr }, () => {
    cy.intercept('POST', '**/vona2/token/check/**').as('partLoad')
    cy.searchPart('SCS12-8')
    cy.wait(1000)
    cy.wait("@partLoad")
    cy.intercept('POST', '**/sinus/api/v1/cad/preview**').as('sinusLoad')
    cy.get('#Tab_preview').click()
    cy.get('[data-cartbox-btn="cart"]').should('exist')
  })

  it('3D Preview Part Number check', { baseUrl: url_uk }, () => {
    cy.intercept('POST', '**/vona2/token/check/**').as('partLoad')
    cy.searchPart('SCS12-8')
    cy.wait(1000)
    cy.wait("@partLoad")
    cy.intercept('POST', '**/sinus/api/v1/cad/preview**').as('sinusLoad')
    cy.get('#Tab_preview').click()
    cy.contains('SCS12-8')
  })

  it('3D Preview Part Number check', { baseUrl: url_fr }, () => {
    cy.intercept('POST', '**/vona2/token/check/**').as('partLoad')
    cy.searchPart('SCS12-8')
    cy.wait(1000)
    cy.wait("@partLoad")
    cy.intercept('POST', '**/sinus/api/v1/cad/preview**').as('sinusLoad')
    cy.get('#Tab_preview').click()
    cy.contains('SCS12-8')
  })

  it('UK MBJ22-60 Error 500', { baseUrl: url_uk }, () => {
    cy.intercept('POST', '**/vona2/token/check/**').as('partLoad')
    cy.searchPart('MBJ22-60')
    cy.wait(1000)
    cy.wait("@partLoad")
    cy.intercept('POST', '**/sinus/api/v1/cad/preview**').as('sinusLoad')
    cy.get('#Tab_preview').click()
    cy.contains('MBJ22-60')
  })

  it('UK M-44-B CAD not exist', { baseUrl: url_uk }, () => {
    cy.intercept('POST', '**/vona2/token/check/**').as('partLoad')
    cy.searchPart('M-44-B')
    cy.wait(1000)
    cy.wait("@partLoad")
    cy.intercept('POST', '**/sinus/api/v1/cad/preview**').as('sinusLoad')
    cy.get('#Tab_preview').should('have.length', 0)
  })
});

// test.page(environment.url_de).httpAuth({
//   username: environment.basic_auth_user,
//   password: environment.basic_auth_password,
// })("Add to cart SINUS CAD", async () => {
//   // await login(t)
//   // await cartPage.deleteCart(t);
//   // await cartPage.addCartItem(t, "LRBS110-115", 1)
// });

// test.page(environment.url_de)("3D Preview Part Number check", async () => {});

// test.page(environment.url_de)("UK HD-B8-20-TC2 Error 500", async () => {});

// test.page(environment.url_de)("UK MBJ22-60 Error 500", async () => {});

// test.page(environment.url_de)("UK M-TCN Error 404", async () => {});

// test.page(environment.url_it)(
//   "UK SFJ10-100 - normal CADENAS flow",
//   async () => {
//     // await checkSinusCAD(SFJ10_100_part, SFJ10_100_part_screenshot)
//   }
// );

// test.page(environment.url_it)("UK LRBS110", async () => {
//   // await checkSinusCAD(LRBS110_part, LRBS110_part_screenshot)
// });

// test.page(environment.url_it)(
//   "UK PSTS10A-30 - normal CADENAS flow",
//   async () => {
//     // await checkSinusCAD(PSTS10A_30_part, PSTS10A_30_part_screenshot)
//   }
// );

// test.page(environment.url_it)("UK RBNT5 - normal CADENAS flow", async () => {
//   // await checkSinusCAD(RBNT5_part, RBNT5_part_screenshot)
// });

// test.page(environment.url_it)(
//   "UK HCCG4-10 - normal CADENAS flow",
//   async () => {
//     // await checkSinusCAD(HCCG4_10_part, HCCG4_10_part_screenshot)
//   }
// );

// test.page(environment.url_it).httpAuth({
//   username: environment.basic_auth_user,
//   password: environment.basic_auth_password,
// })("Add to cart SINUS CAD", async () => {
//   // await login(t)
//   // await cartPage.deleteCart(t);
//   // await cartPage.addCartItem(t, "LRBS110-115", 1)
// });

// test.page(environment.url_it)("3D Preview Part Number check", async () => {});

// test.page(environment.url_it)("UK HD-B8-20-TC2 Error 500", async () => {});

// test.page(environment.url_it)("UK MBJ22-60 Error 500", async () => {});

// test.page(environment.url_it)("UK M-TCN Error 404", async () => {});

// test.page(environment.url_fr)(
//   "UK SFJ10-100 - normal CADENAS flow",
//   async () => {
//     // await checkSinusCAD(SFJ10_100_part, SFJ10_100_part_screenshot)
//   }
// );

// test.page(environment.url_fr)("UK LRBS110", async () => {
//   // await checkSinusCAD(LRBS110_part, LRBS110_part_screenshot)
// });

// test.page(environment.url_fr)(
//   "UK PSTS10A-30 - normal CADENAS flow",
//   async () => {
//     // await checkSinusCAD(PSTS10A_30_part, PSTS10A_30_part_screenshot)
//   }
// );

// test.page(environment.url_fr)("UK RBNT5 - normal CADENAS flow", async () => {
//   // await checkSinusCAD(RBNT5_part, RBNT5_part_screenshot)
// });

// test.page(environment.url_fr)(
//   "UK HCCG4-10 - normal CADENAS flow",
//   async () => {
//     // await checkSinusCAD(HCCG4_10_part, HCCG4_10_part_screenshot)
//   }
// );

// test.page(environment.url_fr).httpAuth({
//   username: environment.basic_auth_user,
//   password: environment.basic_auth_password,
// })("Add to cart SINUS CAD", async () => {
  // await login(t)
  // await cartPage.deleteCart(t);
  // await cartPage.addCartItem(t, "LRBS110-115", 1)
// });

// test.page(environment.url_fr)("3D Preview Part Number check", async () => {});

// test.page(environment.url_fr)("UK HD-B8-20-TC2 Error 500", async () => {});

// test.page(environment.url_fr)("UK MBJ22-60 Error 500", async () => {});

// test.page(environment.url_fr)("UK M-TCN Error 404", async () => {});

// SFJ10-100 - normal CADENAS flow
// PSTS10A-30 - normal CADENAS flow
// RBNT5 - normal CADENAS flow
// HCCG4-10 - normal CADENAS flow

// SINUS CAD part numbers

// LRBS110-15 - normal functionality flow
// HD-B8-20-TC2 - error 500
// MBJ22-60 - error 500
// M-TCN - error 404
