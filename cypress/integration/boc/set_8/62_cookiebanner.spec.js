import Login from '../pom/login';

const authUser = require('../../../fixtures/auth-user.json');
const loginPage = new Login()
const {
	url_uk,
	url_de,
	url_fr,
	url_it
} = authUser;
const LRBS110_part = '/vona2/mech_material/M1401000000/M1401020000/'

describe('Cookie Banner', () => {
	beforeEach(() => {
		cy.clearCookies()
	});
	// ACCEPT
	it('Accept Cookie Banner UK', { baseUrl: url_uk }, () => {
		let assertCookie
		cy.visit(url_uk)
		cy.wait(1000)
		cy.clearCookies()
		cy.getCookies().then(cookie => {
			assert.isAtMost(cookie.length, 1)
		})
		loginPage.acceptCookies().click()
		cy.getCookies().then(cookie => {
			cookie.map(cookie => {
				if (JSON.stringify(cookie.name) === '"cms_cookie"') {
					assertCookie = JSON.stringify(cookie.name)
				}
			})
			expect(assertCookie).eql('"cms_cookie"')
		})
	})

	it('Accept Cookie Banner IT', { baseUrl: url_it }, () => {
		let assertCookie
		cy.visit(url_it)
		cy.wait(1000)
		cy.clearCookies()
		cy.getCookies().then(cookie => {
			assert.isAtMost(cookie.length, 1)
		})
		loginPage.acceptCookies().click()
		cy.getCookies().then(cookie => {
			cookie.map(cookie => {
				if (JSON.stringify(cookie.name) === '"cms_cookie"') {
					assertCookie = JSON.stringify(cookie.name)
				}
			})
			expect(assertCookie).eql('"cms_cookie"')
		})
	})

	it('Accept Cookie Banner DE', { baseUrl: url_de }, () => {
		let assertCookie
		cy.visit(url_de)
		cy.wait(1000)
		cy.clearCookies()
		cy.getCookies().then(cookie => {
			assert.isAtMost(cookie.length, 1)
		})
		loginPage.acceptCookies().click()
		cy.getCookies().then(cookie => {
			cookie.map(cookie => {
				if (JSON.stringify(cookie.name) === '"cms_cookie"') {
					assertCookie = JSON.stringify(cookie.name)
				}
			})
			expect(assertCookie).eql('"cms_cookie"')
		})
	})

	it('Accept Cookie Banner FR', { baseUrl: url_fr }, () => {
		let assertCookie
		cy.visit(url_fr)
		cy.wait(1000)
		cy.clearCookies()
		cy.getCookies().then(cookie => {
			assert.isAtMost(cookie.length, 1)
		})
		loginPage.acceptCookies().click()
		cy.getCookies().then(cookie => {
			cookie.map(cookie => {
				if (JSON.stringify(cookie.name) === '"cms_cookie"') {
					assertCookie = JSON.stringify(cookie.name)
				}
			})
			expect(assertCookie).eql('"cms_cookie"')
		})
	})

	// Not Accepting case - apparently not needed

	// ESSENTIAL
	it('Essential Cookie Banner UK', { baseUrl: url_uk }, () => {
		let assertCookie
		cy.visit(url_uk)
		cy.wait(1000)
		cy.clearCookies()
		cy.getCookies().then(cookie => {
			cy.log(JSON.stringify(cookie))
			assert.isAtMost(cookie.length, 1)
		})
		loginPage.essentialCookies().click()
		cy.getCookies().then(cookie => {
			cookie.map(cookie => {
				if (JSON.stringify(cookie.name) === '"cms_cookie"') {
					assertCookie = JSON.stringify(cookie.name)
				}
			})
			expect(assertCookie).eql('"cms_cookie"')
		})
	})

	it('Essential Cookie Banner IT', { baseUrl: url_it }, () => {
		let assertCookie
		cy.visit(url_it)
		cy.wait(1000)
		cy.clearCookies()
		cy.getCookies().then(cookie => {
			cy.log(JSON.stringify(cookie))
			assert.isAtMost(cookie.length, 1)
		})
		loginPage.essentialCookies().click()
		cy.getCookies().then(cookie => {
			cookie.map(cookie => {
				if (JSON.stringify(cookie.name) === '"cms_cookie"') {
					assertCookie = JSON.stringify(cookie.name)
				}
			})
			expect(assertCookie).eql('"cms_cookie"')
		})
	})

	it('Essential Cookie Banner DE', { baseUrl: url_de }, () => {
		let assertCookie
		cy.visit(url_de)
		cy.wait(1000)
		cy.clearCookies()
		cy.getCookies().then(cookie => {
			cy.log(JSON.stringify(cookie))
			assert.isAtMost(cookie.length, 1)
		})
		loginPage.essentialCookies().click()
		cy.getCookies().then(cookie => {
			cookie.map(cookie => {
				if (JSON.stringify(cookie.name) === '"cms_cookie"') {
					assertCookie = JSON.stringify(cookie.name)
				}
			})
			expect(assertCookie).eql('"cms_cookie"')
		})
	})

	it('Essential Cookie Banner FR', { baseUrl: url_fr }, () => {
		let assertCookie
		cy.visit(url_fr)
		cy.wait(1000)
		cy.clearCookies()
		cy.getCookies().then(cookie => {
			cy.log(JSON.stringify(cookie))
			assert.isAtMost(cookie.length, 1)
		})
		loginPage.essentialCookies().click()
		cy.getCookies().then(cookie => {
			cookie.map(cookie => {
				if (JSON.stringify(cookie.name) === '"cms_cookie"') {
					assertCookie = JSON.stringify(cookie.name)
				}
			})
			expect(assertCookie).eql('"cms_cookie"')
		})
	})

	// DIRECT LINK

	it('Direct Link Not Cookie Banner UK', { baseUrl: url_uk }, () => {
		let assertCookie
		cy.visit(url_uk + LRBS110_part)
		cy.wait(1000)
		cy.clearCookies()
		cy.getCookies().then(cookie => {
			cy.log(JSON.stringify(cookie))
			assert.isAtMost(cookie.length, 1)
		})
		expect(loginPage.essentialCookies()).to.exist
		cy.getCookies().then(cookie => {
			cy.log(assertCookie)
			cookie.map(cookie => {
				if (JSON.stringify(cookie.name) === '"recentryCategoryView"') {
					assertCookie = JSON.stringify(cookie.name)
					expect(assertCookie).eql('"recentryCategoryView"')
				}
				assertCookie = JSON.stringify(cookie.name)
				cy.log(assertCookie)
			})
		})
	})

	it('Direct Link Not Cookie Banner IT', { baseUrl: url_it }, () => {
		let assertCookie
		cy.visit(url_it + LRBS110_part)
		cy.wait(1000)
		cy.clearCookies()
		cy.getCookies().then(cookie => {
			cy.log(JSON.stringify(cookie))
			assert.isAtMost(cookie.length, 1)
		})
		expect(loginPage.essentialCookies()).to.exist
		cy.getCookies().then(cookie => {
			cy.log(assertCookie)
			cookie.map(cookie => {
				if (JSON.stringify(cookie.name) === '"recentryCategoryView"') {
					assertCookie = JSON.stringify(cookie.name)
					expect(assertCookie).eql('"recentryCategoryView"')
				}
				assertCookie = JSON.stringify(cookie.name)
				cy.log(assertCookie)
			})
		})
	})

	it('Direct Link Not Cookie Banner DE', { baseUrl: url_de }, () => {
		let assertCookie
		cy.visit(url_de + LRBS110_part)
		cy.wait(1000)
		cy.clearCookies()
		cy.getCookies().then(cookie => {
			cy.log(JSON.stringify(cookie))
			assert.isAtMost(cookie.length, 1)
		})
		expect(loginPage.essentialCookies()).to.exist
		cy.getCookies().then(cookie => {
			cy.log(assertCookie)
			cookie.map(cookie => {
				if (JSON.stringify(cookie.name) === '"recentryCategoryView"') {
					assertCookie = JSON.stringify(cookie.name)
					expect(assertCookie).eql('"recentryCategoryView"')
				}
				assertCookie = JSON.stringify(cookie.name)
				cy.log(assertCookie)
			})
		})
	})

	it('Direct Link Not Cookie Banner FR', { baseUrl: url_fr }, () => {
		let assertCookie
		cy.visit(url_fr + LRBS110_part)
		cy.wait(1000)
		cy.clearCookies()
		cy.getCookies().then(cookie => {
			cy.log(JSON.stringify(cookie))
			assert.isAtMost(cookie.length, 1)
		})
		expect(loginPage.essentialCookies()).to.exist
		cy.getCookies().then(cookie => {
			cy.log(assertCookie)
			cookie.map(cookie => {
				if (JSON.stringify(cookie.name) === '"recentryCategoryView"') {
					assertCookie = JSON.stringify(cookie.name)
					expect(assertCookie).eql('"recentryCategoryView"')
				}
				assertCookie = JSON.stringify(cookie.name)
				cy.log(assertCookie)
			})
		})
	})
})
