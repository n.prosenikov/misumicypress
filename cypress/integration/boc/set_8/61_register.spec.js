import Login from '../pom/login';

const loginPage = new Login()

const authUser = require('../../../fixtures/auth-user.json');
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Register ', () => {
  beforeEach(() => {
    loginPage.login(username, password)
    cy.visit('/')
  });
  /*
      Description: eCatalog top page - expand the menu of [login user name] on the top right of the page and click [Logout] link.
      Test Steps:
          1. Logout process is executed, and the notation of [login user name] button changes to Login.
  
    */
  it('Register UK', { baseUrl: url_uk },() => {
    loginPage.logOut(0) //0-UK, 1-DE, 2-FR, 3-IT
  })

  it('Register DE', { baseUrl: url_de },() => {
    loginPage.checkLoginBtn(1) //0-UK, 1-DE, 2-FR, 3-IT
  })

  it('Register FR', { baseUrl: url_fr },() => {
    loginPage.checkLoginBtn(2) //0-UK, 1-DE, 2-FR, 3-IT
  })

  it('Register IT', { baseUrl: url_it },() => {
    loginPage.checkLoginBtn(3) //0-UK, 1-DE, 2-FR, 3-IT
  })

});