import Login from "../pom/login"
const loginPage = new Login()

const authUser = require('../../../fixtures/auth-user.json');
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;
describe('Specification Search', () => {
  beforeEach(() => {
    loginPage.login(username, password)
    cy.visit('/')
  });
/*
  Description: Display any category page in the eCatalog and click the [Please assist us in our effort to improve this site.] link on the upper right of the page.
  Test Steps:
      1. Comment and feedback: the page is expanded. (Please give us a feedback on the MISUMI e-Catalog.)
*/ 
  it('Specification Search UK', { baseUrl: url_uk },() => {
    cy.contains('Automation Components').click({force: true})
    cy.contains('Search by category of Automation Components')
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('Linear Shafts').click()
    cy.contains('Straight')
  })

/*
  Description: Display any category page in the eCatalog and click the [Please assist us in our effort to improve this site.] link on the upper right of the page.
  Test Steps:
      1. Comment and feedback: the page is expanded. (Please give us a feedback on the MISUMI e-Catalog.)
*/ 
  it('Specification Search DE', { baseUrl: url_de }, () => {
    cy.contains('Mechanische Komponenten').click({force: true})
    cy.contains('Suche nach Kategorie der Mechanische Komponenten')
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('Linearwellen').click()
    cy.contains('Gerade')
  })

/*
  Description: Display any category page in the eCatalog and click the [Please assist us in our effort to improve this site.] link on the upper right of the page.
  Test Steps:
      1. Comment and feedback: the page is expanded. (Please give us a feedback on the MISUMI e-Catalog.)
*/ 
  it('Specification Search IT', { baseUrl: url_it },() => {
    cy.get('[href="/vona2/mech/"]').click({force: true})
    cy.contains('Eseguire la ricerca specificando la categoria di Componenti meccanici')
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('Alberi lineari').click()
    cy.contains('Dritto')
  })

/*
  Description: Display any category page in the eCatalog and click the [Please assist us in our effort to improve this site.] link on the upper right of the page.
  Test Steps:
      1. Comment and feedback: the page is expanded. (Please give us a feedback on the MISUMI e-Catalog.)
*/ 
  it('Specification Search FR', { baseUrl: url_fr },() => {
    cy.contains('Composants mécaniques').click({force: true})
    cy.contains('Rechercher en précisant la catégorie de Composants mécaniques')
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('Arbres linéaires').click()
    cy.contains('Droit')
  })
});