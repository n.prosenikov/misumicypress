import Login from "../pom/login"
const loginPage = new Login()

const authUser = require('../../../fixtures/auth-user.json');
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Category Check', () => {
  beforeEach(() => {
    loginPage.login(username, password)
    cy.visit('/')
  });
/*
  Description: Mouse over sequentially to categories displayed in the Mega-Navi's category list.
  Test Steps:
      1. The lower layer category information displayed on the right side of Mega-Navi changes according to mouse over.
*/ 
  it('Incad Idea Note UK', { baseUrl: url_uk },() => {
    cy.contains('Automation Components').click({force: true})
    cy.contains('Search by category of Automation Components')
  })

/*
  Description: Mouse over sequentially to categories displayed in the Mega-Navi's category list.
  Test Steps:
      1. The lower layer category information displayed on the right side of Mega-Navi changes according to mouse over.
*/ 
  it('Incad Idea Note DE', { baseUrl: url_de },() => {
    cy.contains('Mechanische Komponenten').click({force: true})
    cy.contains('Suche nach Kategorie der Mechanische Komponenten')
  })

/*
  Description: Mouse over sequentially to categories displayed in the Mega-Navi's category list.
  Test Steps:
      1. The lower layer category information displayed on the right side of Mega-Navi changes according to mouse over.
*/ 
  it('Incad Idea Note IT', { baseUrl: url_it },() => {
    cy.get('[href="/vona2/mech/"]').click({force: true})
    cy.contains('Eseguire la ricerca specificando la categoria di Componenti meccanici')
  })

/*
  Description: Mouse over sequentially to categories displayed in the Mega-Navi's category list.
  Test Steps:
      1. The lower layer category information displayed on the right side of Mega-Navi changes according to mouse over.
*/ 
  it('Incad Idea Note FR', { baseUrl: url_fr },() => {
    cy.contains('Composants mécaniques').click({force: true})
    cy.contains('Rechercher en précisant la catégorie de Composants mécaniques')
  })
});