import Login from "../pom/login"

const authUser = require('../../../fixtures/auth-user.json');
const loginPage = new Login()
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Brand Details', () => {
  beforeEach(() => {
    loginPage.login(username, password)
    cy.visit('/')
  });
  /*
    Description: Click [AX BRAIN] in Manufacturer List
    Test Steps:
        1. Detail page of AX BRAIN is displayed. (uk.misumi-ec.com/vona2/maker/axbrain/)
  */ 
  it('Brand Details UK', { baseUrl: url_uk },() => {
    //cy.contains('Brand Overview').click({ force: true })
    cy.contains("Brands").trigger('mouseover')
    cy.contains('Brand Overview').click({ force: true })
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('ACCURATE').click()
    cy.url().should('include', 'uk.misumi-ec.com/vona2/maker/accurate')
  })

  
  /*
    Description: Click [AX BRAIN] in Manufacturer List
    Test Steps:
        1. Detail page of AX BRAIN is displayed. (de.misumi-ec.com/vona2/maker/axbrain/)
  */ 
  it('Brand Details DE', { baseUrl: url_de },() => {
    cy.contains("Marken").trigger('mouseover')
    cy.contains('Markenübersicht').click({ force: true })

    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('ACCURATE').click()
    cy.url().should('include', 'de.misumi-ec.com/vona2/maker/accurate')
  })

  /*
    Description: Click [AX BRAIN] in Manufacturer List
    Test Steps:
        1. Detail page of AX BRAIN is displayed. (it.misumi-ec.com/vona2/maker/axbrain/)
  */ 
  it('Brand Details IT', { baseUrl: url_it },() => {
    cy.contains("Marchi").trigger('mouseover')
    cy.contains('Tutti i marchi').click({ force: true })

    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('ACCURATE').click()
    cy.url().should('include', 'it.misumi-ec.com/vona2/maker/accurate')
  })


  /*
    Description: Click [AX BRAIN] in Manufacturer List
    Test Steps:
        1. Detail page of AX BRAIN is displayed. (fr.misumi-ec.com/vona2/maker/axbrain/)
  */ 
  it('Brand Details FR', { baseUrl: url_fr },() => {
    cy.contains("Marques").trigger('mouseover')
    cy.contains('Toutes les marques').click({ force: true })

    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('ACCURATE').click()
    cy.url().should('include', 'fr.misumi-ec.com/vona2/maker/accurate')
  })
});