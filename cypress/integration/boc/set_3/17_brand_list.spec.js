import Login from "../pom/login"

const loginPage = new Login()

const authUser = require('../../../fixtures/auth-user.json');
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Brand List', () => {
  beforeEach(() => {
    loginPage.login(username, password)
    cy.visit('/')
  });

/*
  Description: Click [Brand overview] link at the bottom of Mega-Navi in eCatalog top page.
  Test Steps:
      1. Manufacturer List is displayed. (uk.misumi-ec.com/vona2/maker/)
*/ 
  it('Brand Details UK', { baseUrl: url_uk },() => {
    cy.contains('Brand Overview').click({ force: true })
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('rand Overview')
    cy.url().should('include', 'uk.misumi-ec.com/vona2/maker/')
  })


/*
  Description: Click [Brand overview] link at the bottom of Mega-Navi in eCatalog top page.
  Test Steps:
      1. Manufacturer List is displayed. (de.misumi-ec.com/vona2/maker/)
*/ 
  it('Brand Details DE', { baseUrl: url_de },() => {
    cy.contains('Alle Marken').click({ force: true })
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('Alle Marken')
    cy.url().should('include', 'de.misumi-ec.com/vona2/maker/')
  })


/*
  Description: Click [Brand overview] link at the bottom of Mega-Navi in eCatalog top page.
  Test Steps:
      1. Manufacturer List is displayed. (de.misumi-ec.com/vona2/maker/)
*/ 
  it('Brand Details IT', { baseUrl: url_it },() => {
    cy.contains('Tutti i marchi').click({ force: true })
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('Tutti i marchi')
    cy.url().should('include', 'it.misumi-ec.com/vona2/maker/')
  })


/*
  Description: Click [Brand overview] link at the bottom of Mega-Navi in eCatalog top page.
  Test Steps:
      1. Manufacturer List is displayed. (de.misumi-ec.com/vona2/maker/)
*/ 
  it('Brand Details FR', { baseUrl: url_fr },() => {
    cy.contains('Toutes les marques').click({ force: true })
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('Toutes les marques')
    cy.url().should('include', 'fr.misumi-ec.com/vona2/maker/')
  })
});