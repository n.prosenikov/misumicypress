import Login from "../pom/login"

const authUser = require('../../../fixtures/auth-user.json');
const loginPage = new Login()
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;
describe('Suggestion', () => {
    beforeEach(() => {
        loginPage.login(username, password)
        cy.visit('/')
        cy.wait(1500)
    });
    const partName = 'BH6'
    /*
        Description: eCatalog top page - Enter BH6 in the search field at the top of the page.
        Test Steps:
            1. Suggestion for product without information posted is displayed whose part number is inputted. (BH6 [SUNFLAG] *No Product Page (Available to Order))
    */
    it('Suggestion UK', { baseUrl: url_uk }, () => {
        cy.get('#keyword_input').type(partName)
        cy.contains(partName);
    })

    /*
    Description: eCatalog top page - Enter LHQRW10 in the search field at the top of the page.
    Test Steps:
        1. The suggestion for the product not listed in MISUMI catalog, whose part number is inputted, is displayed. (LHQRW10[MISUMI Discontinued product,limited availability])
    */
    it('Suggestion DE', { baseUrl: url_de }, () => {
        cy.get('#keyword_input').type(partName)
        cy.contains(partName);
    })

    /*
    Description: eCatalog top page - Enter LHQRW10 in the search field at the top of the page.
    Test Steps:
        1. The suggestion for the product not listed in MISUMI catalog, whose part number is inputted, is displayed. (LHQRW10[MISUMI Discontinued product,limited availability])
    */
    it('Suggestion IT', { baseUrl: url_it }, () => {
        cy.get('#keyword_input').type("BHS2M")
        cy.contains('BHS2M [MISUMI]');
    })

    /*
    Description: eCatalog top page - Enter LHQRW10 in the search field at the top of the page.
    Test Steps:
        1. The suggestion for the product not listed in MISUMI catalog, whose part number is inputted, is displayed. (LHQRW10[MISUMI Discontinued product,limited availability])
    */
    it('Suggestion FR', { baseUrl: url_fr }, () => {
        cy.get('#keyword_input').type(partName)
        cy.contains(partName);
    })

    /*
        Description: Click the link [BH6 [SUNFLAG] *No Product Page (Available to Order)] which is suggested.
        Test Steps:
            1. Modal of quotation and order for product without information posted is displayed.  (No Product Page (Available to Order))
    */
    it('Suggestion UK', { baseUrl: url_uk }, () => {
        cy.get('#keyword_input').type(partName)
        cy.contains(partName).click()
        cy.get('.m-btn--cartin').should('exist')
    })

    /*
    Description: eCatalog top page - Enter LHQRW10 in the search field at the top of the page.
    Test Steps:
        1. The suggestion for the product not listed in MISUMI catalog, whose part number is inputted, is displayed. (LHQRW10[MISUMI Discontinued product,limited availability])
    */
    it('Suggestion DE', { baseUrl: url_de }, () => {
        cy.get('#keyword_input').type(partName)
        cy.contains(partName).click()
        cy.get('.m-btn--cartin').should('exist')
    })

    /*
    Description: eCatalog top page - Enter LHQRW10 in the search field at the top of the page.
    Test Steps:
        1. The suggestion for the product not listed in MISUMI catalog, whose part number is inputted, is displayed. (LHQRW10[MISUMI Discontinued product,limited availability])
    */
    it('Suggestion IT', { baseUrl: url_it }, () => {
        cy.get('#keyword_input').type("BHS2M20L-130")
        cy.contains('BHS2M20L-130 [MISUMI]').click()
        cy.get('.m-btn--cartin').should('exist')
    })

    /*
    Description: eCatalog top page - Enter LHQRW10 in the search field at the top of the page.
    Test Steps:
        1. The suggestion for the product not listed in MISUMI catalog, whose part number is inputted, is displayed. (LHQRW10[MISUMI Discontinued product,limited availability])
    */
    it('Suggestion FR', { baseUrl: url_fr }, () => {
        cy.get('#keyword_input').type(partName)
        cy.contains(partName).click()
        cy.get('.m-btn--cartin').should('exist')
    })

})