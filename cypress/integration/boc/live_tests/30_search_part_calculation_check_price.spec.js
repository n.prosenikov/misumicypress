import Login from "../pom/login"

const authUser = require('../../../fixtures/auth-user.json');
const loginPage = new Login()
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('Price Check', () => {
    beforeEach(() => {
        loginPage.login(username, password)
        cy.visit('/')
        cy.wait(1500)
    });

    const partName = 'SFJ10-100'
    /*
        Description: Click the [Check Price / discount rate] button with Order Qty. set to 1 and Unit Price to --- on the left side of product details page.
        Test Steps:
            1. Values must be set for three items: Unit Price, Total, Shipping Days
    */
    it('Price Check UK', { baseUrl: url_uk }, () => {
        cy.searchPart(partName)
        cy.url().should('include', partName)
        cy.contains(partName)
        cy.contains("Total") //If it exists then a price is avaiable
        //cy.get('.m-cartBox__desc--inline').find('[type="text"]').dblclick()
        cy.contains('3.08')
    })

    /*
        Description: Click the [Check Price / discount rate] button with Order Qty. set to 1 and Unit Price to --- on the left side of product details page.
        Test Steps:
            1. Values must be set for three items: Unit Price, Total, Shipping Days
    */
    it('Price Check DE', { baseUrl: url_de }, () => {
        cy.searchPart(partName)
        cy.url().should('include', partName)
        cy.contains('Linearwellen / gerade / Bearbeitung wählbar')
        cy.contains("Gesamtsumme")
        //cy.get('.m-inputText--right').type('3')
        cy.contains('3.08')
    })
    /*
        Description: Click the [Check Price / discount rate] button with Order Qty. set to 1 and Unit Price to --- on the left side of product details page.
        Test Steps:
            1. Values must be set for three items: Unit Price, Total, Shipping Days
    */
    it('Price Check IT', { baseUrl: url_it }, () => {
        cy.searchPart(partName)
        cy.url().should('include', partName)
        cy.contains(partName)
        cy.contains("Totale")
        //cy.get('.m-inputText--right').type('3')
        cy.contains('3.08')
    })

    /*
        Description: Click the [Check Price / discount rate] button with Order Qty. set to 1 and Unit Price to --- on the left side of product details page.
        Test Steps:
            1. Values must be set for three items: Unit Price, Total, Shipping Days
    */
    it('Price Check FR', { baseUrl: url_fr }, () => {
        cy.searchPart(partName)
        cy.url().should('include', partName)
        cy.contains(partName)
        cy.contains("Total")
        //cy.get('.m-inputText--right').type('3')
        cy.contains('3.08')
    })


});