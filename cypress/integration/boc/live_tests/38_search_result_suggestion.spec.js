import Login from "../pom/login"

const authUser = require('../../../fixtures/auth-user.json');
const loginPage = new Login()
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Search Result Suggestion', () => {
  beforeEach(() => {
    loginPage.login(username, password)
    cy.visit('/')
    cy.wait(3000)
  });
  /*
    Description: Suggestion - Click the [Linear Shafts] image of Category as the search result of [linear shafts].
    Test Steps:
        1. Specification search page: the page is displayed. (MISUMI Top Page> Automation Components> Linear Motion> Linear Shafts)
  */
  it('Search Result Suggestion UK', { baseUrl: url_uk }, () => {
    cy.get('#keyword_input').type('Linear').type('{enter}')
    cy.contains('Linear shafts / straight / machining selectable').invoke('removeAttr', 'target').click()
    cy.wait(1000)
    cy.contains('Linear shafts / straight / machining selectable')
  })

  /*
    Description: Suggestion - Click the [Linear Shafts] image of Category as the search result of [linear shafts].
    Test Steps:
        1. Specification search page: the page is displayed. (MISUMI Top Page> Automation Components> Linear Motion> Linear Shafts)
  */
  it('Search Result Suggestion DE', { baseUrl: url_de },() => {
    cy.get('#keyword_input').type('Linearwellen').type('{enter}')
    cy.contains('Linearwellen').invoke('removeAttr', 'target').click()
    cy.wait(1000)
    cy.contains('Linearwellen')
  })

  /*
    Description: Suggestion - Click the [Linear Shafts] image of Category as the search result of [linear shafts].
    Test Steps:
        1. Specification search page: the page is displayed. (MISUMI Top Page> Automation Components> Linear Motion> Linear Shafts)
  */
  it('Search Result Suggestion IT', { baseUrl: url_it },() => {
    cy.get('#keyword_input').type('Alberi lineari').type('{enter}')
    cy.contains('Alberi lineari').invoke('removeAttr', 'target').click()
    cy.wait(1000)
    cy.contains('Alberi lineari')
  })

  /*
    Description: Suggestion - Click the [Linear Shafts] image of Category as the search result of [linear shafts].
    Test Steps:
        1. Specification search page: the page is displayed. (MISUMI Top Page> Automation Components> Linear Motion> Linear Shafts)
  */
  it('Search Result Suggestion FR', { baseUrl: url_fr },() => {
    cy.get('#keyword_input').type('Arbres linéaires').type('{enter}')
    cy.contains('Arbres linéaires').invoke('removeAttr', 'target').click()
    cy.wait(1000)
    cy.contains('Arbres linéaires')
  })

});