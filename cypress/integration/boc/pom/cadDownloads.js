class CadDownload {

    pageTitle() {
        return cy.get('.titleBlock__h1')
    }
    
    pageTag() {
        return cy.get('.caddatadlArea')
    }
}

export default CadDownload;