class UserMenu {
    userPanel() {
        return cy.get('[data-header-func="user"]');
    }

    myPageUserMenu() {
        return cy.get('.l-header__usermenu [data-common-userlink="mypagetop"]')
    }

    myPageSideMenu() {
        return cy.get('.l-header__usermenu [data-common-userlink="mypagetop"]')
    }

    myCadDownloads() {
        return cy.get('.l-header__usermenu [data-common-userlink="cadhistory"]')
    }

    myComponents() {
        return cy.get('.l-header__usermenu [data-common-userlink="partslist"]')
    }

    changeUserData() {
        return cy.get('.l-header__usermenu [data-common-userlink="editprofile"]').invoke('removeAttr', 'target')
    }

    changePassword(item) {
        return cy.get('div.l-header__aside > div:nth-child(2) > div > div > div:nth-child(4) > ul > li:nth-child(3) > a').invoke('removeAttr', 'target')
    }

    corporateInfo() {
        return cy.get('div.l-header__aside > div:nth-child(2) > div > div > div:nth-child(4) > ul > li:nth-child(4) > a').invoke('removeAttr', 'target')
    }


}

export default UserMenu;