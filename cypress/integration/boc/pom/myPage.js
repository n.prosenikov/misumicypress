export default class MyPage {

    pageTitle() {
        return cy.get('title')
    }
    
    headerTitle() {
        return cy.get('#header-title')
    }

    pageTopic() {
        return cy.get('.topicPath')
    }

    couponCode() {
        return cy.get('.code');
    }

    couponPrice() {
        return cy.get('.rate');
    }

    couponDate() {
        return cy.get('.date');
    }

    couponType() {
        return cy.get('.type');
    }

    couponTitle() {
        return cy.get('.title');
    }

    couponStatus() {
        return cy.get('.status');
    }
}
