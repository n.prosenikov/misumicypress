class SideMenu {
    myPageUserMenu() {
        return cy.get('.l-top__aside [data-common-userlink="mypagetop"]')
    }

    myPageSideMenu() {
        return cy.get('.l-top__aside [data-common-userlink="mypagetop"]')
    }

    myCadDownloads() {
        return cy.get('.l-top__aside [data-common-userlink="cadhistory"]')
    }

    myComponents() {
        return cy.get('.l-top__aside [data-common-userlink="partslist"]')
    }

    changeUserData() {
        return cy.get('.l-top__aside [data-common-userlink="editprofile"]')
    }

    changePassword() {
        return cy.get('.l-top__aside [data-common-userlink="passchange"]')
    }
}

export default SideMenu;