import Login from "../pom/login"
const loginPage = new Login()

const authUser = require('../../../fixtures/auth-user.json');
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('Cart Order ', () => {
    beforeEach(() => {
        loginPage.login(username, password)
        cy.visit('/')
    });
    /*
      Description: Click the [Place an Order] button on the top of the page after selecting the following items on the cart page.
                  <Select as>
                  Shipping Preference：Specify Ship Dates
                  Number of selected items  ：1 item
                  Selected part number      ：SFJ3-10
                  Selected quantity         ：1
  
      Test Steps:
          1. Cart: Order: the page is displayed, and SFJ3-10 is set to MISUMI Part No. and 1 is set to Quantity.
          2. Click the [Place an Order] button on the bottom of the page after the selecting following items on the cart page.
  
              <Select as>
              Shipping Preference：Specify Ship Dates
              Number of selected items  ：1 item
              Selected part number      ：SFJ3-10
              Selected quantity         ：1
  
              3. Order: the page is displayed, and SFJ3-10 is set to MISUMI Part No. and 1 is set to Quantity.
    */
    it('Cart Order UK', { baseUrl: url_uk }, () => {
        cy.deleteAllCart()

        cy.addToCart('SFJ3-100', '1')
        cy.contains('SFJ3-100')
        cy.get('.lc-cart').click()

        cy.get('.productCounter__count').invoke('text').then(x => {
            cy.log(parseFloat(x))
            const items = parseFloat(x)
            cy.log(items)
            if (items === 1) {
                cy.log('1 item')
                cy.contains('SFJ3-100')
                cy.get('select').should('be.visible')
                cy.get('table > tbody > tr:nth-child(3) > td > div > label > input').click()
            }
        })

        cy.intercept('POST', '**mypage/post_cart**').as('orderPage')
        cy.get('.button--order').click()
        // cy.wait("@orderPage")

        cy.contains('SFJ3-100')
    })

    it('Cart Order DE', { baseUrl: url_de }, () => {
        cy.deleteAllCart()

        cy.addToCart('SFJ3-100', '1')
        cy.contains('SFJ3-100')
        cy.get('.lc-cart').click()

        cy.get('.productCounter__count').invoke('text').then(x => {
            cy.log(parseFloat(x))
            const items = parseFloat(x)
            cy.log(items)
            if (items === 1) {
                cy.log('1 item')
                cy.contains('SFJ3-100')
                cy.get('select').should('be.visible')
                cy.get('table > tbody > tr:nth-child(3) > td > div > label > input').click()
            }
        })

        cy.intercept('POST', '**mypage/post_cart**').as('orderPage')
        cy.get('.button--order').click()
        // cy.wait("@orderPage")

        cy.contains('SFJ3-100')
    })

    it('Cart Order IT', { baseUrl: url_it }, () => {
        cy.deleteAllCart()

        cy.addToCart('SFJ3-100', '1')
        cy.contains('SFJ3-100')
        cy.get('.lc-cart').click()

        cy.get('.productCounter__count').invoke('text').then(x => {
            cy.log(parseFloat(x))
            const items = parseFloat(x)
            cy.log(items)
            if (items === 1) {
                cy.log('1 item')
                cy.contains('SFJ3-100')
                cy.get('select').should('be.visible')
                cy.get('table > tbody > tr:nth-child(3) > td > div > label > input').click()
            }
        })

        cy.intercept('POST', '**mypage/post_cart**').as('orderPage')
        cy.get('.button--order').click()
        // cy.wait("@orderPage")

        cy.contains('SFJ3-100')
    })

    it('Cart Order FR', { baseUrl: url_fr }, () => {
        cy.deleteAllCart()

        cy.addToCart('SFJ3-100', '1')
        cy.contains('SFJ3-100')
        cy.get('.lc-cart').click()

        cy.get('.productCounter__count').invoke('text').then(x => {
            cy.log(parseFloat(x))
            const items = parseFloat(x)
            cy.log(items)
            if (items === 1) {
                cy.log('1 item')
                cy.contains('SFJ3-100')
                cy.get('select').should('be.visible')
                cy.get('table > tbody > tr:nth-child(3) > td > div > label > input').click()
            }
        })

        cy.intercept('POST', '**mypage/post_cart**').as('orderPage')
        cy.get('.button--order').click()
        // cy.wait("@orderPage")

        cy.contains('SFJ3-100')
    })

});