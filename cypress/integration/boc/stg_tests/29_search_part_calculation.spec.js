import Login from "../pom/login"

const authUser = require('../../../fixtures/auth-user.json');
const loginPage = new Login()
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('Suggestion Calculation', () => {
    beforeEach(() => {
        loginPage.login(username, password)
        cy.visit('/')
        cy.wait(1500)
    });
    const partName = "SFJ10-100"
    /*
        Description: Enter LHQRW10 in the Part Number field on the product details page and press the [Enter] key
        Test Steps:
            1. Price calculation: the area is displayed on the left side of the page.
    */
    it('Suggestion Calculation UK', { baseUrl: url_uk }, () => {
        cy.searchPart(partName)
        cy.url().should('include', partName)
        cy.contains(partName)
        cy.contains("Total") //If it exists then a price is avaiable
    })

    /*
        Description: Enter LHQRW10 in the Part Number field on the product details page and press the [Enter] key
        Test Steps:
            1. Price calculation: the area is displayed on the left side of the page.
    */
    it('Suggestion Calculation DE', { baseUrl: url_de },() => {
        cy.searchPart(partName)
        cy.url().should('include', partName)
        cy.contains(partName)
        cy.contains("Gesamtsumme")
    })

    /*
        Description: Enter LHQRW10 in the Part Number field on the product details page and press the [Enter] key
        Test Steps:
            1. Price calculation: the area is displayed on the left side of the page.
    */
    it('Suggestion Calculation IT', { baseUrl: url_it },() => {
        cy.searchPart(partName)
        cy.url().should('include', partName)
        cy.contains(partName)
        cy.contains("Totale")
    })

    /*
        Description: Enter LHQRW10 in the Part Number field on the product details page and press the [Enter] key
        Test Steps:
            1. Price calculation: the area is displayed on the left side of the page.
    */
    it('Suggestion Calculation FR', { baseUrl: url_fr },() => {
        cy.searchPart(partName)
        cy.url().should('include', partName)
        cy.contains(partName)
        cy.contains("Total")
    })

});