import Login from "../pom/login"
const loginPage = new Login()

const authUser = require('../../../fixtures/auth-user.json');
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Breadcrumbs', () => {
  beforeEach(() => {
    loginPage.login(username, password)
    cy.visit('/')
  });
/*
  Description: Product details page [Straight] Confirm the breadcrumbs at the top of the page.
  Test Steps:
      1. The notation of breadcrumbs is shown as below. MISUMI Top Page> Automation Components> Linear Motion> Linear Shafts> Straight
*/ 
  it('Breadcrumbs UK', { baseUrl: url_uk }, () => {
    cy.contains('Automation Components').click({ force: true })
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('Linear Shafts').click()
    cy.intercept('GET', '**categoryCode=M0101000000**').as('loadCategory')
    cy.get('[href="/vona2/detail/110302634310/"]').first().invoke('removeAttr', 'target').click()
    cy.wait("@loadCategory")
    cy.contains('MISUMI Top Page> Products> Automation Components> Linear Motion> Linear Shafts> Straight')
  })

/*
  Description: Product details page [Straight] Confirm the breadcrumbs at the top of the page.
  Test Steps:
      1. The notation of breadcrumbs is shown as below. MISUMI Top Page> Automation Components> Linear Motion> Linear Shafts> Straight
*/ 
  it('Breadcrumbs DE', { baseUrl: url_de }, () => {
    cy.contains('Mechanische Komponenten').click({force: true})
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('Linearwellen').click()
    cy.intercept('GET', '**categoryCode=M0101000000**').as('loadCategory')
    cy.get('[href="/vona2/detail/110302634310/"]').first().invoke('removeAttr', 'target').click()
    cy.wait("@loadCategory")

    cy.contains('MISUMI Startseite> Produkte> Mechanische Komponenten> Linearbewegung> Linearwellen> Gerade')
  })

/*
  Description: Product details page [Straight] Confirm the breadcrumbs at the top of the page.
  Test Steps:
      1. The notation of breadcrumbs is shown as below. MISUMI Top Page> Automation Components> Linear Motion> Linear Shafts> Straight
*/ 
  it('Breadcrumbs IT', { baseUrl: url_it },() => {
    cy.get('[href="/vona2/mech/"]').click({force: true})
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('Alberi lineari').click()
    cy.intercept('GET', '**categoryCode=M0101000000**').as('loadCategory')
    cy.get('[href="/vona2/detail/110302634310/"]').first().invoke('removeAttr', 'target').click()
    cy.wait("@loadCategory")

    cy.contains('Pagina principale MISUMI> Prodotti> Componenti meccanici> Movimentazione lineare> Alberi lineari> Dritti')
  })

/*
  Description: Product details page [Straight] Confirm the breadcrumbs at the top of the page.
  Test Steps:
      1. The notation of breadcrumbs is shown as below. MISUMI Top Page> Automation Components> Linear Motion> Linear Shafts> Straight
*/ 
  it('Breadcrumbs FR', { baseUrl: url_fr },() => {
    cy.get('[href="/vona2/mech/"]').click({force: true})
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
    })
    cy.contains('Arbres linéaires').click()
    cy.intercept('GET', '**categoryCode=M0101000000**').as('loadCategory')
    cy.get('[href="/vona2/detail/110302634310/"]').first().invoke('removeAttr', 'target').click()
    cy.wait("@loadCategory")

    cy.contains('Haut de la page MISUMI> Produits> Composants mécaniques> Mouvement linéaire> Arbres linéaires> Droit')
  })

});