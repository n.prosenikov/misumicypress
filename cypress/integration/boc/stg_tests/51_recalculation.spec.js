import Login from "../pom/login"
const loginPage = new Login()

const authUser = require('../../../fixtures/auth-user.json');
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Cart Recalculation ', () => {
  beforeEach(() => {
    loginPage.login(username, password)
    cy.visit('/')
  });
  /*
     Description: Change the Quantity value of SFJ3-10 registered in cart from 1 to 10, click the [Update] button next to the column whose the value is changed.

 
     Test Steps:
         1. Recalculation is executed, and the values of Unit price and Subtotal are updated. *Only Subtotal may be updated.
         2. Confirm the value set in Subtotal after recalculation.
         The value of Subtotal is Unit price*Quantity.
 
   */
  it('Cart Recalculation UK', { baseUrl: url_uk }, () => {
    cy.deleteAllCart()

    cy.addToCart('SFJ3-100', '1')
    cy.contains('SFJ3-100')
    cy.get('.lc-cart').click()

    cy.url().should('contains', 'misumi-ec.com/cart/');
    cy.get('.quantityBoxWrap').first().click();
    cy.wait(1500)
    cy.get('[data-mypage-quantity="input"]').first().clear().type('10');
    cy.get('.button--reload').first().click();
    cy.contains('30.40')
  })

  it('Cart Recalculation DE', { baseUrl: url_de }, () => {
    cy.deleteAllCart()

    cy.addToCart('SFJ3-100', '1')
    cy.contains('SFJ3-100')
    cy.get('.lc-cart').click()

    cy.url().should('contains', 'misumi-ec.com/cart/');
    cy.get('.quantityBoxWrap').first().click();
    cy.wait(1500)
    cy.get('[data-mypage-quantity="input"]').first().clear().type('10');
    cy.get('.button--reload').first().click();
    cy.contains('30.40')
  })

  it('Cart Recalculation IT', { baseUrl: url_it }, () => {
    cy.deleteAllCart()

    cy.addToCart('SFJ3-100', '1')
    cy.contains('SFJ3-100')
    cy.get('.lc-cart').click()

    cy.url().should('contains', 'misumi-ec.com/cart/');
    cy.get('.quantityBoxWrap').first().click();
    cy.wait(1500)
    cy.get('[data-mypage-quantity="input"]').first().clear().type('10');
    cy.get('.button--reload').first().click();
    cy.contains('30.40')
  })

  it('Cart Recalculation FR', { baseUrl: url_fr }, () => {
    cy.deleteAllCart()

    cy.addToCart('SFJ3-100', '1')
    cy.contains('SFJ3-100')
    cy.get('.lc-cart').click()

    cy.url().should('contains', 'misumi-ec.com/cart/');
    cy.get('.quantityBoxWrap').first().click();
    cy.wait(1500)
    cy.get('[data-mypage-quantity="input"]').first().clear().type('10');
    cy.get('.button--reload').first().click();
    cy.contains('30.40')
  })

});