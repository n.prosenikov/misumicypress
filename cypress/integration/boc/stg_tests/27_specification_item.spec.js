import Login from "../pom/login"

const loginPage = new Login()

const authUser = require('../../../fixtures/auth-user.json');
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('Specification Item', () => {
    beforeEach(() => {
        loginPage.login(username, password)
        cy.visit('/')
    });

    const confItem = () => {
        cy.intercept('GET', '**typeCode=SFJ**').as('configurePart')
        cy.intercept('POST', '**/api/v1/log/add**').as('lengthItem')
        cy.get('[data-unit="mm"]').type('10').type('{enter}')
        cy.wait(3000)
        cy.wait("@lengthItem")
        cy.intercept('POST', '**/api/v1/log/add**').as('postSFJ')
        cy.get('[data-sitelog-value="SFJ"').click({ force: true })
        cy.wait(1000)
        cy.wait("@postSFJ")
        cy.intercept('POST', '**/api/v1/log/add**').as('shaftDIA')
        cy.get('[data-sitelog-value="3"]').click({ force: true })
        cy.wait(1000)
        cy.wait("@shaftDIA")
        cy.get('[data-sitelog-value="LKC"]').click({ force: true })
        cy.wait(3000)
        cy.wait("@configurePart")
        cy.contains('SFJ3-10-LKC').should('exist');
    }


    /*
        Description: Select the following specification items on product details page.

        [Regular item]
        Shaft Dia. D(φ)：3
        Length L(mm)    ：10
        Type            ：SFJ

        [Alteration]
        Changes L Dimension Tolerance [LKC](mm)：LKC

        Test Steps:
            1. The part number of product is fixed as SFJ3-10-LKC.
    */
    it('Specification Item UK', { baseUrl: url_uk }, () => {

        cy.contains('Automation Components').click({ force: true })
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false
        })
        cy.contains('Linear Shafts').click()
        cy.intercept('GET', '**categoryCode=M0101000000**').as('loadCategory')
        cy.get('[href="/vona2/detail/110302634310/"]').first().invoke('removeAttr', 'target').click()
        cy.wait("@loadCategory")
        cy.contains('MISUMI Top Page> Products> Automation Components> Linear Motion> Linear Shafts> Straight')

        if (!url_uk.includes("stg0")) {
            confItem()
        }
    })

    /*
        Description: Select the following specification items on product details page.

        [Regular item]
        Shaft Dia. D(φ)：3
        Length L(mm)    ：10
        Type            ：SFJ

        [Alteration]
        Changes L Dimension Tolerance [LKC](mm)：LKC

        Test Steps:
            1. The part number of product is fixed as SFJ3-10-LKC.
    */
    it('Specification Item DE', { baseUrl: url_de }, () => {

        cy.contains('Mechanische Komponenten').click({ force: true })
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false
        })
        cy.contains('Linearwellen').click()
        cy.intercept('GET', '**categoryCode=M0101000000**').as('loadCategory')
        cy.get('[href="/vona2/detail/110302634310/"]').first().invoke('removeAttr', 'target').click()
        cy.wait("@loadCategory")

        cy.contains('MISUMI Startseite> Produkte> Mechanische Komponenten> Linearbewegung> Linearwellen> Gerade')

        if (!url_de.includes("stg0")) {
            confItem()
        }
    })

    /*
        Description: Select the following specification items on product details page.

        [Regular item]
        Shaft Dia. D(φ)：3
        Length L(mm)    ：10
        Type            ：SFJ

        [Alteration]
        Changes L Dimension Tolerance [LKC](mm)：LKC

        Test Steps:
            1. The part number of product is fixed as SFJ3-10-LKC.
    */
    it('Specification Item IT', { baseUrl: url_it }, () => {

        cy.get('[href="/vona2/mech/"]').click({ force: true })
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false
        })
        cy.contains('Alberi lineari').click()
        cy.intercept('GET', '**categoryCode=M0101000000**').as('loadCategory')
        cy.get('[href="/vona2/detail/110302634310/"]').first().invoke('removeAttr', 'target').click()
        cy.wait("@loadCategory")

        cy.contains('Pagina principale MISUMI> Prodotti> Componenti meccanici> Movimentazione lineare> Alberi lineari> Dritti')

        if (!url_it.includes("stg0")) {
            confItem()
        }
    })

    /*
        Description: Select the following specification items on product details page.

        [Regular item]
        Shaft Dia. D(φ)：3
        Length L(mm)    ：10
        Type            ：SFJ

        [Alteration]
        Changes L Dimension Tolerance [LKC](mm)：LKC

        Test Steps:
            1. The part number of product is fixed as SFJ3-10-LKC.
    */
    it('Specification Item FR', { baseUrl: url_fr }, () => {

        cy.get('[href="/vona2/mech/"]').click({ force: true })
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false
        })
        cy.contains('Arbres linéaires').click()
        cy.intercept('GET', '**categoryCode=M0101000000**').as('loadCategory')
        cy.get('[href="/vona2/detail/110302634310/"]').first().invoke('removeAttr', 'target').click()
        cy.wait("@loadCategory")

        cy.contains('Haut de la page MISUMI> Produits> Composants mécaniques> Mouvement linéaire> Arbres linéaires> Droit')

        if (!url_fr.includes("stg0")) {
            confItem()
        }
    })

    /*
      Description: Click the [Clear all] button at the top of the specification item with the part number of product fixed as SFJ3-10-LKC.

      Test Steps:
          1. Fixed part number of product is canceled.
    */
    it('Specification Item UK', { baseUrl: url_uk }, () => {

        cy.contains('Automation Components').click({ force: true })
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false
        })
        cy.contains('Linear Shafts').click()
        cy.intercept('GET', '**categoryCode=M0101000000**').as('loadCategory')
        cy.get('[href="/vona2/detail/110302634310/"]').first().invoke('removeAttr', 'target').click()
        cy.wait("@loadCategory")
        cy.contains('MISUMI Top Page> Products> Automation Components> Linear Motion> Linear Shafts> Straight')

        if (!url_uk.includes("stg0")) {
            confItem()
        }
    })

    /*
      Description: Click the [Clear all] button at the top of the specification item with the part number of product fixed as SFJ3-10-LKC.

      Test Steps:
          1. Fixed part number of product is canceled.
    */
    it('Specification Item DE', { baseUrl: url_de }, () => {

        cy.contains('Mechanische Komponenten').click({ force: true })
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false
        })
        cy.contains('Linearwellen').click()
        cy.intercept('GET', '**categoryCode=M0101000000**').as('loadCategory')
        cy.get('[href="/vona2/detail/110302634310/"]').first().invoke('removeAttr', 'target').click()
        cy.wait("@loadCategory")

        cy.contains('MISUMI Startseite> Produkte> Mechanische Komponenten> Linearbewegung> Linearwellen> Gerade')

        if (!url_de.includes("stg0")) {
            confItem()
        }
    })

    /*
      Description: Click the [Clear all] button at the top of the specification item with the part number of product fixed as SFJ3-10-LKC.

      Test Steps:
          1. Fixed part number of product is canceled.
    */
    it('Specification Item IT', { baseUrl: url_it }, () => {

        cy.get('[href="/vona2/mech/"]').click({ force: true })
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false
        })
        cy.contains('Alberi lineari').click()
        cy.intercept('GET', '**categoryCode=M0101000000**').as('loadCategory')
        cy.get('[href="/vona2/detail/110302634310/"]').first().invoke('removeAttr', 'target').click()
        cy.wait("@loadCategory")

        cy.contains('Pagina principale MISUMI> Prodotti> Componenti meccanici> Movimentazione lineare> Alberi lineari> Dritti')

        if (!url_it.includes("stg0")) {
            confItem()
        }
    })

    /*
      Description: Click the [Clear all] button at the top of the specification item with the part number of product fixed as SFJ3-10-LKC.

      Test Steps:
          1. Fixed part number of product is canceled.
    */
    it('Specification Item FR', { baseUrl: url_fr }, () => {

        cy.get('[href="/vona2/mech/"]').click({ force: true })
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false
        })
        cy.contains('Arbres linéaires').click()
        cy.intercept('GET', '**categoryCode=M0101000000**').as('loadCategory')
        cy.get('[href="/vona2/detail/110302634310/"]').first().invoke('removeAttr', 'target').click()
        cy.wait("@loadCategory")

        cy.contains('Haut de la page MISUMI> Produits> Composants mécaniques> Mouvement linéaire> Arbres linéaires> Droit')

        if (!url_fr.includes("stg0")) {
            confItem()
        }
    })

});