import Login from '../pom/login';

const authUser = require('../../../fixtures/auth-user.json');
const loginPage = new Login()
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('My Page Quotation History', () => {
  beforeEach(() => {
    loginPage.login(username, password)
    cy.visit('/')
    cy.wait(2000)
  });
  /*
    Description: My page top page - Click the [Quotation History] link on the left side of the page.

    Test Steps:
        1. Quotation history: the page is displayed. (MISUMI Home>My MISUMI>Quotation History)
  */
  it('My Page Quotation History UK', { baseUrl: url_uk }, () => {
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')

  })

  it('My Page Quotation History DE', { baseUrl: url_de }, () => {
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')

  })

  it('My Page Quotation History IT', { baseUrl: url_it }, () => {
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')

  })

  it('My Page Quotation History FR', { baseUrl: url_fr }, () => {
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
  })

});