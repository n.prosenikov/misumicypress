import Login from "../pom/login"

const authUser = require('../../../fixtures/auth-user.json');
const loginPage = new Login()
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('Cart Transfer to My Components ', () => {
    beforeEach(() => {
        loginPage.login(username, password)
        cy.visit('/')
        cy.wait(2000)
    });
    /*
      Description: Select only SFJ3-10 on the cart page and click the [Go to my component list] button on the top of the page.
 
  
      Test Steps:
          1. Modal which confirms a product moves to My components is displayed.
             (Go to my component list)
          2. Click the [Move] button in the modal which confirms that a product is moved to My components.
          Modal which shows a product was moved to My components is displayed.
             (Go to my component list)
         3. Click the [Close] button in the modal which shows that a product was moved to My components.
             The display of SFJ3-10 disappeared which moved from cart to My components, and the registered number of items decreases by one.
         4. Display the main folder page of My components and confirm that moved SFJ3-10 was added from the cart. *Display method of main folder page of My components is optional.
             SFJ3-10 is added in main folder of My components.
  
    */
    it('Cart Transfer to My Components UK', { baseUrl: url_uk }, () => {
        cy.searchPart('SFJ3-100')
        cy.wait(1000)
        cy.contains('SFJ3-100')

    })

    it('Cart Transfer to My Components DE', { baseUrl: url_de }, () => {
        cy.searchPart('SFJ3-100')
        cy.wait(1000)
        cy.contains('SFJ3-100')

    })

    it('Cart Transfer to My Components IT', { baseUrl: url_it }, () => {
        cy.searchPart('SFJ3-100')
        cy.wait(1000)
        cy.contains('SFJ3-100')

    })

    it('Cart Transfer to My Components FR', { baseUrl: url_fr }, () => {
        cy.searchPart('SFJ3-100')
        cy.wait(1000)
        cy.contains('SFJ3-100')
    })

});