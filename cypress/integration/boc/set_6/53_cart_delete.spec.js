import Login from "../pom/login"

const authUser = require('../../../fixtures/auth-user.json');
const loginPage = new Login()
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Cart Delete ', () => {
  beforeEach(() => {
    loginPage.login(username, password)
    cy.visit('/')
    cy.wait(1000)
  });
  /*
     Description: Select any part number of product on the cart page and click [Remove from shopping cart] button on the top of the page.

 
     Test Steps:
         1. Modal which confirms deletion from cart is displayed. (Remove from shopping cart)
         2. Click the [Delete] button in the modal which confirms deletion of a product from cart.
            The selected part number of product has been deleted from cart.
   */
  it('Cart Delete UK', { baseUrl: url_uk },() => {
    cy.wait(2000)
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')

  })

  it('Cart Delete DE', { baseUrl: url_de }, () => {
    cy.wait(2000)
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')

  })

  it('Cart Delete IT', { baseUrl: url_it },() => {
    cy.wait(2000)
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')

  })

  it('Cart Delete FR', { baseUrl: url_fr }, () => {
    cy.wait(2000)
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')

  })
});