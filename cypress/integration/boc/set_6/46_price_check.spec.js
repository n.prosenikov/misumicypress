import Login from "../pom/login"

const authUser = require('../../../fixtures/auth-user.json');
const loginPage = new Login()
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Price Check', () => {
  beforeEach(() => {
    loginPage.login(username, password)
    cy.visit('/')
    cy.wait(2000)
  });
  /*
    Description: Part number of product SFJ3-100: click the [Check Price / discount rate] button with Order Qty. 
                                        set to 1 and Unit price to --- on the right side of product details page.
    Test Steps:
        1. Values must be set for three items: Unit price, Total, Shipping Days

  */
  it('Price Check UK', { baseUrl: url_uk }, () => {
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    // cy.get('.m-btn--checkPrice').click()
    cy.get('.mc-num').should('exist')
  })

  it('Price Check DE', { baseUrl: url_de },() => {
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    // cy.get('.m-btn--checkPrice').click()
    cy.get('.mc-num').should('exist')
  })

  it('Price Check IT', { baseUrl: url_it },() => {
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    // cy.get('.m-btn--checkPrice').click()
    cy.get('.mc-num').should('exist')
  })


  it('Price Check FR', { baseUrl: url_fr }, () => {
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    // cy.get('.m-btn--checkPrice').click()
    cy.get('.mc-num').should('exist')
  })


});