import Login from "../pom/login"

const authUser = require('../../../fixtures/auth-user.json');
const loginPage = new Login()
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

const partName = "SFJ3-100"

describe('My Page Order History', () => {
  beforeEach(() => {
    loginPage.login(username, password)
    cy.visit('/')
    cy.wait(2000)
  });
  /*
    Description: My page top page - Click the [Order History] link on the left side of the page.

    Test Steps:
        1. Order history: the page is displayed. (MISUMI Home>My MISUMI>Order History)
  */
  it('My Page Order History UK', { baseUrl: url_uk }, () => {
    // cy.searchPart('SFJ3-100')
    cy.get('#keyword_input').type(partName + '{enter}')
    cy.contains(partName)

  })

  it('My Page Order History DE', { baseUrl: url_de }, () => {
    // cy.searchPart('SFJ3-100')
    cy.get('#keyword_input').type(partName + '{enter}')
    cy.contains(partName)

  })

  it('My Page Order History IT', { baseUrl: url_it }, () => {
    // cy.searchPart('SFJ3-100')
    cy.get('#keyword_input').type(partName + '{enter}')
    cy.contains(partName)

  })

  it('My Page Order History FR', { baseUrl: url_fr }, () => {
    // cy.searchPart('SFJ3-100')
    cy.get('#keyword_input').type(partName + '{enter}')
    cy.contains(partName)
  })

});