import Login from '../pom/login';
import UserMenu from '../pom/userMenu';

const userMenu = new UserMenu();
const loginPage = new Login()

const authUser = require('../../../fixtures/auth-user.json');
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('My Page Change Password', () => {
  beforeEach(() => {
    loginPage.login(username, password)
    cy.visit('/')
  });

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Change password] link.
    Test Steps:
        1. Password change: the page is displayed. (Change Password)
*/   
  it('Change Password UK', { baseUrl: url_uk },() => {
    userMenu.userPanel().click()
    userMenu.changePassword().click()
    cy.contains('Change Password')
  })

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Change password] link.
    Test Steps:
        1. Password change: the page is displayed. (Change Password)
*/   
  it('Change Password DE', { baseUrl: url_de },() => {
    userMenu.userPanel().click()
    userMenu.changePassword().click()
    cy.contains('Passwort ändern')
  })  

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Change password] link.
    Test Steps:
        1. Password change: the page is displayed. (Change Password)
*/   
  it('Change Password IT', { baseUrl: url_it },() => {
    userMenu.userPanel().click()
    userMenu.changePassword().click()
    cy.contains('Modifica password')
  })  

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Change password] link.
    Test Steps:
        1. Password change: the page is displayed. (Change Password)
*/   
  it('Change Password FR', { baseUrl: url_fr },() => {
    userMenu.userPanel().click()
    userMenu.changePassword().click()
    cy.contains('Changer mon mot de passe')
  })

});