import LoginButton from '../../cssp/pom/login_button';
import Login from '../pom/login';

const authUser = require('../../../fixtures/auth-user.json');
const loginPage = new Login()
const loginBtn = new LoginButton()

const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;
describe('Logout', () => {
  beforeEach(() => {
    loginPage.login(username, password)
    cy.visit('/')
  });
  /*
    Description: eCatalog top page - expand the menu of [login user name] on the top right of the page and click [Logout] link.
    Test Steps:
        1. Logout process is executed, and the notation of [login user name] button changes to Login.

  */
  it('Logout UK', { baseUrl: url_uk }, () => {
    loginBtn.loginBtnSelector().trigger('mouseover')
    loginBtn.logOutBtnSelector().click()
    loginBtn.loginRegisterBtn().should('be.visible')
  })

  it('Logout DE', { baseUrl: url_de }, () => {
    loginPage.loginStepByStep(username, password)
    loginBtn.loginBtnSelector().trigger('mouseover')
    loginBtn.logOutBtnSelector().click()
    loginBtn.loginRegisterBtn().should('be.visible')
  })

  it('Logout IT', { baseUrl: url_it }, () => {
    loginPage.loginStepByStep(username, password)
    loginBtn.loginBtnSelector().trigger('mouseover')
    loginBtn.logOutBtnSelector().click()
    loginBtn.loginRegisterBtn().should('be.visible')
  })

  it('Logout FR', { baseUrl: url_fr }, () => {
    cy.wait(1000)
    loginPage.loginStepByStep(username, password)
    loginBtn.loginBtnSelector().trigger('mouseover')
    loginBtn.logOutBtnSelector().click()
    loginBtn.loginRegisterBtn().should('be.visible')
  })

});