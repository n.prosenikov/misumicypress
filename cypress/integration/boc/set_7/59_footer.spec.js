import Login from '../pom/login';
import Footer from '../../mobile/pom/footer';

const footer = new Footer()
const loginPage = new Login()

const authUser = require('../../../fixtures/auth-user.json');
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Footer ', () => {
  beforeEach(() => {
    loginPage.login(username, password)
    cy.visit('/')
  });
  /*
      Description: eCatalog top page - expand the menu of [login user name] on the top right of the page and click [Logout] link.
      Test Steps:
          1. Logout process is executed, and the notation of [login user name] button changes to Login.
  
    */
  it('Footer UK', { baseUrl: url_uk },() => {
    footer.checkFooterOptionsExistUK()


  })

  it('Footer DE', { baseUrl: url_de },() => {
    footer.checkFooterOptionsExistDE()


  })

  it('Footer IT', { baseUrl: url_it },() => {
    footer.checkFooterOptionsExistIT()
  })

  it('Footer FR', { baseUrl: url_fr },() => {
    footer.checkFooterOptionsExistFR()
  })

});