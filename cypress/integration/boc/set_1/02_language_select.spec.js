const authUser = require('../../../fixtures/auth-user.json')
import Login from "../pom/login"

const loginPage = new Login()
const {
	username,
	password,
	url_uk,
	url_de,
	url_fr,
	url_it
} = authUser;

describe('Switch Language', () => {
	beforeEach(() => {
		loginPage.login(username, password)
		cy.visit('/')
		cy.wait(2000)
	});

	/*
	  Description: Switching between UK and DE local office and asserting that the correct language is displayed.
	  Test Steps:
		1. Login onto the UK server with correct credentials and assert correct username
		2. Switch to German using the language select menu and assert German is displayed
	  */
	it('Language - UK', { baseUrl: url_uk }, () => {
		cy.contains('Place an Order')
		loginPage.checkLanguage()
	})

	/*
	Description: Switching between DE and IT local office and asserting that the correct language is displayed.
	Test Steps:
	  1. Login onto the UK server with correct credentials and assert correct username
	  2. Switch to German using the language select menu and assert German is displayed
	*/
	it('Language - DE', { baseUrl: url_de }, () => {
		cy.contains('Bestellen')
		loginPage.checkLanguage()
	})

	/*
	Description: Switching between IT and FR local office and asserting that the correct language is displayed.
	Test Steps:
	  1. Login onto the UK server with correct credentials and assert correct username
	  2. Switch to German using the language select menu and assert German is displayed
	*/
	it('Language - IT', { baseUrl: url_it }, () => {
		cy.contains('Ordina')
		loginPage.checkLanguage()
	})

	/*
	Description: Switching between FR and UK local office and asserting that the correct language is displayed.
	Test Steps:
	  1. Login onto the UK server with correct credentials and assert correct username
	  2. Switch to German using the language select menu and assert German is displayed
	*/
	it('Language - FR', { baseUrl: url_fr }, () => {
		cy.contains('Commander')
		loginPage.checkLanguage()
	})
})