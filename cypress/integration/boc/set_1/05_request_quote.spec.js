import Login from "../pom/login"

const loginPage = new Login()

const authUser = require('../../../fixtures/auth-user.json');
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Request Quote', () => {
  beforeEach(() => {
    loginPage.login(username, password)
  });

  /*
      Description: eCatalog top page - Click the [Request a Quote] button on the right side of the page.
      Test Steps:
          1. Quotation: the page is displayed. (New Quotation)
  */
  it('Request Quote UK', { baseUrl: url_uk }, () => {
    cy.visit('/')

    cy.contains('Request a Quote').click()
    cy.contains('Create New Quotation')
  })

  /*
      Description: eCatalog top page - Click the [Request a Quote] button on the right side of the page.
      Test Steps:
          1. Quotation: the page is displayed. (New Quotation)
  */
  it('Request Quote DE', { baseUrl: url_de }, () => {
    cy.visit('/')

    cy.contains('Anfragen').click()
    cy.contains('Neue Anfrage')
  })

  /*
      Description: eCatalog top page - Click the [Request a Quote] button on the right side of the page.
      Test Steps:
          1. Quotation: the page is displayed. (New Quotation)
  */
  it('Request Quote IT', { baseUrl: url_it }, () => {
    cy.visit('/')

    cy.contains('Richiedi preventivo').click()
    cy.contains('Richiesta quotazione')
  })

  /*
      Description: eCatalog top page - Click the [Request a Quote] button on the right side of the page.
      Test Steps:
          1. Quotation: the page is displayed. (New Quotation)
  */
  it('Request Quote FR', { baseUrl: url_fr }, () => {
    cy.visit('/')

    cy.contains('Demande de devis').click()
    cy.contains('Demande de devis')
  })


});
