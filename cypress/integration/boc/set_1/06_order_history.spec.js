import Login from "../pom/login"

const loginPage = new Login()

const authUser = require('../../../fixtures/auth-user.json');
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Order History', () => {
  beforeEach(() => {
    loginPage.login(username, password)
  });

  /*
      Description: eCatalog top page - Click the [Manage Order History] link on the right side of the page.
      Test Steps:
          1. Order history: the page is displayed.(MISUMI Home>My MISUMI>Order History)
  */
  it('Order History UK', { baseUrl: url_uk }, () => {
    cy.visit('/')

    cy.contains('Manage Order History').click()
    cy.contains('Order History')
  })

  /*
      Description: eCatalog top page - Click the [Manage Order History] link on the right side of the page.
      Test Steps:
          1. Order history: the page is displayed.(MISUMI Home>My MISUMI>Order History)
  */
  it('Order History DE', { baseUrl: url_de }, () => {
    cy.visit('/')

    cy.contains('Bestellhistorie').click()
    cy.contains('Bestellhistorie')
  })

  /*
      Description: eCatalog top page - Click the [Manage Order History] link on the right side of the page.
      Test Steps:
          1. Order history: the page is displayed.(MISUMI Home>My MISUMI>Order History)
  */
  it('Order History IT', { baseUrl: url_it }, () => {
    cy.visit('/')

    cy.contains('Accedi a storico ordini').click()
    cy.contains('Storico ordini')
  })

  /*
      Description: eCatalog top page - Click the [Manage Order History] link on the right side of the page.
      Test Steps:
          1. Order history: the page is displayed.(MISUMI Home>My MISUMI>Order History)
  */
  it('Order History FR', { baseUrl: url_fr }, () => {
    cy.visit('/')

    cy.contains('Historique des commandes').click()
    cy.contains('Historique des commandes')
  })
});