import Login from "../pom/login"

const loginPage = new Login()

const authUser = require('../../../fixtures/auth-user.json');
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Place Order', () => {
  beforeEach(() => {
    loginPage.login(username, password)
  });

  /*
        Description: eCatalog top page - Click the [Place an Order] button on the right side of the page.
        Test Steps:
            1. Order: the page is displayed.(New Order)
    */
  it('Place Order UK', { baseUrl: url_uk }, () => {
    cy.visit('/')

    cy.contains('Place an Order').click()
    cy.contains('Create New Order')
  })

  /*
      Description: eCatalog top page - Click the [Place an Order] button on the right side of the page.
      Test Steps:
          1. Order: the page is displayed.(New Order)
  */
  it('Place Order DE', { baseUrl: url_de }, () => {
    cy.visit('/')

    cy.contains('Bestellen').click()
    cy.contains('Neue Bestellung')
  })

  /*
      Description: eCatalog top page - Click the [Place an Order] button on the right side of the page.
      Test Steps:
          1. Order: the page is displayed.(New Order)
  */
  it('Place Order IT', { baseUrl: url_it }, () => {
    cy.visit('/')

    cy.contains('Ordina').click()
    cy.contains('Crea richiesta d\'ordine')
  })

  /*
      Description: eCatalog top page - Click the [Place an Order] button on the right side of the page.
      Test Steps:
          1. Order: the page is displayed.(New Order)
  */
  it('Place Order FR', { baseUrl: url_fr }, () => {
    cy.visit('/')

    cy.contains('Commander').click()
    cy.contains('Créer une commande')
  })

});