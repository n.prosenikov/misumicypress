import Login from "../pom/login"

const loginPage = new Login()

const authUser = require('../../../fixtures/auth-user.json');
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Quote History', () => {
  beforeEach(() => {
    loginPage.login(username, password)
    cy.wait(1000)
  });

  /*
    Description: eCatalog top page - Click the [Manage Quote History] link on the right side of the page.
    Test Steps:
        1. Quotation history: the page is displayed.(MISUMI Home>My MISUMI>Quotation History)
  */
  it('Quotation History UK', { baseUrl: url_uk }, () => {
    cy.visit('/')

    cy.contains('Manage Quote History').click()
    cy.contains('Quotation History')
  })

  /*
      Description: eCatalog top page - Click the [Manage Quote History] link on the right side of the page.
      Test Steps:
          1. Quotation history: the page is displayed.(MISUMI Home>My MISUMI>Quotation History)
  */
  it('Quotation History DE', { baseUrl: url_de }, () => {
    cy.visit('/')

    cy.contains('Angebotshistorie').click()
    cy.contains('Angebotshistorie')
  })

  /*
      Description: eCatalog top page - Click the [Manage Quote History] link on the right side of the page.
      Test Steps:
          1. Quotation history: the page is displayed.(MISUMI Home>My MISUMI>Quotation History)
  */
  it('Quotation History IT', { baseUrl: url_it }, () => {
    cy.visit('/')

    cy.contains('Accedi a storico quotazioni').click()
    cy.contains('Storico quotazioni')
  })

  /*
      Description: eCatalog top page - Click the [Manage Quote History] link on the right side of the page.
      Test Steps:
          1. Quotation history: the page is displayed.(MISUMI Home>My MISUMI>Quotation History)
  */
  it('Quotation History FR', { baseUrl: url_fr }, () => {
    cy.visit('/')

    cy.contains('Historique des devis').click()
    cy.contains('Historique des devis')
  })
});