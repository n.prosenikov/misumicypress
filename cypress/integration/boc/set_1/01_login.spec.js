const authUser = require('../../../fixtures/auth-user.json');
import Login from "../pom/login"

const loginPage = new Login()
const {
  name,
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Login', () => {
  beforeEach(() => {
    loginPage.login(username, password)
  });

  /*
    Description: Login onto the UK server using the provided credentials and assert
                 search bar is displayed on English.
    Test Steps:
        1. eCatalog top page - Click the [Login] button in the upper right corner of the page and log in with a common account to all local offices.
        2. Assert logged with correct username is displayed
  */
  it('Login UK', { baseUrl: url_uk }, () => {
    cy.visit('/')
    cy.contains(name)
  })

  /*
    Description: Login onto the DE server using the provided credentials and
                 assert logged with correct username is displayed
    Test Steps:
        1. eCatalog top page - Click the [Login] button in the upper right corner of the page and log in with a common account to all local offices.
        2. Assert logged with correct username is displayed
  */
  it('Login DE', { baseUrl: url_de }, () => {
    cy.visit('/')
    cy.contains(name)
  })

  /*
    Description: Login onto the IT server using the provided credentials and
                 assert logged with correct username is displayed
    Test Steps:
        1. eCatalog top page - Click the [Login] button in the upper right corner of the page and log in with a common account to all local offices.
        2. Assert logged with correct username is displayed
  */
  it('Login IT', { baseUrl: url_it }, () => {
    cy.visit('/')
    cy.contains(name)
  })

  /*
    Description: Login onto the FR server using the provided credentials and
                 assert logged with correct username is displayed
    Test Steps:
        1. eCatalog top page - Click the [Login] button in the upper right corner of the page and log in with a common account to all local offices.
        2. Assert logged with correct username is displayed
  */
  it('Login FR', { baseUrl: url_fr }, () => {
    cy.visit('/')
    cy.contains(name)
  })
});