import MyPage from '../pom/myPage';
import Login from "../pom/login"

const authUser = require('../../../fixtures/auth-user.json');

const loginPage = new Login()
const myPage = new MyPage();
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('My Page CSSP - Login Menu UK', () => {
  beforeEach(() => {
    loginPage.login(username, password)
    cy.visit('/')
  });

  it('My Page CSSP - UK', { baseUrl: url_uk }, () => {
    cy.intercept('GET', '**/settings/**').as('waitForMyPage')
    cy.get('[data-common-userlink="mypagetop"]').click()

    cy.wait(`@waitForMyPage`)
    myPage.headerTitle().contains('Welcome')
  })

  /*
  Description: eCatalog top page - Click the [Login User Name My Page] link on the right side of the page. Link with outlined character on the blue background
  Test Steps:
  1. My page top: the page is displayed. (MISUMI Top Page>Top of My Page)
  */
  it('My Page CSSP - DE', { baseUrl: url_de }, () => {
    cy.intercept('GET', '**/settings/**').as('waitForMyPage')
    cy.get('[data-common-userlink="mypagetop"]').click()

    cy.wait(`@waitForMyPage`)
    myPage.headerTitle().contains('Willkommen')
  })

  /*
      Description: eCatalog top page - Click the [Login User Name My Page] link on the right side of the page. Link with outlined character on the blue background
      Test Steps:
          1. My page top: the page is displayed. (MISUMI Top Page>Top of My Page)
  */
  it('My Page CSSP - IT', { baseUrl: url_it }, () => {
    cy.intercept('GET', '**/settings/**').as('waitForMyPage')
    cy.get('[data-common-userlink="mypagetop"]').click()

    cy.wait(`@waitForMyPage`)
    myPage.headerTitle().contains('Benvenuti')
  })

  /*
      Description: eCatalog top page - Click the [Login User Name My Page] link on the right side of the page. Link with outlined character on the blue background
      Test Steps:
          1. My page top: the page is displayed. (MISUMI Top Page>Top of My Page)
  */
  it('My Page CSSP - FR', { baseUrl: url_fr }, () => {
    cy.intercept('GET', '**/settings/**').as('waitForMyPage')
    cy.get('[data-common-userlink="mypagetop"]').click()

    cy.wait(`@waitForMyPage`)
    myPage.headerTitle().contains('Bienvenue')
  })
});
