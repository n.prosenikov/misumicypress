import Login from "../pom/login"

const authUser = require('../../../fixtures/auth-user.json');
const loginPage = new Login()
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Suggestion No Hits', () => {
  beforeEach(() => {
    loginPage.login(username, password)
    cy.visit('/')
  });
  /*
    Description: eCatalog top page - Enter @@@ in the search field at the top of the page.
    Test Steps:
        1. Suggestion is not displayed.
  */
  it('Suggestion No Hits UK', { baseUrl: url_uk }, () => {
    cy.get('#keyword_input').type('@@@')
    cy.contains('@@@').should('not.exist');
  })

  /*
    Description: eCatalog top page - Enter @@@ in the search field at the top of the page.
    Test Steps:
        1. Suggestion is not displayed.
  */
  it('Suggestion No Hits DE', { baseUrl: url_de },() => {
    cy.get('#keyword_input').type('@@@')
    cy.contains('@@@').should('not.exist');
  })

  /*
    Description: eCatalog top page - Enter @@@ in the search field at the top of the page.
    Test Steps:
        1. Suggestion is not displayed.
  */
  it('Suggestion No Hits IT', { baseUrl: url_it },() => {
    cy.get('#keyword_input').type('@@@')
    cy.contains('@@@').should('not.exist');
  })

  /*
    Description: eCatalog top page - Enter @@@ in the search field at the top of the page.
    Test Steps:
        1. Suggestion is not displayed.
  */
  it('Suggestion No Hits FR', { baseUrl: url_fr },() => {
    cy.get('#keyword_input').type('@@@')
    cy.contains('@@@').should('not.exist');
  })

});