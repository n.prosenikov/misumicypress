import Login from "../pom/login"

const authUser = require('../../../fixtures/auth-user.json');
const loginPage = new Login()
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Suggestion - Part Number', () => {
  beforeEach(() => {
    loginPage.login(username, password)
    cy.visit('/')
    cy.wait(2000)
    
  });
  /*
    Description: eCatalog top page - Enter SFJ3-100 in the search field at the top of the page and click the displayed [SFJ3-100 [MISUMI]] link which is suggested.
    Test Steps:
        1. Search result: Product details page is displayed on which part number of product SFJ3-100 is selected.
  */
  it('Suggestion - Part Number UK', { baseUrl: url_uk },() => {
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
  })

  /*
    Description: eCatalog top page - Enter SFJ3-100 in the search field at the top of the page and click the displayed [SFJ3-100 [MISUMI]] link which is suggested.
    Test Steps:
        1. Search result: Product details page is displayed on which part number of product SFJ3-100 is selected.
  */
  it('Suggestion - Part Number DE', { baseUrl: url_de }, () => {
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
  })

  /*
    Description: eCatalog top page - Enter SFJ3-100 in the search field at the top of the page and click the displayed [SFJ3-100 [MISUMI]] link which is suggested.
    Test Steps:
        1. Search result: Product details page is displayed on which part number of product SFJ3-100 is selected.
  */
  it('Suggestion - Part Number IT', { baseUrl: url_it },() => {
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
  })
  
  /*
    Description: eCatalog top page - Enter SFJ3-100 in the search field at the top of the page and click the displayed [SFJ3-100 [MISUMI]] link which is suggested.
    Test Steps:
        1. Search result: Product details page is displayed on which part number of product SFJ3-100 is selected.
  */
  it('Suggestion - Part Number FR', { baseUrl: url_fr },() => {
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
  })

});