import Login from "../pom/login"

const authUser = require('../../../fixtures/auth-user.json');
const loginPage = new Login()
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Digital Book', () => {
  beforeEach(() => {
    loginPage.login(username, password)
    cy.visit('/')
    cy.wait(2000)
  });
  /*
    Description: Part number of product SFJ3-100: product details page - Click the [Catalog] button at the top right of the page.
    Test Steps:
        1. Digital Book is displayed.
  */
  it('Digital Book UK', { baseUrl: url_uk },() => {
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.get('#Tab_catalog').click()
    cy.get('.catalogViewer')
  })

  it('Digital Book DE', { baseUrl: url_de },() => {
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.get('#Tab_catalog').click()
    cy.get('.catalogViewer')
  })

  it('Digital Book IT', { baseUrl: url_it },() => {
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.get('#Tab_catalog').click()
    cy.get('.catalogViewer')
  })

  it('Digital Book FR', { baseUrl: url_fr },() => {
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.get('#Tab_catalog').click()
    cy.get('.catalogViewer')
  })

});