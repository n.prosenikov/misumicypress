import Login from "../pom/login"

const authUser = require('../../../fixtures/auth-user.json');
const loginPage = new Login()
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('CAD Download', () => {
  beforeEach(() => {
    loginPage.login(username, password)
    cy.visit('/')
    cy.wait(2000)
  });
  /*
    Description: Part number of product SFJ3-10: product details page - Click the [CAD Download] on the right side of the page.
    Test Steps:
        1. *When Terms of use of CAD data is displayed, click [Agree].
        2. CAD data selection page is displayed.
  */
  it('CAD Download UK', { baseUrl: url_uk }, () => {
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.get('#cad_dl_button').click()
    cy.get('#CAD_agree').click()
    cy.contains('Generate')
  })

  /*
    Description: Click the [2d_SFJ.zip] link from 2D CAD data with fixed dim. of CAD data selection page.
    Test Steps:
        1. The file 2d_SFJ.zip is downloaded. 
  */
  it('CAD Download DE', { baseUrl: url_de }, () => {
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.get('#cad_dl_button').click()
    cy.get('#CAD_agree').click()
    // cy.contains('Generieren')
  })

  /*
    Description: Part number of product SFJ3-10: product details page - Click the [CAD Download] on the right side of the page.
    Test Steps:
        1. *When Terms of use of CAD data is displayed, click [Cancel].
  */
  it('CAD Download IT', { baseUrl: url_it }, () => {
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.wait(3000)
    cy.get('#cad_dl_button').click()
    cy.get('#CAD_agree').click()
    // cy.contains('Genera')
  })

  /*
    Description: Select File Format : DWF and Version : V5.5,ASCII
    Test Steps:
        1. click [Generate] of 2D/3D CAD Download of CAD data select page.
  */
  it('CAD Download FR', { baseUrl: url_fr }, () => {
    cy.wait(500)
    cy.searchPart('SFJ3-100')
    cy.contains('SFJ3-100')
    cy.get('#cad_dl_button').click()
    cy.get('#CAD_agree').click()
    // cy.contains('Générer')
  })

});