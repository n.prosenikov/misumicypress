import UserMenu from '../pom/userMenu';
import Login from "../pom/login"

const userMenu = new UserMenu();
const loginPage = new Login()

const authUser = require('../../../fixtures/auth-user.json');
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Change Password', () => {
  beforeEach(() => {
    loginPage.login(username, password)
  });

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Change password] link.
    Test Steps:
        1. Password change: the page is displayed. (Change Password)
*/   
  it('Change Password UK', { baseUrl: url_uk },() => {
    cy.visit('/')

    userMenu.userPanel().click()
    userMenu.changePassword().click()
    cy.contains('Change Password')
  })

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Change password] link.
    Test Steps:
        1. Password change: the page is displayed. (Change Password)
*/   
  it('Change Password DE', { baseUrl: url_de },() => {
    cy.visit('/')

    userMenu.userPanel().click()
    userMenu.changePassword().click()
    cy.contains('Passwort ändern')
  })  

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Change password] link.
    Test Steps:
        1. Password change: the page is displayed. (Change Password)
*/   
  it('Change Password IT', { baseUrl: url_it },() => {
    cy.visit('/')

    userMenu.userPanel().click()
    userMenu.changePassword().click()
    cy.contains('Modifica password')
  })  

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Change password] link.
    Test Steps:
        1. Password change: the page is displayed. (Change Password)
*/   
  it('Change Password FR', { baseUrl: url_fr },() => {
    cy.visit('/')

    userMenu.userPanel().click()
    userMenu.changePassword().click()
    cy.contains('Changer mon mot de passe')
  })



/*
    Description: eCatalog top page - Click the [Change password] link on the right side of the page.
    Test Steps:
        1. Password change: the page is displayed. (Change Password)
*/   
  it('Change Password UK', { baseUrl: url_uk }, () => {
    cy.visit('/')

    cy.contains('Change password').invoke('removeAttr', 'target').click()
    cy.contains('Change Password')
  })

/*
    Description: eCatalog top page - Click the [Change password] link on the right side of the page.
    Test Steps:
        1. Password change: the page is displayed. (Change Password)
*/   
  it('Change Password DE', { baseUrl: url_de }, () => {
    cy.visit('/')

    cy.contains('Passwort ändern').invoke('removeAttr', 'target').click()
    cy.contains('Passwort ändern')
  })

/*
    Description: eCatalog top page - Click the [Change password] link on the right side of the page.
    Test Steps:
        1. Password change: the page is displayed. (Change Password)
*/   
  it('Change Password IT', { baseUrl: url_it },() => {
    cy.visit('/')

    cy.contains('Modifica password').invoke('removeAttr', 'target').click()
    cy.contains('Modifica password')
  })

/*
    Description: eCatalog top page - Click the [Change password] link on the right side of the page.
    Test Steps:
        1. Password change: the page is displayed. (Change Password)
*/   
  it('Change Password FR', { baseUrl: url_fr },() => {
    cy.visit('/')

    cy.contains('Changer mon mot de passe').invoke('removeAttr', 'target').click()
    cy.contains('Changer mon mot de passe')
  })

});