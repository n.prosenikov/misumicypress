import UserMenu from '../pom/userMenu';
import Login from "../pom/login"

const userMenu = new UserMenu();
const authUser = require('../../../fixtures/auth-user.json');

const loginPage = new Login()
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Register user info', () => {
  beforeEach(() => {
    loginPage.login(username, password)
  });


/*
  Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Your Corporate Information] link.
  Test Steps:
      1. Registered company information: the page is displayed. (Your Corporate Information)
*/ 
  it('Register user inf UK', { baseUrl: url_uk },() => {
    cy.visit('/')
    userMenu.userPanel().click()
    userMenu.corporateInfo().click()
    cy.contains('Your Corporate Information')
  })

/*
  Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Your Corporate Information] link.
  Test Steps:
      1. Registered company information: the page is displayed. (Your Corporate Information)
*/ 
  it('Register user inf DE', { baseUrl: url_de },() => {
    cy.visit('/')
    userMenu.userPanel().click()
    userMenu.corporateInfo().click()
    cy.contains('Ihre registrierten Unternehmensinformationen')
  })

/*
  Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Your Corporate Information] link.
  Test Steps:
      1. Registered company information: the page is displayed. (Your Corporate Information)
*/ 
  it('Register user inf IT', { baseUrl: url_it },() => {
    cy.visit('/')
    userMenu.userPanel().click()
    userMenu.corporateInfo().click()
    cy.contains('Informazioni aziendali')
  })

/*
  Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Your Corporate Information] link.
  Test Steps:
      1. Registered company information: the page is displayed. (Your Corporate Information)
*/ 
  it('Register user inf FR', { baseUrl: url_fr },() => {
    cy.visit('/')
    userMenu.userPanel().click()
    userMenu.corporateInfo().click()
    cy.contains('Informations société')
  })


/*
  Description: eCatalog top page - Click the [Your Corporate Information] link on the right side of the page.
  Test Steps:
      1. Registered company information: the page is displayed. (Your Corporate Information)
*/ 
  it('Register user inf UK', { baseUrl: url_uk },() => {
    cy.visit('/')
    cy.contains('Your Corporate Information').invoke('removeAttr', 'target').click()
    cy.contains('Your Corporate Information')
  })

/*
  Description: eCatalog top page - Click the [Your Corporate Information] link on the right side of the page.
  Test Steps:
      1. Registered company information: the page is displayed. (Your Corporate Information)
*/ 
  it('Register user inf DE', { baseUrl: url_de },() => {
    cy.visit('/')
    cy.contains('Ihre registrierten Unternehmensinformationen').invoke('removeAttr', 'target').click()
    cy.contains('Ihre registrierten Unternehmensinformationen')
  })

/*
  Description: eCatalog top page - Click the [Your Corporate Information] link on the right side of the page.
  Test Steps:
      1. Registered company information: the page is displayed. (Your Corporate Information)
*/ 
  it('Register user inf IT', { baseUrl: url_it },() => {
    cy.visit('/')
    cy.contains('Informazioni aziendali').invoke('removeAttr', 'target').click()
    cy.contains('Informazioni aziendali')
  })

/*
  Description: eCatalog top page - Click the [Your Corporate Information] link on the right side of the page.
  Test Steps:
      1. Registered company information: the page is displayed. (Your Corporate Information)
*/ 
  it('Register user inf FR', { baseUrl: url_fr },() => {
    cy.visit('/')
    cy.contains('Informations société').invoke('removeAttr', 'target').click()
    cy.contains('Informations société')
  })

});