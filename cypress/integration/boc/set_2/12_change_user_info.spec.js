import UserMenu from '../pom/userMenu';
import Login from "../pom/login"

const userMenu = new UserMenu();
const authUser = require('../../../fixtures/auth-user.json');

const loginPage = new Login()
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Change user data', () => {
  beforeEach(() => {
    loginPage.login(username, password)
  });

  /*
      Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Change password] link.
      Test Steps:
          1. Password change: the page is displayed. (Change Password)
  */
  it('Change user data UK', { baseUrl: url_uk }, () => {
    cy.visit('/')

    userMenu.userPanel().click()
    userMenu.changeUserData().click()
    cy.wait(2000)
    cy.contains('Change User Information')
  })

  /*
      Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Change password] link.
      Test Steps:
          1. Password change: the page is displayed. (Change Password)
  */
  it('Change user data DE', { baseUrl: url_de }, () => {
    cy.visit('/')

    userMenu.userPanel().click()
    userMenu.changeUserData().click()
    cy.contains('Benutzerdaten ändern')
  })

  /*
      Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Change password] link.
      Test Steps:
          1. Password change: the page is displayed. (Change Password)
  */
  it('Change user data IT', { baseUrl: url_it }, () => {
    cy.visit('/')

    userMenu.userPanel().click()
    userMenu.changeUserData().click()
    cy.contains('Modifica dati utente')
  })

  /*
      Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Change password] link.
      Test Steps:
          1. Password change: the page is displayed. (Change Password)
  */
  it('Change user data FR', { baseUrl: url_fr }, () => {
    cy.visit('/')

    userMenu.userPanel().click()
    userMenu.changeUserData().click()
    cy.contains('Modifier mon profil')
  })


  /*
      Description: eCatalog top page - Click the [Change User Information] link on the right side of the page.
      Test Steps:
          1. User information change: the page is displayed. (Change User Information)
  */
  it('Change user data UK', { baseUrl: url_uk }, () => {
    cy.visit('/')

    cy.contains('Change user data').invoke('removeAttr', 'target').click()
    cy.wait(2000)
    cy.contains('Change User Information')
  })

  /*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Change password] link.
    Test Steps:
        1. Password change: the page is displayed. (Change Password)
  */
  it('Change user data DE', { baseUrl: url_de }, () => {
    cy.visit('/')

    cy.contains('Benutzerdaten ändern').invoke('removeAttr', 'target').click()
    cy.contains('Benutzerdaten ändern')
  })

  /*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Change password] link.
    Test Steps:
        1. Password change: the page is displayed. (Change Password)
  */
  it('Change user data IT', { baseUrl: url_it }, () => {
    cy.visit('/')

    cy.contains('Modifica dati utente').invoke('removeAttr', 'target').click()
    cy.contains('Modifica dati utente')
  })

  /*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [Change password] link.
    Test Steps:
        1. Password change: the page is displayed. (Change Password)
  */
  it('Change user data FR', { baseUrl: url_fr }, () => {
    cy.visit('/')

    cy.contains('Modifier mon profil').invoke('removeAttr', 'target').click()
    cy.contains('Modifier mon profil')
  })

});