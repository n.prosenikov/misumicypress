import Login from "../pom/login"

const loginPage = new Login()

const authUser = require('../../../fixtures/auth-user.json');
const {
  username,
  password,
  url_uk,
  url_de,
} = authUser;

describe('Incad Library Details', () => {
  beforeEach(() => {
    loginPage.login(username, password)
    cy.visit('/')
  });


/*
  Description: Click the case [No.000153 Lifting Mechanism Using Rack and Pinions] at inCAD Library.
  Test Steps:
      1. Case detail of No.000153 Lifting Mechanism Using Rack and Pinions is displayed.
*/ 
  it('Incad Library UK', { baseUrl: url_uk },() => {
    //cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    //cy.login()
    //cy.wait("@initialLoad")
    cy.get('.inCAD-library > .rapid-image > a').invoke('removeAttr', 'target').click()  
    cy.get('.closebutton').click()
    cy.contains('No.000153').click()
    cy.contains('No.000153')
  })

/*
  Description: Click the case [No.000153 Lifting Mechanism Using Rack and Pinions] at inCAD Library.
  Test Steps:
      1. Case detail of No.000153 Lifting Mechanism Using Rack and Pinions is displayed.
*/ 
  it('Incad Library DE', { baseUrl: url_de },() => {
    //cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    //cy.login()
    //cy.wait("@initialLoad")
    cy.get('.inCAD-library > .rapid-image > a').invoke('removeAttr', 'target').click()  
    cy.get('.closebutton').click()
    cy.contains('Nr.000153').click()
    cy.contains('Nr.000153')
  })

});