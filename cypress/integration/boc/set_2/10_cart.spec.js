import Login from "../pom/login"

const loginPage = new Login()

const authUser = require('../../../fixtures/auth-user.json');
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Cart Test', () => {
  beforeEach(() => {
    loginPage.login(username, password)
  });

/*
    Description: eCatalog top page - Click the [Cart] icon on the right upper side of the page.
    Test Steps:
        1. Cart: the page is displayed. (Cart)
*/      
  it('My Components UK', { baseUrl: url_uk }, () => {
    cy.visit('/')

    cy.get('.lc-cart').click()
    cy.contains('Products added to shopping cart')
  })

/*
    Description: eCatalog top page - Click the [Cart] icon on the right upper side of the page.
    Test Steps:
        1. Cart: the page is displayed. (Cart)
*/      
  it('My Components DE', { baseUrl: url_de },() => {
    cy.visit('/')

    cy.get('.lc-cart').click()
    cy.contains('Zu dem Einkaufswagen hinzugefügte Artikel ')
  })

/*
    Description: eCatalog top page - Click the [Cart] icon on the right upper side of the page.
    Test Steps:
        1. Cart: the page is displayed. (Cart)
*/      
  it('My Components IT', { baseUrl: url_it },() => {
    cy.visit('/')

    cy.get('.lc-cart').click()
    cy.contains('Prodotti aggiunti al carrello')
  })

/*
    Description: eCatalog top page - Click the [Cart] icon on the right upper side of the page.
    Test Steps:
        1. Cart: the page is displayed. (Cart)
*/      
  it('My Components FR', { baseUrl: url_fr }, () => {
    cy.visit('/')

    cy.get('.lc-cart').click()
    cy.contains('Produits ajoutés au panier')
  })
});