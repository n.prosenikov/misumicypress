import Login from "../pom/login"

const loginPage = new Login()

const authUser = require('../../../fixtures/auth-user.json');
const {
  username,
  password,
  url_uk,
  url_de,
} = authUser;

describe('Incad Idea Note', () => {
  beforeEach(() => {
    loginPage.login(username, password)
    cy.visit('/')
  });

/*
  Description: Click [IDEA NOTE 1] button at case details page for Nr.000153 Lifting Mechanism Using Rack and Pinions.
  Test Steps:
      1. Content of IDEA NOTE 1 is displayed.(IDEA NOTE Multiple lifters are raised and lowered.)
*/ 
  it('Incad Idea Note UK', { baseUrl: url_uk },() => {
    //cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    //cy.login()
    //cy.wait("@initialLoad")
    cy.get('.inCAD-library > .rapid-image > a').invoke('removeAttr', 'target').click()  
    cy.get('.closebutton').click()
    cy.contains('No.000153').click()
    cy.contains('No.000153')
    cy.get('[src*="btn_ideanote_01.png"]').click()
    cy.contains('Multiple lifters are raised and lowered.')
  })
  
/*
  Description: Click [IDEA NOTE 1] button at case details page for Nr.000153 Lifting Mechanism Using Rack and Pinions.
  Test Steps:
      1. Content of IDEA NOTE 1 is displayed.(IDEA NOTE Mehrere Heber werden angehoben und abgesenkt..)
*/ 
  it('Incad Idea Note DE', { baseUrl: url_de },() => {
    //cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    //cy.login()
    //cy.wait("@initialLoad")
    cy.get('.inCAD-library > .rapid-image > a').invoke('removeAttr', 'target').click()  
    cy.get('.closebutton').click()
    cy.contains('Nr.000153').click()
    cy.contains('Nr.000153')
    cy.get('[src*="btn_ideanote_01.png"]').click()
    cy.contains('Mehrere Heber werden angehoben und abgesenkt.')
  })

});