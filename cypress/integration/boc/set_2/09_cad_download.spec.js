import CadDownload from '../pom/cadDownloads';
import Login from '../pom/login';
import SideMenu from '../pom/sideMenu';
import UserMenu from '../pom/userMenu';

const userMenu = new UserMenu();
const sideMenu = new SideMenu();
const cadDownload = new CadDownload();

const authUser = require('../../../fixtures/auth-user.json');

const loginPage = new Login()
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('CAD Download', () => {
    beforeEach(() => {
        loginPage.login(username, password)
    });

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [My components] link.
    Test Steps:
        1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
*/
    it('CAD Download - Login Menu UK', { baseUrl: url_uk }, () => {
        cy.visit(url_uk)
        //loginPage.login(username,password)

        //cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        //cy.login()
        //cy.wait("@initialLoad")

        userMenu.userPanel().click()
        userMenu.myCadDownloads().click()

        cadDownload.pageTitle().contains('History of CAD data download')
    })

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [My components] link.
    Test Steps:
        1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
*/
    it('CAD Download - Login Menu DE', { baseUrl: url_de },() => {
        cy.visit(url_de)
        // cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        // cy.login()
        // cy.wait("@initialLoad")

        userMenu.userPanel().click()
        userMenu.myCadDownloads().click()

        cadDownload.pageTitle().contains('CAD-Download Historie')
    })

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [My components] link.
    Test Steps:
        1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
*/    
    it('CAD Download - Login Menu IT', { baseUrl: url_it },() => {
        cy.visit(url_it)
        // cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        // cy.login()
        // cy.wait("@initialLoad")

        userMenu.userPanel().click()
        userMenu.myCadDownloads().click()

        cadDownload.pageTitle().contains('Storico download dati CAD')
    })

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [My components] link.
    Test Steps:
        1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
*/    
    it('CAD Download - Login Menu FR', { baseUrl: url_fr },() => {
        cy.visit(url_fr)
        // cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
        // cy.login()
        // cy.wait("@initialLoad")

        userMenu.userPanel().click()
        userMenu.myCadDownloads().click()

        cadDownload.pageTitle().contains('Historique des téléchargements CAO')
    })


/*
    Description: eCatalog top page - Click the [My CAD data downloads] link on the right side of the page.
    Test Steps:
        1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
*/
    it('CAD Download - Side Menu UK', { baseUrl: url_uk },() => {
        cy.visit('/')

        sideMenu.myCadDownloads().click()
        cadDownload.pageTitle().contains('History of CAD data download')
    })

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [My components] link.
    Test Steps:
        1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
*/
    it('CAD Download - Side Menu DE', { baseUrl: url_de },() => {
        cy.visit('/')

        sideMenu.myCadDownloads().click()
        cadDownload.pageTitle().contains('CAD-Download Historie')
    })

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [My components] link.
    Test Steps:
        1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
*/    
    it('CAD Download - Side Menu IT', { baseUrl: url_it },() => {
        cy.visit('/')

        sideMenu.myCadDownloads().click()
        cadDownload.pageTitle().contains('Storico download dati CAD')
    })

/*
    Description: eCatalog top page - Expand the menu of the [Login user name] button in the upper right corner of the page and click the [My components] link.
    Test Steps:
        1. Main folder of My components: the page is displayed. (MISUMI Top Page>Top of My Page>Main folder (My component list)>Main folder)
*/    
    it('CAD Download - Side Menu FR', { baseUrl: url_fr },() => {
        cy.visit('/')

        sideMenu.myCadDownloads().click()
        cadDownload.pageTitle().contains('Historique des téléchargements CAO')
    })
});
