import Login from "../pom/login"
const authUser = require('../../../fixtures/auth-user.json');

const loginPage = new Login()
const {
  username,
  password,
  url_uk,
  url_de,
} = authUser;

describe('inCAD Library', () => {
  beforeEach(() => {
    loginPage.login(username, password)
    cy.visit('/')
  });


/*
  Description: On eCatalog top page, click the [inCAD Library] link at the bottom of eCatalog.
  Test Steps:
      1. inCAD Library is displayed. (https://pre-uk.misumi-ec.com/eu/incadlibrary/)
*/ 
  it('Incad Library UK', { baseUrl: url_uk },() => {
    //cy.visit(url_uk)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    //cy.login()
    //cy.wait("@initialLoad")
    cy.get('.inCAD-library > .rapid-image > a').invoke('removeAttr', 'target').click()
    cy.get('.closebutton').click()
    cy.get('[href="/eu/incadlibrary/detail/000153.html"]')
    cy.url().should('include', 'uk.misumi-ec.com/eu/incadlibrary')
  })

/*
  Description: On eCatalog top page, click the [inCAD Library] link at the bottom of eCatalog.
  Test Steps:
      1. inCAD Library is displayed. (https://pre-de.misumi-ec.com/eu/incadlibrary/)
*/ 
  it('Incad Library DE', { baseUrl: url_de },() => {
    //cy.visit(url_de)
    cy.intercept('GET', '**ViewCategoryRepeatRecommend**').as('initialLoad')
    //cy.login()
    //cy.wait("@initialLoad")
    cy.get('.inCAD-library > .rapid-image > a').invoke('removeAttr', 'target').click()
    cy.get('.closebutton').click()
    cy.get('[href="/eu/incadlibrary/detail/000153.html"]')
    cy.url().should('include', 'de.misumi-ec.com/eu/incadlibrary')
  })

});