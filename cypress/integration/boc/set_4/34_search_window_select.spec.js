import Login from "../pom/login"

const authUser = require('../../../fixtures/auth-user.json');
const loginPage = new Login()
const {
  name,
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Search Result Search Window', () => {
  beforeEach(() => {
    loginPage.login(username, password)
    cy.visit('/')
    cy.wait(1500)
  });

  /*
    Description: Search window - Click the [SFJ3-10] image or link of Part Number / Type displayed as the search result of [SFJ3-10].
    Test Steps:
        1. Part number of product: product details page is displayed which SFJ3-10 is selected on.
  */
  it('Search Result Search Window UK', { baseUrl: url_uk }, () => {
    cy.get('#keyword_input').type('SFJ3-100')
    cy.get('#keyword_go').click()
    cy.contains('SFJ3-100')
    cy.get('[data-clickable="speclink"]').invoke('removeAttr', 'target').click()
    cy.contains('SFJ3-100')
  })

  /*
    Description: Search window - Click the [SFJ3-10] image or link of Part Number / Type displayed as the search result of [SFJ3-10].
    Test Steps:
        1. Part number of product: product details page is displayed which SFJ3-10 is selected on.
  */
  it('Search Result Search Window DE', { baseUrl: url_de },() => {
    cy.get('#keyword_input').type('SFJ3-100')
    cy.get('#keyword_go').click()
    cy.contains('SFJ3-100')
    cy.get('[data-clickable="speclink"]').invoke('removeAttr', 'target').click()
    cy.contains('SFJ3-100')
  })

  /*
    Description: Search window - Click the [SFJ3-10] image or link of Part Number / Type displayed as the search result of [SFJ3-10].
    Test Steps:
        1. Part number of product: product details page is displayed which SFJ3-10 is selected on.
  */
  it('Search Result Search Window IT', { baseUrl: url_it },() => {
    cy.get('#keyword_input').type('SFJ3-100')
    cy.get('#keyword_go').click()
    cy.contains('SFJ3-100')
    cy.get('[data-clickable="speclink"]').invoke('removeAttr', 'target').click()
    cy.contains('SFJ3-100')
  })

  /*
    Description: Search window - Click the [SFJ3-10] image or link of Part Number / Type displayed as the search result of [SFJ3-10].
    Test Steps:
        1. Part number of product: product details page is displayed which SFJ3-10 is selected on.
  */
  it('Search Result Search Window FR', { baseUrl: url_fr },() => {
    cy.get('#keyword_input').type('SFJ3-100')
    cy.get('#keyword_go').click()
    cy.wait(1000)
    cy.contains('SFJ3-100')
    cy.get('[data-clickable="speclink"]').invoke('removeAttr', 'target').click()
    cy.contains('SFJ3-100')
  })

});