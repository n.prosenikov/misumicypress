import Login from "../pom/login"

const authUser = require('../../../fixtures/auth-user.json');
const loginPage = new Login()

const partName = 'BH6'

const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

describe('Add To Cart', () => {
    beforeEach(() => {
        loginPage.login(username, password)
        cy.visit('/')
    });

    /*
        Description: Enter 1 in the quantity of modal for quotation  and order of suggested product without information posted. Click [Add to Cart] button.
        Test Steps:
            1. Modal representing that the product is added to the cart is displayed. (The item has been added to the shopping cart.)
    */
    it('Add To Cart UK', { baseUrl: url_uk }, () => {
        cy.deleteAllCart()
        cy.wait(3000)
        cy.addToCart(partName, 1)
    })

    /*
        Description: Enter 1 in the quantity of modal for quotation  and order of suggested product without information posted. Click [Add to Cart] button.
        Test Steps:
            1. Modal representing that the product is added to the cart is displayed. (The item has been added to the shopping cart.)
    */
    it('Add To Cart DE', { baseUrl: url_de }, () => {
        cy.deleteAllCart()
        cy.wait(3000)
        cy.addToCart(partName, 1)
    })

    /*
        Description: Enter 1 in the quantity of modal for quotation  and order of suggested product without information posted. Click [Add to Cart] button.
        Test Steps:
            1. Modal representing that the product is added to the cart is displayed. (The item has been added to the shopping cart.)
    */
    it('Add To Cart IT', { baseUrl: url_it }, () => {
        cy.deleteAllCart()
        cy.wait(3000)
        cy.addToCart("BHS2M20L-130", 1)
    })

    /*
        Description: Enter 1 in the quantity of modal for quotation  and order of suggested product without information posted. Click [Add to Cart] button.
        Test Steps:
            1. Modal representing that the product is added to the cart is displayed. (The item has been added to the shopping cart.)
    */
    it('Add To Cart FR', { baseUrl: url_fr }, () => {
        cy.deleteAllCart()
        cy.wait(3000)
        cy.addToCart(partName, 1)
    })

    /*
        Description: Click the [View Cart] button which shows that a product is added to cart.
        Test Steps:
            1. The cart page is displayed and BH6 is added to the Product specification with Quantity 1 (Cart)
    */
    it('Add To Cart UK', { baseUrl: url_uk }, () => {
        cy.deleteAllCart()
        cy.wait(3000)
        cy.addToCart(partName, 1)
        cy.get('.lc-cart').click()
        cy.contains("Cart")
    })

    /*
        Description: Click the [View Cart] button which shows that a product is added to cart.
        Test Steps:
            1. The cart page is displayed and BH6 is added to the Product specification with Quantity 1 (Cart)
    */
    it('Add To Cart DE', { baseUrl: url_de }, () => {
        cy.deleteAllCart()
        cy.wait(3000)
        cy.addToCart(partName, 1)
        cy.get('.lc-cart').click()
        cy.contains("Einkaufswagen")
    })

    /*
        Description: Click the [View Cart] button which shows that a product is added to cart.
        Test Steps:
            1. The cart page is displayed and BH6 is added to the Product specification with Quantity 1 (Cart)
    */
    it('Add To Cart IT', { baseUrl: url_it }, () => {
        cy.deleteAllCart()
        cy.wait(3000)
        cy.addToCart("BHS2M20L-130", 1)
        cy.get('.lc-cart').click()
        cy.contains("Carrello")
    })

    /*
        Description: Click the [View Cart] button which shows that a product is added to cart.
        Test Steps:
            1. The cart page is displayed and BH6 is added to the Product specification with Quantity 1 (Cart)
    */
    it('Add To Cart FR', { baseUrl: url_fr }, () => {
        cy.deleteAllCart()
        cy.wait(3000)
        cy.addToCart(partName, 1)
        cy.get('.lc-cart').click()
        cy.contains("Panier")
    })
})