import Login from "../pom/login"

const authUser = require('../../../fixtures/auth-user.json');
const loginPage = new Login()
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Search Window Keyword', () => {
  beforeEach(() => {
    loginPage.login(username, password)
    cy.visit('/')
    cy.wait(1500)
  });

  /*
    Description: eCatalog Top Page - Enter LINEAR in the search field at the top of the screen and click the [Search] button.
    Test Steps:
        1. Search result: search results of LINEAR are displayed in Category of the page (MISUMI Top Page> Search results).
  */
  it('Search Window Keyword UK', { baseUrl: url_uk },() => {
    cy.get('#keyword_input').type('Linear')
    cy.get('#keyword_go').click()
    cy.contains('Linear Motion')
  })

  /*
    Description: eCatalog Top Page - Enter LINEAR in the search field at the top of the screen and click the [Search] button.
    Test Steps:
        1. Search result: search results of LINEAR are displayed in Category of the page (MISUMI Top Page> Search results).
  */
  it('Search Window Keyword DE', { baseUrl: url_de }, () => {
    cy.get('#keyword_input').type('Linearbewegung')
    cy.get('#keyword_go').click()
    cy.contains('Linearbewegung')
  })

  /*
    Description: eCatalog Top Page - Enter LINEAR in the search field at the top of the screen and click the [Search] button.
    Test Steps:
        1. Search result: search results of LINEAR are displayed in Category of the page (MISUMI Top Page> Search results).
  */
  it('Search Window Keyword IT', { baseUrl: url_it },() => {
    cy.get('#keyword_input').type('Movimentazione')
    cy.get('#keyword_go').click()
    cy.contains('Movimentazione lineare')
  })

  /*
    Description: eCatalog Top Page - Enter LINEAR in the search field at the top of the screen and click the [Search] button.
    Test Steps:
        1. Search result: search results of LINEAR are displayed in Category of the page (MISUMI Top Page> Search results).
  */
  it('Search Window Keyword FR', { baseUrl: url_fr }, () => {
    cy.get('#keyword_input').type('linéaire')
    cy.get('#keyword_go').click()
    cy.contains('linéaire')
  })

});