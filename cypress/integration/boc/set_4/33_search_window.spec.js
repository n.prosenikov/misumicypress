import Login from "../pom/login"

const authUser = require('../../../fixtures/auth-user.json');
const loginPage = new Login()
const {
  username,
  password,
  url_uk,
  url_de,
  url_fr,
  url_it
} = authUser;

describe('Search Window Part Number', () => {
  beforeEach(() => {
    loginPage.login(username, password)
    cy.visit('/')
    cy.wait(1500)
  });

  /*
    Description: eCatalog top page - Enter SFJ3-10 in the search field at the top of the page and click [Search] button.
    Test Steps:
        1. Search result: SFJ-3 is displayed in Part Number/Type of page (MISUMI Top Page> Search results).
  */
  it('Search Window Part Number UK', { baseUrl: url_uk },() => {
    cy.get('#keyword_input').type('SFJ3-100')
    cy.get('#keyword_go').click()
    cy.contains('SFJ3-100')
  })

  /*
    Description: eCatalog top page - Enter SFJ3-10 in the search field at the top of the page and click [Search] button.
    Test Steps:
        1. Search result: SFJ-3 is displayed in Part Number/Type of page (MISUMI Top Page> Search results).
  */
  it('Search Window Part Number DE', { baseUrl: url_de },() => {
    cy.get('#keyword_input').type('SFJ3-100')
    cy.get('#keyword_go').click()
    cy.contains('SFJ3-100')
  })

  /*
    Description: eCatalog top page - Enter SFJ3-10 in the search field at the top of the page and click [Search] button.
    Test Steps:
        1. Search result: SFJ-3 is displayed in Part Number/Type of page (MISUMI Top Page> Search results).
  */
  it('Search Window Part Number IT', { baseUrl: url_it },() => {
    cy.get('#keyword_input').type('SFJ3-100')
    cy.get('#keyword_go').click()
    cy.contains('SFJ3-100')
  })
  
  /*
    Description: eCatalog top page - Enter SFJ3-10 in the search field at the top of the page and click [Search] button.
    Test Steps:
        1. Search result: SFJ-3 is displayed in Part Number/Type of page (MISUMI Top Page> Search results).
  */
  it('Search Window Part Number FR', { baseUrl: url_fr },() => {
    cy.get('#keyword_input').type('SFJ3-100')
    cy.get('#keyword_go').click()
    cy.contains('SFJ3-100')
  })

});