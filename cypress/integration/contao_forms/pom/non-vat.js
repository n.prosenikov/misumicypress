let today = new Date()
export const currentDate = (today.getDate().toString() + (today.getMonth() + 1).toString() + today.getFullYear().toString())
const randNumber0 = Math.floor(Math.random() * 36)

export default class NonVAT {
    //Selectors
    corporateUserRegisterBtn() {
        return cy.get('[class*="row_6"]').find('.col_1')
    }

    titleSelectField() {
        return cy.get('#ctrl_salutation')
    }

    firstNameField() {
        return cy.get('#ctrl_firstname')
    }

    lastNameField() {
        return cy.get('#ctrl_lastname')
    }

    positionField() {
        return cy.get('#ctrl_position')
    }

    phoneField() {
        return cy.get('#ctrl_phone')
    }

    eMailField() {
        return cy.get('#ctrl_email')
    }

    loadingMsg() {
        return cy.get('.loading')
    }

    passwordField() {
        return cy.get('#ctrl_password')
    }

    confirmPasswordField() {
        return cy.get('#ctrl_password_confirm')
    }

    continueBtn() {
        return cy.get('#ctrl_submit_personal')
    }

    continueToVerificationBtn() {
        return cy.get('#ctrl_submit_company')
    }

    companyTypeRadioBtn() {
        return cy.get('#lbl_companyType_1')
    }

    companyNameField() {
        return cy.get('#ctrl_companyName')
    }

    streetFIeld() {
        return cy.get('#ctrl_companyStreet')
    }

    additionalStreetFIeld() {
        return cy.get('#ctrl_companyStreet2')
    }

    zipCode() {
        return cy.get('#ctrl_zip')
    }

    cityField() {
        return cy.get('#ctrl_city')
    }

    countryField() {
        return cy.get('#ctrl_country')
    }

    vatID(){
        return cy.get('#ctrl_vatId')
    }

    summaryInfoBox() {
        return cy.get('[class="step review active"]')
    }

    termsAndConditionsCheckbox(){
        return cy.get('#opt_termsAndConditions_0')
    }

    privacyPolicyCheckbox(){
        return cy.get('#opt_privacyPolicy_0')
    }

    submitBtn(){
        return cy.get('#ctrl_submit_review')
    }


    //Function

    generateRandomNumber() {
        const randInt1 = Math.floor(Math.random() * 22)
        const randInt2 = Math.floor(Math.random() * randNumber0)
        const randInt3 = Math.floor(Math.random() * 21)
        return ((randInt1 + randInt2) * randInt3)
    }

    selectTitleAndNames(gender, firstname, lastname) {
        this.titleSelectField().select(gender)
        this.firstNameField().type(firstname)
        this.lastNameField().type(lastname)
    }

    selectPositionAndPhone(position, phoneNum) {
        this.positionField().select(position)
        this.phoneField().type(phoneNum)
    }

    typeEmailAddress(email) {
        this.eMailField().type(`${email}{enter}`).should('not.have.css', 'border-color', 'rgb(231, 76, 60)')
        cy.waitFor(this.loadingMsg().should('not.be.visible'))
    }

    writePSWAndConfirmPSW(password, confirmPass) {
        this.passwordField().type(password)
        this.confirmPasswordField().type(`${confirmPass}`)
    }

    fillUpPersonalDataForm(gender, firstname, lastname, position, phoneNum, emailAndPass) {
        this.selectTitleAndNames(gender, firstname, lastname)
        this.selectPositionAndPhone(position, phoneNum)
        this.typeEmailAddress(emailAndPass)
        this.writePSWAndConfirmPSW(emailAndPass, emailAndPass)
        this.continueBtn().click()
        this.companyTypeRadioBtn().should('be.visible')
    }

    fillUpCompanyData(companyName, street1, street2, zipCode, city, country) {
        this.companyTypeRadioBtn().click()
        this.companyNameField().type(companyName)
        this.streetFIeld().type(street1)
        this.additionalStreetFIeld().type(street2)
        this.zipCode().type(zipCode)
        this.cityField().type(city)
        this.countryField().select(country)
        this.continueToVerificationBtn().click()
        this.summaryInfoBox().should('be.visible')
    }

    fillUpCompanyDataVatID(companyName, street1, street2, zipCode, city, country, vatID) {
        this.companyTypeRadioBtn().click()
        this.companyNameField().type(companyName)
        this.streetFIeld().type(street1)
        this.additionalStreetFIeld().type(street2)
        this.zipCode().type(zipCode)
        this.cityField().type(city)
        this.countryField().select(country)
        this.vatID().type(`${vatID}{enter}`)
        cy.waitFor(this.loadingMsg().should('not.be.visible'))
        this.continueToVerificationBtn().click()
        this.summaryInfoBox().should('be.visible')
    }

    acceptTermsAndPrivacy(){
        this.termsAndConditionsCheckbox().check({force:true})
        this.privacyPolicyCheckbox().check({force:true})
    }
}