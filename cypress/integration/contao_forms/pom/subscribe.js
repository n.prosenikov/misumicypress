export default class Subscribe {
    //Selectors

    genderFieldSelector() {
        return cy.get('[id*="ctrl"]').first()
    }

    firstNameField() {
        return cy.get('[name="firstname"]')
    }

    lastNameField() {
        return cy.get('[name="lastname"]')
    }

    emailField() {
        return cy.get('[name="email"]')
    }

    companyField() {
        return cy.get('[class="widget widget-text mandatory"]').last()
    }

    privacyPolicyCheckBox() {
        return cy.get('[class="widget widget-checkbox mandatory"]').find('.checkbox')
    }

    submitBtn() {
        return cy.get('[class*="submit"]').last()
    }

    //Functions

    fillUpAllFields(gender, firstName, lastName, email, company) {
        this.genderFieldSelector().select(gender)
        this.firstNameField().type(firstName)
        this.lastNameField().type(lastName)
        this.emailField().type(email)
        this.companyField().type(company)
        this.privacyPolicyCheckBox().check({ force: true })
    }

}