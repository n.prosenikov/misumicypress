import Login from "../../boc/pom/login"
import NonVAT, { currentDate } from "../pom/non-vat";

const loginPage = new Login()
const nonVAT = new NonVAT()

const authUser = require('../../../fixtures/auth-user.json');
const contaoFormsURLs = require('../../../fixtures/contao_forms_url.json')

const { username, password, url_it } = authUser;

const randomNumber = nonVAT.generateRandomNumber()

const gender = ["Sig.", "Sig.ra"]
const firstName = "Test"
const lastName = "Automation"
const position = "Proprietario / Direttore"
const phone = "+391234567"
const emailAndPassword = `test${currentDate + randomNumber}@automation.com`
const wrongPSW = `falsch{enter}`
const company = "Test Company"
const street = "Test Street"
const additionalAddress = "Test Addon"
const zipCode = "25992"
const cities = ["UK city", "DE city", "FR city", "IT city"]
const countries = ["United Kingdom", "UK - Vereinigtes Königreich", "Royaume-Uni", "Regno Unito"]
const successfulMsg = "La ringraziamo per la sua richiesta di registrazione!"

describe('Registration as a Corporate User NON VAT country [IT]', () => {
    beforeEach(() => {
        loginPage.login(username, password)
        cy.visit(contaoFormsURLs.register_user_it)
        nonVAT.corporateUserRegisterBtn().click()
    });

    it('Personal data: select title, names, position, phones [IT]', { baseUrl: url_it }, () => {
        nonVAT.selectTitleAndNames(gender[0], firstName, lastName)
        nonVAT.selectPositionAndPhone(position, phone)
        nonVAT.continueBtn().click()
        nonVAT.companyTypeRadioBtn().should('not.be.visible')
    })

    it('Personal data: type incorrect email [IT]', { baseUrl: url_it }, () => {
        nonVAT.selectTitleAndNames(gender[1], firstName, lastName)
        nonVAT.selectPositionAndPhone(position, phone)
        nonVAT.eMailField().type("test").should('have.css', 'border-color', 'rgb(231, 76, 60)')
    })

    it('Personal data: type correct email [IT]', { baseUrl: url_it }, () => {
        nonVAT.selectTitleAndNames(gender[1], firstName, lastName)
        nonVAT.selectPositionAndPhone(position, phone)
        nonVAT.typeEmailAddress(emailAndPassword)
    })

    it('Personal data: confirm wrong password [IT]', { baseUrl: url_it }, () => {
        nonVAT.selectTitleAndNames(gender[1], firstName, lastName)
        nonVAT.selectPositionAndPhone(position, phone)
        nonVAT.typeEmailAddress(emailAndPassword)
        nonVAT.writePSWAndConfirmPSW(firstName, wrongPSW)
        nonVAT.confirmPasswordField().should('have.css', 'border-color', 'rgb(231, 76, 60)')
    })

    it('Personal data: fill up form and continue [IT]', { baseUrl: url_it }, () => {
        nonVAT.fillUpPersonalDataForm(gender[0], firstName, lastName, position, phone, emailAndPassword)
        nonVAT.continueToVerificationBtn().should('be.visible')
    })

    it('Company data: fill up form and continue [IT]', { baseUrl: url_it }, () => {
        nonVAT.fillUpPersonalDataForm(gender[0], firstName, lastName, position, phone, emailAndPassword)
        nonVAT.fillUpCompanyData(company, street, additionalAddress, zipCode, cities[3], countries[3])
    })

    it('Company data: fill up form and continue [IT]', { baseUrl: url_it }, () => {
        nonVAT.fillUpPersonalDataForm(gender[0], firstName, lastName, position, phone, emailAndPassword)
        nonVAT.fillUpCompanyData(company, street, additionalAddress, zipCode, cities[3], countries[3])
        nonVAT.acceptTermsAndPrivacy()
        nonVAT.submitBtn().click()
        cy.contains(successfulMsg)
    })

})