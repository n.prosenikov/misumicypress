import Login from "../../boc/pom/login"
import Subscribe from "../pom/subscribe";
import NonVAT, { currentDate } from "../pom/non-vat";

const nonVAT = new NonVAT()
const loginPage = new Login()
const subscribe = new Subscribe()

const randomNumber = nonVAT.generateRandomNumber()

const authUser = require('../../../fixtures/auth-user.json');
const contaoFormsURLs = require('../../../fixtures/contao_forms_url.json')

const { username, password, url_de } = authUser;

const mister = "Herr"
const missus = "Frau"
const firstName = "Test"
const lastName = "Automation"
const email = `test${currentDate + randomNumber}@automation.com`
const company = "Misumi"
const succsessfullMsg = "Registrierung für die MISUMI News"
const url = "/newsletter-bestellen-bestaetigungsseite/"


describe('Subscribe for Contao Forms [DE]', () => {
    beforeEach(() => {
        loginPage.login(username, password)
        cy.visit(contaoFormsURLs.contao_forms_de)
    });

    it('Select only "Mr." gender [DE]', { baseUrl: url_de }, () => {
        subscribe.genderFieldSelector().select(mister)
        subscribe.submitBtn().click()
        cy.url().should('not.include', url)
    })

    it('Select only "Mrs." gender [DE]', { baseUrl: url_de }, () => {
        subscribe.genderFieldSelector().select(missus)
        subscribe.submitBtn().click()
        cy.url().should('not.include', url)
    })

    it('Select gender + First Name [DE]', { baseUrl: url_de }, () => {
        subscribe.genderFieldSelector().select(mister)
        subscribe.firstNameField().type(firstName)
        subscribe.submitBtn().click()
        cy.url().should('not.include', url)
    })

    it('Select gender + both names [DE]', { baseUrl: url_de }, () => {
        subscribe.genderFieldSelector().select(mister)
        subscribe.firstNameField().type(firstName)
        subscribe.lastNameField().type(lastName)
        subscribe.submitBtn().click()
        cy.url().should('not.include', url)
    })

    it('Select gender, names, email [DE]', { baseUrl: url_de }, () => {
        subscribe.genderFieldSelector().select(mister)
        subscribe.firstNameField().type(firstName)
        subscribe.lastNameField().type(lastName)
        subscribe.emailField().type(email)
        subscribe.submitBtn().click()
        cy.url().should('not.include', url)
    })

    it('Do not accept Privacy Policy [DE]', { baseUrl: url_de }, () => {
        subscribe.genderFieldSelector().select(mister)
        subscribe.firstNameField().type(firstName)
        subscribe.lastNameField().type(lastName)
        subscribe.emailField().type(email)
        subscribe.companyField().type(company)
        subscribe.submitBtn().click()
        cy.url().should('not.include', url)
    })

    it('Create a succesfull subscription [DE]', { baseUrl: url_de }, () => {
        subscribe.fillUpAllFields(mister, firstName, lastName, email, company)
        subscribe.submitBtn().click()
        cy.url().should('include', url)
        cy.contains(succsessfullMsg)
    })

})