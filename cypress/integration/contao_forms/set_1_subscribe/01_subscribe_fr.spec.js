import Login from "../../boc/pom/login"
import Subscribe from "../pom/subscribe";
import NonVAT, { currentDate } from "../pom/non-vat";

const nonVAT = new NonVAT()
const loginPage = new Login()
const subscribe = new Subscribe()

const randomNumber = nonVAT.generateRandomNumber()

const authUser = require('../../../fixtures/auth-user.json');
const contaoFormsURLs = require('../../../fixtures/contao_forms_url.json')

const { username, password, url_fr } = authUser;

const mister = "M."
const missus = "Mme."
const firstName = "Test"
const lastName = "Automation"
const email = `test${currentDate + randomNumber}@automation.com`
const company = "Misumi"
const succsessfullMsg = "Inscription à notre service e-mail"
const url = "/newsletter-inscription-a-notre-service-e-mail-merci/"


describe('Subscribe for Contao Forms [FR]', () => {
    beforeEach(() => {
        loginPage.login(username, password)
        cy.visit(contaoFormsURLs.contao_forms_fr)
    });

    it('Select only "Mr." gender [FR]', { baseUrl: url_fr }, () => {
        subscribe.genderFieldSelector().select(mister)
        subscribe.submitBtn().click()
        cy.url().should('not.include', url)
    })

    it('Select only "Mrs." gender [FR]', { baseUrl: url_fr }, () => {
        subscribe.genderFieldSelector().select(missus)
        subscribe.submitBtn().click()
        cy.url().should('not.include', url)
    })

    it('Select gender + First Name [FR]', { baseUrl: url_fr }, () => {
        subscribe.genderFieldSelector().select(mister)
        subscribe.firstNameField().type(firstName)
        subscribe.submitBtn().click()
        cy.url().should('not.include', url)
    })

    it('Select gender + both names [FR]', { baseUrl: url_fr }, () => {
        subscribe.genderFieldSelector().select(mister)
        subscribe.firstNameField().type(firstName)
        subscribe.lastNameField().type(lastName)
        subscribe.submitBtn().click()
        cy.url().should('not.include', url)
    })

    it('Select gender, names, email [FR]', { baseUrl: url_fr }, () => {
        subscribe.genderFieldSelector().select(mister)
        subscribe.firstNameField().type(firstName)
        subscribe.lastNameField().type(lastName)
        subscribe.emailField().type(email)
        subscribe.submitBtn().click()
        cy.url().should('not.include', url)
    })

    it('Do not accept Privacy Policy [FR]', { baseUrl: url_fr }, () => {
        subscribe.genderFieldSelector().select(mister)
        subscribe.firstNameField().type(firstName)
        subscribe.lastNameField().type(lastName)
        subscribe.emailField().type(email)
        subscribe.companyField().type(company)
        subscribe.submitBtn().click()
        cy.url().should('not.include', url)
    })

    it('Create a succesfull subscription [FR]', { baseUrl: url_fr }, () => {
        subscribe.fillUpAllFields(mister, firstName, lastName, email, company)
        subscribe.submitBtn().click()
        cy.url().should('include', url)
        cy.contains(succsessfullMsg)
    })

})