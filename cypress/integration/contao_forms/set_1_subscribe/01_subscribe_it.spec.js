import Login from "../../boc/pom/login"
import Subscribe from "../pom/subscribe";
import NonVAT, { currentDate } from "../pom/non-vat";

const nonVAT = new NonVAT()
const loginPage = new Login()
const subscribe = new Subscribe()

const randomNumber = nonVAT.generateRandomNumber()

const authUser = require('../../../fixtures/auth-user.json');
const contaoFormsURLs = require('../../../fixtures/contao_forms_url.json')

const { username, password, url_it } = authUser;

const mister = "Sig."
const missus = "Sig.ra"
const firstName = "Test"
const lastName = "Automation"
const email = `test${currentDate + randomNumber}@automation.com`
const company = "Misumi"
const succsessfullMsg = "Registrazione al servizio e-mail di MISUMI"
const url = "/la-nostra-newsletter-grazie/"


describe('Subscribe for Contao Forms [IT]', () => {
    beforeEach(() => {
        loginPage.login(username, password)
        cy.visit(contaoFormsURLs.contao_forms_it)
    });

    it('Select only "Mr." gender [IT]', { baseUrl: url_it }, () => {
        subscribe.genderFieldSelector().select(mister)
        subscribe.submitBtn().click()
        cy.url().should('not.include', url)
    })

    it('Select only "Mrs." gender [IT]', { baseUrl: url_it }, () => {
        subscribe.genderFieldSelector().select(missus)
        subscribe.submitBtn().click()
        cy.url().should('not.include', url)
    })

    it('Select gender + First Name [IT]', { baseUrl: url_it }, () => {
        subscribe.genderFieldSelector().select(mister)
        subscribe.firstNameField().type(firstName)
        subscribe.submitBtn().click()
        cy.url().should('not.include', url)
    })

    it('Select gender + both names [IT]', { baseUrl: url_it }, () => {
        subscribe.genderFieldSelector().select(mister)
        subscribe.firstNameField().type(firstName)
        subscribe.lastNameField().type(lastName)
        subscribe.submitBtn().click()
        cy.url().should('not.include', url)
    })

    it('Select gender, names, email [IT]', { baseUrl: url_it }, () => {
        subscribe.genderFieldSelector().select(mister)
        subscribe.firstNameField().type(firstName)
        subscribe.lastNameField().type(lastName)
        subscribe.emailField().type(email)
        subscribe.submitBtn().click()
        cy.url().should('not.include', url)
    })

    it('Do not accept Privacy Policy [IT]', { baseUrl: url_it }, () => {
        subscribe.genderFieldSelector().select(mister)
        subscribe.firstNameField().type(firstName)
        subscribe.lastNameField().type(lastName)
        subscribe.emailField().type(email)
        subscribe.companyField().type(company)
        subscribe.submitBtn().click()
        cy.url().should('not.include', url)
    })

    it('Create a succesfull subscription [IT]', { baseUrl: url_it }, () => {
        subscribe.fillUpAllFields(mister, firstName, lastName, email, company)
        subscribe.submitBtn().click()
        cy.url().should('include', url)
        cy.contains(succsessfullMsg)
    })

})