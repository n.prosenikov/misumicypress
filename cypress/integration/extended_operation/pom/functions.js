import Login from "../../boc/pom/login"

const login = new Login()

export default class EOT_Functions {
    //Selectors
    headerProductsSelector() {
        return cy.get('.l-header__menu > :nth-child(1) > :nth-child(1)')
    }

    teaserBox(option) {
        return cy.get('.teaser_box').eq(option)
    }

    inCADLibrarySelector() {
        return cy.get('.rapid-btn').eq(1)
    }


    //Functions
    openAutomationComponentsOption(option) {
        this.headerProductsSelector().click()
        this.teaserBox(option).click()
        cy.wait(2000)
    }

    logInByCompare(username, password) {
        cy.intercept('GET', '**userInfo**').as('loginUser')
        login.acceptCookies().click()
        login.loginMenuButton().click()
        login.username().type(username)
        login.password().type(password)
        login.signInButton().click()
        cy.wait("@loginUser")
    }
}