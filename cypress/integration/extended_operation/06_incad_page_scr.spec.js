import EOT_Functions from './pom/functions';
import Login from '../boc/pom/login'

const authUser = require('../../fixtures/auth-user.json');
const eotfunc = new EOT_Functions()
const login = new Login();
const {
    url_uk,
    url_de,
} = authUser;

const pages = [url_uk, url_de]
const sizes = [[1920, 1080]]

describe('inCAD Page Screenshot Checks', () => {
    sizes.forEach(size => {
        pages.forEach(page => {
            it(`List image comparison ${page} in resolution ${size}`, () => {
                cy.setResolution(size)
                cy.visit(page)
                login.acceptCookies().click()
                cy.wait(3000)
                eotfunc.inCADLibrarySelector().click()
                cy.reload()
                cy.get('[class="toplist_box cf VN_accordion"]').matchImageSnapshot()
            })
        })
    })

})