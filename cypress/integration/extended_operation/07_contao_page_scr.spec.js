import Login from '../boc/pom/login'

const authUser = require('../../fixtures/auth-user.json');
const login = new Login();
const {
    url_uk,
} = authUser;

const pages = [url_uk]
const sizes = [[1920, 1080]]

describe('Contao Page Screenshot Checks', () => {

    sizes.forEach(size => {
        pages.forEach(page => {
            it(`Header image comparison ${page} in resolution ${size}`, () => {
                cy.setResolution(size)
                cy.visit(page)
                login.acceptCookies().click()
                cy.wait(3000)
                cy.visit('https://uk.misumi-ec.com/en/campaign/custom-machined-parts/')
                cy.get('#header').matchImageSnapshot()
            })
        })
    })

    sizes.forEach(size => {
        pages.forEach(page => {
            it(`Footer image comparison ${page} in resolution ${size}`, () => {
                cy.setResolution(size)
                cy.visit(page)
                login.acceptCookies().click()
                cy.wait(3000)
                cy.visit('https://uk.misumi-ec.com/en/campaign/custom-machined-parts/')
                cy.get('#footer').matchImageSnapshot()
            })
        })
    })

})