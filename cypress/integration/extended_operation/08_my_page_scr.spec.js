import EOT_Functions from './pom/functions';
import Login from '../boc/pom/login'

const authUser = require('../../fixtures/auth-user.json');
const eotfunc = new EOT_Functions()
const login = new Login();
const {
    username,
    password,
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

const pages = [url_uk, url_de, url_fr, url_it]
const sizes = [[1920, 1080]]

describe('My Page Screenshot Checks', () => {

    sizes.forEach(size => {
        pages.forEach(page => {
            it(`Header image comparison ${page} in resolution ${size}`, () => {
                cy.setResolution(size)
                cy.visit(page)
                eotfunc.logInByCompare(username, password)
                cy.visit(page)
                cy.wait(1000)
                cy.get('.l-userBoxInner > .l-header__linkList > :nth-child(2) > a').click()
                cy.get('#wos_header').matchImageSnapshot()
            })
        })
    })

    sizes.forEach(size => {
        pages.forEach(page => {
            it(`Footer image comparison ${page} in resolution ${size}`, () => {
                cy.setResolution(size)
                cy.visit(page)
                eotfunc.logInByCompare(username, password)
                cy.visit(page)
                cy.get('.l-userBoxInner > .l-header__linkList > :nth-child(2) > a').click()
                cy.get('#wos_footer').matchImageSnapshot()
            })
        })
    })

    sizes.forEach(size => {
        pages.forEach(page => {
            it(`Tabs image comparison ${page} in resolution ${size}`, () => {
                cy.setResolution(size)
                cy.visit(page)
                eotfunc.logInByCompare(username, password)
                cy.visit(page)
                cy.get('.l-userBoxInner > .l-header__linkList > :nth-child(2) > a').click()
                cy.get('#historyNaviSO').matchImageSnapshot()
            })
        })
    })

})