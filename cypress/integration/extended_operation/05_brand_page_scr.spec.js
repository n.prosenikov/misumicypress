import EOT_Functions from './pom/functions';
import Login from '../boc/pom/login'

const authUser = require('../../fixtures/auth-user.json');
const eotfunc = new EOT_Functions()
const login = new Login();
const {
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

const pages = [url_uk, url_de, url_it, url_fr]
const sizes = [[1920, 1080]]

describe('Brand Page Screenshot Checks', () => {
    sizes.forEach(size => {
        pages.forEach(page => {
            it(`Category image comparison ${page} in resolution ${size}`, () => {
                cy.setResolution(size)
                cy.visit(page)
                login.acceptCookies().click()
                cy.wait(3000)
                eotfunc.openAutomationComponentsOption(0)
                cy.get('[data-category="box"]').eq(0).matchImageSnapshot()
            })
        })
    })

    sizes.forEach(size => {
        pages.forEach(page => {
            it(`Navigation image comparison ${page} in resolution ${size}`, () => {
                cy.setResolution(size)
                cy.visit(page)
                login.acceptCookies().click()
                cy.wait(3000)
                eotfunc.openAutomationComponentsOption(0)
                cy.get('.l-navCategoryBox').matchImageSnapshot()
            })
        })
    })

})