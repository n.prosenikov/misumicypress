
import Login from '../boc/pom/login'

const authUser = require('../../fixtures/auth-user.json');
const login = new Login();
const {
    url_uk,
    url_de,
    url_fr,
    url_it
} = authUser;

const partName = "SFJ10-100"
const pages = [url_uk, url_de, url_it, url_fr]
const sizes = [[1920, 1080]]

describe('Product Header/Maininfo/Specblock Comparison UK Product Page', () => {
    sizes.forEach(size => {
        pages.forEach(page => {
            it(`Header image comparison ${page} in resolution ${size}`, () => {
                cy.setResolution(size)
                cy.visit(page)
                login.acceptCookies().click()
                cy.wait(2000)
                cy.searchPart(partName)
                cy.wait(3000)
                cy.get('#Tab_container').matchImageSnapshot()
            })
        })
    })

    sizes.forEach(size => {
        pages.forEach(page => {
            it(`Maininfo image comparison ${page} in resolution ${size}`, () => {
                cy.setResolution(size)
                cy.visit(page)
                login.acceptCookies().click()
                cy.wait(2000)
                cy.searchPart(partName)
                cy.wait(3000)
                cy.get('.m-media--product__img').matchImageSnapshot()
            })
        })
    })

    sizes.forEach(size => {
        pages.forEach(page => {
            it(`Specblock image comparison ${page} in resolution ${size}`, () => {
                cy.setResolution(size)
                cy.visit(page)
                login.acceptCookies().click()
                cy.wait(2000)
                cy.searchPart(partName)
                cy.wait(3000)
                cy.get('[data-spec="spec-block"]').eq(0).matchImageSnapshot()
            })
        })
    })

})